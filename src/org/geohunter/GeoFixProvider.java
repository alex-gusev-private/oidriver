package org.geohunter;

import android.location.Location;

public interface GeoFixProvider {

    public void startUpdates();
    public void stopUpdates();

    public GeoFix getLocation();
    public Location getLastKnownLocation();

    public void addObserver(Refresher refresher);

    public void removeObserver(Refresher refresher);
    
    public boolean isProviderEnabled();

    public float getAzimuth();
}
