package com.oidriver;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ConfirmDialog extends OiDialog {
		
	// Constructor.
	public ConfirmDialog(final Context context, final String title, final String message, final View.OnClickListener onOkClickListener, View.OnClickListener onCancelClickListener) {
		this(context, title, message, R.string.ok, onOkClickListener, R.string.cancel, onCancelClickListener);
	}
	
	public ConfirmDialog(final Context context, final int titleId, final int messageId, final View.OnClickListener onOkClickListener, View.OnClickListener onCancelClickListener) {
		this(context, titleId, messageId, R.string.ok, onOkClickListener, R.string.cancel, onCancelClickListener);
	}
	
	public ConfirmDialog(final Context context, final int titleId, final int messageId, final int okTextId, final View.OnClickListener onOkClickListener, int cancelTextId, final View.OnClickListener onCancelClickListener) {
		super(context, R.layout.dialog_frame, R.style.oidriverDialogBlueBorder, R.layout.dialog_confirm, titleId, 0);
		
		if (cancelTextId == 0) {
			cancelTextId = R.string.cancel;
		}
		
		final TextView textMessage = (TextView) findViewById(R.id.text_dialog_confirm_message);
		textMessage.setText(messageId);
		final Button buttonOk = (Button)findViewById(R.id.text_dialog_confirm_ok);
		buttonOk.setText(okTextId);
		buttonOk.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onOkClickListener.onClick(v);
				dismiss();
			}
		});
		
		final Button buttonCancel = (Button) findViewById(R.id.text_dialog_confirm_cancel);
		buttonCancel.setText(cancelTextId);
		buttonCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (onCancelClickListener != null) {
					onCancelClickListener.onClick(v);
				}
				dismiss();
			}
		});
	}
	
	public ConfirmDialog(final Context context, final String title, final String message, final int okTextId, final View.OnClickListener onOkClickListener, int cancelTextId, final View.OnClickListener onCancelClickListener) {
		super(context, R.layout.dialog_frame, R.style.oidriverDialogBlueBorder, R.layout.dialog_confirm, title, 0);
		
		if (cancelTextId == 0) {
			cancelTextId = R.string.cancel;
		}
		
		final TextView textMessage = (TextView) findViewById(R.id.text_dialog_confirm_message);
		textMessage.setText(message);
		final Button buttonOk = (Button)findViewById(R.id.text_dialog_confirm_ok);
		buttonOk.setText(okTextId);
		buttonOk.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onOkClickListener.onClick(v);
				dismiss();
			}
		});
		
		final Button buttonCancel = (Button) findViewById(R.id.text_dialog_confirm_cancel);
		buttonCancel.setText(cancelTextId);
		buttonCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (onCancelClickListener != null) {
					onCancelClickListener.onClick(v);
				}
				dismiss();
			}
		});
	}
	
	public void setMessage(final String message) {
		final TextView textMessage = (TextView) findViewById(R.id.text_dialog_confirm_message);
		if (textMessage != null) {
			textMessage.setText(message);
		}
	}
}
