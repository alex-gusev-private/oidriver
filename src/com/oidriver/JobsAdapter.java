package com.oidriver;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.oidriver.service.OiDriverService;

import java.util.ArrayList;
import java.util.Locale;

public class JobsAdapter extends ArrayAdapter<Booking> {
	
	Context mContext;
	//GeoFixProvider mGeo;
	OiDriverActivity mActivity;
	LayoutInflater inflater;
	
	public JobsAdapter(Context context, int textViewResourceId, ArrayList<Booking> items, OiDriverActivity activity) //GeoFixProvider geo)
	{
		super(context, textViewResourceId, items);
		mContext = context;
		mActivity = activity;
		inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view;
		
		if (convertView == null)
			view = inflater.inflate(R.layout.jobs_row, null);
		else
			view = convertView;

		Booking item = getItem(position);
		if (item != null && view != null)
		{
			String fromStr = item.getPickupAddress();
			String toStr = item.getDropoffAddress();
			String timeStr = item.getBookingTime();

			//GeoFix loc = mGeo.getLocation();
			Location loc = mActivity.getLastFix();
			String distStr = "";
			try {
				if (loc != null)
				{
					distStr = String.format(Locale.UK, "%.1fmi",
							 distanceTo(loc.getLatitude(), loc.getLongitude(), Double.valueOf(item.getPickupLat()), 
											Double.valueOf(item.getPickupLon())));
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}

			String priceStr = item.getFare();
			String bidStr = String.format("Bid%s", TextUtils.isEmpty(priceStr) || priceStr.equalsIgnoreCase("N/A") ?
					"" : "\n" + priceStr);

			Button dist = (Button) view.findViewById(R.id.buttonMap);
			TextView from = (TextView) view.findViewById(R.id.textViewFrom);
			TextView to = (TextView) view.findViewById(R.id.textViewTo);
			TextView time = (TextView) view.findViewById(R.id.textViewTime);
			dist.setText(distStr);
			from.setText(fromStr);
			to.setText(toStr);
			time.setText(timeStr);
			Button btn = (Button)view.findViewById(R.id.buttonBid);
			ImageView img = (ImageView)view.findViewById(R.id.imageViewSent);
			if (item.isBidSent())
			{
				img.setVisibility(View.VISIBLE);
				btn.setVisibility(View.INVISIBLE);
			}
			else
			{
				img.setVisibility(View.INVISIBLE);
				btn.setVisibility(View.VISIBLE);
				btn.setText(bidStr);
				btn.setOnClickListener(new BidButtonListener(item.getBookingId()));
			}
		}
		
		return view;
	}

	public float distanceTo(double mylat, double mylon, double lat, double lon) {
        float[] results = { 0 };
        Location.distanceBetween(mylat, mylon, 
                lat, lon, results);

        return results[0] * 0.00062137f;
    }
	
	private class BidButtonListener implements OnClickListener {

		String mBidId;
		
		public BidButtonListener(String bidID)
		{
			mBidId = bidID;
		}
		
		@Override
		public void onClick(View v) {
			if (!OiDriverService.isXmppAuthenticated())
			{
				Intent intent = new Intent("com.oidriver.SHOW_ERROR");
				ArrayList<String> errors = new ArrayList<String>();
				errors.add("Server connection is down, please try later!");
				intent.putStringArrayListExtra("errors", errors);
				OiDriverApp.getAppContext().sendBroadcast(intent);
				return;
			}
			Intent i = new Intent("com.oidriver.SEND_JOB_BID");
			i.putExtra("booking_id", mBidId);
			OiDriverApp.getAppContext().sendBroadcast(i);
		}
		
	}
}
