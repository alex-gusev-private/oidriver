package com.oidriver;

public class Zone {
	private String zoneName;
	private String id;
	private String drivers;
	private String jobs;

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setJobs(String jobs) {
		this.jobs = jobs;
	}

	public String getJobs() {
		return jobs;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setDrivers(String drivers) {
		this.drivers = drivers;
	}

	public String getDrivers() {
		return drivers;
	}
}
