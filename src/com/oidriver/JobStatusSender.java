package com.oidriver;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.oidriver.runnable.RunOn;
import com.oidriver.service.OiDriverService;

/**
 * Created by Alex on 12/07/2013.
 * Copyright (C) 2013
 */
public class JobStatusSender {

	private static final String TAG = JobStatusSender.class.getSimpleName();
	OiDriverActivity mOiDriverActivity;

	public interface JobStatusSenderListener {
		void onStatusSent(String jobId, boolean isSent);
	}

	public String getCurrentTask() {
		return mCurrentTask;
	}

	public void setCurrentTask(String currentTask) {
		mCurrentTask = currentTask;
	}

	String mCurrentTask;

	public JobStatusSender(OiDriverActivity activity) {
		mOiDriverActivity = activity;
	}

	void trySendData(final Bundle bundle, final JobStatusSenderListener listener) {
		RunOn.taskThread(new Runnable() {
			@Override
			public void run() {
				String jobStatus = bundle.getString("jobstatus");
				String jobId = bundle.getString("jobid");
				String sendToJid = bundle.getString("manager_jid");
				boolean requireReply = bundle.getBoolean("require_reply", true);

				// Send message to the wire...
				Boolean isSent = OiDriverService.sendMessage(jobStatus, sendToJid);
				if (requireReply && listener != null)
					listener.onStatusSent(jobId, isSent);
			}
		});
	}

	private boolean sendJobStatusToServer(final String sendToJid, final String jobId,
	                                      final String jobStatus,
	                                      final String currentTaskValue,
	                                      final String additionalInfo,
	                                      final JobStatusSenderListener listener) {
		mCurrentTask = null;
		if (OiDriverService.isXmppAuthenticated()) {

			if (!TextUtils.isEmpty(jobId) && !TextUtils.isEmpty(jobStatus)) {
				mOiDriverActivity.showProgressDialog();
				Log.e(TAG, String.format("Sending %s for ID = %s", jobStatus, jobId));
				RunOn.taskThread(new Runnable() {
					@Override
					public void run() {

						String jobStatusStr = String.format("WK#,%s,%s,%s,3,0", jobId, additionalInfo != null ? additionalInfo : "1", jobStatus);

						// Send message to the wire...
						Boolean isSent = OiDriverService.sendMessage(jobStatusStr, sendToJid);
						mCurrentTask = currentTaskValue;

						if (listener != null)
							listener.onStatusSent(jobId, isSent);

						mOiDriverActivity.hideProgressDialog();
					}
				});
				return true;
			}
			return false;
		}
		else {
			mOiDriverActivity.showToast("Not connected to server, please wait until connection established",
					Toast.LENGTH_SHORT);
			return false;
		}
	}


	public boolean sendJobStatusToServer(String sendToJid, String jobId, String jobStatus, String currentTaskValue,
	                                     JobStatusSenderListener listener) {
		return sendJobStatusToServer(sendToJid, jobId, jobStatus, currentTaskValue, null, listener);
	}

	public void sendAcceptJobToServer(final AcceptedJob acceptedJob, JobStatusSenderListener listener) {

		if (acceptedJob != null) {
			boolean res = sendJobStatusToServer(acceptedJob.getSentBy(), acceptedJob.getJobId(),
					Constants.DISPATCHER_STATUS_ACCEPTED, Constants.TASK_ACCEPTED_JOB, listener);
			if (res)
			{
				acceptedJob.setStatus(Constants.STATUS_ACCEPTED);
			}
			else {
				mOiDriverActivity.showToast("Failed to send job status to server", Toast.LENGTH_SHORT);
			}
		}
		else {
			mOiDriverActivity.showToast("Failed to send job status to server: NULL job", Toast.LENGTH_SHORT);
		}
	}

	public void sendRejectJobToServer(String sendToJid, String jobId, JobStatusSenderListener listener) {
		mCurrentTask = null;
		if (!TextUtils.isEmpty(jobId)) {
			sendJobStatusToServer(sendToJid, jobId,
					Constants.DISPATCHER_STATUS_REJECTED, Constants.TASK_REJECTED_JOB, listener);
		}
		else {
			mOiDriverActivity.showToast("Failed to send job status to server: EMPTY job id", Toast.LENGTH_SHORT);
		}
	}

	public void sendReturnJobToServer(String sendToJid, String jobId, String reason,
	                                  JobStatusSenderListener listener) {
		mCurrentTask = null;
		if (!TextUtils.isEmpty(jobId)) {
			sendJobStatusToServer(sendToJid, jobId,
					Constants.DISPATCHER_STATUS_RETURN_JOB, Constants.TASK_RETURNED, reason, listener);
		}
		else {
			mOiDriverActivity.showToast("Failed to send job status to server: EMPTY job id", Toast.LENGTH_SHORT);
		}
	}

	public void sendDriverOnWay(String sendToJid, String jobId, JobStatusSenderListener listener) {

		if (!TextUtils.isEmpty(jobId)) {
			sendJobStatusToServer(sendToJid, jobId,
					Constants.DISPATCHER_STATUS_DOW, Constants.TASK_DOW, listener);
		}
		else {
			mOiDriverActivity.showToast("Failed to send job status to server: EMPTY job id", Toast.LENGTH_SHORT);
		}
	}

	public void sendNoJob(String sendToJid, String jobId, JobStatusSenderListener listener) {

		if (!TextUtils.isEmpty(jobId)) {
			sendJobStatusToServer(sendToJid, jobId,
					Constants.DISPATCHER_STATUS_NO_JOB, Constants.TASK_NO_JOB, listener);
		}
		else {
			mOiDriverActivity.showToast("Failed to send job status to server: EMPTY job id", Toast.LENGTH_SHORT);
		}
	}

	public void sendPassengerOnBoard(String sendToJid, String jobId, JobStatusSenderListener listener) {

		if (!TextUtils.isEmpty(jobId)) {
			sendJobStatusToServer(sendToJid, jobId,
					Constants.DISPATCHER_STATUS_POB, Constants.TASK_POB, listener);
		}
		else {
			mOiDriverActivity.showToast("Failed to send job status to server: EMPTY job id", Toast.LENGTH_SHORT);
		}
	}

	public void sendEOJ(String sendToJid, String jobId, JobStatusSenderListener listener) {

		if (!TextUtils.isEmpty(jobId)) {
			sendJobStatusToServer(sendToJid, jobId,
					Constants.DISPATCHER_STATUS_EOJ, Constants.TASK_EOJ, listener);
		}
		else {
			mOiDriverActivity.showToast("Failed to send job status to server: EMPTY job id", Toast.LENGTH_SHORT);
		}
	}

	public void sendSoonToClear(String sendToJid, String jobId, JobStatusSenderListener listener) {

		if (!TextUtils.isEmpty(jobId)) {
			sendJobStatusToServer(sendToJid, jobId,
					Constants.DISPATCHER_STATUS_STC, Constants.TASK_STC, listener);
		}
		else {
			mOiDriverActivity.showToast("Failed to send job status to server: EMPTY job id", Toast.LENGTH_SHORT);
		}
	}

}
