package com.oidriver.xmpp;

import android.util.Log;

import com.oidriver.OiDriverApp;
import com.oidriver.service.OiDriverService;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParser;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class DriverGetIQProvider implements IQProvider {
	@Override
	public IQ parseIQ(XmlPullParser parser) {
		Log.wtf("AG-SMACK", "   DRIVER-GET-IQPROVIDER");
		try
		{	
			int event = parser.getEventType();
			final StringBuilder builder = new StringBuilder();
			
			while(true){
				switch (event) {  
				  
		        case XmlPullParser.START_DOCUMENT:  
		            
		            break;  
		  
		        case XmlPullParser.END_DOCUMENT:  
		        	 
		        	
		            break;  
		        case XmlPullParser.START_TAG:  
		            builder.append("<"+parser.getName()+">");  
		            
		            break;  
		  
		        case XmlPullParser.END_TAG:  
		            builder.append("</"+parser.getName()+">");  
		  
		            break;  
		  
		        case XmlPullParser.TEXT:  
		            
		            builder.append(parser.getText());  
		              
		            break;  
		        }
				event = parser.next();
				String iqStr = builder.toString();
				if(iqStr.contains("</query>"))
				{
					try {

						String xml = iqStr;
						xml = xml.replace("&gt;", ">")
								.replace("&lt;", "<")
								.replace("&quot;", "'")
								.replace("&amp;","&");

						DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
						DocumentBuilder db = dbf.newDocumentBuilder();
						InputStream is = new ByteArrayInputStream(xml.getBytes());

						Document doc = db.parse(is);
						doc.getDocumentElement().normalize();

						// Get the Driver
						NodeList driverNodeList = doc.getElementsByTagName("query");

						if (driverNodeList != null && driverNodeList.getLength() > 0) {
							for (int i = 0; i < driverNodeList.getLength(); i++) {
								// get the employee element
								Element el = (Element) driverNodeList.item(i);
								OiDriverService.driver = Util.getDriver(el);

								String dateStr = OiDriverService.driver.getPaymentRef();

							}
						}
					} catch (Exception e) {
						e.printStackTrace();
						Log.e(OiDriverApp.TAG, "ERROR: Showing Near by Taxi");
					}
					break;
				}
			}
			
			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public DriverGetIQProvider() {

	}

}
