package com.oidriver.xmpp;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.oidriver.OiDriverApp;
import com.oidriver.service.OiDriverService;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

public class DriverPublishIQProvider implements IQProvider {
	private static final String TAG = OiDriverApp.TAG;
	
	@Override
	public IQ parseIQ(XmlPullParser parser) {
		Log.wtf("AG-SMACK", "   DRIVER-PUBLISH-IQPROVIDER");
		try
		{	
			int event = parser.getEventType();
			final StringBuilder builder = new StringBuilder();
			
			while(true){
				switch (event) {  
				  
		        case XmlPullParser.START_DOCUMENT:  
		            
		            break;  
		  
		        case XmlPullParser.END_DOCUMENT:  
		        	 
		        	
		            break;  
		        case XmlPullParser.START_TAG:  
		            builder.append("<"+parser.getName()+">");  
		            
		            break;  
		  
		        case XmlPullParser.END_TAG:  
		            builder.append("</"+parser.getName()+">");  
		  
		            break;  
		  
		        case XmlPullParser.TEXT:  
		            
		            builder.append(parser.getText());  
		              
		            break;  
		        }
				event = parser.next();
				String iqStr = builder.toString();
				if(iqStr.contains("</query>"))
				{					
					Log.i(TAG, "DriverPublishProvider: " + iqStr);
					String prevZone = OiDriverService.currentZoneId;
					String queueIdx = OiDriverService.parseDriverPublishResponse(builder.toString());
					if (!TextUtils.isEmpty(OiDriverService.currentZoneId))
					{
						boolean outOfZone = OiDriverService.currentZoneId.equalsIgnoreCase("OUT_OF_ZONE");
						if (!outOfZone) {
							if (OiDriverService.bookings.size() == 0) {
								if (TextUtils.isEmpty(prevZone))
									OiDriverService.sendIQToSubscribeZone(OiDriverService.currentZoneId);
								else
								{
									if (!prevZone.equalsIgnoreCase(OiDriverService.currentZoneId))
									{
										OiDriverService.sendIQToUnsubscribeZone(prevZone);
										OiDriverService.sendIQToSubscribeZone(OiDriverService.currentZoneId);
									}
								}
							}
						}
						else if (!TextUtils.isEmpty(prevZone)) {
							OiDriverService.sendIQToUnsubscribeZone(prevZone);
						}
					}
					else
					{
						Intent intent = new Intent("com.oidriver.ZONE_CHANGED");
						intent.putExtra("queueIndex", "0");
						OiDriverApp.getAppContext().sendBroadcast(intent);
					}
					break;
				}
			}

			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public DriverPublishIQProvider() {

	}

}
