package com.oidriver.xmpp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.oidriver.AcceptedJob;
import com.oidriver.Booking;
import com.oidriver.Constants;
import com.oidriver.Driver;
import com.oidriver.OiDriverApp;
import com.oidriver.R;
import com.oidriver.Zone;
import com.oidriver.service.OiDriverService;
import com.oidriver.util.NetworkPing;

import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.provider.PrivacyProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.OfflineMessageManager;
import org.jivesoftware.smackx.PrivateDataManager;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.ping.packet.Ping;
import org.jivesoftware.smackx.provider.AdHocCommandDataProvider;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.MessageEventProvider;
import org.jivesoftware.smackx.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.provider.RosterExchangeProvider;
import org.jivesoftware.smackx.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.provider.VCardProvider;
import org.jivesoftware.smackx.provider.XHTMLExtensionProvider;
import org.jivesoftware.smackx.pubsub.provider.AffiliationProvider;
import org.jivesoftware.smackx.pubsub.provider.AffiliationsProvider;
import org.jivesoftware.smackx.pubsub.provider.ConfigEventProvider;
import org.jivesoftware.smackx.pubsub.provider.EventProvider;
import org.jivesoftware.smackx.pubsub.provider.FormNodeProvider;
import org.jivesoftware.smackx.pubsub.provider.ItemProvider;
import org.jivesoftware.smackx.pubsub.provider.ItemsProvider;
import org.jivesoftware.smackx.pubsub.provider.PubSubProvider;
import org.jivesoftware.smackx.pubsub.provider.SimpleNodeProvider;
import org.jivesoftware.smackx.pubsub.provider.SubscriptionProvider;
import org.jivesoftware.smackx.pubsub.provider.SubscriptionsProvider;
import org.jivesoftware.smackx.search.UserSearch;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class XMPPClient /*implements ConnectionListener */{

	public static final boolean RECONNECTION_MANAGER_ALLOWED = true;
	public static final int RECONNECTION_TIMEOUT = 45000; //3 * 60000;
	public static final String XMPP_HOST_IP = "107.20.244.96";
	public static boolean DEBUG_FAKE_GPS = false;
	private final String TAG = OiDriverApp.TAG;
	public XMPPConnection xmppConnection;

	private static XMPPClient client = new XMPPClient();

	public HashSet<String> zones = new HashSet<String>();

	public List<Driver> drivers = new ArrayList<Driver>();
	
	public ConnectionConfiguration xmppConfig = null;
	private OiDriverService mService;
	public boolean blockingOperationProgress = false;

	private Timer mIcmpTimer;
	private Timer mPingTimer;
	public volatile long lastSuccessfulPingByTask = -1;

	public interface XmppEventsListener
	{
		void onMessage(int errCode, String msg);

		/**
		 * When e is null:
		 * Notification that the connection was closed normally or that the reconnection
		 * process has been aborted.
		 *
		 * When e is not null:
		 * Notification that the connection was closed due to an exception. When
		 * abruptly disconnected it is possible for the connection to try reconnecting
		 * to the server.
		 *
		 * @param e the exception.
		 */
		public void connectionClosed(Exception e);

		/**
		 * The connection will retry to reconnect in the specified number of seconds.
		 *
		 * @param seconds remaining seconds before attempting a reconnection.
		 */
		public void reconnectingIn(int seconds);

		/**
		 * The connection has reconnected successfully to the server. Connections will
		 * reconnect to the server when the previous socket connection was abruptly closed.
		 */
		public void reconnectionSuccessful();

		/**
		 * An attempt to connect to the server has failed. The connection will keep trying
		 * reconnecting to the server in a moment.
		 *
		 * @param e the exception that caused the reconnection to fail.
		 */
		public void reconnectionFailed(Exception e);

		/**
		 * Update zone info
		 */
		public void updateZoneInfo(String queueIdx, boolean playSound);

		/**
		 * Update rank
		 */
		public void updateRankInfo(String queueIdx, String state);

        /**
         * Update meter tariff fare values
         */
        public void updateTariff(String startFare, String regularFare, int currencyFactor);
	}

	public XmppEventsListener mListener;
	
	public static XMPPClient getInstance() {
		if (client == null) {
			client = new XMPPClient();
		}
		return client;
	}

    public static void release() {
        try {
            if (client.isConnected())
                client.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        client = null;
        System.gc();
    }

	private XMPPClient() {
		configure(ProviderManager.getInstance());
	}

	public void setService(final OiDriverService service)
	{
		mService = service;
	}
	
	public ConnectionConfiguration getConfiguration() {
		return xmppConfig;
	}

	public Boolean connectToServer() {
		try {
			if (xmppConnection != null && xmppConnection.isConnected())
			{
				Log.i(TAG, "Already connected, exiting");
				//disconnect();
				//Log.i(TAG, "DONE.");
                return true;
			}

			xmppConfig = new ConnectionConfiguration(OiDriverApp.XMPP_SERVICE, 5222);
			xmppConfig.setSendPresence(false);
			xmppConfig.setReconnectionAllowed(RECONNECTION_MANAGER_ALLOWED);
			xmppConfig.setRosterLoadedAtLogin(false);
			xmppConfig.setVerifyRootCAEnabled(false);
			xmppConfig.setSecurityMode(SecurityMode.disabled);
			SASLAuthentication.supportSASLMechanism("PLAIN",0);
			xmppConfig.setSASLAuthenticationEnabled(true);
			//xmppConfig.setSocketFactory(new OiSocketFactory());
			xmppConfig.setCompressionEnabled(true);
            xmppConfig.setSecurityMode(SecurityMode.disabled);

			SmackConfiguration.setPacketReplyTimeout(Constants.OIDRIVER_XMPP_PACKET_REPLY_TIMEOUT);

			xmppConnection = new XMPPConnection(xmppConfig);

			xmppConnection.addPacketListener(mPacketListener, null);
			xmppConnection.addPacketSendingListener(mPacketSendingListener, null);

			final OfflineMessageManager offlineMessageManager = new OfflineMessageManager(xmppConnection);
			
			try {
				xmppConnection.connect();
				xmppConnection.addConnectionListener(mService);

				if (mPingTimer != null) {
					mPingTimer.cancel();
					mPingTimer = null;
				}

				mPingTimer = new Timer();
				mPingTimer.schedule(new TimerTask() {
					@Override
					public void run() {
						if (lastSuccessfulPingByTask != -1 &&
								System.currentTimeMillis() - lastSuccessfulPingByTask > RECONNECTION_TIMEOUT) {

							//AG: in v136 disconnect();
							lastSuccessfulPingByTask = -1;
							Intent intent = new Intent("com.oidriver.CONN_STATUS");
							OiDriverApp.getAppContext().sendBroadcast(intent);

							// v140
							//xmppConnection.resetConnection();

							return;
						}

						if (xmppConnection != null && xmppConnection.isConnected() && xmppConnection.isAuthenticated()) {
							Ping ping = new Ping(xmppConnection.getUser(), "xmpp.abmarbarros.com");
							xmppConnection.sendPacket(ping);
						}
					}
				}, 10000, 30000);

				// v150 - disable ICMP ping
//				if (mIcmpTimer != null) {
//					mIcmpTimer.cancel();
//					mIcmpTimer = null;
//				}
//				mIcmpTimer = new Timer();
//				mIcmpTimer.schedule(new TimerTask() {
//					@Override
//					public void run() {
//
//						tryPingGoogleDNS();
//					}
//				}, 5000, 10000);

				return true;
			} catch (IllegalStateException e) {
				Log.e(TAG, "ERROR : IllegalStateException while Connecting to Server");
				e.printStackTrace();
				return false;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		} catch (Exception e) {
			Log.e(TAG, "ERROR : Connecting to Server");
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Ping Googl DNS
	 * @return ping result, or true otherwise
	 */
	@SuppressWarnings("unused")
	private boolean tryPingGoogleDNS() {
		try {
			if (xmppConnection != null && xmppConnection.isConnected()) {
				int ret = NetworkPing.pingHost(XMPP_HOST_IP);
				if (ret == 0)
					Log.wtf("PING", "SUCCESS");
				else {
					Log.wtf("PING", "FAILURE");
					xmppConnection.resetConnection();
					return false;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return true;
	}

	public void disconnect()
	{
		if (xmppConnection != null && xmppConnection.isConnected()) {

			if (mPingTimer != null) {
				mPingTimer.cancel();
				mPingTimer = null;
			}
			if (mIcmpTimer != null) {
				mIcmpTimer.cancel();
				mIcmpTimer = null;
			}

			xmppConnection.removePacketSendingListener(mPacketSendingListener);
			xmppConnection.removePacketListener(mPacketListener);
			/////RECONN MGR: v136 - uncommented
			xmppConnection.removeConnectionListener(mService);
			xmppConnection.disconnect();
		}
	}

	public Boolean login() {
		if (TextUtils.isEmpty(OiDriverService.imei))
			return false;

		String userName = OiDriverService.imei.substring(OiDriverService.imei
				.length() - 8);
		try {

			xmppConnection.login(userName, userName, "OiDriver");
            xmppConnection.getChatManager().addChatListener(
                    new ChatManagerListener() {
                        @Override
                        public void chatCreated(Chat chat, boolean createdLocally) {
                            chat.addMessageListener(new MessageListener() {
                                @Override
                                public void processMessage(Chat chat, Message message) {
                                    String body = message.getBody().trim();
                                    if (body.startsWith("rank")) {
                                        Log.wtf(TAG, "ABOUT TO PROCESS RANK MESSAGE");
                                        processRank(body);
                                    } else if (body.startsWith("driver_msg")) {
                                        String msg = body.replace("driver_msg,", "");
                                        Intent intent = new Intent("com.oidriver.DRIVER_MESSAGE");
                                        intent.putExtra("message", msg);
                                        OiDriverApp.getAppContext().sendBroadcast(intent);
                                    } else if (body.startsWith("logon")) {
                                        String msg = body.replace("logon,", "");
                                        Intent intent = new Intent("com.oidriver.DRIVER_LOGON_MESSAGE");
                                        intent.putExtra("message", msg);
                                        OiDriverApp.getAppContext().sendBroadcast(intent);
                                    } else {
                                        String xml = message.toXML();
                                        String from = message.getFrom();
                                        if (!tryProcessMessage(xml, from))
                                            processJob(message, from);
                                    }
                                }
                            });
                        }
                    });

			return true;
		} catch (Exception e) {
			String message = e.getMessage();
			if (message.contains("not-authorized")) {
				Intent intent = new Intent("com.oidriver.SHOW_ERROR");
				ArrayList<String> errors = new ArrayList<String>();
				errors.add("Failed to login: NOT REGISTERED");
				intent.putStringArrayListExtra("errors", errors);
				OiDriverApp.getAppContext().sendBroadcast(intent);
			}
			e.printStackTrace();
			return false;
		}
	}

	public Boolean register(String username, String password) {
		XMPPConnection xmppConnectionTemp = null;
		try {
			ConnectionConfiguration config = 
					new ConnectionConfiguration(Constants.XMPP_SERVICE, 5222);
			config.setSASLAuthenticationEnabled(false);
			config.setSendPresence(false);
 
			SmackConfiguration.setPacketReplyTimeout(30000);
			SASLAuthentication.supportSASLMechanism("PLAIN", 0);
			
			xmppConnectionTemp = new XMPPConnection(config);
			xmppConnectionTemp.connect();
			AccountManager accountManager = xmppConnectionTemp.getAccountManager();
			accountManager.createAccount(username, password);
			xmppConnectionTemp.disconnect();
 
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			if (xmppConnectionTemp != null) {
				xmppConnectionTemp.disconnect();
			}
			return e.getMessage().contains(
					"The requested username already exists.");
		}
	}

	@SuppressWarnings("unused")
	public void setReconnectionAllowed(boolean enable)
	{
		if (xmppConfig != null)
			xmppConfig.setReconnectionAllowed(enable);
	}

	private void processJob(Message arg1, String from)
	{	
		String job = arg1.getBody();
		if(job != null)
		{
			try
			{
				String str = arg1.toXML();
				if (str.toLowerCase().contains("zoneinfo"))
					XMPPClient.parseJob(str, from);
				else
					XMPPClient.parseJobV2(str, from);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				if (mListener != null)
					mListener.onMessage(0, "ERROR : Showing New Job");
			}
		}
	}

	public static void parseJob(String xml, String from)
			throws ParserConfigurationException, SAXException, IOException {
		
		String str = escapeHtml(xml);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		InputStream is = new ByteArrayInputStream(str.getBytes());

		Document doc = db.parse(is);
		doc.getDocumentElement().normalize();

        NodeList main = doc.getElementsByTagName("DocumentElement");
        Element element = (Element) main.item(0);
        String xmlns = element.getAttribute("xmlns");
        Log.wtf("OFFER", "TYPE = " + xmlns);

		NodeList zoneInfoNodeList = doc.getElementsByTagName("ZONEINFO");
		if(zoneInfoNodeList != null && zoneInfoNodeList.getLength() > 0)
		{
			
			for (int i = 0; i < zoneInfoNodeList.getLength(); i++) 
			{
				Element el = (Element) zoneInfoNodeList.item(i);
				AcceptedJob currentJob = Util.parseJob(el);
				currentJob.setSentBy(from);
				Intent intent = new Intent("com.oidriver.NEW_JOB");
				intent.putExtra("job", currentJob);
                intent.putExtra("type", xmlns);
				OiDriverApp.getAppContext().sendBroadcast(intent);
			}
		}
		else
		{
			Log.e(OiDriverApp.TAG, "Unrecognised XML: " + xml);
		}
	}

	public static void parseJobV2(String xml, @SuppressWarnings("unused")String from)
			throws ParserConfigurationException, SAXException, IOException {
		
		String str = escapeHtml(xml);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		InputStream is = new ByteArrayInputStream(str.getBytes());

		Document doc = db.parse(is);
		doc.getDocumentElement().normalize();
		
		NodeList zoneInfoNodeList = doc.getElementsByTagName("notify");
		if(zoneInfoNodeList != null && zoneInfoNodeList.getLength() > 0)
		{
			int totalBookingsAdded = 0;
			int totalBookingsDeleted = 0;
			int[] bidTypes = new int[] {0,0};
			for (int i = 0; i < zoneInfoNodeList.getLength(); i++) 
			{			
				Element el = (Element) zoneInfoNodeList.item(i);
				String action = Util.getAction(el);
				if (action.equalsIgnoreCase("BOOKING_DELETED"))
				{
					String bookingId = Util.getBookingId(el);
					Log.i(OiDriverApp.TAG, "GOT BOOKING_DELETED: " + bookingId);
					boolean res = OiDriverService.deleteBidByBookingId(bookingId);
					if (res)
					{
						totalBookingsDeleted++;
						Log.d(OiDriverApp.TAG, "Deleted bid " + bookingId);
					}
					else
					{
						Log.e(OiDriverApp.TAG, "Failed to delete bid " + bookingId);
					}
				}
				else if (action.equalsIgnoreCase("BOOKING_CREATED"))
				{
					Booking booking = Util.parseBookingV2(el);
					String bookingId = booking.getBookingId();
					Log.i(OiDriverApp.TAG, "GOT BOOKING_CREATED: " + bookingId);
					if (!TextUtils.isEmpty(bookingId))
					{
						long now = Calendar.getInstance().getTimeInMillis();
						long diff = booking.getDtBookingTimestamp().getTime() - now;
						if (diff > Constants.BOOKING_ASAP_THRESHOLD)
						{
							booking.setBookingType(1);
						}
						else
						{
							booking.setBookingType(0);
						}
						bidTypes[booking.getBookingType()]++;
						boolean success = OiDriverService.bookings.add(booking);//dbHelper.insertBid(booking);
						if (success)
							totalBookingsAdded++;
					}
				}
			}
			if (totalBookingsAdded + totalBookingsDeleted > 0 )
			{
				Intent intent = new Intent("com.oidriver.NEW_BOOKINGS_AVAILABLE");
				intent.putExtra("is_deleted", totalBookingsDeleted);
				intent.putExtra("is_added", totalBookingsAdded);
				intent.putExtra("now_bids", bidTypes[0]);
				intent.putExtra("adv_bids", bidTypes[1]);
				OiDriverApp.getAppContext().sendBroadcast(intent);
			}
		}
		else
		{
			Log.i(OiDriverApp.TAG, "Checking for CANCELLED JOBS");
			NodeList cancelledList = doc.getElementsByTagName("cancelbooking");
			if(cancelledList != null && cancelledList.getLength() > 0)
			{
				for (int i = 0; i < cancelledList.getLength(); i++) 
				{
					Element el = (Element) cancelledList.item(i);
					NodeList nl = el.getElementsByTagName("Jobid");
					if (nl != null && nl.getLength() > 0) {
						Element elm = (Element) nl.item(0);
						if (null != elm) {
							Node nodeJobIdText = elm.getFirstChild();
							if (null != nodeJobIdText) {
								String jobId = nodeJobIdText.getNodeValue();
								Log.i(OiDriverApp.TAG, "GOT BOOKING_CANCELLED: " + jobId);
								Intent intent = new Intent("com.oidriver.JOB_CANCELLED");
								intent.putExtra("jobId", jobId);
								OiDriverApp.getAppContext().sendBroadcast(intent);
							}
						}
					}
				}
			}
			else
			{
				NodeList bidResponseList = doc.getElementsByTagName("bidResponse");
				if(bidResponseList != null && bidResponseList.getLength() > 0)
				{
					Element el = (Element) bidResponseList.item(0);
					String mess = Util.getTextValue(el, "mess");
					String jobid = Util.getTextValue(el, "jobid");
					String action = Util.getTextValue(el, "action");
					// Convert 'action' to integer
					Integer act = Integer.parseInt(action);
					Intent intent = new Intent("com.oidriver.BID_RESPONSE");
					intent.putExtra("jobId", jobid);
					intent.putExtra("action", act);
					intent.putExtra("mess", mess);
					OiDriverApp.getAppContext().sendBroadcast(intent);
				}
				else
				{
                    // Get all Booking
                    NodeList bodyNodeList = doc.getElementsByTagName("rank");

                    if (bodyNodeList != null && bodyNodeList.getLength() > 0) {

                        Element el = (Element) bodyNodeList.item(0);
                        String queue = Util.getTextValue(el, "queue");
                        String state = Util.getTextValue(el, "state");
                        if (TextUtils.isEmpty(queue))
                            return;
                        XmppEventsListener listener = OiDriverService.get().getResultsReceiver();
                        if (listener != null) {
                            listener.updateRankInfo(queue, state);
                        }
                    }
                    else
                        Log.e(OiDriverApp.TAG, "Unrecognised XML: " + xml);
				}
			}
		}
	}
	
	public void sendJobBid(Booking booking) {
		try {
			if (xmppConnection == null || !xmppConnection.isConnected()) {
				if (mListener != null)
					mListener.onMessage(0x10001, "No connection to the server");
				return;
			}
			String user = xmppConnection.getUser();
			if (TextUtils.isEmpty(user))
			{
				user = OiDriverService.imei.substring(OiDriverService.imei.length() - 8);
			}
			else
			{
				user = user.split("/")[0];
			}
			String message = "<requestbidjob><driverid>"+user+"</driverid><jobid>"+booking.getJobId()+"</jobid><zoneid>" + 
							(TextUtils.isEmpty(OiDriverService.currentZoneId) ? "8" : OiDriverService.currentZoneId) + "</zoneid></requestbidjob>";
			String sendToJid = booking.getSentBy();
			if (TextUtils.isEmpty(sendToJid))
				sendToJid = OiDriverApp.MANAGER_JID;
			Chat sendJobChat = xmppConnection.getChatManager().createChat(sendToJid, new MessageListener() {
				
						@Override
						public void processMessage(Chat chat, Message message) {
							Log.e(TAG, "Got bid message: " + message.getBody());
						}
					});
			sendJobChat.sendMessage(message);
		} catch (Exception e) {
			Log.e(TAG, "Could not send Bid, please try again");
		}

	}
	
	public void sendIQForGPS(final boolean adjacent, final String status/*final boolean ready*/) {
		try {

			if (xmppConnection == null || !xmppConnection.isConnected()) {
				return;
			}

			//52.403558,-1.534055
			final IQ iq = new IQ() {
				@Override
				public String getChildElementXML() {
					String elem;
					elem =  "<query xmlns='http://rappidcars.co.uk/protocol/driver#publish'>"
							+ getGpsStr()
							+ "<driverImei>"
							+ OiDriverService.imei
							+ "</driverImei>"
							+ "<status>" + status + "</status>"
							+ "<userName>" + OiDriverApp.COMPANY + "</userName>"
							+ "<returnAdjacent>" + adjacent + "</returnAdjacent>"
							+ "</query>";
					return elem;
				}
			};

			iq.setTo("vehicles.abmarbarros.com");
			iq.setFrom(xmppConnection.getUser());
			iq.setType(IQ.Type.SET);
			xmppConnection.sendPacket(iq);
			OiDriverService.hasOutstandingGPS = false;
			
		} catch (Exception e) {
			Log.e(TAG, "Could not Send Query for ZoneInfo Taxi");
		}
	}

	@SuppressWarnings("unused")
	public void sendIQForGPS(final boolean adjacent, final String status, double lat, double lon) {
		try {

			if (xmppConnection == null || !xmppConnection.isConnected()) {
				return;
			}

			//52.403558,-1.534055
			final IQ iq = new IQ() {
				@Override
				public String getChildElementXML() {
					String elem;
					elem =  "<query xmlns='http://rappidcars.co.uk/protocol/driver#publish'>"
							+ getGpsStr()
							+ "<driverImei>"
							+ OiDriverService.imei
							+ "</driverImei>"
							+ "<status>" + status + "</status>"
							+ "<userName>" + OiDriverApp.COMPANY + "</userName>"
							+ "<returnAdjacent>" + adjacent + "</returnAdjacent>"
							+ "</query>";
					return elem;
				}
			};

			iq.setTo("vehicles.abmarbarros.com");
			iq.setFrom(xmppConnection.getUser());
			iq.setType(IQ.Type.SET);
			xmppConnection.sendPacket(iq);
			OiDriverService.hasOutstandingGPS = false;

		} catch (Exception e) {
			Log.e(TAG, "Could not Send Query for ZoneInfo Taxi");
		}
	}

	public void sendIQForZones() {
		try {
			if (xmppConnection == null || !xmppConnection.isConnected()) {
				return;
			}

			final IQ iq = new IQ() {
				@Override
				public String getChildElementXML() {
					return "<query xmlns='http://rappidcars.co.uk/protocol/zone#all'>"
							+ "<userName>" + OiDriverApp.COMPANY + "</userName>"
							+ "</query>";
				}
			};

			iq.setTo("vehicles.abmarbarros.com");
			iq.setFrom(xmppConnection.getUser());
			iq.setType(IQ.Type.GET);
			xmppConnection.sendPacket(iq);
		} catch (Exception e) {
			Log.e(TAG, "Could not Send Query to get all zones");
		}
	}
	
	public void sendIQForDrivers() {
		try {
			if (xmppConnection == null || !xmppConnection.isConnected()) {
				return;
			}

			final IQ iq = new IQ() {
				@Override
				public String getChildElementXML() {
					return "<query xmlns='http://rappidcars.co.uk/protocol/driver#search'>"
							+ getGpsStr()
							+ "<userName>" + OiDriverApp.COMPANY + "</userName>"
							//+ "<radius>100000000</radius>"
							+ getRadius()
							//+ "<expiration>10000000</expiration>"
							+ "<status>RTW</status>"
							+ "<set xmlns='http://jabber.org/protocol/rsm'>"
							+ "<index>0</index>"
							+ "<max>10</max>"
							+ "</set>"
							+ "</query>";

				}
			};

			iq.setTo("vehicles.abmarbarros.com");
			iq.setFrom(xmppConnection.getUser());
			iq.setType(IQ.Type.GET);
			xmppConnection.sendPacket(iq);
		} catch (Exception e) {
			Log.e(TAG, "Could not Send Query to Search Taxi");
		}
	}

	public void sendIQToGetBookings(final int maxCount) {
		try {

			BookingIQProvider.mMaxBookingsCount = maxCount;

			if (xmppConnection == null || !xmppConnection.isConnected()) {
				connectToServer();
			}
			if (xmppConnection == null || !xmppConnection.isConnected()) {
				Log.e(OiDriverApp.TAG, "sendIQToGetBookings(): Cannot connect to XMPP server!");
				Intent intent = new Intent("com.oidriver.SEARCH_BOOKINGS");
				intent.putExtra("total", 0);
				OiDriverApp.getAppContext().sendBroadcast(intent);
				blockingOperationProgress = false;
				return;
			}

			final IQ iq = new IQ() {
				@Override
				public String getChildElementXML() {
					return "<query xmlns='http://rappidcars.co.uk/protocol/booking#search'>"
							+ getGpsStr()
							+ getRadius()
							+ "<userName>" + OiDriverApp.COMPANY + "</userName>"
							+ "<status>CREATED</status>"
							//+ "<radius>100000</radius>"
							+ "<set xmlns='http://jabber.org/protocol/rsm'>"
							+ "<index>0</index>"
							+ "<max>" + maxCount + "</max>"
							+ "</set>"
							+ "</query>";

				}
				//+ "<expiration>10000000</expiration>"
			};

			iq.setTo("passengers.abmarbarros.com");
			iq.setFrom(xmppConnection.getUser());
			iq.setType(IQ.Type.GET);
			xmppConnection.sendPacket(iq);
			blockingOperationProgress = true;
		} catch (Exception e) {
			Log.e(TAG, "Could not Send Query to Search Bookings");
		}
	}

	public void sendIQToSubscribeToBookings(final ArrayList<String> bookingIds) {
		try {
			if (xmppConnection == null || !xmppConnection.isConnected()) {
				connectToServer();
			}
			if (xmppConnection == null || !xmppConnection.isConnected()) {
				Log.e(OiDriverApp.TAG, "sendIQToSubscribeToBookings(): Cannot connect to XMPP server!");
				return;
			}

			final IQ iq = new IQ() {
				@Override
				public String getChildElementXML() {
					String iq =  "<query xmlns='http://rappidcars.co.uk/protocol/booking#subscribe'>"
							+ "<bookings>";
					for (String id: bookingIds)
						iq += "<bookingId>" + id + "</bookingId>";
					iq += "</bookings></query>";
					return iq;
				}
				//+ "<expiration>10000000</expiration>"
			};

			iq.setTo("passengers.abmarbarros.com");
			iq.setFrom(xmppConnection.getUser());
			iq.setType(IQ.Type.SET);
			xmppConnection.sendPacket(iq);
		} catch (Exception e) {
			Log.e(TAG, "Could not Send Query to Search Bookings");
		}
	}

	public void sendIQToUnsubscribeFromBookings(final ArrayList<String> bookingIds) {
		try {
			if (xmppConnection == null || !xmppConnection.isConnected()) {
				connectToServer();
			}
			if (xmppConnection == null || !xmppConnection.isConnected()) {
				Log.e(OiDriverApp.TAG, "sendIQToSubscribeToBookings(): Cannot connect to XMPP server!");
				return;
			}

			final IQ iq = new IQ() {
				@Override
				public String getChildElementXML() {
					String iq =  "<query xmlns='http://rappidcars.co.uk/protocol/booking#unsubscribe'>"
							+ "<bookings>";
					for (String id: bookingIds)
						iq += "<bookingId>" + id + "</bookingId>";
					iq += "</bookings></query>";
					return iq;
				}
				//+ "<expiration>10000000</expiration>"
			};

			iq.setTo("passengers.abmarbarros.com");
			iq.setFrom(xmppConnection.getUser());
			iq.setType(IQ.Type.SET);
			xmppConnection.sendPacket(iq);
		} catch (Exception e) {
			Log.e(TAG, "Could not Send Query to Search Bookings");
		}
	}

	public void sendIQToUnsubscribeFromBooking(final String bookingId) {
		try {
			if (xmppConnection == null || !xmppConnection.isConnected()) {
				connectToServer();
			}
			if (xmppConnection == null || !xmppConnection.isConnected()) {
				Log.e(OiDriverApp.TAG, "sendIQToSubscribeToBookings(): Cannot connect to XMPP server!");
				return;
			}

			final IQ iq = new IQ() {
				@Override
				public String getChildElementXML() {
					String iq =  "<query xmlns='http://rappidcars.co.uk/protocol/booking#unsubscribe'>";
					iq += "<bookingId>" + bookingId + "</bookingId>";
					iq += "</query>";
					return iq;
				}
				//+ "<expiration>10000000</expiration>"
			};

			iq.setTo("passengers.abmarbarros.com");
			iq.setFrom(xmppConnection.getUser());
			iq.setType(IQ.Type.SET);
			xmppConnection.sendPacket(iq);
		} catch (Exception e) {
			Log.e(TAG, "Could not Send Query to Search Bookings");
		}
	}

    /**
     * ConnectPay request:
     * https://connectpayadmin.co.uk/mpos_xpospay.asp?uid=demo&pwd=user&accountid=Demo4&
     * profileid=Demo4_PAY0001&sessionid=testpayment&amount=527&cardno=4444333322221111&expmonth=12&
     * expyear=2015&issueno=&cvv=123&avshouseno=&avspostcode=
     */
    public boolean sendPaymentInfo(String amount, String cardNo, int expMonth, int expYear, String cvv, String postcode) {

        String booking = "<payment>" +
                "<amount>" + amount + "</amount>" +
                "<cardNo>" + cardNo + "</cardNo>"+
                "<expMonth>" + expMonth + "</expMonth>" +
                "<expYear>" + expYear + "</expYear>" +
                "<cvv>" + cvv + "</cvv>"+
                "<postcode>" + postcode + "</postcode>" +
                "</payment>";

        try {
            Chat chat = xmppConnection.getChatManager().createChat(
                    OiDriverApp.MANAGER_JID, new MessageListener() {

                @Override
                public void processMessage(Chat chat, Message message) {
                    Log.wtf(TAG, message.toXML());
                    Util.showNotification("PAYMENT: " + message.toXML());
                }
            });

            Log.wtf(TAG, booking);
            chat.sendMessage(booking);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Could not send Payment");
            return false;
        }
    }

	private String getRadius() {
		final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
		int pickupRange = sp.getInt("pickupRange", 0);
		int[] ranges = new int[] { 2, 6, 10 };
		int radius = ranges[pickupRange];
		return String.format(Locale.UK, "<radius>%d</radius>", radius);
	}

	public final Object lockMe = new Object();

	public void sendIQToSubscribeZone(final String zoneId) {

		try {

			if (xmppConnection == null || !xmppConnection.isConnected()) {
				return;
			}

			final IQ iq = new IQ() {
				@Override
				public String getChildElementXML() {
					return "<query xmlns='http://rappidcars.co.uk/protocol/zone#subscribe'>"
							+ "<zones>"
							+ "<zoneId>"
							+ getZoneIdStr(zoneId)
							+ "</zoneId>"
							+ "</zones>"
							+ "<userName>" + OiDriverApp.COMPANY + "</userName>"
							+ "<summarized>false</summarized>"
							+ "</query>";
				}
			};

			iq.setTo("passengers.abmarbarros.com");
			iq.setFrom(xmppConnection.getUser());
			iq.setType(IQ.Type.SET);
			xmppConnection.sendPacket(iq);
		} catch (Exception e) {
			Log.e(TAG, "Could not Send IQ for Subscribe to Zone");
		}
	}
	
	public void sendIQToUnsubscribeZone(final String zoneId) {

		try {

			if (xmppConnection == null || !xmppConnection.isConnected()) {
				return;
			}

			final IQ iq = new IQ() {
				@Override
				public String getChildElementXML() {
					return "<query xmlns='http://rappidcars.co.uk/protocol/zone#unsubscribe'>"
							+ "<zoneId>"
							+ getZoneIdStr(zoneId)
							+ "</zoneId>"
							+ "<userName>" + OiDriverApp.COMPANY + "</userName>"
							+ "</query>";

				}
			};

			iq.setTo("passengers.abmarbarros.com");
			iq.setFrom(xmppConnection.getUser());
			iq.setType(IQ.Type.SET);
			xmppConnection.sendPacket(iq);
			
		} catch (Exception e) {
			Log.e(TAG, "Could not Send IQ for Subscribe to Zone");
		}

	}

	public void sendIQToSubscribeZones(final ArrayList<String> zoneIds) {

		try {

			if (xmppConnection == null || !xmppConnection.isConnected()) {
				return;
			}

			final IQ iq = new IQ() {
				@Override
				public String getChildElementXML() {
					return "<query xmlns='http://rappidcars.co.uk/protocol/zone#subscribe'>"
							+ "<zones>"
							+ getZoneIdTags(zoneIds)
							+ "</zones>"
							+ "<userName>" + OiDriverApp.COMPANY + "</userName>"
							+ "</query>";

				}
			};

			iq.setTo("passengers.abmarbarros.com");
			iq.setFrom(xmppConnection.getUser());
			iq.setType(IQ.Type.SET);
			xmppConnection.sendPacket(iq);
		} catch (Exception e) {
			Log.e(TAG, "Could not Send IQ for Subscribe to Zone");
		}

	}

	public void logout() {
		try {
			if (xmppConnection != null && xmppConnection.isConnected()) {
				xmppConnection.disconnect();
			}
		} catch (Exception e) {
			xmppConnection = null;
		}
	}

	public void setConnectionToNull() {
		xmppConnection = null;
	}

	public Boolean isConnected() {
		return xmppConnection != null && xmppConnection.isConnected();
	}

	public Boolean isAuthenticated() {
		return xmppConnection != null && xmppConnection.isConnected() && xmppConnection.isAuthenticated();
	}

	public boolean sendMessage(String message, String sendToJid) {
		try {
			if (xmppConnection == null || !xmppConnection.isConnected()) {
				return false;
			}
			Chat chat = xmppConnection.getChatManager().createChat(sendToJid, new MessageListener() {

						@Override
						public void processMessage(Chat arg0, Message arg1) {
							System.out.println("CHAT 1: " + arg1.toXML());
						}
					});

			chat.sendMessage(message);
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "Could not send message");
			return false;
		}
	}

	synchronized public void parseDriverResultXML(String xml) {
		try {

			String str = escapeHtml(xml);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputStream is = new ByteArrayInputStream(str.getBytes());

			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();
			
			// Get all Driver
			NodeList driverNodeList = doc.getElementsByTagName("driver");

			if (driverNodeList != null && driverNodeList.getLength() > 0) {
				drivers.clear();
				for (int i = 0; i < driverNodeList.getLength(); i++) {
					// get the employee element
					Element el = (Element) driverNodeList.item(i);
					Driver driver = Util.getDriver(el);
					drivers.add(driver);
				}
			} else {
				NodeList errorNodeList = doc.getElementsByTagName("error");
				if (errorNodeList != null && errorNodeList.getLength() > 0) {
					ArrayList<String> errors = new ArrayList<String>();
					for (int i = 0; i < errorNodeList.getLength(); i++) {
						Element el = (Element) errorNodeList.item(i);
						String errorText = Util.getTextValue(el, "text");
						if (!TextUtils.isEmpty(errorText))
							errors.add(errorText);
						else
						{
							String code = el.getAttribute("code");
							Node firstChild = el.getFirstChild();
							String name = "";
							if (firstChild != null)
							{
								name = firstChild.getNodeName();
							}
							errorText = code + " " + name;
							errorText = errorText.trim();
							if (!TextUtils.isEmpty(errorText))
							{
								//errors.add(errorText);
								Log.e(TAG, errorText);
							}
						}
					}
					if (errors.size() > 0)
					{
						Intent intent = new Intent("com.oidriver.SHOW_ERROR");
						intent.putStringArrayListExtra("errors", errors);
						OiDriverApp.getAppContext().sendBroadcast(intent);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "ERROR: Showing Near by Taxi");
		}
	}
	
	synchronized public String parseZoneInfoXML(String xml) {
		try {
			String str = escapeHtml(xml);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputStream is = new ByteArrayInputStream(str.getBytes());

			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();

			//03-08 22:59:12.905: I/OIDRIVER(16907): DriverPublishProvider: <query><info>Driver location updated.</info><queueIndex>1</queueIndex><lat>52.205836</lat><lon>0.127561</lon><user_id>goldnblack1</user_id><current_zone><name>CAMBRIDGE</name><id>99</id><drivers>1</drivers></current_zone><adjacents></adjacents></query>
			String queueIdx = Util.getTextValue(doc.getDocumentElement(), "queueIndex");

			NodeList zoneNodeList = doc.getElementsByTagName("current_zone");
			if (zoneNodeList != null && zoneNodeList.getLength() > 0) {
				int len = zoneNodeList.getLength();
				if (len > 0) {
					// get the 1st zone element
					Element el = (Element) zoneNodeList.item(0);
					Zone zone = Util.getZone(el);

					String currId = OiDriverService.currentZoneId;
					boolean playSound = false;
					OiDriverService.currentZoneId = zone.getId();
					OiDriverService.currentZoneName = zone.getZoneName().toUpperCase(Locale.UK);
					if (TextUtils.isEmpty(OiDriverService.currentZoneName) ||
						OiDriverService.currentZoneId.equals("OUT_OF_ZONE")) {
						OiDriverService.currentZoneName = OiDriverApp.getAppContext().getString(R.string.no_zone);
						if (TextUtils.isEmpty(currId) ||
							(!TextUtils.isEmpty(currId) && !currId.equals(OiDriverService.currentZoneId)))
							playSound = true;
					}
					else {
						if (TextUtils.isEmpty(currId) ||
							(!TextUtils.isEmpty(currId) && !currId.equals(OiDriverService.currentZoneId)))
							playSound = true;
					}

					Log.wtf("ZONE", OiDriverService.currentZoneName);

                    if (OiDriverService.get() != null) {
                        XmppEventsListener listener = OiDriverService.get().getResultsReceiver();
                        if (listener != null) {
                            listener.updateZoneInfo(queueIdx, playSound);
                        }
                        else {
                            Intent intent = new Intent("com.oidriver.ZONE_CHANGED");
                            intent.putExtra("queueIndex", queueIdx);
                            intent.putExtra("playSounf", playSound);
                            OiDriverApp.getAppContext().sendBroadcast(intent);
                        }
                    }
                    else {
                        Intent intent = new Intent("com.oidriver.ZONE_CHANGED");
                        intent.putExtra("queueIndex", queueIdx);
                        intent.putExtra("playSounf", playSound);
                        OiDriverApp.getAppContext().sendBroadcast(intent);
                    }
				}
			}
			else {
				OiDriverService.currentZoneId = "OUT_OF_ZONE";
				OiDriverService.currentZoneName = OiDriverApp.getAppContext().getString(R.string.no_zone);
                if (OiDriverService.get() != null) {
                    XmppEventsListener listener = OiDriverService.get().getResultsReceiver();
                    if (listener != null) {
                        listener.updateZoneInfo(queueIdx, false);
                    }
                    else {
                        Intent intent = new Intent("com.oidriver.ZONE_CHANGED");
                        intent.putExtra("queueIndex", queueIdx);
                        OiDriverApp.getAppContext().sendBroadcast(intent);
                    }
                }
                else {
                    Intent intent = new Intent("com.oidriver.ZONE_CHANGED");
                    intent.putExtra("queueIndex", queueIdx);
                    OiDriverApp.getAppContext().sendBroadcast(intent);
                }
				return "0";
			}
			
			NodeList adjacentNodeList = doc.getElementsByTagName("adjacent");
			if (adjacentNodeList != null && adjacentNodeList.getLength() > 0) {
				int len = adjacentNodeList.getLength();
				ArrayList<String> newZones = new ArrayList<String>();
				for (int i = 0; i < len; ++i) {
					Element el = (Element) adjacentNodeList.item(i);
					Zone zone = Util.getZone(el);
					newZones.add(zone.getId());
				}
				
				for (String zoneId: zones)
				{
					sendIQToUnsubscribeZone(zoneId);
				}
				zones.clear();
				for (String zoneId: newZones)
				{
					zones.add(zoneId);
				}
				sendIQToSubscribeZones(newZones);
				
			}
			
			return queueIdx;
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "ERROR: Getting ZoneInfo");

			return "0";
		}
	}

	public static String getServiceGpsStringRaw() {

		if (DEBUG_FAKE_GPS)
		{
			return "52.332687,-2.064482";
		}
		else
		{
			return String.format("%f,%f", OiDriverService.lat, OiDriverService.lon);
		}
	}

	private String getGpsStr()
	{
		//if ((getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != ApplicationInfo.FLAG_DEBUGGABLE)
		//double homelat = 52.332687;
		//double homelon = -2.064482;
		if (DEBUG_FAKE_GPS)
		{
//			return "<lat>52.4033393859863</lat><lon>-1.53406774997711</lon>";
			return "<lat>52.332687</lat><lon>-2.064482</lon>";
		}
		else
		{
			//52.4033393859863</lat><lon>-1.53406774997711
			return String.format("<lat>%f</lat><lon>%f</lon>", OiDriverService.lat, OiDriverService.lon);
			//FOR NO MANS LAND: return "<lat>52.4033393859863</lat><lon>6.53406774997711</lon>";
			//return String.format("<lat>%.13f</lat><lon>%.14f</lon>", OiDriverService.lat, OiDriverService.lon);
		}
	}

	@SuppressWarnings("unused")
	private String getGpsStr(double lat, double lon)
	{
		//if ((getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != ApplicationInfo.FLAG_DEBUGGABLE)
		if (DEBUG_FAKE_GPS)
		{
			return "<lat>52.4033393859863</lat><lon>-1.53406774997711</lon>";
		}
		else
		{
			//52.4033393859863</lat><lon>-1.53406774997711
			return String.format("<lat>%f</lat><lon>%f</lon>", OiDriverService.lat, OiDriverService.lon);
			//return String.format("<lat>%.13f</lat><lon>%.14f</lon>", OiDriverService.lat, OiDriverService.lon);
		}
	}

	private String getZoneIdStr(String zoneId)
	{
		//if ((getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != ApplicationInfo.FLAG_DEBUGGABLE)
		if (DEBUG_FAKE_GPS)
		{
			return "8";
		}
		else
		{
			return zoneId; 
			//return String.format("<lat>%.13f</lat><lon>%.14f</lon>", OiDriverService.lat, OiDriverService.lon); 
		}
	}

	private String getZoneIdTags(ArrayList<String> zoneIds)
	{
		if (DEBUG_FAKE_GPS)
		{
			return "<zoneId>8</zoneId><zoneId>5</zoneId><zoneId>4</zoneId>";
		}
		String res = "";
		for (String zoneId: zoneIds)
		{
			res += "<zoneId>" + zoneId + "</zoneId>";
		}
			
		return res;
	}

	boolean tryProcessMessage(String xml, String from)
	{
		boolean res = false;
		
		try {
			String str = escapeHtml(xml);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Log.d(TAG, "Going to parse xml:\n" + str);
			InputStream is = new ByteArrayInputStream(str.getBytes());

			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();
			
			NodeList bodyNodeList = doc.getElementsByTagName("body");

			if (bodyNodeList != null && bodyNodeList.getLength() > 0) {
				
				Element el = (Element) bodyNodeList.item(0);
				String msg = Util.getTextValue(el, "message");
				if (msg != null && msg.trim().equalsIgnoreCase("ping"))
				{
					sendMessage("Ping", from);
					res = true;
				}
				else if (msg != null && msg.trim().equalsIgnoreCase(Constants.DRIVER_STATUS_BRK))
				{
					Log.w(TAG, "RECEIVED BRK MESSAGE");

					Intent intent = new Intent("com.oidriver.DRIVER_STATUS");
					intent.putExtra("status", false);
					OiDriverApp.getAppContext().sendBroadcast(intent);

					res = true;
				}
			}
			else
			{
				NodeList list = doc.getElementsByTagName("error");
				if (list != null && list.getLength() > 0) {
					
					Element el = (Element) list.item(0);
					String error = Util.getTextValue(el, "text");
					Log.e(TAG, error);
					res = true;
				}
			}
			/**/
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
	void processDriverMessage(String xml, @SuppressWarnings("unused")String from)
	{
		try {
			String str = escapeHtml(xml);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputStream is = new ByteArrayInputStream(str.getBytes());

			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();
			
			// Get all Booking
			NodeList bodyNodeList = doc.getElementsByTagName("body");

			if (bodyNodeList != null && bodyNodeList.getLength() > 0) {
				
				Element el = (Element) bodyNodeList.item(0);
				String msg = Util.getTextValue(el, "driver_msg");
				if (TextUtils.isEmpty(msg))
					return;
				Intent intent = new Intent("com.oidriver.DRIVER_MESSAGE");
				intent.putExtra("message", msg);
				OiDriverApp.getAppContext().sendBroadcast(intent);
			}
			/**/
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void requestDriverInfo()
	{
		try {
			if (xmppConnection == null || !xmppConnection.isConnected()) {
				return;
			}

			final IQ iq = new IQ() {
				@Override
				public String getChildElementXML() {
					return "<query xmlns='http://rappidcars.co.uk/protocol/driver#get'>"
							+ "<imei>" + OiDriverService.imei + "</imei>"
							+ "</query>";
				}
			};

			iq.setTo("vehicles.abmarbarros.com");
			iq.setFrom(xmppConnection.getUser());
			iq.setType(IQ.Type.GET);
			xmppConnection.sendPacket(iq);
		} catch (Exception e) {
			Log.e(TAG, "Could not Send Query to Search Taxi");
		}
	}
	
	public void configure(ProviderManager pm) 
	{
		// Custom queries
		pm.addIQProvider("query", "http://rappidcars.co.uk/protocol/driver#search", new DriverIQProvider());// search for other drivers in the area
		pm.addIQProvider("query", "http://rappidcars.co.uk/protocol/booking#search", new BookingIQProvider());//
		pm.addIQProvider("query", "http://rappidcars.co.uk/protocol/booking#get", new BookingGetIQProvider());//
		pm.addIQProvider("query", "http://rappidcars.co.uk/protocol/zone#info", new ZoneIQProvider());// zone info (currently not in use)
		pm.addIQProvider("query", "http://rappidcars.co.uk/protocol/zone#subscribe", new ZoneSubscriptionsIQProvider());
		pm.addIQProvider("query", "http://rappidcars.co.uk/protocol/zone#all", new ZoneAllIQProvider());
//		pm.addIQProvider("query", "http://rappidcars.co.uk/protocol/zone#unsubscribe", new ZoneSubscriptionsIQProvider());
		pm.addIQProvider("query", "http://rappidcars.co.uk/protocol/driver#publish", new DriverPublishIQProvider());// driver sends out GPS
		pm.addIQProvider("query", "http://rappidcars.co.uk/protocol/driver#get", new DriverGetIQProvider());// get driver info

        //  Private Data Storage
        pm.addIQProvider("query","jabber:iq:private", new PrivateDataManager.PrivateDataIQProvider());

        //  Time
        try {
            pm.addIQProvider("query","jabber:iq:time", Class.forName("org.jivesoftware.smackx.packet.Time"));
        } catch (ClassNotFoundException e) {
            Log.w(TAG, "Can't load class for org.jivesoftware.smackx.packet.Time");
        }
 
        //  Roster Exchange
        pm.addExtensionProvider("x","jabber:x:roster", new RosterExchangeProvider());
 
        //  Message Events
        pm.addExtensionProvider("x","jabber:x:event", new MessageEventProvider());
 
        //  Chat State
        pm.addExtensionProvider("active","http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
 
        pm.addExtensionProvider("composing","http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
 
        pm.addExtensionProvider("paused","http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
 
        pm.addExtensionProvider("inactive","http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
 
        pm.addExtensionProvider("gone","http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
 
        //  XHTML
        pm.addExtensionProvider("html","http://jabber.org/protocol/xhtml-im", new XHTMLExtensionProvider());
 
        //  Group Chat Invitations
        pm.addExtensionProvider("x","jabber:x:conference", new GroupChatInvitation.Provider());
 
        //  Service Discovery # Items    
        pm.addIQProvider("query","http://jabber.org/protocol/disco#items", new DiscoverItemsProvider());
 
        //  Service Discovery # Info
        pm.addIQProvider("query","http://jabber.org/protocol/disco#info", new DiscoverInfoProvider());
 
        //  Data Forms
        pm.addExtensionProvider("x","jabber:x:data", new DataFormProvider());
 
        //  MUC User
        pm.addExtensionProvider("x","http://jabber.org/protocol/muc#user", new MUCUserProvider());
 
        //  MUC Admin    
        pm.addIQProvider("query","http://jabber.org/protocol/muc#admin", new MUCAdminProvider());
 
 
        //  MUC Owner    
        pm.addIQProvider("query","http://jabber.org/protocol/muc#owner", new MUCOwnerProvider());
 
        //  Delayed Delivery
        pm.addExtensionProvider("x","jabber:x:delay", new DelayInformationProvider());
 
        //  Version
        try {
            pm.addIQProvider("query","jabber:iq:version", Class.forName("org.jivesoftware.smackx.packet.Version"));
        } catch (ClassNotFoundException e) {
            //  Not sure what's happening here.
        }
 
        //  VCard
        pm.addIQProvider("vCard","vcard-temp", new VCardProvider());
 
        //  Offline Message Requests
        pm.addIQProvider("offline","http://jabber.org/protocol/offline", new OfflineMessageRequest.Provider());
 
        //  Offline Message Indicator
        pm.addExtensionProvider("offline","http://jabber.org/protocol/offline", new OfflineMessageInfo.Provider());
 
        //  Last Activity
        pm.addIQProvider("query","jabber:iq:last", new LastActivity.Provider());
 
        //  User Search
        pm.addIQProvider("query","jabber:iq:search", new UserSearch.Provider());
 
        //  SharedGroupsInfo
        pm.addIQProvider("sharedgroup","http://www.jivesoftware.org/protocol/sharedgroup", new SharedGroupsInfo.Provider());
 
        //  JEP-33: Extended Stanza Addressing
        pm.addExtensionProvider("addresses","http://jabber.org/protocol/address", new MultipleAddressesProvider());
 
        //   FileTransfer
        pm.addIQProvider("si","http://jabber.org/protocol/si", new StreamInitiationProvider());
 
        pm.addIQProvider("query","http://jabber.org/protocol/bytestreams", new BytestreamsProvider());
 
//        pm.addIQProvider("open","http://jabber.org/protocol/ibb", new IBBProviders.Open());
// 
//        pm.addIQProvider("close","http://jabber.org/protocol/ibb", new IBBProviders.Close());
// 
//        pm.addExtensionProvider("data","http://jabber.org/protocol/ibb", new IBBProviders.Data());
 
        //  Privacy
        pm.addIQProvider("query","jabber:iq:privacy", new PrivacyProvider());
 
        pm.addIQProvider("command", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider());
        pm.addExtensionProvider("malformed-action", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider.MalformedActionError());
        pm.addExtensionProvider("bad-locale", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider.BadLocaleError());
        pm.addExtensionProvider("bad-payload", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider.BadPayloadError());
        pm.addExtensionProvider("bad-sessionid", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider.BadSessionIDError());
        pm.addExtensionProvider("session-expired", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider.SessionExpiredError());
    
		// Pubsub
		pm.addIQProvider("pubsub", "http://jabber.org/protocol/pubsub", new PubSubProvider());
		pm.addExtensionProvider("create", "http://jabber.org/protocol/pubsub", new SimpleNodeProvider());
		pm.addExtensionProvider("items", "http://jabber.org/protocol/pubsub", new ItemsProvider());
		pm.addExtensionProvider("item", "http://jabber.org/protocol/pubsub", new ItemProvider());
		pm.addExtensionProvider("subscriptions", "http://jabber.org/protocol/pubsub", new SubscriptionsProvider());
		pm.addExtensionProvider("subscription", "http://jabber.org/protocol/pubsub", new SubscriptionProvider());
		pm.addExtensionProvider("affiliations", "http://jabber.org/protocol/pubsub", new AffiliationsProvider());
		pm.addExtensionProvider("affiliation", "http://jabber.org/protocol/pubsub", new AffiliationProvider());
		pm.addExtensionProvider("options", "http://jabber.org/protocol/pubsub", new FormNodeProvider());

		// pubsub#owner
		pm.addIQProvider("pubsub", "http://jabber.org/protocol/pubsub#owner", new PubSubProvider());
		pm.addExtensionProvider("configure", "http://jabber.org/protocol/pubsub#owner", new FormNodeProvider());
		pm.addExtensionProvider("default", "http://jabber.org/protocol/pubsub#owner", new FormNodeProvider());
		
		// pubsub#event
		pm.addExtensionProvider("event", "http://jabber.org/protocol/pubsub#event", new EventProvider());
		pm.addExtensionProvider("configuration", "http://jabber.org/protocol/pubsub#event", new ConfigEventProvider());
		pm.addExtensionProvider("delete", "http://jabber.org/protocol/pubsub#event", new SimpleNodeProvider());
		pm.addExtensionProvider("options", "http://jabber.org/protocol/pubsub#event", new FormNodeProvider());
		pm.addExtensionProvider("items", "http://jabber.org/protocol/pubsub#event", new ItemsProvider());
		pm.addExtensionProvider("item",	"http://jabber.org/protocol/pubsub#event", new ItemProvider());
		pm.addExtensionProvider("retract", "http://jabber.org/protocol/pubsub#event", new OiRetractEventProvider());//RetractEventProvider());
		pm.addExtensionProvider("purge", "http://jabber.org/protocol/pubsub#event", new SimpleNodeProvider());
		
//		pm.addExtensionProvider("booking", "newjob", new BookingProvider());
		////pm.addExtensionProvider("item",	"http://jabber.org/protocol/pubsub#event", new ItemProvider());
//		pm.addExtensionProvider("notify","http://rappidcars.co.uk/protocol/zone#updated", new ZoneNotifyProvider());
//		pm.addExtensionProvider("query","http://rappidcars.co.uk/protocol/booking#create", new BookingCreateProvider());
		
		pm.addExtensionProvider("DocumentElement", "http://jabber.org/protocol/pubsub", new JobOffersProvider());

		// XEP-0199 XMPP Ping
		//pm.addIQProvider("ping", "urn:xmpp:ping", new PingProvider());

	    ClassLoader appClassLoader = OiDriverApp.getAppContext().getClass().getClassLoader();

	    try {
		    Class.forName(org.jivesoftware.smackx.ServiceDiscoveryManager.class.getName(), true, appClassLoader);
		    Class.forName(org.jivesoftware.smack.PrivacyListManager.class.getName(), true, appClassLoader);
		    Class.forName(org.jivesoftware.smackx.XHTMLManager.class.getName(), true, appClassLoader);
		    Class.forName(org.jivesoftware.smackx.muc.MultiUserChat.class.getName(), true, appClassLoader);
		    Class.forName(org.jivesoftware.smackx.bytestreams.ibb.InBandBytestreamManager.class.getName(), true, appClassLoader);
		    Class.forName(org.jivesoftware.smackx.bytestreams.socks5.Socks5BytestreamManager.class.getName(), true, appClassLoader);
		    Class.forName(org.jivesoftware.smackx.filetransfer.FileTransferManager.class.getName(), true, appClassLoader);
		    Class.forName(org.jivesoftware.smackx.LastActivityManager.class.getName(), true, appClassLoader);
//		    if (RECONNECTION_MANAGER_ALLOWED)
	        Class.forName(org.jivesoftware.smack.ReconnectionManager.class.getName(), true, appClassLoader);
		    Class.forName(org.jivesoftware.smackx.commands.AdHocCommandManager.class.getName(), true, appClassLoader);
		    //Class.forName(OfflineMessageManager.class.getName(), true, appClassLoader);

	    } catch (ClassNotFoundException e) {
		    throw new IllegalStateException("Could not init static class blocks", e);
	    }

	}
	
	private static String escapeHtml(String xml)
	{
		return xml.replaceAll("&gt;", ">")
				.replaceAll("&lt;", "<")
				.replaceAll("&quot;", "'")
				.replaceAll("&amp;","&");
	}

    private void processRank(String csv) {
        if(TextUtils.isEmpty(csv))
            return;
        String[] data = csv.split(",");
        String queue = data[1];
        String zone = data[2];
        String state = data[3];
        String startFare = "3.00";
        String regularFare = "1.20";
        int currencyFactor = 100;
        if (data.length > 5) {
            startFare = data[4];
            regularFare = data[5];
            int pos1 = startFare.indexOf('.');
            int delta = startFare.length() - pos1;
            currencyFactor = (int)Math.pow(10, delta - 1);
        }

        try {
            XmppEventsListener listener = OiDriverService.get().getResultsReceiver();
            if (listener != null) {
                listener.updateRankInfo(queue, state);
                listener.updateTariff(startFare, regularFare, currencyFactor);
            } else {
                notifyRank(queue, state);
                notifyTariff(startFare, regularFare, currencyFactor);
            }
        } catch (Exception e) {
            e.printStackTrace();
            notifyRank(queue, state);
            notifyTariff(startFare, regularFare, currencyFactor);
        }
    }

    private void notifyRank(String queue, String state) {
        Intent intent = new Intent("com.oidriver.DRIVER_RANK");
        intent.putExtra("queue", queue);
        intent.putExtra("state", state);
        OiDriverApp.getAppContext().sendBroadcast(intent);
    }

    private void notifyTariff(String startFare, String regularFare, int currencyFactor) {
        Intent intent = new Intent("com.oidriver.TARIFF");
        intent.putExtra("start", startFare);
        intent.putExtra("regular", regularFare);
        intent.putExtra("factor", currencyFactor);
        OiDriverApp.getAppContext().sendBroadcast(intent);
    }

    /*
    private void processRank(String xml, @SuppressWarnings("unused")String from)
	{
		if(TextUtils.isEmpty(xml))
			return;
		try
		{
			String str = escapeHtml(xml);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputStream is = new ByteArrayInputStream(str.getBytes());

			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();

			// Get all Booking
			NodeList bodyNodeList = doc.getElementsByTagName("rank");

			if (bodyNodeList != null && bodyNodeList.getLength() > 0) {

				Element el = (Element) bodyNodeList.item(0);
				String queue = Util.getTextValue(el, "queue");
				String state = Util.getTextValue(el, "state");
				if (TextUtils.isEmpty(queue))
					return;
				XmppEventsListener listener = OiDriverService.get().getResultsReceiver();
				if (listener != null) {
					listener.updateRankInfo(queue, state);
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if (mListener != null)
				mListener.onMessage(0, "ERROR : Showing New Job");
		}
	}
*/
	private PacketListener mPacketListener = new PacketListener() {

		@Override
		public void processPacket(Packet arg0) {
			if (arg0 == null)
			{
				Log.e(TAG, "NULL Packet in processPacket()!");
				return;
			}
			String xml = arg0.toXML();
			String from = arg0.getFrom();
			Log.wtf(TAG, "RECV from " + from + ": " + xml);
			OiDriverService.mLastPingTime = System.currentTimeMillis();

			if (xml.contains("type=\"error\"")) {
                Log.e(TAG, "*********************** ERROR ? *******************************");
                handleXmppError(xml);
                Log.e(TAG, "*************** ERROR HANDLED ***************");
				return;
			}

            // Moved to connection -> chatCreated -> message listener
//			if (xml.contains("rank"))
//			{
//				Log.e(TAG, "*********************** RANK ? *******************************");
//				processRank(xml);//, from);
//				Log.e(TAG, "*************** RANK MESSAGE HANDLED ***************");
//				return;
//			}

            // Moved to connection -> chatCreated -> message listener
//			if (xml.contains("<message"))
//			{
//				Log.d(TAG, "*********************** MESSAGE ? *******************************");
//				boolean handled = tryProcessMessage(xml, from);
//				if (handled)
//				{
//					Log.d(TAG, "*************** MESSAGE HANDLED ***************");
//					return;
//				}
//			}

//			if (xml.contains("driver_msg"))
//			{
//				Log.d(TAG, "*********************** DRIVER MESSAGE ? *******************************");
//				processDriverMessage(xml, from);
//				Log.d(TAG, "*************** DRIVER MESSAGE HANDLED ***************");
//				return;
//			}

			if ("xmpp.abmarbarros.com".equals(from)) {
				Log.wtf(TAG, "GOT PONG");
				lastSuccessfulPingByTask = System.currentTimeMillis();
			}
			else if ("vehicles.abmarbarros.com".equals(from)) {
				Log.d(TAG, "*********************** DRIVERS SEARCH RESPONSE ? *****************************");
				parseDriverResultXML(xml);
				Log.d(TAG, "*************** DRIVER RESPONSE HANDLED ***************");
			}
			else if (arg0 instanceof Message)
			{
				if (xml.contains("logon,")) {
					Log.e(TAG, "*********************** MESSAGE-LOGON *******************************");
					Intent intent = new Intent("com.oidriver.DRIVER_LOGON_MESSAGE");
					intent.putExtra("message", xml);
					OiDriverApp.getAppContext().sendBroadcast(intent);
				}
			}
			else
			{
				Log.d(TAG, "*************** MESSAGE NOT HANDLED ***************");
			}
		}
	};

    private void handleXmppError(String xml) {
        Intent intent = new Intent("com.oidriver.SHOW_ERROR");
        ArrayList<String> errors = new ArrayList<String>();
        Pattern p = Pattern.compile("<error (.*) type");
        Matcher m = p.matcher(xml);
        String text;
        if (m.find())
            text = "Server error: " + m.group(1);
        else
            text = "Server error";
        errors.add(text);
        intent.putStringArrayListExtra("errors", errors);
        OiDriverApp.getAppContext().sendBroadcast(intent);
    }

    PacketListener mPacketSendingListener = new PacketListener() {
		public void processPacket(Packet arg0) {
			String xml = arg0.toXML();

			String to = arg0.getTo();
			Log.i(TAG, "SENT to " + to + ": " + xml);
		}
	};
}