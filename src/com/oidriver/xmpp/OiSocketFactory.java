package com.oidriver.xmpp;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.net.SocketFactory;

/**
 * Created by Alex on 21/08/2013.
 * Copyright (C) Touchnote Ltd. 2013
 */
public class OiSocketFactory extends SocketFactory {

//	public class OiSocket extends Socket {
//
//		@Override
//		public InputStream getInputStream() throws IOException {
//			return super.getInputStream();
//		}
//
//		@Override
//		public OutputStream getOutputStream() throws IOException {
//			return super.getOutputStream();
//		}
//	}

	void init(Socket socket) throws SocketException {
		//socket.setKeepAlive(true);
		//socket.setReceiveBufferSize(1024);
		socket.setOOBInline(true);
		//socket.setTcpNoDelay(true);
		socket.setSoTimeout(45000);
		//socket.setSoLinger(true, 10000);
	}

	/**
	 * Creates a new socket which is not connected to any remote host. This
	 * method has to be overridden by a subclass otherwise a {@code
	 * SocketException} is thrown.
	 *
	 * @return the created unconnected socket.
	 * @throws IOException
	 *             if an error occurs while creating a new socket.
	 */
	public Socket createSocket() throws IOException {
		Socket socket = new Socket();
		init(socket);
		return socket;
	}


	/**
	 * Creates a new socket which is connected to the remote host specified by
	 * the parameters {@code host} and {@code port}. The socket is bound to any
	 * available local address and port.
	 *
	 * @param host the remote host address the socket has to be connected to.
	 * @param port the port number of the remote host at which the socket is
	 *             connected.
	 * @return the created connected socket.
	 * @throws java.io.IOException           if an error occurs while creating a new socket.
	 * @throws java.net.UnknownHostException if the specified host is unknown or the IP address could not
	 *                                       be resolved.
	 */
	@Override
	public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
		Socket socket = getDefault().createSocket(host, port);
		init(socket);
		return socket;
	}

	/**
	 * Creates a new socket which is connected to the remote host specified by
	 * the parameters {@code host} and {@code port}. The socket is bound to the
	 * local network interface specified by the InetAddress {@code localHost} on
	 * port {@code localPort}.
	 *
	 * @param host      the remote host address the socket has to be connected to.
	 * @param port      the port number of the remote host at which the socket is
	 *                  connected.
	 * @param localHost the local host address the socket is bound to.
	 * @param localPort the port number of the local host at which the socket is
	 *                  bound.
	 * @return the created connected socket.
	 * @throws java.io.IOException           if an error occurs while creating a new socket.
	 * @throws java.net.UnknownHostException if the specified host is unknown or the IP address could not
	 *                                       be resolved.
	 */
	@Override
	public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException, UnknownHostException {
		Socket socket = getDefault().createSocket(host, port, localHost, localPort);
		init(socket);
		return socket;
	}

	/**
	 * Creates a new socket which is connected to the remote host specified by
	 * the InetAddress {@code host}. The socket is bound to any available local
	 * address and port.
	 *
	 * @param host the host address the socket has to be connected to.
	 * @param port the port number of the remote host at which the socket is
	 *             connected.
	 * @return the created connected socket.
	 * @throws java.io.IOException if an error occurs while creating a new socket.
	 */
	@Override
	public Socket createSocket(InetAddress host, int port) throws IOException {
		Socket socket = getDefault().createSocket(host, port);
		init(socket);
		return socket;
	}

	/**
	 * Creates a new socket which is connected to the remote host specified by
	 * the InetAddress {@code address}. The socket is bound to the local network
	 * interface specified by the InetAddress {@code localHost} on port {@code
	 * localPort}.
	 *
	 * @param address      the remote host address the socket has to be connected to.
	 * @param port         the port number of the remote host at which the socket is
	 *                     connected.
	 * @param localAddress the local host address the socket is bound to.
	 * @param localPort    the port number of the local host at which the socket is
	 *                     bound.
	 * @return the created connected socket.
	 * @throws java.io.IOException if an error occurs while creating a new socket.
	 */
	@Override
	public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
		Socket socket = getDefault().createSocket(address, port, localAddress, localPort);
		init(socket);
		return socket;
	}
}
