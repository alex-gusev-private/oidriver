package com.oidriver.xmpp;

import android.util.Log;

import com.oidriver.OiDriverApp;
import com.oidriver.service.OiDriverService;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

public class ZoneSubscriptionsIQProvider implements IQProvider {

	@Override
	public IQ parseIQ(XmlPullParser parser) {
		Log.wtf("AG-SMACK", "   ZONE-SUBSCRIPTION-IQPROVIDER");
		try
		{
			String namespace = parser.getNamespace();
			Log.i(OiDriverApp.TAG, "ZONE NAMESPACE: " + namespace);
			int event = parser.getEventType();
			final StringBuilder builder = new StringBuilder();
			
			while(true){
				switch (event) {  
				  
		        case XmlPullParser.START_DOCUMENT:  
		            
		            break;  
		  
		        case XmlPullParser.END_DOCUMENT:  
		        	 
		        	
		            break;  
		        case XmlPullParser.START_TAG:  
		            builder.append("<"+parser.getName()+">");  
		            
		            break;  
		  
		        case XmlPullParser.END_TAG:  
		            builder.append("</"+parser.getName()+">");  
		  
		            break;  
		  
		        case XmlPullParser.TEXT:  
		            
		            builder.append(parser.getText());  
		              
		            break;  
		        }
				event = parser.next();
				String iqStr = builder.toString();
				if(iqStr.contains("</query>"))
				{
					OiDriverService.sendIQToGetBookings(0, null);
					break;
				}
			}
			
			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public ZoneSubscriptionsIQProvider() {

	}

}
