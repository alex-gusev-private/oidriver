/**
 * All rights reserved. Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oidriver.xmpp;

import android.util.Log;

import com.oidriver.OiDriverApp;

import org.jivesoftware.smack.packet.DefaultPacketExtension;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.EmbeddedExtensionProvider;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Parses the <b>subscription</b> element out of the pubsub IQ message from 
 * the server as specified in the <a href="http://xmpp.org/extensions/xep-0060.html#schemas-pubsub">subscription schema</a>.
 * 
 * @author Robin Collier
 */
public class ZoneNotifyProvider extends EmbeddedExtensionProvider
{
	@Override
	protected PacketExtension createReturnExtension(String currentElement,
			String currentNamespace, Map<String, String> attributeMap,
			List<? extends PacketExtension> content) {

		Log.wtf("AG-SMACK", "   ZONE-NOTIFY-PROVIDER");
		Log.i(OiDriverApp.TAG, String.format("Processing Element: %s, Namespace: %s, Content Count: %d", currentElement, currentNamespace, content.size()));
		for (PacketExtension ext: content)
		{
			if (ext instanceof DefaultPacketExtension)
			{
				DefaultPacketExtension dex = (DefaultPacketExtension)ext;
				Collection<String> coll = dex.getNames();
				for (String name: coll)
				{
					String field = dex.getValue(name);
					Log.i(OiDriverApp.TAG, String.format("%s = %s", name, field));
				}
			}
		}
		
		return null;
	}
}
