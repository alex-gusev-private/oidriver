package com.oidriver.xmpp;

import android.content.Intent;
import android.util.Log;

import com.oidriver.OiDriverApp;
import com.oidriver.ZoneInfo;
import com.oidriver.service.OiDriverService;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

public class ZoneAllIQProvider implements IQProvider {
	@Override
	public IQ parseIQ(XmlPullParser parser) {
		Log.wtf("AG-SMACK", "   ZONE-ALL-IQPROVIDER");
		try
		{
			int event = parser.getEventType();
			/*
			final StringBuilder builder = new StringBuilder();
			
			while(true){
				switch (event) {  
				  
		        case XmlPullParser.START_DOCUMENT:  
		            
		            break;  
		  
		        case XmlPullParser.END_DOCUMENT:  
		        	 
		        	
		            break;  
		        case XmlPullParser.START_TAG:  
		            builder.append("<"+parser.getName()+">");  
		            
		            break;  
		  
		        case XmlPullParser.END_TAG:  
		            builder.append("</"+parser.getName()+">");  
		  
		            break;  
		  
		        case XmlPullParser.TEXT:  
		            
		            builder.append(parser.getText());  
		              
		            break;  
		        }
				event = parser.next();
				String iqStr = builder.toString();
				if(iqStr.contains("</query>"))
				{
					Intent intent = new Intent("com.oidriver.ZONES_SEARCH_FINISHED");
					intent.putExtra("xml", iqStr);
					OiDriverApp.getAppContext().sendBroadcast(intent);
					break;
				}
			}
			*/
			boolean done = false;
			OiDriverService.mZones.clear();
			ZoneInfo zi;
			boolean zoneStarted = false;
			String zoneNname = "", drivers = "", passengers = "";
			while (!done) {
				if (event == XmlPullParser.START_TAG) {
					String name = parser.getName();
					if (name.equalsIgnoreCase("zone")) {
						zoneStarted = true;
						zoneNname = drivers = passengers = "";
					}
					else if(name.equalsIgnoreCase("name")) {
						zoneNname = parser.nextText();
					}
					else if(name.equalsIgnoreCase("drivers")) {
						drivers = parser.nextText();
					}
					else if(name.equalsIgnoreCase("passengers")) {
						passengers   = parser.nextText();
					}
				}
				else if (event == XmlPullParser.END_TAG) {
					if (parser.getName().equals("zone") && zoneStarted) {
						ZoneInfo zone = new ZoneInfo(zoneNname, drivers, passengers);
						OiDriverService.mZones.add(zone);
						zoneStarted = false;
					}
					else if (parser.getName().equals("query")) {
						done = true;
					}
				}
				event = parser.next();
			}
			Intent intent = new Intent("com.oidriver.ZONES_SEARCH_FINISHED");
			OiDriverApp.getAppContext().sendBroadcast(intent);
			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public ZoneAllIQProvider() {

	}

}
