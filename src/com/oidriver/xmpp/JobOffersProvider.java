/**
 * All rights reserved. Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oidriver.xmpp;

import android.util.Log;

import com.oidriver.OiDriverApp;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smackx.pubsub.Item;
import org.xmlpull.v1.XmlPullParser;

/**
 * Parses the <b>subscription</b> element out of the pubsub IQ message from 
 * the server as specified in the <a href="http://xmpp.org/extensions/xep-0060.html#schemas-pubsub">subscription schema</a>.
 * 
 * @author Robin Collier
 */
public class JobOffersProvider implements PacketExtensionProvider
{
	private final String providerNode = "DocumentElement";
	public PacketExtension parseExtension(XmlPullParser parser) throws Exception
	{
		Log.wtf("AG-SMACK", "   JOB-OFFERS-PROVIDER");
		final StringBuilder builder = new StringBuilder();
		//JobOffersItem item = new JobOffersItem(UUID.randomUUID().toString(), providerNode);
        boolean done = false;
        while (!done) {
            int eventType = parser.next();
            if (eventType == XmlPullParser.START_TAG) {
                String name = parser.getName();
                // If an empty element, set the value with the empty string.
                if (parser.isEmptyElementTag()) {
                    ////setValue(name,"");
                }
                // Otherwise, get the the element text.
                else {
                    eventType = parser.next();
                    if (eventType == XmlPullParser.TEXT) {
                        String value = parser.getText();
                        ////extension.setValue(name, value);
                        Log.d(OiDriverApp.TAG, "JOBOFFERS: " +  name + "=" + value);
                    }
                }
            }
            else if (eventType == XmlPullParser.END_TAG) {
                if (parser.getName().equalsIgnoreCase(providerNode)) {
                    done = true;
                }
            }
        }

//		boolean done = false;
//		while(!done){
//			int event = parser.getEventType();
//            String elementName = parser.getName();
//            String namespace = parser.getNamespace();
//            Log.d("ALEX", "Node: " + elementName + ", NS: " + namespace);
//
//			switch (event) {  
//			  		  
//	        case XmlPullParser.END_DOCUMENT:  
//	        	return null;
//	        	
//	        case XmlPullParser.START_TAG:  
//	            builder.append("<"+parser.getName()+">");  
//	            break;  
//	  
//	        case XmlPullParser.END_TAG:  
//	            builder.append("</"+parser.getName()+">");  
//	            break;  
//	  
//	        case XmlPullParser.TEXT:  
//	            
//	            builder.append(parser.getText());  
//	            break;  
//	        }
//			event = parser.next();
//			String iqStr = builder.toString();
//			if(iqStr.contains("</query>"))
//			{					
//				//RappidService.client.onIQRecieve(builder.toString());
//				done = true;
//				//break;
//			}
//		}

//		while (!done)
//		{
//			if (tag == XmlPullParser.END_TAG)// && parser.getName().equals(name))
//				done = true;
//			else if (!((tag == XmlPullParser.START_TAG) && parser.isEmptyElementTag()))
//				payloadText.append(parser.getText());
//			
//			if (!done)
//				tag = parser.next();
//		}
		
//		String jid = parser.getAttributeValue(null, "jid");
//		String nodeId = parser.getAttributeValue(null, "node");
//		String subId = parser.getAttributeValue(null, "subid");
//		String state = parser.getAttributeValue(null, "subscription");
//		boolean isRequired = false;
//
//		parsePacketExtension
//		
//		if ((tag == XmlPullParser.START_TAG) && parser.getName().equals("subscribe-options"))
//		{
//			tag = parser.next();
//			
//			if ((tag == XmlPullParser.START_TAG) && parser.getName().equals("required"))
//				isRequired = true;
//			
//			while (parser.next() != XmlPullParser.END_TAG && parser.getName() != "subscribe-options");
//		}
//		while (parser.getEventType() != XmlPullParser.END_TAG) parser.next();
		return new Item();//SimplePayload("DocumentElement", "http://jabber.org/protocol/pubsub", builder.toString());//new Subscription(jid, nodeId, subId, (state == null ? null : Subscription.State.valueOf(state)), isRequired);
	}

}
