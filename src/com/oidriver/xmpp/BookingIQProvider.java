package com.oidriver.xmpp;

import android.content.Intent;
import android.util.Log;

import com.oidriver.Booking;
import com.oidriver.Constants;
import com.oidriver.OiDriverActivity;
import com.oidriver.OiDriverApp;
import com.oidriver.service.OiDriverService;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

public class BookingIQProvider implements IQProvider {

	public static int mMaxBookingsCount = -1;
	public static int mBookingsCount = 0;

	@Override
	public IQ parseIQ(XmlPullParser parser) {
		Log.wtf("AG-SMACK", "   BOOKING-IQPROVIDER");
		try
		{	
			int event = parser.getEventType();

			List<String> arrSentBidsIds = OiDriverService.getSentBidsIds();
			OiDriverService.bookings.clear();

			boolean done = false;
			Booking booking = null;
			boolean bookingStarted = false;
			int[] bidTypes = new int[] {0,0};
			int totalBookings = 0;
			int first = -1, last = -1, count = 0;
			while (!done) {
				if (event == XmlPullParser.START_TAG) {
					String name = parser.getName();
					if (name.equalsIgnoreCase("booking")) {
						bookingStarted = true;
						booking = new Booking();
						Log.wtf("BOOKING-IQ", "NEW BOOKING[" + totalBookings + "]");
					}
					else if(name.equalsIgnoreCase("first")) {
						first = Integer.parseInt(parser.nextText());
						Log.wtf("BOOKING-IQ", "FIRST = " + first);
					}
					else if(name.equalsIgnoreCase("last")) {
						last = Integer.parseInt(parser.nextText());
						Log.wtf("BOOKING-IQ", "LAST = " + last);
					}
					else if(name.equalsIgnoreCase("count")) {
						count = Integer.parseInt(parser.nextText());
						Log.wtf("BOOKING-IQ", "COUNT = " + count);
						mBookingsCount = count;
					}
					else {
						parseName(parser, name, booking, bookingStarted);
					}

				}
				else if (event == XmlPullParser.END_TAG) {
					if (parser.getName().equals("booking") && bookingStarted) {
						//TODO: when the sun rises from the west....
						if (booking != null)
						{
							long now = Calendar.getInstance().getTimeInMillis();
							long diff = booking.getDtBookingTimestamp().getTime() - now;
							totalBookings++;
							if (diff > Constants.BOOKING_ASAP_THRESHOLD)
							{
								booking.setBookingType(1);
							}
							else
							{
								booking.setBookingType(0);
							}
							if (arrSentBidsIds != null && arrSentBidsIds.contains(booking.getBookingId()))
							{
								booking.setBidSent(true);
								OiDriverService.putSentBidsId(booking.getBookingId());
							}
							Log.wtf("BOOKING-IQ", booking.toString());
							OiDriverService.bookings.add(booking);
							bidTypes[booking.getBookingType()]++;
						}

						bookingStarted = false;
					}
					else if (parser.getName().equals("query")) {
						done = true;
					}
				}
				event = parser.next();
			}

			String ns = Constants.NAMESPACE_BOOKING_SEARCH;
			OiDriverActivity.OnBookingSearchListener listener =
					(OiDriverActivity.OnBookingSearchListener) OiDriverService.mStanzaListeners.get(ns);
			if (listener != null)
				listener.onSearchCompleted();
			else {
				Intent intent = new Intent("com.oidriver.SEARCH_BOOKINGS");
				intent.putExtra("total", totalBookings);
				if (totalBookings > 0) {
					intent.putExtra("now_bids", bidTypes[0]);
					intent.putExtra("adv_bids", bidTypes[1]);
				}
				OiDriverApp.getAppContext().sendBroadcast(intent);
			}
			OiDriverService.getXmppClient().blockingOperationProgress = false;
			OiDriverService.sendIQToUnsubscribeZone(OiDriverService.currentZoneId);
			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public BookingIQProvider() {

	}

	private void parseName(XmlPullParser parser, String name, Booking booking, boolean bookingStarted)
			throws IOException, XmlPullParserException {
		if(name.equalsIgnoreCase("bookingId") && bookingStarted) {
			booking.setBookingId(parser.nextText());
		}
		else if(name.equalsIgnoreCase("passengerName") && bookingStarted) {
			booking.setPassengerName(parser.nextText());
		}
		else if(name.equalsIgnoreCase("zoneId") && bookingStarted) {
			booking.setZoneId(parser.nextText());
		}
		else if(name.equalsIgnoreCase("jobId") && bookingStarted) {
			booking.setJobId(parser.nextText());
		}
		else if(name.equalsIgnoreCase("bookingTime") && bookingStarted) {
			booking.setBookingTime(parser.nextText());
		}
		else if(name.equalsIgnoreCase("status") && bookingStarted) {
			booking.setStatus(parser.nextText());
		}
		else if(name.equalsIgnoreCase("pickupAddress") && bookingStarted) {
			booking.setPickupAddress(parser.nextText());
		}
		else if(name.equalsIgnoreCase("dropoffAddress") && bookingStarted) {
			booking.setDropoffAddress(parser.nextText());
		}
		else if(name.equalsIgnoreCase("pickupLat") && bookingStarted) {
			booking.setPickupLat(parser.nextText());
		}
		else if(name.equalsIgnoreCase("pickupLon") && bookingStarted) {
			booking.setPickupLon(parser.nextText());
		}
		else if(name.equalsIgnoreCase("dropoffLat") && bookingStarted) {
			booking.setDropoffLat(parser.nextText());
		}
		else if(name.equalsIgnoreCase("dropoffLon") && bookingStarted) {
			booking.setDropoffLon(parser.nextText());
		}
		else if(name.equalsIgnoreCase("notes") && bookingStarted) {
			booking.setNotes(parser.nextText());
		}
		else if(name.equalsIgnoreCase("vehicleType") && bookingStarted) {
			booking.setVehicleType(parser.nextText());
		}
		else if(name.equalsIgnoreCase("ratings") && bookingStarted) {
			booking.setRatings(parser.nextText());
		}
		else if(name.equalsIgnoreCase("fare") && bookingStarted) {
			booking.setFare(parser.nextText());
		}
		else if(name.equalsIgnoreCase("extra1") && bookingStarted) {
			booking.setSentBy(parser.nextText());
		}
		else {
			Log.wtf("BOOKING-IQ", "UNPARSED: [" + name + "] = " + parser.getText());
		}
	}
}
