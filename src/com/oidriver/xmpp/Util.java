package com.oidriver.xmpp;

import android.content.Context;
import android.location.Location;
import android.os.Environment;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.util.TypedValue;
import android.widget.Toast;

import com.oidriver.AcceptedJob;
import com.oidriver.Booking;
import com.oidriver.Driver;
import com.oidriver.OiDriverApp;
import com.oidriver.Zone;
import com.oidriver.runnable.RunOn;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class Util {

	public static Driver getDriver(Element ele) {
		String id = getTextValue(ele, "id");
		String name = getTextValue(ele, "name");	
		String registration = getTextValue(ele, "registration");
		String imei = getTextValue(ele, "imei");
		String jid = getTextValue(ele, "jid");	
		String carType = getTextValue(ele, "carType");
		String carColour = getTextValue(ele, "carColour");
		String carModel = getTextValue(ele, "carModel");
		String paymentRef = getTextValue(ele, "paymentRef");
		String lat = getTextValue(ele, "lat");
		String lon = getTextValue(ele, "lon");
		String status = getTextValue(ele, "status");
		String userName = getTextValue(ele, "userName");
		
		Driver driver = new Driver();
		driver.setCarColor(carColour);
		driver.setCarModel(carModel);
		driver.setCarType(carType);
		driver.setId(id);
		driver.setImei(imei);
		driver.setJid(jid);
		driver.setLat(lat);
		driver.setLon(lon);
		driver.setName(name);
		driver.setPaymentRef(paymentRef);
		driver.setRegistration(registration);
		driver.setStatus(status);
		driver.setUserName(userName);
		return driver;

	}

	public static Booking parseBookingV2(Element ele)
	{		
		String id = getTextValue(ele, "bookingId");
		String passengerName = getTextValue(ele, "passengerName");
		String zoneId = getTextValue(ele, "zoneId");
		String jobId = getTextValue(ele, "jobId");
		String bookingTime = getTextValue(ele, "bookingTime");
		String status = getTextValue(ele, "status");
		String pickupAddress = getTextValue(ele, "pickupAddress");
		String dropoffAddress = getTextValue(ele, "dropoffAddress");
		String pickupLat = getTextValue(ele, "pickupLat");
		String pickupLon = getTextValue(ele, "pickupLon");
		String dropoffLat = getTextValue(ele, "dropoffLat");
		String dropoffLon = getTextValue(ele, "dropoffLon");
		String notes = getTextValue(ele, "notes");
		String vehicleType = getTextValue(ele, "vehicleType");
		String ratings = getTextValue(ele, "ratings");
		String fare = getTextValue(ele, "fare");
		String extra1 = getTextValue(ele, "extra1");
		
		Booking booking = new Booking();
		booking.setBookingId(id);
		booking.setBookingTime(bookingTime);
		booking.setDropoffAddress(dropoffAddress);
		booking.setDropoffLat(dropoffLat);
		booking.setDropoffLon(dropoffLon);
		booking.setFare(fare);
		booking.setRatings(ratings);
		booking.setVehicleType(vehicleType);
		booking.setNotes(notes);
		booking.setPickupAddress(pickupAddress);
		booking.setPickupLat(pickupLat);
		booking.setPickupLon(pickupLon);
		booking.setStatus(status);
		booking.setPassengerName(passengerName);
		booking.setZoneId(zoneId);
		booking.setJobId(jobId);
		booking.setSentBy(extra1);
		
		return booking;
	}
	
	public static String getAction(Element el)
	{
		String action = getTextValue(el, "action");
		return action;
	}

	public static String getBookingId(Element el)
	{
		String id = getTextValue(el, "bookingId");
		return id;
	}
	
	public static String getJobId(Element el)
	{
		String id = getTextValue(el, "jobId");
		return id;
	}

	public static AcceptedJob parseJob(Element ele) {
		
		String bookingId = getTextValue(ele, "bookingId");
		String name = getTextValue(ele, "name");
		String pickup = getTextValue(ele, "pickup");
		String dropoff = getTextValue(ele, "dropoff");
		String phone = getTextValue(ele, "telephone");
		String timeofbooking = getTextValue(ele, "actualbooking");
		String jobid = getTextValue(ele, "jobid");
		String notes = getTextValue(ele, "notes");
		String customerpickuplat = getTextValue(ele, "customerpickuplat");
		String customerpickuplon = getTextValue(ele, "customerpickuplon");
		String customerdropofflat = getTextValue(ele, "customerdropofflat");
		String customerdropofflon = getTextValue(ele, "customerdropofflon");
		String passangers = getTextValue(ele, "passengers");
		String jobref = getTextValue(ele, "jobref");
		String postcode1 = getTextValue(ele, "postcode1");
		String postcode2 = getTextValue(ele, "postcode2");
		String housenumber1 = getTextValue(ele, "housenumber1");
		String housenumber2 = getTextValue(ele, "housenumber2");
		String pickupzoneid = getTextValue(ele, "pickupzoneid");
		String dropoffzoneid = getTextValue(ele, "dropoffzoneid");
		String fareprice = getTextValue(ele, "fareprice");
		String custtype = getTextValue(ele, "custtype");

		AcceptedJob currentJob = new AcceptedJob();
		
		currentJob.setBookingId(bookingId);
		currentJob.setBookingTime(timeofbooking);
		currentJob.setCustomerDropoffLat(customerdropofflat);
		currentJob.setCustomerDropoffLon(customerdropofflon);
		currentJob.setCustomerPickupLat(customerpickuplat);
		currentJob.setCustomerPickupLon(customerpickuplon);
		currentJob.setCustType(custtype);
		currentJob.setDropoff(dropoff);
		currentJob.setDropoffZoneId(dropoffzoneid);
		currentJob.setFarePrice(fareprice);
		currentJob.setHouseNumber1(housenumber1);
		currentJob.setHouseNumber2(housenumber2);
		currentJob.setJobId(jobid);
		currentJob.setJobRef(jobref);
		currentJob.setName(name);
		currentJob.setNop(passangers);
		currentJob.setNote(notes);
		currentJob.setPickup(pickup);
		currentJob.setPickupZoneId(pickupzoneid);
		currentJob.setPostCode1(postcode1);
		currentJob.setPostCode2(postcode2);
		currentJob.setTelephone(phone);
		
		return currentJob;

	}

	/**
	 * I take a xml element and the tag name, look for the tag and get the text
	 * content i.e for <employee><name>John</name></employee> xml snippet if the
	 * Element points to employee node and tagName is 'name' I will return John
	 */
	public static String getTextValue(Element ele, String tagName) {
		String textVal = "";

		NodeList nl = ele.getElementsByTagName(tagName);
		if (nl != null && nl.getLength() > 0) {
			Element el = (Element) nl.item(0);
			if (null != el) {
				Node node = el.getFirstChild();
				if (null != node) {
					textVal = node.getNodeValue();
				}
			}
		}

		return textVal;
	}

	public static boolean isValidEmail(String email) {
		String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
				+ "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
				+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
				+ "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
				+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
				+ "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

		Pattern patternObj = Pattern.compile(regExpn);

		Matcher matcherObj = patternObj.matcher(email);
		if (matcherObj.matches()) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isPhoneValid(String phone)
	{
		return PhoneNumberUtils.isGlobalPhoneNumber(phone);
	}
	
	public static boolean checkIfNullOrBlank(String value)
	{
		if(value == null || "".equals(value))
		{
			return true;
		}
		return false;
	}

	public static String getEventId(Element el) {
		String id = getTextValue(el, "eventId");
		return id;
	}

//	public static Offer getOffer(Element ele) {
//		String driverId = getTextValue(ele, "driverid");
//		String message = getTextValue(ele, "message");	
//		String distance = getTextValue(ele, "distance");
//		String lat = getTextValue(ele, "lat");
//		String lon = getTextValue(ele, "lon");
//		
//		Offer offer = new Offer();
//		offer.setDistance(distance);
//		offer.setDriverId(driverId);
//		offer.setLat(lat);
//		offer.setLon(lon);
//		offer.setMessage(message);
//		
//		return offer;
//	}
//
//	public static Confirmation getConfirmation(Element ele) {
//		String message = getTextValue(ele, "message");
//		String jobId = getTextValue(ele, "jobid");	
//		String dispatchId = getTextValue(ele, "dispatchid");
//		
//		Confirmation con = new Confirmation();
//		
//		con.setDispatchId(dispatchId);
//		con.setJobId(jobId);
//		con.setMessage(message);
//		
//		return con;
//	}
//
//	public static AcceptJob getAcceptJob(Element ele) {
//		String message = getTextValue(ele, "comments");
//		String jobId = getTextValue(ele, "jobid");	
//		String driverToTrack = getTextValue(ele, "drivertotrack");
//		
//		AcceptJob acceptJob = new AcceptJob();
//		acceptJob.setDriverId(driverToTrack);
//		acceptJob.setJobId(jobId);
//		acceptJob.setMessage(message);
//		
//		return acceptJob;
//		
//		
//	}
//
//	public static EndOfJob getEndOfJob(Element ele) {
//		String message = getTextValue(ele, "comments");
//		String jobId = getTextValue(ele, "jobid");	
//		String driverToTrack = getTextValue(ele, "drivertotrack");
//		String farePrice = getTextValue(ele, "fareprice");
//		
//		EndOfJob endOfJob = new EndOfJob();
//		endOfJob.setDriverToTrack(driverToTrack);
//		endOfJob.setJobId(jobId);
//		endOfJob.setComment(message);
//		endOfJob.setFarePrice(farePrice);
//		
//		return endOfJob;
//		
//		
//	}

	public static Zone getZone(Element ele) {
		 String zoneName = getTextValue(ele, "name");
		 String zoneId = getTextValue(ele, "id");
		 String drivers = getTextValue(ele, "drivers");
		 String jobs = getTextValue(ele, "jobs");
		 
		 Zone zone = new Zone();
		 zone.setDrivers(drivers);
		 zone.setId(zoneId);
		 zone.setJobs(jobs);
		 zone.setZoneName(zoneName);
		 
		 return zone;
	}
	
	public static Date parseDateStr(String dateStr)
	{
		Log.d("OI-DATE", "Parsing " + dateStr);
		Date dt = null;
		try {
			SimpleDateFormat sdf24h = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss", Locale.UK);
			if (!dateStr.toUpperCase(Locale.UK).contains("UTC"))
			{
				try {
					dt = sdf24h.parse(dateStr);
					return dt;
				} catch (ParseException e) {
					Log.d("OI-DATE", "Parsing MMM dd, yyyy HH:mm:ss failed");
				}
			}
			
			SimpleDateFormat sdfUK = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
			try {
				dt = sdfUK.parse(dateStr);
				return dt;
			} catch (ParseException e) {
				Log.d("OI-DATE", "Parsing yyyy-MM-dd HH:mm:ss failed");
			}

			SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss 'UTC' yyyy", Locale.UK);
			try {
				dt = sdf.parse(dateStr);
			} catch (ParseException e) {
				Log.d("OI-DATE", "Parsing EEE MMM dd HH:mm:ss 'UTC' yyyy failed");
			}
		} finally {
			Log.d("OI-DATE", "Parsed into " + (dt != null ? dt.toGMTString() : "null"));
		}
		
		return dt;
	}
	
	public static String getErrorText(String xml)
	{
		String error = "";
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db;
				db = dbf.newDocumentBuilder();
			InputStream is = new ByteArrayInputStream(xml.getBytes());
	
			Document doc;
		
			doc = db.parse(is);
			doc.getDocumentElement().normalize();

			NodeList errorNodeList = doc.getElementsByTagName("error");
			if (errorNodeList != null && errorNodeList.getLength() > 0) {
				int totalErrors = errorNodeList.getLength();
				for (int i = 0; i < totalErrors; i++) {
					// get the employee element
					Element el = (Element) errorNodeList.item(i);
					error += Util.getTextValue(el, "text") + "\n";
				}
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return error;
	}
	
	public static String getBookingIdFromGetError(String xml)
	{
		String error = "";
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db;
				db = dbf.newDocumentBuilder();
			InputStream is = new ByteArrayInputStream(xml.getBytes());
	
			Document doc;
		
			doc = db.parse(is);
			doc.getDocumentElement().normalize();

			NodeList errorNodeList = doc.getElementsByTagName("booking");
			if (errorNodeList != null && errorNodeList.getLength() > 0) {
				int totalErrors = errorNodeList.getLength();
				for (int i = 0; i < totalErrors; i++) {
					// get the employee element
					Element el = (Element) errorNodeList.item(i);
					String jobId = el.getNodeValue();
					return jobId;
				}
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return error;
	}
	
	public static String getInfo(String xml)
	{
		String error = "";
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputStream is = new ByteArrayInputStream(xml.getBytes());
	
			Document doc;
		
			doc = db.parse(is);
			doc.getDocumentElement().normalize();

			NodeList errorNodeList = doc.getElementsByTagName("booking");
			if (errorNodeList != null && errorNodeList.getLength() > 0) {
				int totalErrors = errorNodeList.getLength();
				for (int i = 0; i < totalErrors; i++) {
					// get the employee element
					Element el = (Element) errorNodeList.item(i);
					String jobInfo = getTextValue(el, "info");
					return jobInfo;
				}
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return error;
	}
	
	public static void dumpCallStackToFile(String message)
	{
		Map<Thread, StackTraceElement[]> stacks = Thread.getAllStackTraces();
		Thread currentThread = Thread.currentThread();
		StackTraceElement[] traces = stacks.get(currentThread);
		String path = Environment.getExternalStorageDirectory() + "/callstack.txt";
		File f = new File(path);
		if (!f.exists())
			try {
				f.createNewFile();
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		try {
			if (f.exists())
			{
				FileWriter fw = new FileWriter(f,true);
				fw.append("===> " + Calendar.getInstance().getTime().toGMTString() + ": " + message + "\r\n");
				for (StackTraceElement trace: traces)
				{
					String str = trace.toString();
					fw.append(str + "\r\n");
				}
				fw.append("<===\r\n");
				fw.close();
			}
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	public static synchronized void dumpCallStack(String message)
	{
		Map<Thread, StackTraceElement[]> stacks = Thread.getAllStackTraces();
		Thread currentThread = Thread.currentThread();
		StackTraceElement[] traces = stacks.get(currentThread);
		try {
			Log.w("TRACE", "============> " + message + " <============");
			for (StackTraceElement trace: traces)
			{
				String str = trace.toString();
				Log.w("TRACE", str);
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}

	public static Double getDistance(Double srcLat, Double srcLon, Double dstLat, Double dstLon)
	{
		float[] results = new float[1];
		try {
			Location.distanceBetween(srcLat, srcLon, dstLat, dstLon, results);
		} catch (NumberFormatException e) {
			results[0] = 0.0f;
			e.printStackTrace();
		}

		Double distInMiles = results[0] * 0.00062137;
		return distInMiles;
	}
	
	public static int toPixels(final Context context, final float valueDP) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueDP, context.getResources().getDisplayMetrics());
	}

	public static String parseLogonMessage(String xml) {
		Pattern p = Pattern.compile("<body>logon,(.*)</body>");
		Matcher m = p.matcher(xml);
		String text = null;
		if (m.find())
			text = m.group(1);
		return text;
	}

    public static Class getClazz(Object o) {
        return ((Object)o).getClass();
    }

    public static void showNotification(final String text) {
        try {
            RunOn.mainThread(new Runnable() {

                @Override
                public void run() {
                    try {
                        Toast.makeText(OiDriverApp.getAppContext(), text, Toast.LENGTH_LONG).show();
                    } catch (Exception ignored) {

                    }
                }
            });
        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
