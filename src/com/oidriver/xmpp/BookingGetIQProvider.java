package com.oidriver.xmpp;

import android.text.TextUtils;
import android.util.Log;

import com.oidriver.Booking;
import com.oidriver.Constants;
import com.oidriver.service.OiDriverService;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParser;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class BookingGetIQProvider implements IQProvider {
	@Override
	public IQ parseIQ(XmlPullParser parser) {
		Log.wtf("AG-SMACK", "   BOOKINGGETIQPROVIDER");
		try
		{	
			int event = parser.getEventType();
			final StringBuilder builder = new StringBuilder();
			String tagName = "";
			boolean done = false;
			builder.append("<iq>");
			while(true){
				switch (event) {  
				  
		        case XmlPullParser.START_DOCUMENT:  
		            
		            break;  
		  
		        case XmlPullParser.END_DOCUMENT:  
		        	 
		        	
		            break;  
		        case XmlPullParser.START_TAG:  
		        	tagName = parser.getName();
		        	if (tagName.equalsIgnoreCase("error"))
		        	{
		        		String code = parser.getAttributeValue(0);
		        		builder.append("<" + tagName +" code = '" + code + "'>"); 
		        	}
		        	else
		        		builder.append("<"+tagName+">");  
		            
		            break;  
		  
		        case XmlPullParser.END_TAG: 
	        		builder.append("</"+parser.getName()+">");  
	        		if (parser.getName().equalsIgnoreCase("iq"))
		        		done = true;		  
		            break;  
		  
		        case XmlPullParser.TEXT:  
		            
		            builder.append(parser.getText());  
		              
		            break;  
		        }
				if (!done)
					event = parser.next();
				String iqStr = builder.toString();
				Log.e("GET", iqStr);
				if(done)//iqStr.contains("</iq>"))
				{
					String error = Util.getErrorText(iqStr);
					if (!TextUtils.isEmpty(error))
					{
						String bookingId = "";
						//Util.getBookingIdFromGetError(iqStr);
						try {
							DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
							DocumentBuilder db;
								db = dbf.newDocumentBuilder();
							InputStream is = new ByteArrayInputStream(iqStr.getBytes());
					
							Document doc;
						
							doc = db.parse(is);
							doc.getDocumentElement().normalize();

							NodeList errorNodeList = doc.getElementsByTagName("bookings");
							if (errorNodeList != null && errorNodeList.getLength() > 0) {
								int totalErrors = errorNodeList.getLength();
								for (int i = 0; i < totalErrors; i++) {
									// get the employee element
									Element el = (Element) errorNodeList.item(i);
									bookingId = Util.getTextValue(el, "bookingId");
								}
							}
						} catch (ParserConfigurationException e) {
							e.printStackTrace();
						} catch (SAXException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
						if (!TextUtils.isEmpty(bookingId))
						{
//							if (OiDriverService.dbHelper.deleteBidID(bookingId))
//							{
//								Log.i(OiDriverApp.TAG, "BOOKING ID " + bookingId + " DOES NOT EXISTS ON SERVER AND DELETED LOCALLY");
//							}
						}
					}
					else
					{
						//OiDriverService.client.onBookingIQRecieved(builder.toString());
						try {
							DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
							DocumentBuilder db = dbf.newDocumentBuilder();
							InputStream is = new ByteArrayInputStream(iqStr.getBytes());
					
							Document doc;
						
							doc = db.parse(is);
							doc.getDocumentElement().normalize();

							NodeList errorNodeList = doc.getElementsByTagName("booking");
							if (errorNodeList != null && errorNodeList.getLength() > 0) {
								int totalErrors = errorNodeList.getLength();
								for (int i = 0; i < totalErrors; i++) {
									// get the employee element
									Element el = (Element) errorNodeList.item(i);
									String jobInfo = Util.getTextValue(el, "info");
									String[] data = jobInfo.split(";");
									String name, value;
									int pos;
									Log.i("DBG", "" + data.length);
									Booking booking = new Booking();
									for (String line: data)
									{
										//String[] pair = line.split(":");
										//String name = pair[0];
										//String value = pair[1];
										pos = line.indexOf(":");
										if (pos < 0)
											continue;
										name = line.substring(0, pos).trim();
										value = line.substring(pos + 1).trim();
										if (name.equals("id"))
											booking.setBookingId(value);
										else if (name.equals("pi"))
											booking.setBookingTime(value);
										else if (name.equals("zi"))
											booking.setZoneId(value);
										else if (name.equals("pa"))
											booking.setPickupAddress(value);
										else if (name.equals("pla"))
											booking.setPickupLat(value);
										else if (name.equals("plo"))
											booking.setPickupLon(value);
										else if (name.equals("da"))
											booking.setDropoffAddress(value);
										else if (name.equals("dla"))
											booking.setDropoffLat(value);
										else if (name.equals("dlo"))
											booking.setDropoffLon(value);
										else if (name.equals("no"))
											booking.setNotes(value);
										
									}
									if (!TextUtils.isEmpty(booking.getBookingId()))
									{
										long now = Calendar.getInstance().getTimeInMillis();
										long diff = booking.getDtBookingTimestamp().getTime() - now;
										if (diff > Constants.BOOKING_ASAP_THRESHOLD)
										{
											booking.setBookingType(1);
											//OiDriverService.newBidsAdv++;
										}
										else
										{
											booking.setBookingType(0);
											//OiDriverService.newBidsNow++;
										}
										OiDriverService.bookings.add(booking);
//										OiDriverService.dbHelper.deleteBidID(booking.getBookingId());
									}
								}
							}
						} catch (ParserConfigurationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SAXException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					synchronized(OiDriverService.getClientLock())
					{
						OiDriverService.getClientLock().notifyAll();
					}
					break;
				}
			}
			
			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public BookingGetIQProvider() {

	}

}
