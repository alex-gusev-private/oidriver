package com.oidriver.fragments;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.oidriver.AcceptedJob;
import com.oidriver.Booking;
import com.oidriver.Constants;
import com.oidriver.FutureJobsAdapter;
import com.oidriver.JobOffersAdapter;
import com.oidriver.JobsAdapter;
import com.oidriver.OiDriverActivity;
import com.oidriver.OiDriverApp;
import com.oidriver.R;
import com.oidriver.SummaryAdapter;
import com.oidriver.runnable.RunOn;
import com.oidriver.service.OiDriverService;
import com.oidriver.xmpp.BookingIQProvider;
import com.oidriver.xmpp.Util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

public class OiBookingsFragment extends Fragment implements OnClickListener {

	private Button mBtnBids;
	private Button mBtnBack;
	private Button mBtnMyBookings;
	private ListView mInfoList = null;
	private JobsAdapter mJobsAdapter;
	private FutureJobsAdapter mFutureJobsAdapter;
	private JobOffersAdapter mJobOffersAdapter;

	private boolean refreshRequiredNow = false;
	private boolean refreshRequiredAdv = false;
	private boolean refreshRequiredJob = false;

	public static OiBookingsFragment newInstance() {
		return new OiBookingsFragment();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.jobs_info_panel, null);

		mInfoList = (ListView) view.findViewById(R.id.lvInfo);
		mJobOffersAdapter = new JobOffersAdapter(OiDriverApp.getAppContext(), R.id.textViewDummy, new ArrayList<AcceptedJob>(), (OiDriverActivity)getActivity());

		mBtnBids = (Button) view.findViewById(R.id.button_bids);
		mBtnBack = (Button) view.findViewById(R.id.button_back);
		mBtnMyBookings = (Button) view.findViewById(R.id.button_my_bookings);
		
		mBtnBids.setOnClickListener(this);
		mBtnBack.setOnClickListener(this);
		mBtnMyBookings.setOnClickListener(this);

		return view;
	}
	
	public void onClick(View v) {

		if (v == mBtnBids)
		{
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					if (getActivity() != null) getActivity().showDialog(0);
				}
			}, 200);
			OiDriverService.sendIQToGetBookings(Constants.MAX_BOOKINGS_COUNT, new OiDriverActivity.OnBookingSearchListener() {
				@Override
				public void onSearchCompleted() {
					OiDriverService.getXmppClient().blockingOperationProgress = false;
					RunOn.mainThread(new Runnable() {
						@Override
						public void run() {
							selectPage(Constants.BIDS_PAGE);
							displayBids();
							ArrayList<String> ids = new ArrayList<String>();
							for (Booking b: OiDriverService.bookings)
								ids.add(b.getBookingId());
							OiDriverService.getXmppClient().sendIQToSubscribeToBookings(ids);
						}
					});
				}
			});
			mBtnBids.setTextColor(Color.BLACK);
		}
		else if (v == mBtnBack)
		{
			// Reset all to initial state
			selectPage(Constants.BIDS_PAGE);
			// Hide fragment
			((OiDriverActivity)getActivity()).handlePassengerClick();
		}
		else if (v == mBtnMyBookings)
		{
			selectPage(Constants.JOBS_PAGE);
			ColorStateList csl = mBtnMyBookings.getTextColors();
			new JobOffersCursorTask().execute();
			mInfoList.invalidateViews();
			refreshRequiredJob = false;
			mBtnMyBookings.setTextColor(Color.BLACK);
		}
	}

	void selectPage(int pageNo)
	{
		View v = getView();
		TextView tv = (TextView)v.findViewById(R.id.textViewPageTitle);
		switch (pageNo)
		{
		case Constants.BIDS_PAGE:
			tv.setText(getResources().getString(R.string.bids));
			if (mJobsAdapter == null)
				mJobsAdapter = new JobsAdapter(OiDriverApp.getAppContext(), R.id.textViewDummy, 
						new ArrayList<Booking>(), (OiDriverActivity)getActivity());
			mInfoList.setAdapter(mJobsAdapter);
			break;
		case Constants.JOBS_PAGE:
			tv.setText(getResources().getString(R.string.my_bookings));
			mInfoList.setAdapter(mJobOffersAdapter);
			break;
		}
	}

	public void setRefreshRequiredBids(boolean refreshRequiredNow) {
		this.refreshRequiredNow = refreshRequiredNow;
	}

	public void setRefreshRequiredJob(boolean refreshRequiredJob) {
		this.refreshRequiredJob = refreshRequiredJob;
	}

	public void setBidsCount() {
		mBtnBids.setText("BIDS (" + BookingIQProvider.mBookingsCount + ")");
	}

	public void displayBids() {
		selectPage(Constants.BIDS_PAGE);
		if (mJobsAdapter == null)
			mJobsAdapter = new JobsAdapter(OiDriverApp.getAppContext(), R.id.textViewDummy,
					new ArrayList<Booking>(), (OiDriverActivity)getActivity());
		else
			mJobsAdapter.clear();
		if (OiDriverService.bookings != null)
		{
			final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
			int pickupRange = sp.getInt("pickupRange", 0);
			int dropoffRange = sp.getInt("dropoffRange", 0);
			double[] ranges = new double[] { 1.99999999, 5.99999999, 6.0 };

			Collections.sort(OiDriverService.bookings, new BookingDistanceComparator());
			Location location = ((OiDriverActivity)getActivity()).getLastFix();
			String latStr, lonStr;
			for (Booking booking: OiDriverService.bookings)
			{
				if (location != null)
				{
					latStr = booking.getPickupLat();
					lonStr = booking.getPickupLon();
					if (TextUtils.isEmpty(latStr))
						latStr = "0.0";
					if (TextUtils.isEmpty(lonStr))
						lonStr = "0.0";
					Double dist1 = Util.getDistance(Double.valueOf(latStr), Double.valueOf(lonStr), location.getLatitude(), location.getLongitude());
					latStr = booking.getDropoffLat();
					lonStr = booking.getDropoffLon();
					if (TextUtils.isEmpty(latStr))
						latStr = "0.0";
					if (TextUtils.isEmpty(lonStr))
						lonStr = "0.0";
					Double dist2 = Util.getDistance(Double.valueOf(latStr), Double.valueOf(lonStr), location.getLatitude(), location.getLongitude());
					if (pickupRange < 2)
					{
						if (dist1 <= ranges[pickupRange] || dist2 <= ranges[dropoffRange] )
							mJobsAdapter.add(booking);
					}
					else
						mJobsAdapter.add(booking);
				}
				else
					mJobsAdapter.add(booking);
			}
			mJobsAdapter.notifyDataSetChanged();
		}
		((OiDriverActivity)getActivity()).dismissAny(0);
	}

	class JobsCursorTask extends AsyncTask<Integer, Void, ArrayList<Booking>>
	{
		Integer cursorType = 0;
		
		@Override
		protected void onPreExecute()
		{
			if (getActivity() != null) getActivity().showDialog(0);
		}
		
		@Override
		protected ArrayList<Booking> doInBackground(Integer... params) {

			return OiDriverService.bookings;
		}
		
		@Override
		protected void onPostExecute(ArrayList<Booking> arr)
		{
			if (mJobsAdapter == null)
				mJobsAdapter = new JobsAdapter(OiDriverApp.getAppContext(), R.id.textViewDummy,
						new ArrayList<Booking>(), (OiDriverActivity)getActivity());
			else
				mJobsAdapter.clear();
			if (arr != null)
			{
				final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
				int pickupRange = sp.getInt("pickupRange", 0);
				int dropoffRange = sp.getInt("dropoffRange", 0);
				double[] ranges = new double[] { 1.99999999, 5.99999999, 6.0 };

				Collections.sort(arr, new BookingDistanceComparator());
				Location location = ((OiDriverActivity)getActivity()).getLastFix();
				String latStr, lonStr;
				for (Booking booking: arr)
				{
					if (location != null)
					{
						latStr = booking.getPickupLat();
						lonStr = booking.getPickupLon();
						if (TextUtils.isEmpty(latStr))
							latStr = "0.0";
						if (TextUtils.isEmpty(lonStr))
							lonStr = "0.0";
						Double dist1 = Util.getDistance(Double.valueOf(latStr), Double.valueOf(lonStr), location.getLatitude(), location.getLongitude());
						latStr = booking.getDropoffLat();
						lonStr = booking.getDropoffLon();
						if (TextUtils.isEmpty(latStr))
							latStr = "0.0";
						if (TextUtils.isEmpty(lonStr))
							lonStr = "0.0";
						Double dist2 = Util.getDistance(Double.valueOf(latStr), Double.valueOf(lonStr), location.getLatitude(), location.getLongitude());
						if (pickupRange < 2)
						{
							if (dist1 <= ranges[pickupRange] || dist2 <= ranges[dropoffRange] )
								mJobsAdapter.add(booking);
						}
						else
							mJobsAdapter.add(booking);
					}
					else
						mJobsAdapter.add(booking);
				}
				mJobsAdapter.notifyDataSetChanged();
			}
			((OiDriverActivity)getActivity()).dismissAny(0);
		}
	}
	
	public class BookingDistanceComparator implements Comparator<Booking>
	{
	    public int compare(Booking left, Booking right) {
	    	Location location = ((OiDriverActivity)getActivity()).getLastFix();
	    	if (location == null)
				return 0;
	    	try {
				Double d1 = Util.getDistance(Double.valueOf(left.getPickupLat()), Double.valueOf(left.getPickupLon()), location.getLatitude(), location.getLongitude());
				Double d2 = Util.getDistance(Double.valueOf(right.getPickupLat()), Double.valueOf(right.getPickupLon()), location.getLatitude(), location.getLongitude());
				return (int) (d1 - d2);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
	    	return 0;
	    }
	}

	public static class BookingTimeComparator implements Comparator<Booking>
	{
	    public int compare(Booking left, Booking right) {
	    	Long now = Calendar.getInstance().getTimeInMillis();
	    	Long toLeft = left.getDtBookingTimestamp().getTime() - now;
	    	Long toRight = right.getDtBookingTimestamp().getTime() - now;
	    	Long diff = (toLeft - toRight);
	    	return diff.intValue();
	    }
	}

	class JobOffersCursorTask extends AsyncTask<Void, Void, ArrayList<AcceptedJob>>
	{

		@Override
		protected void onPreExecute()
		{
			if (getActivity() != null) getActivity().showDialog(0);
		}

		@Override
		protected ArrayList<AcceptedJob> doInBackground(Void... params) {

			return OiDriverService.dbHelper.getAllActiveJobs();
		}
		
		@Override
		protected void onPostExecute(ArrayList<AcceptedJob> result)
		{
			if (mInfoList != null)
			{
				if (result != null)
					mInfoList.setAdapter(new JobOffersAdapter(OiDriverApp.getAppContext(), R.id.textViewDummy, result, ((OiDriverActivity)getActivity())));
				else
					mInfoList.setAdapter(null);
			}
			((OiDriverActivity)getActivity()).dismissAny(0);
		}
	}
	
	public void startLoadingBids(int type)
	{
		if (type == 0)
			new JobsCursorTask().execute(0);
		else
			new JobsCursorTask().execute(1);
	}

	public void startLoadingJobs()
	{
		new JobOffersCursorTask().execute();
	}
	
	public void highlightButton(int pageNo, int colour)
	{
		switch (pageNo)
		{
		case Constants.BIDS_PAGE:
			mBtnBids.setTextColor(colour);
			break;
		case Constants.JOBS_PAGE:
			mBtnMyBookings.setTextColor(colour);
			break;
		}
	}
	
	public void populateTotals()
	{
		ContentValues cv = new ContentValues();
		cv.put("BIDS", OiDriverService.bookings.size());
		int jobsValue = OiDriverService.dbHelper.getActiveJobsCount();
		cv.put("BOOKINGS", jobsValue);
		mInfoList.setAdapter(new SummaryAdapter(getActivity(), cv));
	}
	
	public int getAvailableCount()
	{
		int bidsNowValue = OiDriverService.bookings.size();
		int jobsValue = mJobOffersAdapter != null ? mJobOffersAdapter.getCount() : 0;
		return bidsNowValue + jobsValue;
	}
}