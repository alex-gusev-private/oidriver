package com.oidriver.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.oidriver.AcceptedJob;
import com.oidriver.Constants;
import com.oidriver.OiDriverActivity;
import com.oidriver.OiDriverApp;
import com.oidriver.R;
import com.oidriver.runnable.RunOn;
import com.oidriver.service.OiDriverService;
import com.oidriver.xmpp.Util;

import java.util.Timer;
import java.util.TimerTask;

public class OiMapFragment extends Fragment implements SensorEventListener {

	private final static String TAG = OiDriverApp.TAG + "-MAPV2";
	public static final float DISTANCE_RTW = 25.0f;
	public static final float DISTANCE_BRK = 50.0f;
	private GoogleMap.OnMapClickListener mMapClickedListener;
	private GoogleMap mMap;
	private Marker mPassengerMarker;
	private BitmapDescriptor mPassengerMarkerIcon;
    private long lastGpsFix = 0;
    private float fMovedDistance = 0.0f;

    Timer mGpsStatusTimer = new Timer();
	String mGpsStatus = "V";
	long mNmeaTimestamp = 0;

	private SensorManager mSensorManager;
    private Sensor mAccelerometer;

    public interface OiDriverLocationListener {
        void onLocationChanged(Location location);
    }

    OiDriverLocationListener mListener;

	public static OiMapFragment newInstance() {
		return new OiMapFragment();
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onDestroy()
	{
		if (mGpsStatusTimer != null)
			mGpsStatusTimer.cancel();
		super.onDestroyView();
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	public void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.page_map, null);
		mPassengerMarkerIcon = BitmapDescriptorFactory.fromResource(R.drawable.passenger_inverted);
		final ImageButton myLocationButton = (ImageButton) view.findViewById(R.id.imageButtonMyLocation);
		myLocationButton.setVisibility(View.GONE);
		myLocationButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetToMyLocation();
			}
		});
		
		setupGoogleMaps();

		final Context context = view.getContext();
		final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		mSensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        
        if ( !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ) {
            buildAlertMessageNoGps();
        }
		locationManager.addNmeaListener(new GpsStatus.NmeaListener() {
			public void onNmeaReceived(long timestamp, String nmea) {

				Log.v("NMEA","Timestamp is :" +timestamp+"   nmea is :"+nmea);
				//$GPRMC,123212.0,A,5212.356194,N,00007.654499,E,0.0,0.0,220913,3.3,W,A*11
				if (nmea.contains("GPRMC")) {
					String[] data = nmea.split(",");
					mGpsStatus = data[2];
					Log.v("NMEA", mGpsStatus);

					mNmeaTimestamp = timestamp;
				}

			}});
		/*locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 100, new LocationListener() {
			@Override
			public void onLocationChanged(Location location) {
				Intent i = new Intent("com.oidriver.GPS_STATUS");
				i.putExtra("status", 2);
				OiDriverApp.getAppContext().sendBroadcast(i);
			}

			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {

				Intent i = new Intent("com.oidriver.GPS_STATUS");
				if (status == LocationProvider.OUT_OF_SERVICE)
					i.putExtra("status", 0);
				else if (status == LocationProvider.TEMPORARILY_UNAVAILABLE)
					i.putExtra("status", 1);
				else
					i.putExtra("status", 2);
				OiDriverApp.getAppContext().sendBroadcast(i);
			}

			@Override
			public void onProviderEnabled(String provider) {

			}

			@Override
			public void onProviderDisabled(String provider) {
				Intent i = new Intent("com.oidriver.GPS_STATUS");
				i.putExtra("status", 0);
				OiDriverApp.getAppContext().sendBroadcast(i);
			}
		});*/


		mGpsStatusTimer = new Timer();
		mGpsStatusTimer.schedule(new TimerTask() {

			@Override
			public void run() {

				RunOn.mainThread(new Runnable() {

					@Override
					public void run() {
						if (((OiDriverActivity)getActivity()).isJobHidden())
							centerMap();
						else
						{
							if (((OiDriverActivity)getActivity()).getNavigatedState())
							{
								if (mPassengerMarker != null)
								{
									LatLng pos = mPassengerMarker.getPosition();
									if (pos != null)
										showMapWithPassenger(pos.latitude, pos.longitude);
								}
							}
							else
							{
								centerMap();
							}
						}
						Intent i = new Intent("com.oidriver.GPS_STATUS");
						i.putExtra("status", getGpsStatusIdx());
						OiDriverApp.getAppContext().sendBroadcast(i);
					}
				});
			}
		}, 2000, 10000);
		
        return view;
	}
	
	private void setupGoogleMaps()
	{
		mMap = ((SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
		mMap.setMyLocationEnabled(true);//
		mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		mMap.setIndoorEnabled(true);
		UiSettings uis = mMap.getUiSettings();
		uis.setMyLocationButtonEnabled(true);//
		uis.setCompassEnabled(false);
		uis.setZoomControlsEnabled(true);
		uis.setRotateGesturesEnabled(false);
		uis.setScrollGesturesEnabled(true);
		uis.setZoomGesturesEnabled(true);
		uis.setTiltGesturesEnabled(true);
		
		mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
			
			@Override
			public void onMapClick(LatLng arg0) {
				if (mMapClickedListener != null)
					mMapClickedListener.onMapClick(arg0);
			}
		});

		mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
			
			@Override
			public void onMyLocationChange(Location location) {
					handleLocationChange(location);
			}
		});
	}
	
	public GoogleMap getMap()
	{
		return mMap;
	}
	
	public void setMapClickedListener(GoogleMap.OnMapClickListener listener)
	{
		mMapClickedListener = listener;
	}

	public void centerMap()
	{
		if (mMap == null)
			return;
		Location loc = mMap.getMyLocation();
		if (loc == null)
			return;
		centerMap(loc);
	}
	
	public void centerMap(Location loc)
	{
        double lat = loc.getLatitude();
        double lon = loc.getLongitude();
		centerMap(lat, lon);
	}
	
	public void centerMap(double lat, double lon)
	{
		Log.v(TAG, String.format("Centering map to [%f,%f]", lat, lon));

		// Remove passenger marker if it exists
		if (mPassengerMarker != null)
		{
			mPassengerMarker.remove();
			mPassengerMarker = null;
		}
		
		if (lastGpsFix == 0)
		{
			LatLng currentPoint = new LatLng(lat, lon);
	    	mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentPoint, Constants.ZOOM_LEVEL));
		}
		
		//Move the camera to the user's location once it's available!
		lastGpsFix = SystemClock.currentThreadTimeMillis();
		Projection projection = this.mMap.getProjection();
        LatLngBounds bounds = projection.getVisibleRegion().latLngBounds;
    	LatLng currentPoint = new LatLng(lat, lon);
        if(!bounds.contains(currentPoint))
        {
        	Log.e(TAG, "Out of bounds");
        }
        else
        {
        	//////Log.e(TAG, "In bounds");
        	mMap.stopAnimation();
        }
    	mMap.moveCamera(CameraUpdateFactory.newLatLng(currentPoint));
	}

	public Location getLocation()
	{
		if (mMap == null)
			return null;
		
		return mMap.getMyLocation();
	}
	
	public void showMapWithPassenger(double latP, double lonP)
	{
		if (mPassengerMarker != null)
		{
			mPassengerMarker.remove();
			mPassengerMarker = null;
		}

		//LatLng currentPoint = new LatLng(OiDriverService.lat, OiDriverService.lon);
		Location loc = mMap.getMyLocation();
		LatLng currentPoint;
		if (loc != null)
			currentPoint = new LatLng(loc.getLatitude(), loc.getLongitude());
		else
			currentPoint = new LatLng(OiDriverService.lat, OiDriverService.lon);
		LatLng passengerPoint = new LatLng(latP, lonP);
		LatLngBounds.Builder builder = LatLngBounds.builder();
		builder.include(currentPoint);
		builder.include(passengerPoint);
		mPassengerMarker = mMap.addMarker(new MarkerOptions().position(passengerPoint).icon(mPassengerMarkerIcon).anchor(0.50f, 0.50f));
		mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), Util.toPixels(getView().getContext(), 60)));
	}
	
	public void showMapWithoutPassenger()
	{
		if (mPassengerMarker != null)
		{
			mPassengerMarker.remove();
			mPassengerMarker = null;
		}
		LatLng currentPoint = new LatLng(OiDriverService.lat, OiDriverService.lon);
		mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentPoint, Constants.ZOOM_LEVEL));
	}
	
	public void resetToMyLocation()
	{
		if (mPassengerMarker != null)
		{
			mPassengerMarker.remove();
			mPassengerMarker = null;
		}
		centerMap();
	}

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
               .setCancelable(false)
               .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                   public void onClick(final DialogInterface dialog, final int id) {
                       startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                   }
               })
               .setNegativeButton("No", new DialogInterface.OnClickListener() {
                   public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                   }
               });
        final AlertDialog alert = builder.create();
        alert.show();
    }
	
	public boolean hasGPSFix()
	{
		Location loc = mMap.getMyLocation();
		if (loc == null)
			return false;
		long lastValue = loc.getTime();
		return (System.currentTimeMillis() - lastValue <= 2 * 60 * 1000) || mGpsStatus.equalsIgnoreCase("A");
	}

	double mLastLat, mLastLon;

	private void handleLocationChange(Location location)
	{
        if (mListener != null) mListener.onLocationChanged(location);

		double newLat = location.getLatitude();
		double newLon = location.getLongitude();
		Log.v(TAG, String.format("CHANGED: [%f,%f]", newLat, newLon));
		long timeDiff;
		float[] results = new float[1];

        if (location != null && location.hasSpeed()) {
            OiDriverService.speed = location.getSpeed();
            Log.wtf(TAG, "LAST GMAPS SPEED: " + OiDriverService.speed);
        }
        if (location != null && location.hasAccuracy())
            OiDriverService.accuracy = location.getAccuracy();

		if(OiDriverService.lat != newLat && OiDriverService.lon != newLon)
		{
//			Intent i = new Intent("com.oidriver.GPS_STATUS");
//			i.putExtra("status", 2);
//			OiDriverApp.getAppContext().sendBroadcast(i);

			Location.distanceBetween(mLastLat, mLastLon, newLat, newLon, results);
			timeDiff = Math.abs(SystemClock.currentThreadTimeMillis() - lastGpsFix);

			// Save last lat/lon
			mLastLat = newLat;
			mLastLon = newLon;

			// Check if we jumped
			if (results[0] > 500.0f && (timeDiff < 300 * 1000 && lastGpsFix != 0)) {
				Log.e("GPS", "JUMPING TOO MUCH: " + results[0]);
				return;
			}
			OiDriverService.lat = newLat;
			OiDriverService.lon = newLon;
			fMovedDistance += results[0];
			float distanceThreshold = OiDriverService.readyStatus ? DISTANCE_RTW : DISTANCE_BRK;
			if (fMovedDistance > distanceThreshold || timeDiff > 300 * 1000 || lastGpsFix == 0)
			{
				if (!OiDriverService.isXmppAuthenticated())
				{
					OiDriverService.hasOutstandingGPS = true;
				}
				else 
				{
					OiDriverService.sendIQForGPS(true, "GPS");
					fMovedDistance = 0.0f;
					Log.d(TAG, String.format("GPS CHANGED SENT: [%f,%f]", newLat, newLon));
				}
			}

			if (AcceptedJob.hasActiveJob() && !OiDriverActivity.sentSTC)
			{
				((OiDriverActivity)getActivity()).validateSTC();
			}
		}
		else
		{
			Log.v(TAG, String.format("CHANGED: [%f,%f]/%f SKIPPED for [%f,%f]",
					newLat, newLon, location.getAccuracy(),
					OiDriverService.lat, OiDriverService.lon));
		}
		Intent i = new Intent("com.oidriver.GPS_STATUS");
		i.putExtra("status", 2);
		OiDriverApp.getAppContext().sendBroadcast(i);
	}

	// from SensorEventListener
	@Override
	public void onSensorChanged(SensorEvent event) {

	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}

	public int getGpsStatusIdx() {
		boolean hasGpsFix = hasGPSFix();
		if (hasGpsFix)
			return 2;
		if (mGpsStatus.equalsIgnoreCase("V")) {
			if (System.currentTimeMillis() - mNmeaTimestamp <= 2 * 60 * 1000)
				return 1;
		}
		return 0;
	}

    private void setListener(OiDriverLocationListener listener) {
        mListener = listener;
    }

    /**
     * A helper function that given a type and a fragment, will attempt to extract
     * the fragment's listener.
     *
     * @param cls - the listener's type
     * @param fragment - the fragment whose listener we're trying to find
     * @return - the fragment's listener or null if neither the activity or the parent fragment subclass cls
     */
    @SuppressWarnings("unchecked")
    public static <T> T getListener(Class<T> cls, android.support.v4.app.Fragment fragment) {
        FragmentActivity activity = fragment.getActivity();
        android.support.v4.app.Fragment parent = fragment.getParentFragment();
        android.support.v4.app.Fragment target = fragment.getTargetFragment();

        if(target != null && cls.isAssignableFrom(Util.getClazz(target))) {
            return (T) target;
        } else if(parent != null && cls.isAssignableFrom(Util.getClazz(parent))) {
            return (T) parent;
        } else if (activity != null && cls.isAssignableFrom(activity.getClass())){
            return (T) activity;
        }

        return null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        setListener(getListener(OiDriverLocationListener.class, this));
    }
}
