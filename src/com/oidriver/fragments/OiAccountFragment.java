package com.oidriver.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.oidriver.OiDriverApp;
import com.oidriver.R;
import com.oidriver.db.AccountListProvider;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class OiAccountFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
	
	private static int ACCOUNT_LIST_LOADER = 1;
    private SimpleCursorAdapter adapter;
	String[] columns = new String[] { "_id", "jobid", "fareprice", "payment" };
	int[] to = new int[] { R.id.tvId, R.id.tvJobId, R.id.tvAmount, R.id.tvPayment };
	SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
	SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy", Locale.UK);

	public static OiAccountFragment newInstance() {
		OiAccountFragment fragment = new OiAccountFragment();
		return fragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.account_list, null);
		return view;
	}
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        Calendar currentDate = Calendar.getInstance();
    	String thisDate = sdfDate.format(currentDate.getTime());
    	String start = thisDate + " 00:00:00";
		String end = thisDate + " 23:59:59";
		Bundle args = new Bundle();
		args.putString("selection", String.format("jobdate >= '%s' AND jobdate <= '%s'", start, end));
		args.putString("today", sdf.format(currentDate.getTime()));
        getLoaderManager().initLoader(ACCOUNT_LIST_LOADER, args, this);

        adapter = new SimpleCursorAdapter(
                OiDriverApp.getAppContext(), R.layout.account_row,
                null, columns, to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        adapter.setViewBinder(new AccountViewBinder());
        setListAdapter(adapter);
        setHasOptionsMenu(false);
        //setEmptyText(getResources().getText(R.string.no_data));
        getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

	
	// From LoaderManager.LoaderCallbacks<Cursor>
	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle args) {
		String selection = args.getString("selection");
		String today = args.getString("today");
	    CursorLoader cursorLoader = new CursorLoader(getActivity(),
	            AccountListProvider.CONTENT_URI, columns, selection, null, null);
	    FragmentActivity act = getActivity();
	    if (act != null)
	    	((TextView)act.findViewById(R.id.textViewDate)).setText(today);
	    return cursorLoader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		adapter.swapCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		adapter.swapCursor(null);
	}

	private class AccountViewBinder implements SimpleCursorAdapter.ViewBinder {

        @Override
        public boolean setViewValue(View view, Cursor cursor, int index) {
        	if (index == cursor.getColumnIndex("_d")) {
                TextView tv = (TextView) view;
                tv.setText("");
                return true;
            }
        	else if (index == cursor.getColumnIndex("jobid")) {
                String jobId = cursor.getString(index);
                TextView tv = (TextView) view;
                tv.setText(jobId);
                return true;
            } else if (index == cursor.getColumnIndex("fareprice")) {
                String fare = cursor.getString(index);
                TextView tv = (TextView) view;
                if (fare.equalsIgnoreCase("n/a"))
                	tv.setText("N/A");
                else
                	tv.setText("£" + fare);
                return true;
            } else if (index == cursor.getColumnIndex("payment")) {
                String paymentMethod = cursor.getString(index);
                TextView tv = (TextView) view;
                tv.setText(paymentMethod);
                return true;
            } else {
                return false;
            }
        }
    }

	public void refresh(Date dt)
	{
		String thisDate = sdfDate.format(dt);
		String from = thisDate + " 00:00:00";
		String to = thisDate + " 23:59:59";
		Bundle args = new Bundle();
		args.putString("selection", String.format("jobdate >= '%s' AND jobdate <= '%s'", from, to));
		args.putString("today", sdf.format(dt));
		getLoaderManager().restartLoader(ACCOUNT_LIST_LOADER, args, this);
	}
}