package com.oidriver.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.oidriver.OiDriverActivity;
import com.oidriver.R;
import com.oidriver.ZonesAdapter;

public class OiZonesFragment extends Fragment {

	private ListView mZonesList;
	private Button mBtnClose;
	private Button mBtnDown;
	private Button mBtnUp;
	
//	private int mSelectedZone = -1;

	public static OiZonesFragment newInstance() {
		OiZonesFragment fragment = new OiZonesFragment();
		return fragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.zones_panel, null);
		
		mZonesList = (ListView) view.findViewById(R.id.lvZones);
		mZonesList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				ZonesAdapter adapter = (ZonesAdapter) mZonesList.getAdapter();
				adapter.setSelectedZone(position);
				adapter.notifyDataSetChanged();
				mZonesList.smoothScrollToPosition(position);
			}
		});
		mBtnClose = (Button) view.findViewById(R.id.button_close);
		mBtnClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				((OiDriverActivity)getActivity()).hideZonesFragment(true);
			}
		});
		mBtnDown = (Button) view.findViewById(R.id.buttonDown);
		mBtnDown.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				ZonesAdapter adapter = (ZonesAdapter) mZonesList.getAdapter();
				int selectedZone = adapter.getSelectedZone();
				selectedZone++;
				if (selectedZone >= mZonesList.getCount())
					selectedZone--;
				adapter.setSelectedZone(selectedZone);
				adapter.notifyDataSetChanged();
				mZonesList.smoothScrollToPosition(selectedZone);
			}
		});
		mBtnUp = (Button) view.findViewById(R.id.buttonUp);
		mBtnUp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				ZonesAdapter adapter = (ZonesAdapter) mZonesList.getAdapter();
				int selectedZone = adapter.getSelectedZone();
				selectedZone--;
				if (selectedZone < 0)
					selectedZone = 0;
				adapter.setSelectedZone(selectedZone);
				adapter.notifyDataSetChanged();
				mZonesList.smoothScrollToPosition(selectedZone);
			}
		});
//		mZonesList.setSelection(0);

		return view;
	}
}