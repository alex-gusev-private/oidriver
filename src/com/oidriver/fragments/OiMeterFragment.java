package com.oidriver.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.FloatMath;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.oidriver.Constants;
import com.oidriver.OiDriverActivity;
import com.oidriver.OiDriverApp;
import com.oidriver.R;
import com.oidriver.nmea.NmeaProcessor;
import com.oidriver.runnable.RunOn;
import com.oidriver.service.OiDriverService;
import com.oidriver.xmpp.Util;

import org.geohunter.GeoFix;
import org.geohunter.GeoFixProviderLive;
import org.geohunter.Refresher;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import au.com.bytecode.opencsv.CSVReader;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

public class OiMeterFragment extends Fragment
        implements Refresher, NmeaProcessor.NmeaProcessorListener, SensorEventListener {

    private final static String TAG = OiDriverApp.TAG + "-METER";

    private TextView[] mTariff = new TextView[2];
    private TextView[] mExtras = new TextView[6];
    private TextView[] mFare = new TextView[9];
    private TextView[] mSpeed = new TextView[3];

    private TextView[] mTariffBkg = new TextView[2];
    private TextView[] mExtrasBkg = new TextView[6];
    private TextView[] mFareBkg = new TextView[9];
    private TextView[] mSpeedBkg = new TextView[3];

    private TextView mNoGpsView;
    private View mButtonsPanel;
    private TextView mForHire, mHired, mStopped;
    private TextView mJourneyDist;
    private TextView mSpeedDist;

    private TextView mNmeaInfo;
    private TextView mSalettitesInfo;

    public final static String KPH = "KPH";
    public final static String MPH = "MPH";

    public final static int DIGIT_ACTIVE  = 0xFFFB222C;
    public final static int STATE_PASSIVE = 0xFFFFFFFF;
    public final static int DIGIT_BACKGROUND = 0xFF381313;

    private boolean mIsTestNmea = false;
    private boolean mUsePoll = true;
    private static boolean mSaveLog = true;

    /**
     * NMEA processing is extracted to dedicated class
     */
    NmeaProcessor mNmeaProcessor;

    /**
     * Various members to keep track on the journey and fare
     */
    double _minimumDistance;
    public int _totalFare = 0;
    double _currencyFactor = 100;
    private Tariff _tariff = null;
    public int _startFare;
    int _clickDist = 133;

    ArrayList<Tariff> mTariffs = new ArrayList<Tariff>();
    int mCurrentTariff = 0;

    public enum MeterState
    {
        ForHire, Hired, Stopped
    }

    MeterState mState = MeterState.ForHire;
    boolean mFirstTime = true;

    double mJourneyStartLat, mJourneyStartLon;

    GeoFixProviderLive mGpsProvider;
    LocationManager mLocationManager;
    private SensorManager mSensorManager;

    private boolean mIsMoving = false;

    Typeface mLcdTypeface;

    Timer mTestTimer;
    ArrayList<String> mNmeaTestData = new ArrayList<String>();
    int mCurrentTestLine = 0;

    Timer mGpsStatusTimer;

    final SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());

    private Integer data_points = 2; // how many data points to calculate for
    private Double[][] positions;
    private Long[] times;

    Timer mBackupTimer;

    private float mAccel;
    private float mAccelCurrent;
    private float mAccelLast;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLcdTypeface = Typeface.createFromAsset(OiDriverApp.getAppContext().getAssets(), "fonts/lcd_font.ttf");

        AssetManager assetManager = getActivity().getAssets();
        InputStream in = null;
        try {
            in = assetManager.open("nmea.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String nmea;
            while ((nmea = reader.readLine()) != null) {
                mNmeaTestData.add(nmea);
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // two arrays for position and time.
        positions = new Double[data_points][2];
        times = new Long[data_points];

        mNmeaProcessor = new NmeaProcessor(this);
    }

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.meter, null);

        if (view == null) {
            Toast.makeText(OiDriverApp.getAppContext(), "FATAL ERROR: cannot create METER view", Toast.LENGTH_LONG).show();
            return null;
        }

        setupTariff(view);
        setupExtras(view);
        setupSpeed(view);
        setupFare(view);

        TextView tvLocale = (TextView) view.findViewById(R.id.tvSpeedUnits);
        if (tvLocale != null) {
            tvLocale.setText(mNmeaProcessor.get_localeStr());
        }

        mGmapGPS = (TextView) view.findViewById(R.id.tvMap);
        mJourneyDist = (TextView) view.findViewById(R.id.tvDistance);
        mSpeedDist = (TextView) view.findViewById(R.id.tvSpeedDist);

        mNmeaInfo = (TextView) view.findViewById(R.id.tvNMEA);
        mSalettitesInfo = (TextView) view.findViewById(R.id.tvSalettites);

        mNoGpsView = (TextView) view.findViewById(R.id.tvNoGPS);
        mButtonsPanel = view.findViewById(R.id.llState);
        mForHire = (TextView) view.findViewById(R.id.forHire);
        mHired = (TextView) view.findViewById(R.id.hired);
        mStopped = (TextView) view.findViewById(R.id.stopped);

        mForHire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initVariables();

                setValue("0.00", true, mFare, mFareBkg);
                setState(MeterState.ForHire);
                //mBtnBack.setEnabled(true);

                updateUI(0);

                mPrefs.edit().putInt("_state", mState.ordinal()).commit();
            }
        });
        mHired.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onHiredClicked();
                mPrefs.edit().putInt("_state", mState.ordinal()).commit();
            }
        });
        mStopped.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsTestNmea && mTestTimer != null) {
                    mTestTimer.cancel();
                    mTestTimer = null;
                }
                setState(MeterState.Stopped);
                //mBtnBack.setEnabled(true);

                mPrefs.edit().putInt("_state", mState.ordinal()).commit();
            }
        });

        final Button mBtnBack = (Button) view.findViewById(R.id.btnBack);
        if (mBtnBack != null) {
            mBtnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideMeterFragment();
                }
            });
            mBtnBack.setEnabled(true);
        }

        final Button mBtnMenu = (Button) view.findViewById(R.id.btnMenu);
        if (mBtnMenu != null) {
            mBtnMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Meter Menu");
                    String[] certs = new String[] {"SEND LOG", "TEST"/*, "PAY"*/};
                    builder.setItems(certs, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                switch (which) {
                                    case 0:
                                        sendEmail(new String[]{}, "NMEA log", "Log from last journey");
                                        break;
                                    case 1:
                                        mIsTestNmea = true;
                                        break;
                                    /*case 2:
                                        if (mState != MeterState.Stopped) {
                                            Toast.makeText(OiDriverApp.getAppContext(),
                                                    "Please stop the meter first", Toast.LENGTH_SHORT)
                                                .show();
                                            break;
                                        }

                                        CardIOActivity.canReadCardWithCamera(OiDriverApp.getAppContext());
                                        onScanPress();
                                        break;*/
                                }
                                dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });
        }

        loadTariffs();

        mLocationManager = (LocationManager) OiDriverApp.getAppContext().getSystemService(Context.LOCATION_SERVICE);

        setState(MeterState.ForHire);

        mSensorManager = (SensorManager) OiDriverApp.getAppContext().getSystemService(Context.SENSOR_SERVICE);
        final Sensor mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mAccel = 0.00f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);

        if (!mUsePoll) {
            final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
            mGpsProvider = new GeoFixProviderLive(mLocationManager, mSensorManager, sp);
        }

        view.setKeepScreenOn(true);
		return view;
	}

    @Override
    public void onStart() {
        super.onStart();

        if ( mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ) {
            mLocationManager.addNmeaListener(mNmeaListener);
        } else {
            Toast.makeText(OiDriverApp.getAppContext(), "GPS is disabled", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mGpsStatusTimer != null) {
            mGpsStatusTimer.cancel();
            mGpsStatusTimer = null;
        }

        mGpsStatusTimer = new Timer("MeterGpsTimer");
        mGpsStatusTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                RunOn.mainThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            querySatellites();
                            Activity activity = getActivity();
                            if (activity != null) {
                                Drawable d = ((OiDriverActivity) activity).getGpsStatusDrawable();
                                ImageView statusGps = (ImageView) getView().findViewById(R.id.statusGps);
                                statusGps.setImageDrawable(d);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }, 0, 1000);

        if (!mUsePoll) {
            mGpsProvider.addObserver(this);
            mGpsProvider.startUpdates();
        }

//        setAccelerometer();

//        pollLastKnownLocation();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mBackupTimer != null) {
            mBackupTimer.cancel();
            mBackupTimer = null;
        }
        //mSensorManager.unregisterListener(xyzAcc);

        if (!mUsePoll)
            mGpsProvider.stopUpdates();

        if (mGpsStatusTimer != null) {
            mGpsStatusTimer.cancel();
            mGpsStatusTimer = null;
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mLocationManager != null) {
            mLocationManager.removeNmeaListener(mNmeaListener);
        }
        if (mTestTimer != null) {
            mTestTimer.cancel();
            mTestTimer = null;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void setupTariff(View view) {
        mTariff[1] = (TextView) view.findViewById(R.id.tariff1);
        mTariff[0] = (TextView) view.findViewById(R.id.tariff0);
        mTariffBkg[1] = (TextView) view.findViewById(R.id.tariff1b);
        mTariffBkg[0] = (TextView) view.findViewById(R.id.tariff0b);

        mTariffBkg[1].setTypeface(mLcdTypeface);
        mTariffBkg[0].setTypeface(mLcdTypeface);

        mTariff[1].setTypeface(mLcdTypeface);
        mTariff[0].setTypeface(mLcdTypeface);
    }

    private void setupExtras(View view) {
        mExtras[5] = (TextView) view.findViewById(R.id.extras2);
        mExtras[4] = (TextView) view.findViewById(R.id.extras1);
        mExtras[3] = (TextView) view.findViewById(R.id.extras0);
        mExtras[2] = (TextView) view.findViewById(R.id.extrasDot);
        mExtras[1] = (TextView) view.findViewById(R.id.extrasDD);
        mExtras[0] = (TextView) view.findViewById(R.id.extrasDH);

        mExtrasBkg[5] = (TextView) view.findViewById(R.id.extras2b);
        mExtrasBkg[4] = (TextView) view.findViewById(R.id.extras1b);
        mExtrasBkg[3] = (TextView) view.findViewById(R.id.extras0b);
        mExtrasBkg[2] = (TextView) view.findViewById(R.id.extrasDot);
        mExtrasBkg[1] = (TextView) view.findViewById(R.id.extrasDDb);
        mExtrasBkg[0] = (TextView) view.findViewById(R.id.extrasDHb);

        for (TextView tv: mExtrasBkg)
            tv.setTypeface(mLcdTypeface);
        for (TextView tv: mExtras)
            tv.setTypeface(mLcdTypeface);
    }

    private void setupSpeed(View view) {
        mSpeed[2] = (TextView) view.findViewById(R.id.speed2);
        mSpeed[1] = (TextView) view.findViewById(R.id.speed1);
        mSpeed[0] = (TextView) view.findViewById(R.id.speed0);
        mSpeedBkg[2] = (TextView) view.findViewById(R.id.speed2b);
        mSpeedBkg[1] = (TextView) view.findViewById(R.id.speed1b);
        mSpeedBkg[0] = (TextView) view.findViewById(R.id.speed0b);

        for (TextView tv: mSpeedBkg)
            tv.setTypeface(mLcdTypeface);
        for (TextView tv: mSpeed)
            tv.setTypeface(mLcdTypeface);
    }

    private void setupFare(View view) {
        mFare[8] = (TextView) view.findViewById(R.id.fare5);
        mFare[7] = (TextView) view.findViewById(R.id.fare4);
        mFare[6] = (TextView) view.findViewById(R.id.fare3);
        mFare[5] = (TextView) view.findViewById(R.id.fare2);
        mFare[4] = (TextView) view.findViewById(R.id.fare1);
        mFare[3] = (TextView) view.findViewById(R.id.fare0);
        mFare[2] = (TextView) view.findViewById(R.id.fareDot);
        mFare[1] = (TextView) view.findViewById(R.id.fareD1);
        mFare[0] = (TextView) view.findViewById(R.id.fareD0);

        mFareBkg[8] = (TextView) view.findViewById(R.id.fare5b);
        mFareBkg[7] = (TextView) view.findViewById(R.id.fare4b);
        mFareBkg[6] = (TextView) view.findViewById(R.id.fare3b);
        mFareBkg[5] = (TextView) view.findViewById(R.id.fare2b);
        mFareBkg[4] = (TextView) view.findViewById(R.id.fare1b);
        mFareBkg[3] = (TextView) view.findViewById(R.id.fare0b);
        mFareBkg[2] = (TextView) view.findViewById(R.id.fareDot);
        mFareBkg[1] = (TextView) view.findViewById(R.id.fareD1b);
        mFareBkg[0] = (TextView) view.findViewById(R.id.fareD0b);

        for (TextView tv: mFareBkg)
            tv.setTypeface(mLcdTypeface);
        for (TextView tv: mFare)
            tv.setTypeface(mLcdTypeface);
    }

    public void onHiredClicked() {
        //mBtnBack.setEnabled(false);
        initVariables();

        // Clear GMap variables
        mMapDistance = 0;
        counter = 0;
        positions = new Double[data_points][2];
        times = new Long[data_points];

//        positions[counter][0] = OiDriverService.lat;
//        positions[counter][1] = OiDriverService.lon;
//        times[counter] = System.currentTimeMillis();
//        counter = (counter + 1) % data_points;
        updateUI(-1);

        if (mSaveLog) {
            File f = new File(Environment.getExternalStorageDirectory(), "nmea.txt");
            boolean deleted = f.delete();
            Log.d(TAG, "Deleted old log: " + deleted);
        }

        mJourneyStartLat = OiDriverService.lat;
        mJourneyStartLon = OiDriverService.lon;

        pollLastKnownLocation();

        setValue(String.valueOf(_startFare /_currencyFactor), true, mFare, mFareBkg);
        setState(MeterState.Hired);

        if (mIsTestNmea) {
            mCurrentTestLine = 0;
            mTestTimer = new Timer();
            mTestTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (mCurrentTestLine >= mNmeaTestData.size()) {
                        mCurrentTestLine = 0;
                        mTestTimer.cancel();
                        RunOn.mainThread(new Runnable() {
                            @Override
                            public void run() {
                                mStopped.performClick();
                            }
                        });
                        return;
                    }
                    String nmea = mNmeaTestData.get(mCurrentTestLine);
                    if (nmea != null) {
                        nmea = nmea.replace("[\n|\r|\r\n]", "").trim();
                        safeUpdateNmea(nmea);
                        mNmeaProcessor.meterTick(nmea, mState);
                    }
                    mCurrentTestLine++;
                }
            }, 250, 250);
        }
    }

    void setValue(String extras, boolean isDecimal, TextView[] ctls, TextView[] ctlsBkg) {

        Log.wtf(TAG, "VALUE: " + extras);

        // Zero all
        for (int i = ctls.length - 1; i >= 0; --i) {
            if (isDecimal && i == 2) {
                ctls[i].setText(".");
                ctls[i].setTextColor(DIGIT_ACTIVE);
            }
            else {
                ctls[i].setText("0");
                ctls[i].setTextColor(DIGIT_BACKGROUND);
                ctlsBkg[i].setText("0");
                ctlsBkg[i].setTextColor(DIGIT_BACKGROUND);
                ctlsBkg[i].setVisibility(View.VISIBLE);
            }
        }
        //int value = (int) (Float.valueOf(extras) * 100);
        String data;
        if (isDecimal)
            data = String.format(Locale.UK, "%.02f", Float.valueOf(extras));
        else
            data = extras;
        int idx = data.length() - 1;
        if (idx > -1) {
            for (int i = idx; i >= 0; --i) {
                if (isDecimal && i == 2)
                    continue;
                char ch = data.charAt(idx - i);
                ctls[i].setText("" + ch);
                ctls[i].setTextColor(DIGIT_ACTIVE);
                ctlsBkg[i].setTextColor(DIGIT_BACKGROUND);
                ctlsBkg[i].setVisibility(View.INVISIBLE);
            }
        }
    }

    void setState(MeterState state) {
        mState = state;
        mNmeaProcessor.setMeterState(state);
        switch (mState) {
            case ForHire:
                mForHire.setTextColor(DIGIT_ACTIVE);
                mForHire.setEnabled(false);
                mHired.setTextColor(STATE_PASSIVE);
                mHired.setEnabled(true);
                mStopped.setTextColor(STATE_PASSIVE);
                mStopped.setEnabled(false);
                mPrefs.edit()
                        .putInt("_currentTariff", mCurrentTariff)
                        .putFloat("_newDistance", (float)mNmeaProcessor.get_newDistance())
                        .putString("_gpRmcLat", String.valueOf(mNmeaProcessor.get_gpRmcLat()))
                        .putString("_gpRmcLng", String.valueOf(mNmeaProcessor.get_gpRmcLng()))
                        .putString("_gpGgaLat", String.valueOf(mNmeaProcessor.get_gpGgaLat()))
                        .putString("_gpGgaLng", String.valueOf(mNmeaProcessor.get_gpGgaLng()))
                        .putInt("_startFare", _startFare)
                        .commit();
                break;
            case Hired:
                mForHire.setTextColor(STATE_PASSIVE);
                mForHire.setEnabled(false);
                mHired.setTextColor(DIGIT_ACTIVE);
                mHired.setEnabled(false);
                mStopped.setTextColor(STATE_PASSIVE);
                mStopped.setEnabled(true);
                break;
            case Stopped:
                if (mBackupTimer != null) {
                    mBackupTimer.cancel();
                    mBackupTimer = null;
                }
                mForHire.setTextColor(STATE_PASSIVE);
                mForHire.setEnabled(true);
                mHired.setTextColor(STATE_PASSIVE);
                mHired.setEnabled(false);
                mStopped.setTextColor(DIGIT_ACTIVE);
                mStopped.setEnabled(false);
                mPrefs.edit()
                        .putInt("_currentTariff", mCurrentTariff)
                        .putFloat("_newDistance", (float)mNmeaProcessor.get_newDistance())
                        .putString("_gpRmcLat", String.valueOf(mNmeaProcessor.get_gpRmcLat()))
                        .putString("_gpRmcLng", String.valueOf(mNmeaProcessor.get_gpRmcLng()))
                        .putString("_gpGgaLat", String.valueOf(mNmeaProcessor.get_gpGgaLat()))
                        .putString("_gpGgaLng", String.valueOf(mNmeaProcessor.get_gpGgaLng()))
                        .putInt("_startFare", _startFare)
                        .commit();
                break;
        }
    }
/*
    boolean meterTick(String nmea)
    {
        boolean ret = true;
        Log.d(TAG, "Daemon.MeterTick()");
        if (!TextUtils.isEmpty(nmea))
        {
            Log.d(TAG, "Daemon.MeterTick(): has data => " + nmea);

            String[] strArr = nmea.split("$");
            for (String strTemp : strArr) {
                if (TextUtils.isEmpty(strTemp))
                    continue;

                String[] lineArr = strTemp.split(",");
                int posChecksum = strTemp.indexOf('*');
                if (posChecksum < 0)
                    continue;
                String strTestLine = strTemp.substring(0, posChecksum);
                String checksum = getChecksum(strTestLine);
                if (checksum.compareToIgnoreCase(strTemp.substring(posChecksum + 1)) != 0) {
                    Log.e(TAG, "Invalid checksum for " + strTemp);
                    continue;
                }
                if (lineArr[0].equalsIgnoreCase("$GPRMC")) {
                    if (!handleRMC(lineArr)) {
                        ret = false;
                        break;
                    }
                } else if (lineArr[0].equalsIgnoreCase("$GPGGA")) {
                    if (!handleGGA(lineArr)) {
                        ret = false;
                        break;
                    }
                }
            }
        }
        else
            ret = false;
        Log.d(TAG, "Daemon.MeterTick() Exit: " + ret);
        return ret;
    }

    private static String getChecksum(String sentence)
    {
        // Loop through all chars to get a checksum
        int checksum = 0;
        for (char chr: sentence.toCharArray())
        {
            switch (chr)
            {
                case '$':
                    // Ignore the dollar sign
                    break;
                case '*':
                    // Stop processing before the asterisk
                    return String.format(Locale.UK, "%02X", checksum);
                default:
                    // Is this the first value for the checksum?
                    if (checksum == 0)
                    {
                        // Yes. Set the checksum to the value
                        checksum = (int)chr;
                    }
                    else
                    {
                        // No. XOR the checksum with this character's value
                        checksum = checksum ^ ((int)chr);
                    }
                    break;
            }
        }
        // Return the checksum formatted as a two-character hexadecimal
        return String.format(Locale.UK, "%02X", checksum);
    }

    /@@
     * Process NMEA RMC sentence.
     * Speed is calculated in meters per second.
     * @param lineArr - An array of RMC sentence items
     * @return true or false to indicate processing result
     @/
    private boolean handleRMC(String[] lineArr)
    {
        String speedStr;

        try
        {
            speedStr = lineArr[7];
            _gpRmcLat = decimalToFractionalDegrees(Double.parseDouble(lineArr[3]));
            _gpRmcLng = decimalToFractionalDegrees(Double.parseDouble(lineArr[5]));
            Log.wtf(TAG, "RMC => LAT = " + _gpRmcLat + " LON = " + _gpRmcLng);

            _utcTimeStr = lineArr[1];
            _utcDateStr = lineArr[9];

            if (lineArr[6].equals("W"))
            {
                _gpRmcLng = -_gpRmcLng;
            }

            double testDistance = calculateDistance(_lastRmcLat, _lastRmcLng, _gpRmcLat, _gpRmcLng);
            if (TextUtils.isEmpty(speedStr))
            {
                speedStr = "0";
            }

//            if (_localeStr.equals(KPH))
//            {
//                tempSpeed = Double.parseDouble(_speedStr) * 1.852;// knots to kph
//                _lastKnownSpeed = tempSpeed * 0.277777778;
//                _THRESHOLD = 1.60934;
//            } else {
//                tempSpeed = Double.parseDouble(_speedStr) * 1.15077945;
//                _lastKnownSpeed = tempSpeed * 0.44704;
//                _THRESHOLD = 1;
//            }

            // Speed in M/S
            _lastKnownSpeed = Double.parseDouble(speedStr) * 0.51444444444;

            if (_lastKnownSpeed > 0)
            {
                if ((_gpRmcLng == _lastRmcLng) && (_gpRmcLat == _lastRmcLat))
                {
                    _lastKnownSpeed = 0;
                }
            }

            if (!TextUtils.isEmpty(_utcDateStr) && _utcDateStr.length() >= 6 &&
                    !TextUtils.isEmpty(_utcTimeStr) && _utcTimeStr.length() >= 6 &&
                    !TextUtils.isEmpty(_lastUtcDate2Str) && _lastUtcDate2Str.length() >= 6 &&
                    !TextUtils.isEmpty(_lastUtcTime2Str) && _lastUtcTime2Str.length() >= 6)
            {
                double totalSeconds = getRmcTimeDeltaInSeconds();
                // speed distance in meters
                double avgSpeed = _lastRmcSpeed > 0 ? (_lastKnownSpeed + _lastRmcSpeed) / 2 : _lastKnownSpeed;
                double speedDist = avgSpeed * totalSeconds;
                if (testDistance > 0 && speedDist <= 0.001)
                    _lastKnownSpeed = 0;

                _speedDistance += speedDist;
            }

            if (_lastKnownSpeed >= 3.6d * 1.60934)//_THRESHOLD)
                _moving++;
            else
                _moving = 0;

            //updateUI();

            _validStr = lineArr[2];
            _utcTimeStr = lineArr[1];
        }
        catch (Exception ex)
        {
            Log.e(TAG, "[2] handleRMS(): " + ex.getMessage());
            return false;
        }

        _lastRmcLng = _gpRmcLng;
        _lastRmcLat = _gpRmcLat;

        _lastRmcSpeed = _lastKnownSpeed;
        _lastUtcDate2Str = _utcDateStr;
        _lastUtcTime2Str = _utcTimeStr;

        mPrefs.edit()
                .putInt("_currentTariff", mCurrentTariff)
                .putFloat("_newDistance", (float)_newDistance)
                .putString("_gpRmcLat", String.valueOf(_gpRmcLat))
                .putString("_gpRmcLng", String.valueOf(_gpRmcLng))
                .putString("_gpGgaLat", String.valueOf(_gpGgaLat))
                .putString("_gpGgaLng", String.valueOf(_gpGgaLng))
                .putInt("_startFare", _startFare)
                .putInt("_state", mState.ordinal())
                .commit();

        return true;
    }

    private double getRmcTimeDeltaInSeconds() {
        Date dtUtcNow = new Date(
                Integer.parseInt(_utcDateStr.substring(0, 2)),
                Integer.parseInt(_utcDateStr.substring(2, 4)),
                Integer.parseInt(_utcDateStr.substring(4, 6)),
                Integer.parseInt(_utcTimeStr.substring(0, 2)),
                Integer.parseInt(_utcTimeStr.substring(2, 4)),
                Integer.parseInt(_utcTimeStr.substring(4, 6)));
        Date dtUtcLast = new Date(
                Integer.parseInt(_lastUtcDate2Str.substring(0, 2)),
                Integer.parseInt(_lastUtcDate2Str.substring(2, 4)),
                Integer.parseInt(_lastUtcDate2Str.substring(4, 6)),
                Integer.parseInt(_lastUtcTime2Str.substring(0, 2)),
                Integer.parseInt(_lastUtcTime2Str.substring(2, 4)),
                Integer.parseInt(_lastUtcTime2Str.substring(4, 6)));
        long timeDelta = dtUtcNow.getTime() - dtUtcLast.getTime();
        return (double) (timeDelta / 1000);
    }

    String _ggaTimeStr, _ggaLastTimeStr;

    private boolean handleGGA(String[] lineArr)
    {
        try
        {
            String fixQualityStr = lineArr[6];
            if (TextUtils.isEmpty(fixQualityStr))
                fixQualityStr = "0";
            int fixQuality = Integer.parseInt(fixQualityStr);
            if (fixQuality == 0) {
                Log.wtf(TAG, "INVALID GGA");
                return false;
            }

            _ggaTimeStr = lineArr[1];
            if (!TextUtils.isEmpty(_ggaTimeStr) && _ggaTimeStr.length() >= 6 &&
                !TextUtils.isEmpty(_ggaLastTimeStr) && _ggaLastTimeStr.length() >= 6)
            {
                long nowSecs =
                        3600 * Integer.parseInt(_ggaTimeStr.substring(0, 2)) +
                        60   * Integer.parseInt(_ggaTimeStr.substring(2, 4)) +
                               Integer.parseInt(_ggaTimeStr.substring(4, 6));
                long lastSecs =
                        3600 * Integer.parseInt(_ggaLastTimeStr.substring(0, 2)) +
                        60   * Integer.parseInt(_ggaLastTimeStr.substring(2, 4)) +
                               Integer.parseInt(_ggaLastTimeStr.substring(4, 6));
                _timeDelta = (nowSecs - lastSecs) * 1000;
            }
            _ggaLastTimeStr = _ggaTimeStr;

            double newLatitude = decimalToFractionalDegrees(Double.parseDouble(lineArr[2]));
            double newLongitude = decimalToFractionalDegrees(Double.parseDouble(lineArr[4]));
            if (!isValidPosition(newLatitude, newLongitude)) {
                Log.wtf(TAG, "REALLY WTF");
                return false;
            }

            _Latitude = newLatitude;
            _Longitude = newLongitude;
            _gpGgaLat = _Latitude;
            _gpGgaLng = _Longitude;
            Log.wtf(TAG, "GGA => LAT = " + _gpGgaLat + " LON = " + _gpGgaLng);
            if (lineArr[5].equals("W"))
            {
                _Longitude = -_Longitude;
                _gpGgaLng = _Longitude;
            }

            String hdopStr = lineArr[8];
            if (TextUtils.isEmpty(hdopStr))
                hdopStr = "1000.0";
            double hdop = Double.parseDouble(hdopStr);

            Log.wtf("GGA", "Moving: " + mIsMoving + ", HDOP: " + hdop);

            double ggaSpeed;
            if (_moving > 1 && fixQuality > 0)
            {
                if (_firstTime && _validStr.equals("A"))
                {
                    _lastLat = _Latitude;
                    _lastLon = _Longitude;
                    _firstTime = false;
                }

                if (mState == MeterState.Hired)
                {
                    double distanceDiff = calculateDistance(_lastLat, _lastLon, _Latitude, _Longitude);
                    if (isValidDouble(distanceDiff) && distanceDiff > 0) {
                        ggaSpeed = _timeDelta > 0 ? 1000.0 * distanceDiff / _timeDelta : 0;
                        Log.d(TAG, String.format("distanceDiff = %.02f, newDist = %.02f",
                                distanceDiff, _newDistance + distanceDiff));
                        Log.d(TAG, String.format("ggaSpeed = %.02f, rmcSpeed = %.02f",
                                ggaSpeed, _lastRmcSpeed));

                        if (_timeDelta > 0) {
                            float q;
                            if (_localeStr.equals(MPH))
                                q = (float)(_lastRmcSpeed * 0.000621371192) / 3.6f;
                            else
                                q = (float)_lastRmcSpeed / 3.6f;
                            KalmanLatLong k = new KalmanLatLong(q);
                            k.SetState(_lastLat, _lastLon, 10f, _timeDelta);
                            k.Process(_Latitude, _Longitude, 10f, _timeDelta);
                            Log.wtf("KALMAN", String.format(Locale.ENGLISH,
                                    "Lat: [%f vs %f], Lon: [%f vs %f], Accuracy: %f",
                                    _Latitude, k.get_lat(), _Longitude, k.get_lng(), k.get_accuracy()));
                            double kDiff =
                                    calculateDistance(_Latitude, _Longitude, k.get_lat(), k.get_lng());
                            double kDiff2 =
                                    calculateDistance(_lastRmcLat, _lastRmcLng, k.get_lat(), k.get_lng());
                            Log.wtf("KALMAN",
                                    String.format(Locale.ENGLISH, "DIFF1: %f, DIFF2: %f", kDiff, kDiff2));
//                            if (Math.abs(distanceDiff -  kDiff) > _clickDist / 5)
//                                distanceDiff = kDiff;
                        }
                    }

                    _newDistance += distanceDiff;
                    boolean mUseJumpsFilter = true;
                    if (mUseJumpsFilter && isValidDouble(_speedDistance) &&  _speedDistance > _newDistance) {
                        _newDistance = (_newDistance + _speedDistance) / 2.0;
                    }

                    _lastLon = _Longitude;
                    _lastLat = _Latitude;
                }
                _moving = 0;

                if (mState == MeterState.Hired)
                {
                    if (_newDistance >= _minimumDistance && !_addClick)
                    {
                        _addClick = true;
                        return true;
                    }

                    //_totalFare = calculateTotalFare(_newDistance);
                    //updateUI(_totalFare);
                    updateUI(-1);
                }
            }
        }
        catch (Exception ex)
        {
            mPrefs.edit()
                    .putInt("_currentTariff", mCurrentTariff)
                    .putFloat("_newDistance", (float)_newDistance)
                    .putString("_gpRmcLat", String.valueOf(_gpRmcLat))
                    .putString("_gpRmcLng", String.valueOf(_gpRmcLng))
                    .putString("_gpGgaLat", String.valueOf(_gpGgaLat))
                    .putString("_gpGgaLng", String.valueOf(_gpGgaLng))
                    .putInt("_startFare", _startFare)
                    .putInt("_state", mState.ordinal())
                    .commit();

            Log.e(TAG, "[2] handleGGA(): " + ex.getMessage() + Arrays.toString(ex.getStackTrace()));
            Log.e(TAG, "[2-1] handleGGA(): " + Arrays.toString(lineArr));

            return false;
        }

        mPrefs.edit()
                .putInt("_currentTariff", mCurrentTariff)
                .putFloat("_newDistance", (float)_newDistance)
                .putString("_gpRmcLat", String.valueOf(_gpRmcLat))
                .putString("_gpRmcLng", String.valueOf(_gpRmcLng))
                .putString("_gpGgaLat", String.valueOf(_gpGgaLat))
                .putString("_gpGgaLng", String.valueOf(_gpGgaLng))
                .putInt("_startFare", _startFare)
                .putInt("_state", mState.ordinal())
                .commit();

        return true;
    }
*/
//    private static boolean isValidPosition(double newLatitude, double newLongitude) {
//        return !(Double.isNaN(newLatitude) || Double.isInfinite(newLatitude) ||
//                Double.isNaN(newLongitude) || Double.isInfinite(newLongitude));
//    }

    private static boolean isValidDouble(double value) {
        return !(Double.isNaN(value) || Double.isInfinite(value));
    }

    //format (dd.dddd) to decimal minute format (ddmm.mmmm).
    //Example 58.65375° => 5839.225 (58° 39.225min)
//    private static double decimalToFractionalDegrees(double dec) {
//        double deg = Math.floor(dec / 100);
//        return (round(deg + ((dec - (deg * 100)) / 60), 5)); //5839.2250
//    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        if (!isValidDouble(value)) {
            Log.wtf(TAG, "ROUND VALUE = NaN or Infinity");
            return 0;
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }

    /**
     * Computes the distance in METERS
     *
     * @param lat1 Src Latitude
     * @param lon1 Src Longitude
     * @param lat2 Dst Latitude
     * @param lon2 Dst Longitude
     * @return Distance in meters
     */
    public static double calculateDistance(double lat1, double lon1, double lat2, double lon2)
    {
        if ((lat1 == lat2) && (lon1 == lon2))
        {
            return 0;
        }

        if ((lat1 == 0) && (lon1 == 0))
        {
            return 0;
        }

        float[] results = new float[3];
        Location.distanceBetween(lat1, lon1, lat2, lon2, results);

        return results[0];
    }

//    void handleDistTick(int ticks)
//    {
//        _totalFare = calculateTotalFare(_newDistance) - _startFare;
//        updateMeterControl((_startFare + _totalFare) / _currencyFactor);
//    }

//    void updateMeterControl(final double fare) {
//
//        RunOn.mainThread(new Runnable() {
//            @Override
//            public void run() {
//                double f = ((int)(fare * 10)) / 10.0;
//                setValue(String.valueOf(f), true, mFare, mFareBkg);
//            }
//        });
//    }

    final Object mTVlock = new Object();
    void safeUpdateTextView(final TextView tv, final String text) {

        Log.wtf(TAG, "INFO => " + text);
        RunOn.mainThread(new Runnable() {
            @Override
            public void run() {
                synchronized (mTVlock) {
                    tv.setText(text);
                }
            }
        });
    }

    void safeUpdateNmea(final String text) {

        Log.wtf(TAG, "INFO => " + text);
        RunOn.mainThread(new Runnable() {
            @Override
            public void run() {
                mNmeaInfo.setText(text.trim().replaceAll("[\r\n|\r|\n]", ""));
            }
        });
    }

    private void findAndApplyTariff(int idx)
    {
        if (idx > -1) {
            _tariff = mTariffs.get(idx);
        } else {
            _tariff = getTariff();
        }
        _clickDist = _tariff.mClickDist;
//        _waitTime = _tariff.mClickWaiting;
        _startFare = _tariff.mMinCharge;
        _totalFare = _startFare;
        _currencyFactor = _tariff.mCurrencyFactor;
        _minimumDistance = _tariff.mMinDistance;

        mNmeaProcessor.setMinimumDistance(_minimumDistance);
        // Changeover speed in KM/H
//        _minSpeed = _tariff.mChargeForWating ? _minimumDistance * 60.0 / 1000.0 : 1.0;
    }

    Tariff getTariff() {
        Tariff tariff = new Tariff();
        tariff.mMinCharge = 30;
        tariff.mMinDistance = 1609;
        tariff.mCharge = 10;
        tariff.mWaitingCost = 10;
        tariff.mClickWaiting = 48;
        tariff.mClickDist = 133;

        return tariff;
    }

    public static class Tariff implements Serializable
    {
        public int mID;
        public String mVehicleType;

        public double mMinDistance;
        public int mMinCharge;

        public int mClickDist;
        public int mCharge;

        public boolean mChargeForWating = false;
        public int mClickWaiting;
        public int mWaitingCost;

        public boolean mWeekDay = true;
        public String mDescription;

        public int mCurrencyFactor;
    }

    private void initVariables()
    {
        mFirstTime = true;

        mSpeedEstimatedDistance = 0;
        mLastGpsFixPoll = 0;
        mLastNonGpsSpeed = 0;

        mNmeaProcessor.initVariables();
        _totalFare = 0;
        _tariff = null;

        findAndApplyTariff(mCurrentTariff);

        mPrefs.edit()
            .putInt("_currentTariff", mCurrentTariff)
            .putFloat("_newDistance", (float) mNmeaProcessor.get_newDistance())
            .putString("_gpRmcLat", String.valueOf(mNmeaProcessor.get_gpRmcLat()))
            .putString("_gpRmcLng", String.valueOf(mNmeaProcessor.get_gpRmcLng()))
            .putString("_gpGgaLat", String.valueOf(mNmeaProcessor.get_gpGgaLat()))
            .putString("_gpGgaLng", String.valueOf(mNmeaProcessor.get_gpGgaLng()))
            .putInt("_startFare", _startFare)
        .commit();
    }

    GpsStatus.NmeaListener mNmeaListener =  new GpsStatus.NmeaListener() {
        public void onNmeaReceived(long timestamp, String nmea) {

//            if (mState != MeterState.Hired || mIsTestNmea)
//                return;
            if (mIsTestNmea)
                return;

            //$GPRMC,123212.0,A,5212.356194,N,00007.654499,E,0.0,0.0,220913,3.3,W,A*11
            if (nmea.startsWith("$GPRMC") || nmea.startsWith("$GPGGA") || nmea.startsWith("$GPGSA"))
                Log.wtf(TAG, nmea);
            else {
                Log.d(TAG, nmea);
                return;
            }
            logNmea(nmea);
            safeUpdateNmea(nmea);

            final String nmeaStr = nmea;
            RunOn.mainThread(new Runnable() {
                @Override
                public void run() {
                    mNmeaProcessor.meterTick(nmeaStr, mState);
                }
            });
            //meterTick(nmea);
        }
    };

    public static void logNmea(String log) {

        if (!mSaveLog)
            return;

        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        String pathSync = root + "/nmea.txt";
        File f = new File(pathSync);
        try {
            FileWriter fw = new FileWriter(f, true);
            fw.append(log).append("\n");
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadTariffs() {
        CSVReader reader;
        String [] nextLine;

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
        String defaultTariff = sp.getString("default_tariff", "");

        mTariffs.clear();

        Tariff t0 = new Tariff();
        if (TextUtils.isEmpty(defaultTariff)) {
            t0.mID = 0;
            t0.mVehicleType = "car";
            t0.mMinDistance = 1609;
            t0.mMinCharge = 300;
            t0.mCharge = 10;
            t0.mClickDist = 133;
            t0.mChargeForWating = false;
            t0.mWaitingCost = 10;
            t0.mClickWaiting = 48;
            t0.mWeekDay = false;
            t0.mDescription = "default";
            t0.mCurrencyFactor = 100;
        } else {
            try {
                JSONObject jObj = new JSONObject(defaultTariff);

                t0.mID = jObj.getInt("id");
                t0.mVehicleType = jObj.getString("vehicleType");
                t0.mMinDistance = jObj.getInt("minDistance");
                t0.mMinCharge = jObj.getInt("minCharge");
                t0.mCharge = jObj.getInt("charge");
                t0.mClickDist = jObj.getInt("clickDist");
                t0.mChargeForWating = jObj.getBoolean("chargeForWaiting");
                t0.mWaitingCost = jObj.getInt("waitingCost");
                t0.mClickWaiting = jObj.getInt("clickWaiting");
                t0.mWeekDay = jObj.getBoolean("weekDay");
                t0.mDescription = jObj.getString("description");
                t0.mCurrencyFactor = jObj.getInt("currencyFactor");
            } catch (JSONException e) {
                e.printStackTrace();

                t0.mID = 0;
                t0.mVehicleType = "car";
                t0.mMinDistance = 1609;
                t0.mMinCharge = 300;
                t0.mCharge = 10;
                t0.mClickDist = 133;
                t0.mChargeForWating = false;
                t0.mWaitingCost = 10;
                t0.mClickWaiting = 48;
                t0.mWeekDay = false;
                t0.mDescription = "default";
                t0.mCurrencyFactor = 100;
            }
        }
        mTariffs.add(t0);

        try {
            String path = Environment.getExternalStorageDirectory().toString() + "/tariffs.csv";
            reader = new CSVReader(new FileReader(path));

            // Read headers
            reader.readNext();

            while ((nextLine = reader.readNext()) != null) {
                // nextLine[] is an array of values from the line
                Tariff t = new Tariff();
                t.mID = Integer.parseInt(nextLine[0]);
                t.mVehicleType = nextLine[1];
                t.mMinDistance = Double.parseDouble(nextLine[2]);
                t.mMinCharge = Integer.parseInt(nextLine[3]);
                t.mCharge = Integer.parseInt(nextLine[4]);
                t.mClickDist = Integer.parseInt(nextLine[5]);
                int chargeForWaiting = Integer.parseInt(nextLine[6]);
                t.mChargeForWating = chargeForWaiting == 1;
                t.mWaitingCost = Integer.parseInt(nextLine[7]);
                t.mClickWaiting = Integer.parseInt(nextLine[8]);
                int weekDay = Integer.parseInt(nextLine[9]);
                t.mWeekDay = weekDay == 1;
                t.mDescription = nextLine[10];
                t.mCurrencyFactor = Integer.parseInt(nextLine[11]);

                mTariffs.add(t);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int calculateTotalFare(double dist) {
        int fare = _startFare;

        if (dist < _minimumDistance)
            return fare;

        int chargeableDelta = (int)round((dist - _minimumDistance + _clickDist) / _clickDist,0);
        fare += chargeableDelta * _tariff.mCharge;

        return fare;
    }

    public void setData(int tariffIdx, String extras) {

        loadTariffs();

        String tariff = String.format(Locale.UK, "%02d", tariffIdx);
        setValue(tariff, false, mTariff, mTariffBkg);
        if (!TextUtils.isEmpty(extras))
            setValue(extras, true, mExtras, mExtrasBkg);

        setValue("0.00", true, mFare, mFareBkg);
        setValue("0", false, mSpeed, mSpeedBkg);

        try {
            mCurrentTariff = tariffIdx;
            if (mCurrentTariff >= mTariffs.size())
                mCurrentTariff = 0;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            mCurrentTariff = 0;
        }
        findAndApplyTariff(mCurrentTariff);
        initVariables();

        mCurrentTariff = 0;
        setState(MeterState.ForHire);
        updateUI(0);

        checkGpsStatus();
    }

    private void checkGpsStatus() {
//        int gpsStatusIdx = ((OiDriverActivity)getActivity()).getGpsStatusIdx();
//
//        if (!mUsePoll) {
//            GeoFix fix = mGpsProvider.getLocation();
//            if (fix == GeoFix.NO_FIX || gpsStatusIdx == 0)
//                showGpsWaitingDialog();
//            else
//                hideGpsWaitingDialog();
//        } else if (gpsStatusIdx == 0)
//            showGpsWaitingDialog();
//        else
//            hideGpsWaitingDialog();
    }

    private String formatDistance(String label, double distance) {
        String distInfo;
        if (mNmeaProcessor.get_localeStr().equalsIgnoreCase(MPH))
            distInfo = String.format(Locale.UK, "%s: %.02f MI", label, distance * 0.000621371192);
        else
            distInfo = String.format(Locale.UK, "%s: %.02f KM", label, distance * 0.001);
        return distInfo;
    }

    private String formatSpeed(double speed) {
        String speedInfo;
        speedInfo = String.format(Locale.ENGLISH, "%d",
                (int)round(mNmeaProcessor.get_localeStr().equalsIgnoreCase(MPH) ? speed * 2.23693629d : speed * 3.6d, 0));
        return speedInfo;
    }

    private void updateUI(final int fare) {

        final String distInfo = formatDistance("NMEA", mNmeaProcessor.get_newDistance());
        final String speedInfo = formatDistance("LM", mMapDistance + mSpeedEstimatedDistance);

        Log.wtf(TAG, "GMAPS: " + OiDriverService.speed * 2.23693629d + "MPH, RMC: " +
                mNmeaProcessor.get_lastRmcSpeed() * 2.23693629d + "MPH, LM: " +
                mSpeedLM * 2.23693629d + "MPH");

        if (fare > -1) {
            double f = ((int)(fare * 10 / 10.0)) / _currencyFactor;
            setValue(String.valueOf(f), true, mFare, mFareBkg);
        }

        mJourneyDist.setText(distInfo);
        mSpeedDist.setText(speedInfo);
    }

    public void hideMeterFragment()
    {
        // Stop meter
        if (mIsTestNmea && mTestTimer != null) {
            mTestTimer.cancel();
            mTestTimer = null;
        }
        setState(MeterState.Stopped);
        mPrefs.edit().putInt("_state", mState.ordinal()).commit();
        //
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left,
                R.anim.in_from_left, R.anim.out_to_right);
        ft.hide(this);
        ft.commitAllowingStateLoss();

        OiDriverActivity.mIsTaximeterVisible = false;
        mPrefs.edit().putBoolean("isMeterVisible", false).commit();
    }

    public void restoreMeterState() {

        mCurrentTariff = mPrefs.getInt("_currentTariff", mCurrentTariff);
        mNmeaProcessor.set_newDistance(1.0d * mPrefs.getFloat("_newDistance", 0f));
        mNmeaProcessor.set_gpRmcLat(Double.parseDouble(mPrefs.getString("_gpRmcLat", "0.00")));
        mNmeaProcessor.set_gpRmcLng(Double.parseDouble(mPrefs.getString("_gpRmcLng", "0.00")));
        mNmeaProcessor.set_gpGgaLat(Double.parseDouble(mPrefs.getString("_gpGgaLat", "0.00")));
        mNmeaProcessor.set_gpGgaLng(Double.parseDouble(mPrefs.getString("_gpGgaLng", "0.00")));
        _startFare = mPrefs.getInt("_startFare", _startFare);
        int state = mPrefs.getInt("_state", mState.ordinal());
        mState = MeterState.values()[state];
        switch (mState) {
            case ForHire: mForHire.performClick(); break;
            case Hired: mHired.performClick(); break;
            case Stopped: mStopped.performClick(); break;
        }
    }

    public void sendEmail(final String[] addr, final String subject, final String body) {

        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(android.content.Intent.EXTRA_EMAIL, addr);
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(android.content.Intent.EXTRA_TEXT, body);

		intent.putExtra(android.content.Intent.EXTRA_STREAM,
				Uri.parse("file://" + Environment.getExternalStorageDirectory() + "/nmea.txt"));
        Intent chooserIntent = Intent.createChooser(intent, "TAXI");
        startActivity(chooserIntent);
    }

    Integer counter = 0;
    TextView mGmapGPS;
    double mMapDistance = 0;
    double mSpeedLM;

    private double clampSpeed(double speed) {
//        if (speed < mNmeaProcessor.getSpeedThreshold() || (!mGpsProvider.mIsMoving || System.currentTimeMillis() - mGpsProvider.mMovementTimestamp > 60000))
//            return 0;
        return speed;
    }

    public void onLocationChanged(Location location) {

        if (location == null)
            return;

        double lat = location.getLatitude();
        double lon = location.getLongitude();
        long time = location.getTime() / 1000;
        float accuracy = location.getAccuracy();

        double speed = 0;
        double dist = 0, dist2;
        long tm = 1;
        try {
            if (location.hasSpeed()) {
                speed = location.getSpeed() * 1.0;
    //            if (speed < mNmeaProcessor.getSpeedThreshold())
    //                speed = 0;
                speed = clampSpeed(speed);
                if (mState == MeterState.Hired) {
                    dist = speed * (time - times[(counter + (data_points - 1)) % data_points]);
                    dist2 = calculateDistance(lat, lon,
                            positions[(counter+(data_points - 1)) % data_points][0],
                            positions[(counter + (data_points -1)) %data_points][1]);
                    if (dist > dist2) {
                        dist = (dist2 + dist) / 2.0;
                    } else {
                        dist = dist2;
                    }
                }
            } else {
                if (mState == MeterState.Hired) {
                        // get the distance and time between the current position, and the previous position.
                        // using (counter - 1) % data_points doesn't wrap properly
                        dist = calculateDistance(lat, lon,
                                        positions[(counter + (data_points - 1)) % data_points][0],
                                        positions[(counter + (data_points - 1)) % data_points][1]);
                        tm = time - times[(counter + (data_points - 1)) % data_points];
                        if (tm <= 0)
                            tm = 1;
                }
                speed = dist / tm; // m/s
            }
            speed = clampSpeed(speed);
        }
        catch (NullPointerException e) {
            //all good, just not enough data yet.
        }

        positions[counter][0] = lat;
        positions[counter][1] = lon;
        times[counter] = time;
        counter = (counter + 1) % data_points;

        // Recalculate 1 MPH => m/s
//        if (speed < 1.60934)//0.44704)
//            speed = 0;
        mSpeedLM = speed;
        if (mSpeedLM < 0)
            mSpeedLM = 0.0;
        if (mFirstTime)// && mSpeedLM * 2.23693629d < 5)
            mSpeedLM = 0.0;

        setValue(formatSpeed(mSpeedLM), false, mSpeed, mSpeedBkg);
        Log.wtf(TAG, "CURRENT SPEED: " + formatSpeed(mSpeedLM));
        mLastNonGpsSpeed = mSpeedLM;
        if (mState != MeterState.Hired)
            return;

//        if (mGpsProvider.mIsMoving && dist >= 0.5)
        if (dist > 1.0 && !mFirstTime && accuracy < 30)
            mMapDistance += dist;
        mFirstTime = false;

        _totalFare = calculateTotalFare(mMapDistance);
        updateUI(_totalFare);
    }

    ////////////////////
    // From Refresher //
    ////////////////////
    @Override
    public void refresh() {
        if (!mUsePoll)
            return;

        GeoFix fix = mGpsProvider.getLocation();
        if (fix == GeoFix.NO_FIX)
        {
            Log.w(TAG, String.format("REFRESH: NO_FIX"));
            return;
        }
        hideGpsWaitingDialog();

        if (!mUsePoll)
            this.onLocationChanged(fix.getLocation());
    }

    @Override
    public void forceRefresh() {
        refresh();
    }

    int mProviderStatus;

    @Override
    public void providerStateChanged(String provider, int status, Bundle extras) {
        if (status != mProviderStatus) {
//            if (status == LocationProvider.OUT_OF_SERVICE) {
//                showGpsWaitingDialog();
//            } else {
//                hideGpsWaitingDialog();
//            }
            mProviderStatus = status;
        }
    }

//    private void showGpsWaitingDialog() {
//        mButtonsPanel.setVisibility(View.INVISIBLE);
//        mNoGpsView.setVisibility(View.VISIBLE);
//    }

    private void hideGpsWaitingDialog() {
        mNoGpsView.setVisibility(View.GONE);
        mButtonsPanel.setVisibility(View.VISIBLE);
    }

    /**
     * ACCELEROMETER
     */
/*
    static final int TIMER_DONE = 2;
    static final int START = 3;
    static final int CAL_TIMER_DONE = 4;
    static final int ERROR = 5;

    private static final long UPDATE_INTERVAL = 500;
    private static final long MEASURE_TIMES = 20;

    private XYZAccelerometer xyzAcc;
    private Timer timer;
    int counterSensor;
    private MeasureData mdXYZ;

    // handler for async events
    Handler hRefresh = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case TIMER_DONE:

                    onMeasureDone();
                    String es1 = Float.toString(Math.round(mdXYZ.getLastSpeedKm()*100)/100f);
                    Toast.makeText(OiDriverApp.getAppContext(), "DONE: " + es1, Toast.LENGTH_LONG).show();
                    break;
                case START:
                    timer = new Timer();
                    timer.scheduleAtFixedRate(
                            new TimerTask() {

                                public void run() {
                                    dumpSensor();
                                }
                            },
                            0,
                            UPDATE_INTERVAL);

                    break;
                case ERROR:
                    Toast.makeText(OiDriverApp.getAppContext(), "ERROR", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    void dumpSensor() {
        ++counterSensor;
        mdXYZ.addPoint(xyzAcc.getPoint());

        if (counterSensor > MEASURE_TIMES) {
            timer.cancel();
            hRefresh.sendEmptyMessage(TIMER_DONE);
        }
    }

    private void setAccelerometer() {
        xyzAcc = new XYZAccelerometer();
        mSensorManager.registerListener(xyzAcc,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_GAME);
    }

    private void onMeasureDone() {
        try {
            mdXYZ.process();
            long now = System.currentTimeMillis();
            mdXYZ.saveExt(OiDriverApp.getAppContext(), Long.toString(now) + ".csv");
        } catch (Throwable ex) {
            Toast.makeText(OiDriverApp.getAppContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
*/
    @Override
    public void onRmcProcessed() {
        mPrefs.edit()
                .putInt("_currentTariff", mCurrentTariff)
                .putFloat("_newDistance", (float)mNmeaProcessor.get_newDistance())
                .putString("_gpRmcLat", String.valueOf(mNmeaProcessor.get_gpRmcLat()))
                .putString("_gpRmcLng", String.valueOf(mNmeaProcessor.get_gpRmcLng()))
                .putString("_gpGgaLat", String.valueOf(mNmeaProcessor.get_gpGgaLat()))
                .putString("_gpGgaLng", String.valueOf(mNmeaProcessor.get_gpGgaLng()))
                .putInt("_startFare", _startFare)
                .commit();
        updateUI(-1);
    }

    @Override
    public void onGgaProcessed() {
        mPrefs.edit()
                .putInt("_currentTariff", mCurrentTariff)
                .putFloat("_newDistance", (float)mNmeaProcessor.get_newDistance())
                .putString("_gpRmcLat", String.valueOf(mNmeaProcessor.get_gpRmcLat()))
                .putString("_gpRmcLng", String.valueOf(mNmeaProcessor.get_gpRmcLng()))
                .putString("_gpGgaLat", String.valueOf(mNmeaProcessor.get_gpGgaLat()))
                .putString("_gpGgaLng", String.valueOf(mNmeaProcessor.get_gpGgaLng()))
                .putInt("_startFare", _startFare);
        updateUI(-1);
    }

    private void querySatellites() {
        int mNumSatellites = mNmeaProcessor.getNumSatellites();
        safeUpdateTextView(mSalettitesInfo, "SATELLITES: " + mNumSatellites);
        checkGpsStatus();
    }

    /**
     * EXPERIMENT: DO NOT START ANY PROVIDER, RUN A TIMER AND ASK FOR LAST KNOWN LOCATION
     */

    /** After this time duration, use the network location instead of old GPS value */
    public static final int GPS_TIMEOUT_SEC = 60;
    private long mLastGpsFixPoll = 0;
    private double mSpeedEstimatedDistance = 0;
    private double mLastNonGpsSpeed = 0;

    private void pollLastKnownLocation() {
        if (mBackupTimer != null) {
            mBackupTimer.cancel();
            mBackupTimer = null;
        }

        mLastGpsFixPoll = 0;
        mLastNonGpsSpeed = 0;
        mSpeedEstimatedDistance = 0;

//        mdXYZ = new MeasureData(1000);

        mBackupTimer = new Timer("GPS-BACKUP");
        mBackupTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (!mUsePoll)
                    return;

                Location gpsLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                Location netLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                final Location theBest = choose(gpsLocation, netLocation);
                if (theBest == null)
                    return;
                RunOn.mainThread(new Runnable() {
                    @Override
                    public void run() {
                        long currTime = System.currentTimeMillis();
                        long delta = mLastGpsFixPoll > 0 ? currTime - theBest.getTime() : 0;
                        if (delta < 0)
                            delta = 0;
                        delta /= 1000;
                        Log.wtf(TAG, "POLL: [ " + delta + "] " + theBest.toString());
                        if (delta <= 30) {
                            onLocationChanged(theBest);
                            mSpeedEstimatedDistance = 0;
                        } /*else if (mState == MeterState.Hired) {
                            long noGpsDelta = mLastGpsFixPoll > 0 ? currTime - mLastGpsFixPoll : 0;
                            calculateDistanceFromLastSpeed(noGpsDelta / 1000);
//                            noGpsDelta /= 1000;
//                            double speed = mLastNonGpsSpeed + mAccel * noGpsDelta;
//                            // We set 5PMH as a threshold
////                            if (speed * 2.23693629d < 5 && mLastNonGpsSpeed > 0)
////                                speed = 0;
//                            if (speed < 0)
//                                speed = 0;
//                            Log.wtf(TAG, String.format(Locale.ENGLISH, "SENSOR SPEED [%d]: %f => %f",
//                                    noGpsDelta, mAccel, speed * 2.23693629d));
//                            mLastNonGpsSpeed = speed;
                        }*/
                        mLastGpsFixPoll = currTime;

                    }
                });
            }
        }, 0, 1000);
    }

/*
    private void calculateDistanceFromLastSpeed(long timeDelta) {
        double dist = mLastNonGpsSpeed * timeDelta;

        if (mIsMoving && dist > 1.0)
            mSpeedEstimatedDistance += dist;

        _totalFare = calculateTotalFare(mMapDistance + mSpeedEstimatedDistance);
        updateUI(_totalFare);
    }
*/

    /**
     * Choose the better of two locations: If one location is newer and more
     * accurate, choose that. (This favors the gps). Otherwise, if one location
     * is newer, less accurate, but farther away than the sum of the two
     * accuracies, choose that. (This favors the network locator if you've
     * driven a distance and haven't been able to get a gps fix yet.)
     */
    private static Location choose(Location oldLocation, Location newLocation) {
        if (oldLocation == null)
            return newLocation;
        if (newLocation == null)
            return oldLocation;

        long timeDiff = newLocation.getTime() - oldLocation.getTime();
        if (timeDiff > GPS_TIMEOUT_SEC*1000) {
            //Log.d("geohunter", "Discarding GeoFix that's " + timeDiff/60000.0 + " min older");
            return newLocation;
        }
        if (timeDiff >= -GPS_TIMEOUT_SEC*1000) {
            float distance = newLocation.distanceTo(oldLocation);
            // Log.d("geohunter", "onLocationChanged distance="+distance +
            // "  provider=" + newLocation.getProvider());
            if (distance < 1 &&
                    Math.abs(newLocation.getAccuracy() - oldLocation.getAccuracy()) < 1)
                return oldLocation;

            if (newLocation.getAccuracy() <= oldLocation.getAccuracy())
                return newLocation;

            if (distance >= oldLocation.getAccuracy() + newLocation.getAccuracy()) {
                return newLocation;
            }
        } else {
            Log.d("geohunter", "Got "+timeDiff/1000+"s older GeoFix from " +
                    newLocation.getProvider() + " than old GeoFix from " +
                    oldLocation.getProvider());
        }
        return oldLocation;
    }

    /** SENSOR CALLBACKS **/

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION){
            float[] mGravity = event.values.clone();
            // Shake detection
            float x = mGravity[0];
            float y = mGravity[1];
            float z = mGravity[2];
            mAccelLast = mAccelCurrent;
            mAccelCurrent = FloatMath.sqrt(x * x + y * y + z * z);
            float delta = mAccelCurrent - mAccelLast;
            mAccel = mAccel * 0.9f + delta;
            // Make this higher or lower according to how much
            // motion you want to detect
            mIsMoving = mAccel > 0.001;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /** PAYMENT FORM **/
    private static final String CARD_IO_APP_TOKEN = "ea2c314a69c24f7dba6a1fd8c7196547";
    public static String mFareAmount = "";

    public void onScanPress() {

        mFareAmount = String.valueOf(_totalFare);

        // This method is set up as an onClick handler in the layout xml
        // e.g. android:onClick="onScanPress"

        Intent scanIntent = new Intent(OiDriverApp.getAppContext(), CardIOActivity.class);

        // required for authentication with card.io
        scanIntent.putExtra(CardIOActivity.EXTRA_APP_TOKEN, CARD_IO_APP_TOKEN);

        // customize these values to suit your needs.
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: true
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, true); // default: false

        // hides the manual entry button
        // if set, developers should provide their own manual entry mechanism in the app
        scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, false); // default: false

        // CARD_IO_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
        ((OiDriverActivity)getActivity()).startScanCardActivity(scanIntent);
    }
}