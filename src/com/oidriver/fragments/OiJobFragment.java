package com.oidriver.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.oidriver.AcceptedJob;
import com.oidriver.CommonUtils;
import com.oidriver.Constants;
import com.oidriver.JobStatusSender;
import com.oidriver.OiDriverActivity;
import com.oidriver.OiDriverApp;
import com.oidriver.R;
import com.oidriver.service.OiDriverService;


public class OiJobFragment extends Fragment implements OnClickListener {
	
	// Job screen controls
	private Button mBtnNav;
	private Button mBtnDOW;
	private Button mBtnAct;

	private TextView mJobId;
	private TextView mName;
	private TextView mTime;
	private ImageView mSearch;
	private TextView mAddress;
	////private RelativeLayout mPopoverLayout = null;
	
	private boolean navigated = false;
	private AlertDialog mPopup;
    private AlertDialog mNewDestAlert;

	public static OiJobFragment newInstance() {
		return new OiJobFragment();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.page_job, null);
		
		////mPopoverLayout = (RelativeLayout) view.findViewById(R.id.layoutPopover);
		mSearch = (ImageView) view.findViewById(R.id.imageViewMagnifier);
		mBtnNav = (Button) view.findViewById(R.id.button_nav);
		mBtnDOW = (Button) view.findViewById(R.id.button_dow);
		mBtnAct = (Button) view.findViewById(R.id.button_act);

		mSearch.setOnClickListener(this);
		mBtnNav.setOnClickListener(this);
		mBtnDOW.setOnClickListener(this);
		mBtnAct.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View content = inflater.inflate(R.layout.action_popup, null);
				setupActButtonPopover(content);
				TextView notes = (TextView)content.findViewById(R.id.textViewNotes);
				AcceptedJob job = AcceptedJob.getActiveJob();
				if (job != null) {
					String notesStr = job.getNote();
					if (!TextUtils.isEmpty(notesStr))
						notes.setText(notesStr);
				}
				AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext())
						.setCancelable(false)
						.setView(content);
				mPopup = builder.create();
				WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
				lp.copyFrom(mPopup.getWindow().getAttributes());
				lp.width = WindowManager.LayoutParams.MATCH_PARENT;
				lp.height = WindowManager.LayoutParams.MATCH_PARENT;
				mPopup.show();
				mPopup.getWindow().setAttributes(lp);
			}
		});
		
		mJobId = (TextView) view.findViewById(R.id.textViewJobId);
		mName = (TextView) view.findViewById(R.id.textViewName);
		mTime = (TextView) view.findViewById(R.id.textViewTime);
		mAddress = (TextView) view.findViewById(R.id.textViewAddress);
		//resetJobUI();

        return view;
	}


    @Override
	public void onResume() {
		super.onResume();
		resetJobUI();
    }

    public void onClick(View v) {
		if (v == mBtnNav)
		{
			if (!navigated)
			{
				Log.i(OiDriverApp.TAG, "NOT NAVIGATED, ABOUT TO NAVIGATE");
				AcceptedJob job = AcceptedJob.getActiveJob();
				((OiDriverActivity)getActivity()).showMapWithPassenger(job);
			}
			else
			{
				Log.i(OiDriverApp.TAG, "NAVIGATED, ABOUT TO RESET");
				((OiDriverActivity)getActivity()).showMapWithoutPassenger();
			}
			navigated = !navigated;
		}
		else if (v == mBtnDOW)
		{
			AcceptedJob job = AcceptedJob.getActiveJob();
			if (job != null)
			{
				String jobId = job.getJobId();
				boolean success;
				int intStatus = job.getIntStatus();
				Log.i(OiDriverApp.TAG, "JOB STATUS: " + intStatus);
				switch (intStatus)
				{
				case 0:
				case 1:
					{
						success = AcceptedJob.updateJobStatusByJobId(jobId, Constants.STATUS_DOW);
						if (success)
						{
							OiDriverService.dbHelper.deleteJobByJobId(jobId);
							((OiDriverActivity)getActivity()).mBookingsFragment.startLoadingJobs();
							((OiDriverActivity)getActivity()).playSoundSync(R.raw.driver_on_way, null);
							mBtnDOW.setBackgroundResource(R.drawable.pob_selector);
							
							OiDriverService.sendIQForGPS(false, "JOB");
							success = sendJobStatusToServer(job.getSentBy(), jobId, Constants.DISPATCHER_STATUS_DOW);
							if (!success)
							{
								((OiDriverActivity)getActivity()).showToast("Can't send DOW to server", Toast.LENGTH_LONG);
							}
						}
						else
						{
							Log.e(OiDriverApp.TAG, "[3] ERROR: couldn't update JOB[" + jobId + "]");
							((OiDriverActivity)getActivity()).showToast("Can't update JOB status", Toast.LENGTH_LONG);
						}
					}
					break;
				case 2:
					{
//                        mNewDestAlert = createNewDestinationDialog();
//                        mNewDestAlert.show();

                        success = AcceptedJob.updateJobStatusByJobId(jobId, Constants.STATUS_POB);
						if (!success)
						{
							Log.e(OiDriverApp.TAG, "[1] ERROR: couldn't update JOB[" + jobId + "]");
						}
						else
						{
							((OiDriverActivity)getActivity()).playSoundSync(R.raw.pass_on_board, null);
							mBtnDOW.setBackgroundResource(R.drawable.eoj_selector);
							TextView from = (TextView) getView().findViewById(R.id.textViewAddress);
							from.setText(job.getDropoffAddress(true));

							success = sendJobStatusToServer(job.getSentBy(), jobId, Constants.DISPATCHER_STATUS_POB);
							if (!success)
							{
								((OiDriverActivity)getActivity()).showToast("Can't send POB to server", Toast.LENGTH_LONG);
							}
						}
                        handleTaxiMeter();
                    }
					break;
				default:
					{
						//((OiDriverActivity)getActivity()).sendEOJ(job.getSentBy(), job.getJobId());
						//
						OiDriverActivity act = (OiDriverActivity)getActivity();
						if (!sendJobStatusToServer(job.getSentBy(), jobId, Constants.DISPATCHER_STATUS_EOJ))
						{
							act.showToast("Can't send EOJ to server", Toast.LENGTH_LONG);
						}
						success = AcceptedJob.deleteJobByJobId(jobId);
						if (!success)
							Log.e(OiDriverApp.TAG, "[2] ERROR: couldn't delete JOB[" + jobId + "]");
						act.hideViewSwitcher();
						act.mBookingsFragment.startLoadingJobs();
						act.resetToMyLocation();
						OiDriverService.dbHelper.insertJobDone(jobId, job.getFarePrice(), "cash");
						//
						((OiDriverActivity)getActivity()).playSoundSync(R.raw.job_completed, null);
					}
					break;
				}	
			}
			else
			{
				Toast.makeText(getActivity(), "[0] NO ACTIVE JOB FOUND!", Toast.LENGTH_LONG).show();
				((OiDriverActivity)getActivity()).hideJobFragment(true);
				((OiDriverActivity)getActivity()).resetToMyLocation();
			}
		}
		else if (v == mSearch)
		{
			((OiDriverActivity)getActivity()).showPopups();
		}
	}

    void setupActButtonPopover(View view)
	{
		final Button btnCall = (Button) view.findViewById(R.id.buttonCall);
		final Button btnReturnJob = (Button) view.findViewById(R.id.buttonReturnJob);
		final Button btnNoJob = (Button) view.findViewById(R.id.buttonNoJob);
		final Button btnOK = (Button) view.findViewById(R.id.buttonOK);
		final Button btnCancel = (Button) view.findViewById(R.id.buttonCancel);
		btnCall.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				AcceptedJob job = AcceptedJob.getActiveJob();
				if (job != null)
				{
					String tel = job.getTelephone();
					if (!TextUtils.isEmpty(tel))
						callOnPhone(tel);
					if (mPopup != null) mPopup.dismiss();
				}
				else
				{
					Toast.makeText(getActivity(), "[3] NO ACTIVE JOB FOUND!", Toast.LENGTH_LONG).show();
                    AcceptedJob.resetActiveJob();
                    ((OiDriverActivity)getActivity()).hideViewSwitcher();
				}
			}
		});
		btnReturnJob.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setTitle("Return Job Reason").setItems(
						R.array.return_job_reasons,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// The 'which' argument contains the index
								// position
								// of the selected item
								AcceptedJob job = AcceptedJob.getActiveJob();
								if (job != null) {
									final FragmentActivity act = getActivity();
									if (act != null)
									{
										String[] reasons = act.getResources().getStringArray(
														R.array.return_job_reasons);
										if (reasons.length > which) {
											((OiDriverActivity)act).getJobStatusSender()
													.sendReturnJobToServer(job.getSentBy(), job.getJobId(),
															reasons[which],
															new JobStatusSender.JobStatusSenderListener() {
																@Override
																public void onStatusSent(String jobId, boolean isSent) {
																	boolean success = AcceptedJob.deleteJobByJobId(jobId);
																	((OiDriverActivity)act).hideViewSwitcher();
																}
															});
											//mPopoverLayout.setVisibility(View.GONE);
											if (mPopup != null) mPopup.dismiss();
											((OiDriverActivity)act).hideViewSwitcher();
										}
									}
								}
								else
								{
									Toast.makeText(getActivity(), "[2] NO ACTIVE JOB FOUND!", Toast.LENGTH_LONG).show();
                                    AcceptedJob.resetActiveJob();
                                    ((OiDriverActivity)getActivity()).hideViewSwitcher();
								}
							}
						});
				builder.show();

			}
		});
		btnNoJob.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//mPopoverLayout.setVisibility(View.GONE);
				if (mPopup != null) mPopup.dismiss();

				AcceptedJob job = AcceptedJob.getActiveJob();
				if (job != null)
				{
					((OiDriverActivity)getActivity()).getJobStatusSender()
							.sendNoJob(job.getSentBy(), job.getJobId(),
									new JobStatusSender.JobStatusSenderListener() {
										@Override
										public void onStatusSent(String jobId, boolean isSent) {
											boolean success = AcceptedJob.deleteJobByJobId(jobId);
											((OiDriverActivity)getActivity()).hideViewSwitcher();
										}
									});
					((OiDriverActivity)getActivity()).hideViewSwitcher();
				}
				else
				{
					Toast.makeText(getActivity(), "[1] NO ACTIVE JOB FOUND!", Toast.LENGTH_LONG).show();
                    AcceptedJob.resetActiveJob();
                    ((OiDriverActivity)getActivity()).hideViewSwitcher();
				}
			}
		});
		btnOK.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//mPopoverLayout.setVisibility(View.GONE);
				if (mPopup != null) mPopup.dismiss();
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//mPopoverLayout.setVisibility(View.GONE);
				if (mPopup != null) mPopup.dismiss();
			}
		});
	}

	private void callOnPhone(String phoneNumber) {
		try {
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri.parse("tel:" + phoneNumber));
			startActivity(callIntent);
		} catch (ActivityNotFoundException e) {
			((OiDriverActivity)getActivity()).showToast("Call failed : " + e.getMessage(), 4000);
		}
	}
	
	public void resetJobUI()
	{
		AcceptedJob job = AcceptedJob.getActiveJob();
		if (job != null)
		{
			int intStatus = job.getIntStatus();
			Log.i(OiDriverApp.TAG, "JOB STATUS: " + intStatus);
			mJobId.setText("#" + job.getJobId());
			mName.setText(job.getName());
			String notes = job.getNote();
			if (!TextUtils.isEmpty(notes)) {
				notes = notes.trim();
				if (!TextUtils.isEmpty(notes) && !notes.equalsIgnoreCase(getString(R.string.no_notes)))
					mName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_mail, 0, 0, 0);
			}
			mTime.setText(job.getFormattedBookingTime());
			switch (intStatus)
			{
			case 0:
			case 1:
				{
					mAddress.setText(job.getPickupAddress(true));
					mBtnDOW.setBackgroundResource(R.drawable.driver_on_way_selector);
				}
				break;
			case 2:
				{
					mAddress.setText(job.getPickupAddress(true));
					mBtnDOW.setBackgroundResource(R.drawable.pob_selector);
				}
				break;
			case 5:
				{
					mBtnDOW.setBackgroundResource(R.drawable.eoj_selector);
					mAddress.setText(job.getDropoffAddress(true));
				}
				break;
			}	
		}
	}

	private boolean sendJobStatusToServer(String sendToJid, String jobId, String jobStatus) {
		((OiDriverActivity)getActivity()).getJobStatusSender().setCurrentTask("");
		if (!TextUtils.isEmpty(jobId) && !TextUtils.isEmpty(jobStatus)) {
			Log.e(OiDriverApp.TAG, String.format("Sending %s for ID = %s", jobStatus, jobId));
			ContentValues cv = new ContentValues();
			cv.put("jobstatus", String.format("WK#,%s,%s,%s,3,0", jobId, "1", jobStatus));
			cv.put("jobid", jobId);
			cv.put("manager_jid", sendToJid);
			OiDriverService.enqueueJobStatusRequest(cv);

			return true;
		}
		return false;
	}

	public boolean getNavigatedState()
	{
		return navigated;
	}

    private AlertDialog createNewDestinationDialog() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View content = inflater.inflate(R.layout.custom_new_destination, null);
        if (content == null) {
            Toast.makeText(getActivity(), "Cannot inflate destination layout!", Toast.LENGTH_SHORT).show();
            return null;
        }

        final Button button1 = (Button)content.findViewById(R.id.button1);
        final Button button2 = (Button)content.findViewById(R.id.button2);
        AlertDialog newAlert = new AlertDialog.Builder(getActivity())
                .setTitle("CONFIRM DESTINATION")
                .setIcon(R.drawable.passenger_inverted)
                .setCancelable(false)
                .setView(content).create();

        button1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });
        button2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mNewDestAlert != null) mNewDestAlert.dismiss();
            }
        });
        return newAlert;
    }

    private void handleTaxiMeter() {//throws PackageManager.NameNotFoundException, ClassNotFoundException {
        Activity activity = getActivity();
        //CommonUtils.startExternapApp(activity, Constants.TAXIMETER_PACKAGE);
        if (activity == null)
            return;
        ((OiDriverActivity)activity).hideMeterFragment(false);
        ((OiDriverActivity)activity).setHired();
    }
}