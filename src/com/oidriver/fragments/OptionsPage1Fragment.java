package com.oidriver.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.QuickContactBadge;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.oidriver.AccountActivity;
import com.oidriver.Constants;
import com.oidriver.OiDriverApp;
import com.oidriver.R;
import com.oidriver.service.OiDriverService;

public class OptionsPage1Fragment extends Fragment {
	
	private QuickContactBadge mAvatar;
	//private TextView mInfo;
	private WebView mInfo;

	public static OptionsPage1Fragment newInstance() {
		OptionsPage1Fragment fragment = new OptionsPage1Fragment();
		return fragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.options_page1, null);
		
		mAvatar = (QuickContactBadge) view.findViewById(R.id.avatar);
		mInfo = (WebView) view.findViewById(R.id.webViewInfo);
		mInfo.setVisibility(View.VISIBLE);
		mInfo.setBackgroundColor(Color.WHITE);

        final WebSettings settings = mInfo.getSettings();

        settings.setUseWideViewPort(true);

		if (OiDriverService.driver != null)
		{
			updateDriverInfo();
		}

		final Button btnAccount = (Button) view.findViewById(R.id.buttonAccount);
        if (btnAccount != null)
            btnAccount.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), AccountActivity.class));
                }
            });

		final Button btnStatus = (Button) view.findViewById(R.id.buttonStatus);
        if (btnStatus != null) {
		    btnStatus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (OiDriverService.readyStatus) {
                        btnStatus.setText("SET RTW");
                    } else {
                        btnStatus.setText("SET BRK");
                    }
                    OiDriverService.readyStatus = !OiDriverService.readyStatus;
                    String message = String.format("WK#,0000,1,%s,3,0,%s", OiDriverService.readyStatus ?
                            Constants.DRIVER_STATUS_RTW : Constants.DRIVER_STATUS_BRK,
                            OiDriverService.currentZoneName);
                    OiDriverService.sendMessage(message, OiDriverApp.MANAGER_JID);

                    OiDriverService.sendIQForGPS(false, OiDriverService.readyStatus ?
                            Constants.DRIVER_STATUS_RTW : Constants.DRIVER_STATUS_BRK);

                    final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
                    sp.edit().putBoolean("RTW", OiDriverService.readyStatus).commit();
                    updateDriverInfo();
                }
            });
            if (OiDriverService.readyStatus)
            {
                btnStatus.setText("SET BRK");
            }
            else
            {
                btnStatus.setText("SET RTW");
            }
        }

		final Button btnLogout = (Button) view.findViewById(R.id.buttonLogout);
        if (btnLogout != null)
            btnLogout.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    FragmentActivity act = getActivity();
                    if (act != null) {
                        act.setResult(Activity.RESULT_FIRST_USER);
                        act.finish();
                    }
                }
            });

		final int[] pickupRanges = new int[] { R.id.radio0, R.id.radio1, R.id.radio2 };
		final int[] dropoffRanges = new int[] { R.id.radio4, R.id.radio5, R.id.radio6 };

		final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
		int pickupRange = sp.getInt("pickupRange", 0);
		int dropoffRange = sp.getInt("dropoffRange", 0);
		final RadioGroup rgPickup = (RadioGroup) view.findViewById(R.id.rgPickup);
        if (rgPickup != null) {
            rgPickup.check(pickupRanges[pickupRange]);
            rgPickup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    for (int i = 0; i < 3; ++i)
                        if (pickupRanges[i] == checkedId)
                        {
                            sp.edit().putInt("pickupRange", i).commit();
                            break;
                        }
                    updateDriverInfo();
                }
            });
        }
		final RadioGroup rgDropoff = (RadioGroup) view.findViewById(R.id.rgDropoff);
        if (rgDropoff != null) {
            rgDropoff.check(dropoffRanges[dropoffRange]);
            rgDropoff.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    for (int i = 0; i < 3; ++i)
                        if (dropoffRanges[i] == checkedId)
                        {
                            sp.edit().putInt("dropoffRange", i).commit();
                            break;
                        }
                    updateDriverInfo();
                }
            });
        }
        updateDriverInfo();
		return view;
	}

	private void updateDriverInfo()
	{
		final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
		boolean readyStatus = sp.getBoolean("RTW", false);
		int pickupRange = sp.getInt("pickupRange", 0);
		int dropoffRange = sp.getInt("dropoffRange", 0);
		String[] ranges = new String[] { "Local", "Middle", "Far" };
		String info1 = String.format(getString(R.string.driver_info_row1),
				OiDriverService.driver != null ? OiDriverService.driver.getName() : "");
		String info2 = String.format(getString(R.string.driver_info_row2), "1234");
		String info3 = String.format(getString(R.string.driver_info_row3),
				OiDriverService.driver != null ? OiDriverService.driver.getRegistration() : "");
		String info4 = String.format(getString(R.string.driver_info_row4), ranges[pickupRange]);
		String info5 = String.format(getString(R.string.driver_info_row5), ranges[dropoffRange]);
		String info6 = String.format(getString(R.string.driver_info_row6),  readyStatus ?
				Constants.DRIVER_STATUS_RTW : Constants.DRIVER_STATUS_BRK);
		StringBuilder builder = new StringBuilder();
		builder.append("<table style = 'font-size: 13px;'>").append(info1).append(info2).append(info3).append(info4).append(info5).append(info6).append("</table>");
		mInfo.loadData(builder.toString(), "text/html", "UTF-8");
	}
}