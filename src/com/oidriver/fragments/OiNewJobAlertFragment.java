package com.oidriver.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.oidriver.AcceptedJob;
import com.oidriver.Constants;
import com.oidriver.OiDriverActivity;
import com.oidriver.R;
import com.oidriver.runnable.RunOn;
import com.oidriver.service.OiDriverService;
import com.oidriver.xmpp.Util;

public class OiNewJobAlertFragment extends Fragment {

    private static final String ARG_TITLE = "title";
    private static final String ARG_JOB_ID = "job_id";
    private static final String ARG_PICKUP = "pickup";
    private static final String ARG_DROPOFF = "dropoff";

    public static final int ACCEPT_JOB_TIMEOUT = 20 * 1000;

    private ProgressBar mJobProgress;

    public static OiNewJobAlertFragment newInstance(String title, String jobId, String pickup, String dropoff) {
		OiNewJobAlertFragment fragment = new OiNewJobAlertFragment();
        Bundle data = new Bundle();
        data.putString(ARG_TITLE, title);
        data.putString(ARG_JOB_ID, jobId);
        data.putString(ARG_PICKUP, pickup);
        data.putString(ARG_DROPOFF, dropoff);
        fragment.setArguments(data);
		return fragment;
	}

    public interface NewJobListener {
        void onAccept(String jobId);
        void onReject(String jobId);
        void onTimedOut(String jobId);
        void onHidden();
    }

    private NewJobListener mListener;

    private String mTitle;
    private String mJobId;
    private String mPickup;
    private String mDropoff;

    CountDownTimer mJobAlertCountdownTimer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle data = savedInstanceState != null ? savedInstanceState : getArguments();

        mTitle = data.getString(ARG_TITLE);
        mJobId = data.getString(ARG_JOB_ID);
        mPickup = data.getString(ARG_PICKUP);
        mDropoff = data.getString(ARG_DROPOFF);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        setListener(getListener(NewJobListener.class, this));
    }

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.alert_new_job, null);

        TextView title = (TextView) view.findViewById(R.id.tvTitle);
        title.setText(mTitle);
        TextView pickupAddress = (TextView) view.findViewById(R.id.tvPickup);
        pickupAddress.setText(mPickup);
        TextView dropoffAddress = (TextView) view.findViewById(R.id.tvDropoff);
        dropoffAddress.setText(mDropoff);

        mJobProgress = (ProgressBar) view.findViewById(R.id.progressBarTimeout);

        Button mBtnAccept = (Button) view.findViewById(R.id.button1);
        mBtnAccept.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mListener != null) mListener.onAccept(mJobId);
            }
        });
        Button mBtnReject = (Button) view.findViewById(R.id.button2);
        mBtnReject.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mListener != null) mListener.onReject(mJobId);
            }
        });

        return view;
	}

    @Override
    public void onStart() {
        super.onStart();

        mJobAlertCountdownTimer = createCountDownTimer();
        RunOn.taskThread(new Runnable() {

            @Override
            public void run() {
                OiDriverActivity.playSoundSync(R.raw.new_job_offer,
                        new OiDriverActivity.MediaCallback() {

                            @Override
                            public void onFinished() {
                                mJobAlertCountdownTimer.start();
                            }
                        });
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mJobAlertCountdownTimer != null)
            mJobAlertCountdownTimer.cancel();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(ARG_JOB_ID, mJobId);
        outState.putString(ARG_PICKUP, mPickup);
        outState.putString(ARG_DROPOFF, mDropoff);
    }

    private void setListener(NewJobListener listener) {
        mListener = listener;
    }

    /**
     * A helper function that given a type and a fragment, will attempt to extract
     * the fragment's listener.
     *
     * @param cls - the listener's type
     * @param fragment - the fragment whose listener we're trying to find
     * @return - the fragment's listener or null if neither the activity or the parent fragment subclass cls
     */
    @SuppressWarnings("unchecked")
    public static <T> T getListener(Class<T> cls, android.support.v4.app.Fragment fragment) {
        FragmentActivity activity = fragment.getActivity();
        android.support.v4.app.Fragment parent = fragment.getParentFragment();
        android.support.v4.app.Fragment target = fragment.getTargetFragment();

        if(target != null && cls.isAssignableFrom(Util.getClazz(target))) {
            return (T) target;
        } else if(parent != null && cls.isAssignableFrom(Util.getClazz(parent))) {
            return (T) parent;
        } else if (activity != null && cls.isAssignableFrom(activity.getClass())){
            return (T) activity;
        }

        return null;
    }

    private CountDownTimer createCountDownTimer()
    {
        return new CountDownTimer(ACCEPT_JOB_TIMEOUT, 1000) {

            public void onTick(long millisUntilFinished) {
                if (mJobProgress != null)
                    mJobProgress.incrementProgressBy(1);
                //playSound(R.raw.job_timer);
                OiDriverActivity.playSoundSync(R.raw.job_timer, null);
            }

            public void onFinish() {
                if (mJobProgress != null)
                    mJobProgress.setProgress(mJobProgress.getMax());
                if (mListener != null) mListener.onTimedOut(mJobId);
            }
        };
    }

    public boolean removeFragment() {

        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.abc_slide_out_bottom);
        if (animation == null)
            return false;

        animation.setDuration(500);

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (mListener != null)
                    mListener.onHidden();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        //Start the animation.
        getView().startAnimation(animation);

        return true;
    }
}