package com.oidriver.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.oidriver.OiDriverApp;
import com.oidriver.R;
import com.oidriver.service.OiDriverService;

public class OptionsPage2Fragment extends Fragment {
	
	public static OptionsPage2Fragment newInstance() {
		OptionsPage2Fragment fragment = new OptionsPage2Fragment();
		return fragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.options_page2, null);
				
		final Button btnAlarm = (Button) view.findViewById(R.id.buttonAlarm);
		btnAlarm.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String panicMsg = String.format("<emergency><lat>%f</lat><lon>%f</lon></emergency>", OiDriverService.lat, OiDriverService.lon);
				OiDriverService.sendMessage(panicMsg, OiDriverApp.MANAGER_JID);
			}
		});
		
		return view;
	}
}