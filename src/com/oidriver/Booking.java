package com.oidriver;


import com.oidriver.xmpp.Util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Booking {
	
	private Integer id;
	private String bookingId;
	private String bookingTime;
	private String passengerName;
	private String zoneId;
	private String jobId;
	private String status;
	private String pickupAddress;
	private String dropoffAddress;
	private String pickupLat;
	private String pickupLon;
	private String dropoffLat;
	private String dropoffLon;
	private String notes;
	private String vehicleType;
	private String ratings;
	private String fare;
	private int bookingType;
	private boolean bidSent = false;
	private Date dtBookingTimestamp;
	private String sentby = OiDriverApp.MANAGER_JID;
	
	public Booking() {
		
	}
	
	public String getBookingId() {
		return bookingId;
	}
	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getBookingTime() {
		return bookingTime;
	}
	
	//"Sun Dec 23 12:59:00 UTC 2012"
	public void setBookingTime(String bookingTimeStr) {
		SimpleDateFormat sdf24h = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss", Locale.UK);
		dtBookingTimestamp = Util.parseDateStr(bookingTimeStr);
		if (dtBookingTimestamp != null)
			bookingTime = sdf24h.format(dtBookingTimestamp);
		else
			bookingTime = bookingTimeStr;
//		if (!bookingTime.toUpperCase(Locale.UK).contains("UTC"))
//		{
//			this.bookingTime = bookingTime;
//			DateFormat sdf = SimpleDateFormat.getDateTimeInstance();
//			try {
//				dtBookingTimestamp = sdf.parse(bookingTime);
//			} catch (ParseException e) {
//				SimpleDateFormat sdfUK = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.UK);
////				DateFormat dt = SimpleDateFormat.getDateTimeInstance();
//				try {
//					dtBookingTimestamp = sdfUK.parse(bookingTime);
//					this.bookingTime = sdf24h.format(dtBookingTimestamp);
//				} catch (ParseException e2) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//					this.bookingTime = bookingTime;
//				}
//			}
//			return;
//		}
//		SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd kk:mm:ss 'UTC' yyyy", Locale.UK);
////		DateFormat dt = SimpleDateFormat.getDateTimeInstance();
//		try {
//			dtBookingTimestamp = sdf.parse(bookingTime);
//			this.bookingTime = sdf24h.format(dtBookingTimestamp);
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			this.bookingTime = bookingTime;
//		}
//		//this.bookingTime = bookingTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPickupAddress() {
		return pickupAddress;
	}
	public void setPickupAddress(String pickupAddress) {
		this.pickupAddress = pickupAddress;
	}
	public String getDropoffAddress() {
		return dropoffAddress;
	}
	public void setDropoffAddress(String dropoffAddress) {
		this.dropoffAddress = dropoffAddress;
	}
	public String getPickupLat() {
		return pickupLat;
	}
	public void setPickupLat(String pickupLat) {
		this.pickupLat = pickupLat;
	}
	public String getPickupLon() {
		return pickupLon;
	}
	public void setPickupLon(String pickupLon) {
		this.pickupLon = pickupLon;
	}
	public String getDropoffLat() {
		return dropoffLat;
	}
	public void setDropoffLat(String dropoffLat) {
		this.dropoffLat = dropoffLat;
	}
	public String getDropoffLon() {
		return dropoffLon;
	}
	public void setDropoffLon(String dropoffLon) {
		this.dropoffLon = dropoffLon;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public int getBookingType() {
		return bookingType;
	}

	public void setBookingType(int bookingType) {
		this.bookingType = bookingType;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getRatings() {
		return ratings;
	}

	public void setRatings(String ratings) {
		this.ratings = ratings;
	}

	public String getFare() {
		return fare;
	}

	public void setFare(String fare) {
		this.fare = fare;
	}

	public boolean isBidSent() {
		return bidSent;
	}

	public void setBidSent(boolean bidSent) {
		this.bidSent = bidSent;
	}

	public Date getDtBookingTimestamp() {
		return dtBookingTimestamp;
	}

	public void setDtBookingTimestamp(Date dtBookingTimestamp) {
		this.dtBookingTimestamp = dtBookingTimestamp;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getSentBy() {
		return sentby;
	}
	public void setSentBy(String sentBy) {
		this.sentby = sentBy;
	}
	@Override
	public String toString() {
		return "Booking [bookingId=" + bookingId + ", bookingTime=" + bookingTime
				+ ", status=" + status + ", passengerName=" + passengerName
				+ ", zoneId=" + zoneId + ", jobId=" + jobId 
				+ ", pickupAddress=" + pickupAddress
				+ ", dropoffAddress=" + dropoffAddress 
				+ ", pickupLat=" + pickupLat + ", pickupLon=" + pickupLon 
				+ ", dropoffLat="+ dropoffLat + ", dropoffLon=" + dropoffLon 
				+ ", notes=" + notes + ", vehicleType=" + vehicleType
				+ ", ratings=" + ratings + ", fare=" + fare + "]";
	}
	
}