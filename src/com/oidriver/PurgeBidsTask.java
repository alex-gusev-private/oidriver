package com.oidriver;

import android.os.AsyncTask;

import com.oidriver.service.OiDriverService;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


class PurgeBidsTask extends AsyncTask<Void, Integer, Boolean>
{
	interface PurgeBidsListener
	{
		void onCompleted(boolean success);
	}
	
	PurgeBidsListener mListener;
	
	public PurgeBidsTask(PurgeBidsListener listener)
	{
		mListener = listener;
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {

		Calendar now = Calendar.getInstance();
		now.add(Calendar.MINUTE, -30);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
		String timestamp = sdf.format(now.getTime());
		int recs = OiDriverService./*dbHelper.*/deleteAllBids(timestamp);
		return recs > 0;
	}
	
	@Override
	protected void onPostExecute(Boolean success)
	{
		if (mListener != null)
			mListener.onCompleted(success);
	}
}	

