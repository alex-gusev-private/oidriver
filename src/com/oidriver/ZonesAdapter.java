package com.oidriver;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ZonesAdapter extends ArrayAdapter<ZoneInfo> {
	
	Context mContext;
	private int mSelectedZone = -1;
	
	public ZonesAdapter(Context context, int textViewResourceId, ArrayList<ZoneInfo> items)
	{
		super(context, textViewResourceId, items);
		mContext = context;
	}
	
	public int getSelectedZone()
	{
		return mSelectedZone;
	}

	public void setSelectedZone(int idx)
	{
		mSelectedZone = idx;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = null;
		
		if (convertView == null)
		{
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.zone_row, null);
		}
		else
		{
			view = convertView;
		}
		
		ZoneInfo item = getItem(position);
		if (item != null && view != null)
		{
			TextView name = (TextView) view.findViewById(R.id.tvName);
			TextView drivers = (TextView) view.findViewById(R.id.tvDrivers);
			TextView passengers = (TextView) view.findViewById(R.id.tvPassengers);
			name.setText(item.name);
			drivers.setText(!TextUtils.isEmpty(item.drivers) ? item.drivers : "0");
			passengers.setText(!TextUtils.isEmpty(item.passengers) ? item.passengers : "0");
			
			if (position != mSelectedZone)
				view.setBackgroundResource(R.drawable.grey_shape_background_semi);
			else
				view.setBackgroundResource(R.drawable.selected_zone_row);

		}
		
		return view;
	}
}
