package com.oidriver.service;

import com.oidriver.OiDriverActivity;
import com.oidriver.R;
import com.oidriver.runnable.RunOn;
import com.oidriver.xmpp.XMPPClient;

/**
 * Created by Alex on 30/08/2013.
 * Copyright (C) Touchnote Ltd. 2013
 */
public class XmppResultsReceiver implements XMPPClient.XmppEventsListener {

	OiDriverActivity mOiDriverActivity;

	public XmppResultsReceiver(OiDriverActivity activity) {
		mOiDriverActivity = activity;
	}

	/**
	 * XmppEventsListener Methods
	 *
	 * @param errCode
	 * @param msg
	 */
	@Override
	public void onMessage(int errCode, String msg) {

	}

	/**
	 * When e is null:
	 * Notification that the connection was closed normally or that the reconnection
	 * process has been aborted.
	 * <p/>
	 * When e is not null:
	 * Notification that the connection was closed due to an exception. When
	 * abruptly disconnected it is possible for the connection to try reconnecting
	 * to the server.
	 *
	 * @param e the exception.
	 */
	@Override
	public void connectionClosed(Exception e) {
		final boolean error = e != null;
		RunOn.mainThread(new Runnable() {
			@Override
			public void run() {
				mOiDriverActivity.updateConnectionStatus();
				mOiDriverActivity.playSoundSync(R.raw.data_lost, null);
				if (error) {
//					AlertDialog.Builder builder = new AlertDialog.Builder(mOiDriverActivity);
//					mOiDriverActivity.mConnectionLostAlert = builder.setTitle("OiDriver").setMessage("XMPP CONNECTION IS LOST!")
//							//.setPositiveButton("OK", null)
//							.setCancelable(false)
//							.show();
					mOiDriverActivity.updateReconnectionStatus("Connection lost...");
					mOiDriverActivity.showReconnectionStatus(true);
				}
				else {
					mOiDriverActivity.updateReconnectionStatus("Connection CLOSED w/o error!");
					mOiDriverActivity.showReconnectionStatus(true);
					RunOn.mainThreadDelayed(new Runnable() {
						@Override
						public void run() {
							mOiDriverActivity.showReconnectionStatus(false);
						}
					}, 2000);
				}
			}
		});

	}

	/**
	 * The connection will retry to reconnect in the specified number of seconds.
	 *
	 * @param seconds remaining seconds before attempting a reconnection.
	 */
	@Override
	public void reconnectingIn(int seconds) {
		final int value = seconds;
		RunOn.mainThread(new Runnable() {
			@Override
			public void run() {
				//Toast.makeText(mOiDriverActivity, "Reconnecting in " + value + " seconds...", Toast.LENGTH_SHORT).show();
				if (value > 0) {
					mOiDriverActivity.updateReconnectionStatus("Reconnecting in " + value + " seconds...");
					mOiDriverActivity.showReconnectionStatus(true);
				}
				else {
					mOiDriverActivity.updateReconnectionStatus("Reconnecting...");
					//mOiDriverActivity.showReconnectionStatus(false);
				}
			}
		});
	}

	/**
	 * The connection has reconnected successfully to the server. Connections will
	 * reconnect to the server when the previous socket connection was abruptly closed.
	 */
	@Override
	public void reconnectionSuccessful() {
		RunOn.mainThread(new Runnable() {
			@Override
			public void run() {
				mOiDriverActivity.updateConnectionStatus();
				mOiDriverActivity.showReconnectionStatus(true);
				mOiDriverActivity.updateReconnectionStatus("Reconnection successful!");
				mOiDriverActivity.showReconnectionStatus(false);
				mOiDriverActivity.playSoundSync(R.raw.data_connected, null);
			}
		});
	}

	/**
	 * An attempt to connect to the server has failed. The connection will keep trying
	 * reconnecting to the server in a moment.
	 *
	 * @param e the exception that caused the reconnection to fail.
	 */
	@Override
	public void reconnectionFailed(Exception e) {
		RunOn.mainThread(new Runnable() {
			@Override
			public void run() {
				//Toast.makeText(mOiDriverActivity, "Reconnection failed!", Toast.LENGTH_SHORT).show();
				mOiDriverActivity.updateReconnectionStatus("Reconnection failed!");
			}
		});
	}

	@Override
	public void updateZoneInfo(String queueIdx, boolean playSound) {
		mOiDriverActivity.updateZoneInfo(queueIdx, playSound);
	}

	@Override
	public void updateRankInfo(String queueIdx, String state) {
		mOiDriverActivity.updateRankInfo(queueIdx, state);
	}

    @Override
    public void updateTariff(String startFare, String regularFare, int currencyFactor) {
        mOiDriverActivity.updateTariff(startFare, regularFare, currencyFactor);
    }
}
