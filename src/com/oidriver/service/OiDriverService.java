package com.oidriver.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Process;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.oidriver.Booking;
import com.oidriver.Constants;
import com.oidriver.Driver;
import com.oidriver.OiDriverActivity;
import com.oidriver.OiDriverApp;
import com.oidriver.ZoneInfo;
import com.oidriver.db.MyDBAdapter;
import com.oidriver.receiver.NetworkUtils;
import com.oidriver.xmpp.XMPPClient;

import org.jivesoftware.smack.ConnectionListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;

public class OiDriverService extends Service implements ConnectionListener {
	
	public static final String TAG = "OIDRIVER";

	public static MyDBAdapter dbHelper;
	public static Set<String> setBids = new HashSet<String>();
	public static ArrayList<Booking> bookings = new ArrayList<Booking>();

	private static XMPPClient client = XMPPClient.getInstance();
	
	private static OiDriverService serviceInstance;

	public static boolean readyStatus = true;
    public static boolean readyStatusNext = readyStatus;

	public static double lat;
	public static double lon;
    public static float speed;
    public static float accuracy;

	public static String imei;
	public static String currentZoneId = "";
	public static String currentZoneName = "";
	public static final String NO_ID_EMPTY_STRING = "";

	public static boolean hasOutstandingGPS = false;

	public static Driver driver = null;
	
	public static ArrayBlockingQueue<Object> JOB_STATUS_QUEUE = new ArrayBlockingQueue<Object>(500);
	public Timer mJobQueueTimer;

	public static long mLastPingTime = 0;

	public static ArrayList<ZoneInfo> mZones = new ArrayList<ZoneInfo>();

	//public ResultReceiver mResultReceiver;
	public XMPPClient.XmppEventsListener mXmppEventsListener;

	public static HashMap<String, Object> mStanzaListeners = new HashMap<String, Object>();

	@Override
	public IBinder onBind(Intent intent) {
		return new LocalBinder<OiDriverService>(this);
	}
	
	static {
		serviceInstance = null;
		dbHelper = new MyDBAdapter();
	}
	
	public static OiDriverService get()
	{
		return serviceInstance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		
		Log.i(TAG, String.format("SERVICE.onCreate(): PID = 0x%08X, TID = 0x%08X", Process.myPid(), Process.myTid()));
		
		serviceInstance = this;
				
		client = XMPPClient.getInstance();
		client.setService(serviceInstance);
		
		imei = OiDriverService.getDeviceId(OiDriverApp.getAppContext());

		// This is a follow-up receiver for network status changes
		registerReceiver(networkChangeReceiver, new IntentFilter("com.oidriver.CONNECTION_CHANGE"));
		
		Intent i = new Intent("com.oidriver.SERVICE_STARTED");
		OiDriverApp.getAppContext().sendBroadcast(i);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		Log.i(TAG, String.format("SERVICE.onStartCommand(): PID = 0x%08X, TID = 0x%08X", Process.myPid(), Process.myTid()));

		if (client.isConnected() && client.isAuthenticated())
		{
			//v145 disableReconnection();
			Log.i(TAG, "SERVICE.onStartCommand(): LOGGED IN ALREADY");
			return START_STICKY;
		}

		//mResultReceiver = intent.getExtras().getParcelable("receiver");

		final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
		boolean userLoggedIn = sp.getBoolean("UserLoggedIn", true);
		Log.i(TAG, String.format("SERVICE.onStartCommand(): userLoggedIn = %s, client.isConnected() = %s,  client.isAuthenticated() = %s",
				userLoggedIn ? "true" : "false",
				client.isConnected() ? "true" : "false",
				client.isAuthenticated() ? "true" : "false"));
		if (userLoggedIn && (!client.isConnected() || !client.isAuthenticated()))
		{
			Log.i(TAG, "SERVICE.onStartCommand(): Starting RE-LOGIN task");
			//sendLogin();
			connectToXmpp();
		}
		return START_STICKY;
	}

    @SuppressWarnings("deprecation")
	public void onDestroy() {
    	super.onDestroy();
    	Log.i(TAG, "SERVICE.onDestroy()");
    	try {
			unregisterReceivers();
		    if (mJobQueueTimer != null) mJobQueueTimer.cancel();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

//	public boolean sendLogin()
//	{
//		Log.i(TAG, "SERVICE.sendLogin()");
//		// If semaphore is busy, we're still connecting...
//		if (!mLoginSemaphone.tryAcquire())
//		{
//			Log.i(TAG, "SERVICE.sendLogin(): Already logging in...");
//			return false;
//		}
//		Log.i(TAG, "SERVICE.sendLogin(): Starting LOGIN task");
//		LoginTask setGN = new LoginTask();
//		setGN.execute("start");
//		return true;
//	}

	public synchronized boolean connectToXmpp() {

		Boolean isConnected = client.connectToServer();

		if(isConnected)
		{
			if (isXmppAuthenticated())
				return true;

			boolean result = client.login();
			if(result)
			{
				Log.e(TAG, "Logged in!");
				if (hasOutstandingGPS)
				{
					Timer t = new Timer(true);
					t.schedule(new TimerTask() {

						@Override
						public void run() {
							client.sendIQForGPS(true, readyStatus ?
									Constants.DRIVER_STATUS_RTW :
									Constants.DRIVER_STATUS_BRK);
						}
					}, 5000);
				}

			}
			return result;
		}
		return false;
	}
	
	public void unregisterReceivers() {
		try
		{
			unregisterReceiver(networkChangeReceiver);
		}
		catch(Exception e)
		{
			Log.e(TAG,"ERROR : Unregistering Receiver");
		}
	}

	public static boolean deleteBidByBookingId(String bookingId) {
		for (int i = 0; i < bookings.size(); ++i) {
			Booking booking = bookings.get(i);
			if (booking.getBookingId().equalsIgnoreCase(bookingId))
			{
				bookings.remove(i);
				return true;
			}
		}
		return false;
	}

	public static int deleteAllBids(String timestamp) {
		for (int i = bookings.size() - 1; i >= 0; --i) {
			Booking booking = bookings.get(i);
			if (booking.getBookingTime().compareToIgnoreCase(timestamp) < 0)
			{
				bookings.remove(i);
			}
		}
		return 0;
	}

	public static Booking getBidByJobId(String jobId) {
		for (Booking booking : bookings) {
			if (booking.getJobId().equalsIgnoreCase(jobId)) {
				return booking;
			}
		}
		return null;
	}

	public static Booking getBidByBookingId(String bookingId) {
		for (Booking booking : bookings) {
			if (booking.getBookingId().equalsIgnoreCase(bookingId)) {
				return booking;
			}
		}
		return null;
	}

	public static List<String> getSentBidsIds() {
		final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
		String ids = sp.getString("sent_bids", null);
		if (TextUtils.isEmpty(ids))
			return null;
		String[] items = ids.split(",");
		return Arrays.asList(items);
	}

	public static void putSentBidsId(String bookingId) {
		if (setBids.contains(bookingId))
			return;
		final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
		String ids = sp.getString("sent_bids", null);
		if (TextUtils.isEmpty(ids))
			ids = bookingId;
		else
			ids += bookingId + ",";
		sp.edit().putString("sent_bids", ids).commit();
		setBids.add(bookingId);
	}

	public void onLoginSuccessful() {

		if (mJobQueueTimer != null) {
			mJobQueueTimer.cancel();
			mJobQueueTimer.purge();
			mJobQueueTimer = null;
		}
		mJobQueueTimer = new Timer("jobQueueTimer", true);
		mJobQueueTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				try {
					boolean hasNetwork =
							NetworkUtils.getConnectivityStatus() != NetworkUtils.TYPE_NOT_CONNECTED;
					if (!hasNetwork)
						return;
					ContentValues cv = (ContentValues) JOB_STATUS_QUEUE.take();
					if (!sendJobStatusToServer(cv.get("manager_jid").toString(),
							cv.get("jobid").toString(), cv.get("jobstatus").toString()))
					{
						Object[] queue = JOB_STATUS_QUEUE.toArray();
						JOB_STATUS_QUEUE.clear();
						JOB_STATUS_QUEUE.add(cv);
						Collections.addAll(JOB_STATUS_QUEUE, queue);
					}
				}
				catch (Exception e)
				{
					Log.w(TAG, "JOB QUEUE: " + e.getMessage());
				}
			}
		}, 2000, 2000);
	}

	public void doLogout()
	{
		try
		{
	
			Thread t = new Thread(new DisconnectRunnable(),	"xmpp-disconnector");
			// we don't want this thread to hold up process shutdown so
			// mark as daemon.
			//FIXME:
			t.setDaemon(true);
			t.start();
	
			try {
				t.join(1000 * 10);
			}
			catch (InterruptedException ignored) {
			}
			// the thread is still alive, this means that the disconnect
			// is still running
			// we don't have the time, so prepare for a new connection
			if (t.isAlive()) {
				Log.i(TAG,
						t.getName()
								+ " was still alive: connection will be set to null");
				client.setConnectionToNull();
			}
		}
		catch(Exception r)
		{
			Log.e(TAG,"ERROR : Logout");
		}
	}
	public class DisconnectRunnable implements Runnable {
		
		public DisconnectRunnable() {
		}

		@Override
		public void run() {
			Log.i(TAG,"disconnecting xmpp connection");
			float start = System.currentTimeMillis();
			try {
				if(client.isConnected())
				{
					client.logout();
					client.setConnectionToNull();
				}
			} catch (Exception e2) {
				Log.i(TAG,"xmpp disconnect failed: "+ e2);
			}
			float stop = System.currentTimeMillis();
			float diff = stop - start;
			diff = diff / 1000;
			Log.i(TAG,"disconnectED xmpp connection. Took: "+ diff + " s");
		}
	}

	/**
	 * If it's the emulator return a dummy ID, otherwise get the 
	 * device ID from the telephony manager
	 */
	public static String getDeviceId(final Context context) {

		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD &&  Build.SERIAL.equals("unknown")) {
			return "358350040458868";
		}

		TelephonyManager telephonyMgr = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		if(telephonyMgr != null) {
			String id = telephonyMgr.getDeviceId();
			if(id != null) {
				return id;
			}
		}

		WifiManager wMgr = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
		if( wMgr == null ) {
			return NO_ID_EMPTY_STRING;
		}

		WifiInfo netInfo = wMgr.getConnectionInfo();
		if(netInfo == null ) {
			return NO_ID_EMPTY_STRING;
		}

		if(netInfo.getMacAddress() != null) {
			String mac = netInfo.getMacAddress();
			StringBuilder strippedMac = new StringBuilder(mac.length());
			for(char c : mac.toCharArray()) {
				if(Character.isLetterOrDigit(c)) {
					strippedMac.append(c);
				}
			}
			return strippedMac.toString();
		}

		return NO_ID_EMPTY_STRING;
	}

	private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {				
			try
			{
				Bundle bundle = intent.getExtras();
				if (bundle == null)
					return;

				Boolean isConnected = bundle.getBoolean("isConnected");
				Intent i = new Intent("com.oidriver.CONN_STATUS");
				
				/*if (!isConnected) {
					Log.wtf(TAG, "NETWORK CONNECTION LOST");
					// NB: don't disable reconnection => setReconnectionProcess(false);
					i.putExtra("status", 0);

					if(client != null)
					{
						client.setReconnectionAllowed(false);
//						if (client.isConnected())
//							doLogout();
					}
				}
				else {
					Log.wtf(TAG, "NETWORK CONNECTION RESTORED");
//					if(client != null)
//						client.setReconnectionAllowed(true);

//					setReconnectionProcess(true);
					i.putExtra("status", 1);
				}*/
				if (!isConnected) {
					Log.wtf(TAG, "NETWORK CONNECTION LOST");
					i.putExtra("status", 0);
//					if(client != null)
//						client.setReconnectionAllowed(false);
				}
				else {
					Log.wtf(TAG, "NETWORK CONNECTION RESTORED");
					i.putExtra("status", 1);
//					if(client != null)
//						client.setReconnectionAllowed(true);
				}
				OiDriverApp.getAppContext().sendBroadcast(i);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	};

	// ConnectionListener callbacks
	@Override
	public void connectionClosed() {
		if (mXmppEventsListener != null) {
			mXmppEventsListener.connectionClosed(null);
		}
		Log.e(TAG, "connectionClosed()");
	}

	@Override
	public void connectionClosedOnError(Exception e) {
		if (mXmppEventsListener != null) {
			mXmppEventsListener.connectionClosed(e);
		}
		if (e != null) Log.e(TAG, "connectionClosedOnError[" + e.getMessage() + "]");
	}

	@Override
	public void reconnectingIn(int seconds) {
		Log.wtf(TAG, "RECONNECTING IN " + seconds);
		if (mXmppEventsListener != null) {
			mXmppEventsListener.reconnectingIn(seconds);
		}
	}

	@Override
	public void reconnectionSuccessful() {
		Log.wtf(TAG, "RECONNECTION SUCCESSFUL");
		if (mXmppEventsListener != null) {
			mXmppEventsListener.reconnectionSuccessful();
		}
	}

	@Override
	public void reconnectionFailed(Exception e) {
		Log.wtf(TAG, "RECONNECTION FAILED");
		if (mXmppEventsListener != null) {
			mXmppEventsListener.reconnectionFailed(e);
		}
	}

	public static int getBidsCount()
	{
		return bookings.size();// dbHelper.getBidsCount();
	}
	
	// STATIC METHODS FOR XMPP
//	public static void onBookingIQRecieved(String iq) {
//		Log.d(TAG, "==========> BOOKING :\n"+iq);
//		if (client != null) client.parseBookingSearchResultXML(iq);
//	}

	public static void onDriverIQRecieve(String iq) {
		if (client != null) client.parseDriverResultXML(iq);
	}

	public static String parseDriverPublishResponse(String iq) {
		if (client != null) return client.parseZoneInfoXML(iq); else return "0";
	}
	
	public static void sendIQToSubscribeZone(String zoneId) {
		if (client != null) client.sendIQToSubscribeZone(zoneId);
	}
	
	public static void sendIQToUnsubscribeZone(String zoneId) {
		if (client != null) client.sendIQToUnsubscribeZone(zoneId);
	}
	
	public static void sendIQToGetBookings(final int maxCount, OiDriverActivity.OnBookingSearchListener onBookingSearchListener) {
		if (client != null) {
			mStanzaListeners.put(Constants.NAMESPACE_BOOKING_SEARCH, onBookingSearchListener);
			client.sendIQToGetBookings(maxCount);
		}
	}
	
	public static void sendIQForDrivers() {
		if (client != null) client.sendIQForDrivers();
	}
	
	public static void sendIQForGPS(boolean adjacent, String status) {
		if (client != null) client.sendIQForGPS(adjacent, status);
	}
	
	public static void sendJobBid(Booking booking) {
		if (client != null) client.sendJobBid(booking);
	}
	
	public static boolean isBlockingOperationProgress()
	{
		return client.blockingOperationProgress;
	}
	
	public static void sendIQForZones() {
		if (client != null) client.sendIQForZones();
	}
	
	public static boolean sendMessage(String message, String sendToJid) {
		return client != null && client.sendMessage(message, sendToJid);
	}
	
	public static String getLoggedUserJid()
	{
		if (client == null)
			return "";
		if (client.xmppConnection == null)
			return "";
		String loggedUser = client.xmppConnection.getUser();
		if (!TextUtils.isEmpty(loggedUser))
			return loggedUser.split("/")[0];
		else
			return "";
	}
	
	public static Object getClientLock()
	{
		return client.lockMe;
	}

	public static void requestDriverInfo() {
		if (client != null) client.requestDriverInfo();
	}
	
	public static void enqueueJobStatusRequest(ContentValues cv) {
		JOB_STATUS_QUEUE.add(cv);
	}
	
	private boolean sendJobStatusToServer(String sendToJid, String jobId, String jobStatus) {
		if (client.isAuthenticated()) {

			if (!TextUtils.isEmpty(jobId) && !TextUtils.isEmpty(jobStatus)) {
				Log.e(TAG, String.format("Sending %s for ID = %s", jobStatus, jobId));
				return client.sendMessage(jobStatus, sendToJid);
			}
			return false;
		} else {
			return false;
		}
	}

	public static boolean isXmppConnected() {
		return client != null ? client.isConnected() : false;
	}

	public static boolean isXmppAuthenticated() {
		return client != null ? client.isAuthenticated() : false;
	}

	public static boolean connect() {
		if (client == null) return  false;
		return client.connectToServer();
	}

	public static XMPPClient getXmppClient() {
		return client;
	}

	public void setResultsReceiver(XmppResultsReceiver resultsReceiver) {
		mXmppEventsListener = resultsReceiver;
	}

	public XMPPClient.XmppEventsListener getResultsReceiver() {
		return mXmppEventsListener;
	}

    public static boolean sendPaymentInfo(String amount, String cardNo, int expMonth, int expYear, String cvv, String postcode) {
        return client != null && client.sendPaymentInfo(amount, cardNo, expMonth, expYear, cvv, postcode);
    }
}
