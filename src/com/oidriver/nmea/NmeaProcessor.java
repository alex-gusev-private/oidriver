package com.oidriver.nmea;

import android.text.TextUtils;
import android.util.Log;

import com.oidriver.fragments.OiMeterFragment;
import com.oidriver.util.KalmanLatLong;

import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

/**
 * Created by touchnote on 09/01/2014.
 */
public class NmeaProcessor {

    public final static String KPH = "KPH";
    public final static String MPH = "MPH";

    public final static String TAG = "NMEA-CPU";

    private long _timeDelta = 0;
    private double _speedDistance = 0;
    private double _Latitude;
    private int _moving;
    private double _Longitude;
    private double _lastLat = 0;
    private double _lastLon = 0;
    private double _lastRmcLng = 0;
    private double _lastRmcLat = 0;
    private boolean _firstTime = true;
    private double _lastKnownSpeed = 0.0;
    private String _validStr;
    private String _utcTimeStr;
    private String _utcDateStr;
    private String _lastUtcTime2Str;
    private String _lastUtcDate2Str;
    private double _gpRmcLng = 0;
    private double _gpRmcLat = 0;
    private double _gpGgaLng = 0;
    private double _gpGgaLat = 0;
    private double _lastRmcSpeed = 0;

    private String _localeStr = MPH;

    private Boolean _addClick = false;
    double _minimumDistance;
    private double _newDistance = 0;
    private int mNumSatellites = 0;

    public interface NmeaProcessorListener {
        void onRmcProcessed();
        void onGgaProcessed();
    }

    private NmeaProcessorListener mListener;

    OiMeterFragment.MeterState mState = OiMeterFragment.MeterState.ForHire;

    public NmeaProcessor(NmeaProcessorListener listener) {
        mListener = listener;
    }

    public boolean meterTick(String nmea, OiMeterFragment.MeterState state)
    {
        boolean ret = true;
        setMeterState(state);

        Log.d(TAG, "Daemon.MeterTick()");
        if (!TextUtils.isEmpty(nmea))
        {
            Log.d(TAG, "Daemon.MeterTick(): has data => " + nmea);

            String[] strArr = nmea.split("$");
            for (String strTemp : strArr) {
                if (TextUtils.isEmpty(strTemp))
                    continue;

                String[] lineArr = strTemp.split(",");
                int posChecksum = strTemp.indexOf('*');
                if (posChecksum < 0)
                    continue;
                String strTestLine = strTemp.substring(0, posChecksum);
                String checksum = getChecksum(strTestLine);
                if (checksum.compareToIgnoreCase(strTemp.substring(posChecksum + 1)) != 0) {
                    Log.e(TAG, "Invalid checksum for " + strTemp);
                    continue;
                }
                if (lineArr[0].equalsIgnoreCase("$GPRMC")) {
                    if (!handleRMC(lineArr)) {
                        ret = false;
                        break;
                    }
                } else if (lineArr[0].equalsIgnoreCase("$GPGGA")) {
                    if (!handleGGA(lineArr)) {
                        ret = false;
                        break;
                    }
                } else if (lineArr[0].equalsIgnoreCase("$GPGSA")) {
                    if (!handleGSA(lineArr)) {
                        ret = false;
                        break;
                    }
                }
            }
        }
        else
            ret = false;
        Log.d(TAG, "Daemon.MeterTick() Exit: " + ret);
        return ret;
    }

    private static String getChecksum(String sentence)
    {
        // Loop through all chars to get a checksum
        int checksum = 0;
        for (char chr: sentence.toCharArray())
        {
            switch (chr)
            {
                case '$':
                    // Ignore the dollar sign
                    break;
                case '*':
                    // Stop processing before the asterisk
                    return String.format(Locale.UK, "%02X", checksum);
                default:
                    // Is this the first value for the checksum?
                    if (checksum == 0)
                    {
                        // Yes. Set the checksum to the value
                        checksum = (int)chr;
                    }
                    else
                    {
                        // No. XOR the checksum with this character's value
                        checksum = checksum ^ ((int)chr);
                    }
                    break;
            }
        }
        // Return the checksum formatted as a two-character hexadecimal
        return String.format(Locale.UK, "%02X", checksum);
    }

    /**
     * Process NMEA RMC sentence.
     * Speed is calculated in meters per second.
     * @param lineArr - An array of RMC sentence items
     * @return true or false to indicate processing result
     */
    private boolean handleRMC(String[] lineArr)
    {
        String speedStr;

        try
        {
            speedStr = lineArr[7];
            _gpRmcLat = decimalToFractionalDegrees(Double.parseDouble(lineArr[3]));
            _gpRmcLng = decimalToFractionalDegrees(Double.parseDouble(lineArr[5]));
            Log.wtf(TAG, "RMC => LAT = " + _gpRmcLat + " LON = " + _gpRmcLng);

            _utcTimeStr = lineArr[1];
            _utcDateStr = lineArr[9];

            if (lineArr[6].equals("W"))
            {
                _gpRmcLng = -_gpRmcLng;
            }

            double testDistance = OiMeterFragment.calculateDistance(_lastRmcLat, _lastRmcLng, _gpRmcLat, _gpRmcLng);
            if (TextUtils.isEmpty(speedStr))
            {
                speedStr = "0";
            }

            // Speed in M/S
            _lastKnownSpeed = Double.parseDouble(speedStr) * 0.51444444444;

            if (_lastKnownSpeed > 0)
            {
                if ((_gpRmcLng == _lastRmcLng) && (_gpRmcLat == _lastRmcLat))
                {
                    _lastKnownSpeed = 0;
                }
            }

            if (!TextUtils.isEmpty(_utcDateStr) && _utcDateStr.length() >= 6 &&
                    !TextUtils.isEmpty(_utcTimeStr) && _utcTimeStr.length() >= 6 &&
                    !TextUtils.isEmpty(_lastUtcDate2Str) && _lastUtcDate2Str.length() >= 6 &&
                    !TextUtils.isEmpty(_lastUtcTime2Str) && _lastUtcTime2Str.length() >= 6)
            {
                double totalSeconds = getRmcTimeDeltaInSeconds();
                // speed distance in meters
                double avgSpeed = _lastRmcSpeed > 0 ? (_lastKnownSpeed + _lastRmcSpeed) / 2 : _lastKnownSpeed;
                double speedDist = avgSpeed * totalSeconds;
                if (testDistance > 0 && speedDist <= 0.001)
                    _lastKnownSpeed = 0;

                _speedDistance += speedDist;
            }

            if (_lastKnownSpeed >= 3.6d * 1.60934)//_THRESHOLD)
                _moving++;
            else
                _moving = 0;

            _validStr = lineArr[2];
            _utcTimeStr = lineArr[1];
        }
        catch (Exception ex)
        {
            Log.e(TAG, "[2] handleRMS(): " + ex.getMessage());
            return false;
        }

        _lastRmcLng = _gpRmcLng;
        _lastRmcLat = _gpRmcLat;

        _lastRmcSpeed = _lastKnownSpeed;
        _lastUtcDate2Str = _utcDateStr;
        _lastUtcTime2Str = _utcTimeStr;

        if (mListener != null)
            mListener.onRmcProcessed();

        return true;
    }

    private double getRmcTimeDeltaInSeconds() {
        Date dtUtcNow = new Date(
                Integer.parseInt(_utcDateStr.substring(0, 2)),
                Integer.parseInt(_utcDateStr.substring(2, 4)),
                Integer.parseInt(_utcDateStr.substring(4, 6)),
                Integer.parseInt(_utcTimeStr.substring(0, 2)),
                Integer.parseInt(_utcTimeStr.substring(2, 4)),
                Integer.parseInt(_utcTimeStr.substring(4, 6)));
        Date dtUtcLast = new Date(
                Integer.parseInt(_lastUtcDate2Str.substring(0, 2)),
                Integer.parseInt(_lastUtcDate2Str.substring(2, 4)),
                Integer.parseInt(_lastUtcDate2Str.substring(4, 6)),
                Integer.parseInt(_lastUtcTime2Str.substring(0, 2)),
                Integer.parseInt(_lastUtcTime2Str.substring(2, 4)),
                Integer.parseInt(_lastUtcTime2Str.substring(4, 6)));
        long timeDelta = dtUtcNow.getTime() - dtUtcLast.getTime();
        return (double) (timeDelta / 1000);
    }

    String _ggaTimeStr, _ggaLastTimeStr;

    private boolean handleGGA(String[] lineArr)
    {
        try
        {
            String fixQualityStr = lineArr[6];
            if (TextUtils.isEmpty(fixQualityStr))
                fixQualityStr = "0";
            int fixQuality = Integer.parseInt(fixQualityStr);
            if (fixQuality == 0) {
                Log.wtf(TAG, "INVALID GGA");
                return false;
            }

            _ggaTimeStr = lineArr[1];
            if (!TextUtils.isEmpty(_ggaTimeStr) && _ggaTimeStr.length() >= 6 &&
                    !TextUtils.isEmpty(_ggaLastTimeStr) && _ggaLastTimeStr.length() >= 6)
            {
                long nowSecs =
                        3600 * Integer.parseInt(_ggaTimeStr.substring(0, 2)) +
                                60   * Integer.parseInt(_ggaTimeStr.substring(2, 4)) +
                                Integer.parseInt(_ggaTimeStr.substring(4, 6));
                long lastSecs =
                        3600 * Integer.parseInt(_ggaLastTimeStr.substring(0, 2)) +
                                60   * Integer.parseInt(_ggaLastTimeStr.substring(2, 4)) +
                                Integer.parseInt(_ggaLastTimeStr.substring(4, 6));
                _timeDelta = (nowSecs - lastSecs) * 1000;
            }
            _ggaLastTimeStr = _ggaTimeStr;

            double newLatitude = decimalToFractionalDegrees(Double.parseDouble(lineArr[2]));
            double newLongitude = decimalToFractionalDegrees(Double.parseDouble(lineArr[4]));
            if (!isValidPosition(newLatitude, newLongitude)) {
                Log.wtf(TAG, "REALLY WTF");
                return false;
            }

            _Latitude = newLatitude;
            _Longitude = newLongitude;
            _gpGgaLat = _Latitude;
            _gpGgaLng = _Longitude;
            Log.wtf(TAG, "GGA => LAT = " + _gpGgaLat + " LON = " + _gpGgaLng);
            if (lineArr[5].equals("W"))
            {
                _Longitude = -_Longitude;
                _gpGgaLng = _Longitude;
            }

            String hdopStr = lineArr[8];
            if (TextUtils.isEmpty(hdopStr))
                hdopStr = "1000.0";
            double hdop = Double.parseDouble(hdopStr);

            double ggaSpeed;
            if (_moving > 1 && fixQuality > 0)
            {
                if (_firstTime && _validStr.equals("A"))
                {
                    _lastLat = _Latitude;
                    _lastLon = _Longitude;
                    _firstTime = false;
                }

                if (mState == OiMeterFragment.MeterState.Hired)
                {
                    double distanceDiff = OiMeterFragment.calculateDistance(_lastLat, _lastLon, _Latitude, _Longitude);
                    if (isValidDouble(distanceDiff) && distanceDiff > 0) {
                        ggaSpeed = _timeDelta > 0 ? 1000.0 * distanceDiff / _timeDelta : 0;
                        Log.d(TAG, String.format("distanceDiff = %.02f, newDist = %.02f",
                                distanceDiff, _newDistance + distanceDiff));
                        Log.d(TAG, String.format("ggaSpeed = %.02f, rmcSpeed = %.02f",
                                ggaSpeed, _lastRmcSpeed));

                        if (_timeDelta > 0) {
                            float q;
                            if (_localeStr.equals(MPH))
                                q = (float)(_lastRmcSpeed * 0.000621371192) / 3.6f;
                            else
                                q = (float)_lastRmcSpeed / 3.6f;
                            KalmanLatLong k = new KalmanLatLong(q);
                            k.SetState(_lastLat, _lastLon, 10f, _timeDelta);
                            k.Process(_Latitude, _Longitude, 10f, _timeDelta);
                            Log.wtf("KALMAN", String.format(Locale.ENGLISH,
                                    "Lat: [%f vs %f], Lon: [%f vs %f], Accuracy: %f",
                                    _Latitude, k.get_lat(), _Longitude, k.get_lng(), k.get_accuracy()));
                            double kDiff =
                                    OiMeterFragment.calculateDistance(_Latitude, _Longitude, k.get_lat(), k.get_lng());
                            double kDiff2 =
                                    OiMeterFragment.calculateDistance(_lastRmcLat, _lastRmcLng, k.get_lat(), k.get_lng());
                            Log.wtf("KALMAN",
                                    String.format(Locale.ENGLISH, "DIFF1: %f, DIFF2: %f", kDiff, kDiff2));
//                            if (Math.abs(distanceDiff -  kDiff) > _clickDist / 5)
//                                distanceDiff = kDiff;
                        }
                    }

                    _newDistance += distanceDiff;
                    boolean mUseJumpsFilter = true;
                    if (mUseJumpsFilter && isValidDouble(_speedDistance) &&  _speedDistance > _newDistance) {
                        _newDistance = (_newDistance + _speedDistance) / 2.0;
                    }

                    _lastLon = _Longitude;
                    _lastLat = _Latitude;
                }
                _moving = 0;

                if (mState == OiMeterFragment.MeterState.Hired)
                {
                    if (_newDistance >= _minimumDistance && !_addClick)
                    {
                        _addClick = true;
                        return true;
                    }

                    if (mListener != null)
                        mListener.onGgaProcessed();
                }
            }
        }
        catch (Exception ex)
        {
            if (mListener != null)
                mListener.onGgaProcessed();

            Log.e(TAG, "[2] handleGGA(): " + ex.getMessage() + Arrays.toString(ex.getStackTrace()));
            Log.e(TAG, "[2-1] handleGGA(): " + Arrays.toString(lineArr));

            return false;
        }


        return true;
    }

    /*
    $GPGSA

        GPS DOP and active satellites

        eg1. $GPGSA,A,3,,,,,,16,18,,22,24,,,3.6,2.1,2.2*3C
        eg2. $GPGSA,A,3,19,28,14,18,27,22,31,39,,,,,1.7,1.0,1.3*35


        1    = Mode:
               M=Manual, forced to operate in 2D or 3D
               A=Automatic, 3D/2D
        2    = Mode:
               1=Fix not available
               2=2D
               3=3D
        3-14 = IDs of SVs used in position fix (null for unused fields)
        15   = PDOP
        16   = HDOP
        17   = VDOP
    */
    private boolean handleGSA(String[] lineArr)
    {
        try
        {
            int count = 0;
            if (lineArr[2].equalsIgnoreCase("1"))
                return false;
            for (int i = 3; i < 15; ++i) {
                if (!TextUtils.isEmpty(lineArr[i]))
                    count++;
            }
            mNumSatellites = count;
        } catch (Exception e) {

        }

        return true;
    }

    private static boolean isValidPosition(double newLatitude, double newLongitude) {
        return !(Double.isNaN(newLatitude) || Double.isInfinite(newLatitude) ||
                Double.isNaN(newLongitude) || Double.isInfinite(newLongitude));
    }

    private static boolean isValidDouble(double value) {
        return !(Double.isNaN(value) || Double.isInfinite(value));
    }

    //format (dd.dddd) to decimal minute format (ddmm.mmmm).
    //Example 58.65375° => 5839.225 (58° 39.225min)
    private static double decimalToFractionalDegrees(double dec) {
        double deg = Math.floor(dec / 100);
        return (OiMeterFragment.round(deg + ((dec - (deg * 100)) / 60), 5)); //5839.2250
    }

    public void initVariables()
    {
        _speedDistance = 0;
        _Latitude = 0;
        _moving = 0;
        _Longitude = 0;
        _lastLat = 0;
        _lastLon = 0;
        _lastRmcLng = 0;
        _lastRmcLat = 0;
        _firstTime = true;
        _lastKnownSpeed = 0.0;
        _validStr = "V";
        _utcTimeStr = "";
        _utcDateStr = "";
        _lastUtcTime2Str = "";
        _lastUtcDate2Str = "";
        _ggaTimeStr = "";
        _ggaLastTimeStr = "";
        _newDistance = 0;
        _gpRmcLng = 0;
        _gpRmcLat = 0;
        _gpGgaLng = 0;
        _gpGgaLat = 0;
        _lastRmcSpeed = 0;
        _localeStr = MPH;
        _addClick = false;
    }

    public void setMeterState(OiMeterFragment.MeterState state) {
        mState = state;
    }

    public void setMinimumDistance(double minimumDistance) {
        _minimumDistance = minimumDistance;
    }

    public double get_gpRmcLng() {
        return _gpRmcLng;
    }

    public double get_gpRmcLat() {
        return _gpRmcLat;
    }

    public String get_localeStr() {
        return _localeStr;
    }

    public double get_gpGgaLng() {
        return _gpGgaLng;
    }

    public double get_gpGgaLat() {
        return _gpGgaLat;
    }

    public double get_newDistance() {
        return _newDistance;
    }


    public void set_gpRmcLng(double _gpRmcLng) {
        this._gpRmcLng = _gpRmcLng;
    }

    public void set_gpRmcLat(double _gpRmcLat) {
        this._gpRmcLat = _gpRmcLat;
    }

    public void set_gpGgaLng(double _gpGgaLng) {
        this._gpGgaLng = _gpGgaLng;
    }

    public void set_gpGgaLat(double _gpGgaLat) {
        this._gpGgaLat = _gpGgaLat;
    }

    public void set_newDistance(double _newDistance) {
        this._newDistance = _newDistance;
    }

    public double getSpeedThreshold() {
        return (_localeStr.equalsIgnoreCase(MPH) ? 0.44704000041056 : 0.27777777777778);
    }

    public double get_lastRmcSpeed() {
        return _lastRmcSpeed;
    }

    public int getNumSatellites() {
        return mNumSatellites;
    }
}
