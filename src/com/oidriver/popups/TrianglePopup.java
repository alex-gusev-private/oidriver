package com.oidriver.popups;

import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.oidriver.R;

public abstract class TrianglePopup extends PopupWindow {
	/* DON'T FORGET TO CHANGE IT IN popup_fadein.xml AS WELL! */
	private static final long FADE_IN_DURATION = 750;
	private static final long DEFAULT_HIDE_TIMEOUT = 2000;
	
	private static final int TRIANGLE_WIDTH = 32;
	private static final int DISTANCE_FROM_EDGE_DIP = 5;
	
	private LinearLayout layout;

	private View triangleUp, triangleDown;

	private Context mCtx;
	private WindowManager manager;
	private DisplayMetrics metrics = new DisplayMetrics();
	
	private long hideMillis = DEFAULT_HIDE_TIMEOUT;
	private boolean mAboveKeyboard = false;

	public TrianglePopup(Context ctx) {
		super(ctx);
		
		initialize(ctx);
	}
	
	public TrianglePopup(Context ctx, String title, String message) {
		super(ctx);
		
		initialize(ctx);
	}
	
	private void initialize(Context ctx) {
		mCtx = ctx;

		manager = (WindowManager) mCtx.getSystemService(Context.WINDOW_SERVICE);
		manager.getDefaultDisplay().getMetrics(metrics);
		
		if(layout == null) {
			LayoutInflater inflater = (LayoutInflater) mCtx
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			layout = (LinearLayout) inflateContentView(inflater);
			setContentView(layout);
		}
		
		triangleUp = layout.findViewById(R.id.triangleUp);
		triangleDown = layout.findViewById(R.id.triangleDown);

		initializeListeners();

		this.setAnimationStyle(R.style.PopupAnimation);

		// Remove default popup background
		this.setBackgroundDrawable(null);
	}

	protected abstract View inflateContentView(LayoutInflater inflater);

	public void dismiss(boolean immediate) {
		if (immediate) {
			setAnimationStyle(0);
			update();
		}

		super.dismiss();
	}

	public void dismiss() {
		this.dismiss(false);
	}
	
	/**
	 * Whether activities should dismiss this popup when the soft keyboard is 
	 * displayed. 
	 * 
	 * @param aboveKeyboard
	 */
	public void setShownAboveKeyboard(boolean aboveKeyboard) {
		mAboveKeyboard = aboveKeyboard;
	}
	
	public boolean getShownAboveKeyboard() {
		return mAboveKeyboard;
	}
	
	private void initializeListeners() {
		layout.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dismiss();
			}
		});
	}

	private void measureOnTop(View v) {
		layout.measure(MeasureSpec.makeMeasureSpec(manager.getDefaultDisplay()
				.getWidth(), MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(
						getMaxAvailableHeight(v), MeasureSpec.UNSPECIFIED));

		this.setContentView(layout);

		this.setWidth(layout.getMeasuredWidth());
		this.setHeight(layout.getMeasuredHeight());
	}
	
	private void measureAbove(View v) {
		layout.measure(MeasureSpec.makeMeasureSpec(manager.getDefaultDisplay()
				.getWidth() - 2*getDistanceFromEdge(), MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(
				getMaxAvailableHeight(v, -v.getHeight()), MeasureSpec.AT_MOST));

		this.setContentView(layout);

		this.setWidth(layout.getMeasuredWidth());
		this.setHeight(layout.getMeasuredHeight());
	}
	
	private void measureBelow(View v) {
		layout.measure(MeasureSpec.makeMeasureSpec(manager.getDefaultDisplay()
				.getWidth() - 2*getDistanceFromEdge(), MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(
				getMaxAvailableHeight(v, 0), MeasureSpec.AT_MOST));

		this.setContentView(layout);

		this.setWidth(layout.getMeasuredWidth());
		this.setHeight(layout.getMeasuredHeight());
	}
	
	private int getTriangleHalfWidth() {
		
		return Math.round(TRIANGLE_WIDTH/2 * metrics.density);
	}
	
	private int getDistanceFromEdge() {
		return Math.round(DISTANCE_FROM_EDGE_DIP * metrics.density);
	}
	
	private enum Position { Above, Below, OnTop };
	private Point calculatePosition(View v, Position pos) {
		int[] vpos = new int[2];
		v.getLocationInWindow(vpos);
		
		Point viewCenter = new Point(vpos[0] + v.getMeasuredWidth() / 2, 
				vpos[1]	+ v.getMeasuredHeight() / 2);
		
		// optimal position - the one we want
		int perfectPositioning = viewCenter.x - getWidth() / 2;
		
		int edgeDist = getDistanceFromEdge();
		// the farthest right we can actually fit
		int maximumLeft = metrics.widthPixels - getWidth() - edgeDist;
		
		int leftEdge = Math.min(perfectPositioning, maximumLeft), 
				topEdge = vpos[1];
		
		if(pos == Position.Above)
			topEdge -= getHeight();
		else if(pos == Position.Below)
			topEdge += v.getMeasuredHeight();
		else if(pos == Position.OnTop)
			topEdge -= (getHeight() - v.getMeasuredHeight())/2;
		
		// Clamp at left screen edge
		leftEdge = leftEdge < edgeDist ? edgeDist : leftEdge;
		
		// Move the triangle sideways
		int triangleLeft = viewCenter.x  - leftEdge - getTriangleHalfWidth();
		
		if(pos == Position.Above)
			((LinearLayout.LayoutParams) triangleDown.getLayoutParams()).leftMargin = triangleLeft;
		else if(pos == Position.Below)
			((LinearLayout.LayoutParams) triangleUp.getLayoutParams()).leftMargin = triangleLeft;
		
		getContentView().invalidate();
		
		return new Point(leftEdge, topEdge);
	}
	
	private void handleAutoHide() {
		if(hideMillis != -1) {
			Handler handler = new Handler(Looper.getMainLooper());
			handler.postDelayed(new Runnable() {
				public void run() {
					dismiss();
				}
			}, hideMillis + FADE_IN_DURATION);
		}
	}
	
	private boolean verifyWindowToken(final View v, final Runnable retryRunnable) {
		if (v == null) return false;
		
		if(v.getWindowToken() == null) {
			if(retryRunnable != null)	v.post(retryRunnable);
			
			return false;
		}
		
		return true;
	}

	public void showAbove(final View v) {
		boolean tokenValid = verifyWindowToken(v, null); // don't retry
		
		if(!tokenValid) return;
		
		triangleUp.setVisibility(View.GONE);
		triangleDown.setVisibility(View.VISIBLE);
		measureAbove(v);

		Point pt = calculatePosition(v, Position.Above);
		this.showAtLocation(v, Gravity.NO_GRAVITY, pt.x, pt.y);
		
		handleAutoHide();
	}
	
	public void showBelow(final View v) {
		boolean tokenValid = verifyWindowToken(v, null); // don't retry
		
		if(!tokenValid) return;
		
		triangleUp.setVisibility(View.VISIBLE);
		triangleDown.setVisibility(View.GONE);
		measureBelow(v);
		
		Point pt = calculatePosition(v, Position.Below);
		this.showAtLocation(v, Gravity.NO_GRAVITY, pt.x, pt.y);
		
		handleAutoHide();
	}
	
	public void showOnTop(final View v) {
		boolean tokenValid = verifyWindowToken(v, null); // don't retry
		
		if(!tokenValid) return;
		
		triangleUp.setVisibility(View.GONE);
		triangleDown.setVisibility(View.GONE);
		measureOnTop(v);

		Point pt = calculatePosition(v, Position.OnTop);
		this.showAtLocation(v, Gravity.NO_GRAVITY, pt.x, pt.y);
		
		handleAutoHide();
	}
	
	public void setAutoHideTimeout(long time_milllis) {
		hideMillis = time_milllis;
	}
	
	public void setAutoHide(boolean val) {
		if(!val)
			hideMillis = -1;
		else if(hideMillis == -1)
			hideMillis = DEFAULT_HIDE_TIMEOUT;
	}
}
