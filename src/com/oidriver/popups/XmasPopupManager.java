package com.oidriver.popups;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * A helper class that keeps track of the display counts for the various
 * Christmas popups. Use {@link #shouldDisplay(Popup)} and {@link #notifyDisplayed(Popup)
 * to keep track of the display counts.
 *
 * @author Delyan Kratunov (Touchnote)
 * 
 */
public class XmasPopupManager {

	private static final int POPUP_SHOW_FIRST_TIMES = 2;
	private static final int GESTURES_TUTORIAL_SHOW_FIRST_TIMES = 3;
	private static final String POPUP_PREFERENCES_KEY = "XMAS_POPUP_MANAGER_PREFERENCES";
	private static XmasPopupManager instance;
	public static XmasPopupManager getInstance(Context ctx) {
		if(instance == null)
			instance = new XmasPopupManager(ctx);
		
		return instance;
	}
	
	public static enum Popup { FrontText, 
		FrontNext, InsideText, InsideNext, PreviewDragDown, 
		PreviewText, PreviewChangesOnlyThisOne, PreviewSend,
		GesturesTutorial};
	
	private Set<Popup> mSessionPopups = new LinkedHashSet<Popup>();
	
	private int mSessionHash = Integer.MIN_VALUE;
	private Context mCtx;
	private SharedPreferences mPrefs;
	
	private XmasPopupManager(Context ctx) {
		mCtx = ctx;
		
		mPrefs = mCtx.getSharedPreferences(POPUP_PREFERENCES_KEY, Context.MODE_PRIVATE);
	}
	
	/**
	 * Begin a card-creation session. 
	 * @param obj - an object whose hash is used to identify this session
	 */
	public void beginSession(Object obj) {
		mSessionHash = obj.hashCode();
	}
	
	public void endSession(Object obj) {
		if(isSessionStarted() && obj.hashCode() == mSessionHash) {
			mSessionHash = Integer.MIN_VALUE;
			mSessionPopups.clear();
		}else
			throw new IllegalStateException("Session not started or using different object");
	}
	
	public boolean isSessionStarted() {
		return mSessionHash != Integer.MIN_VALUE;
	}
	
	private void checkSessionStarted() {
		if(!isSessionStarted())
			throw new IllegalStateException("Session not started, did you call beginSession?");
	}
	
	public boolean shouldDisplay(Popup popup) {
		checkSessionStarted();
		
		int count = mPrefs.getInt(popup.toString(), 0);
		
		if(popup == Popup.GesturesTutorial)
			return !mSessionPopups.contains(popup) && (count < GESTURES_TUTORIAL_SHOW_FIRST_TIMES);
		else // normal popups
			return !mSessionPopups.contains(popup) && (count < POPUP_SHOW_FIRST_TIMES);
	}
	
	public void notifyDisplayed(Popup popup) {
		checkSessionStarted();
		
		int count = mPrefs.getInt(popup.toString(), 0);
		
		mPrefs.edit().putInt(popup.toString(), count + 1).commit();
		mSessionPopups.add(popup);
	}
}
