package com.oidriver.popups;

import android.content.Context;
import android.text.TextUtils.TruncateAt;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.oidriver.R;

public class TextPopup extends TrianglePopup {

	protected TextView title;
	protected TextView text;

	public TextPopup(Context ctx, String title, String message) {
		super(ctx);
		
		initialize();
		
		setTitle(title);
		setMessage(message);
	}

	protected void initialize() {
		View layout = getContentView();
		title = (TextView) layout.findViewById(R.id.popupTitle);
		text = (TextView) layout.findViewById(R.id.popupText);
	}
	
	public void setTitle(String text) {
		if(text == null)
			title.setVisibility(View.GONE);
		else {
			title.setVisibility(View.VISIBLE);
			title.setText(text);
		}
	}
	
	public void setMessage(String message) {
		if(message == null)
			text.setVisibility(View.GONE);
		else {
			text.setVisibility(View.VISIBLE);
			text.setText(message);
		}
	}
	
	public void setSingleLine(boolean singleLine) {
		text.setSingleLine(singleLine);
		if(singleLine)
			text.setEllipsize(TruncateAt.END);
		else
			text.setEllipsize(TruncateAt.MARQUEE);
	}

	@Override
	protected View inflateContentView(LayoutInflater inflater) {
		return (LinearLayout) inflater.inflate(R.layout.popup, null);
	}
}
