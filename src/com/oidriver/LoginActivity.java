package com.oidriver;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.oidriver.receiver.NetworkUtils;
import com.oidriver.runnable.RunOn;
import com.oidriver.service.OiDriverService;
import com.oidriver.xmpp.Util;
import com.oidriver.xmpp.XMPPClient;

import android.content.res.Configuration;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Locale;


public class LoginActivity extends Activity implements OnClickListener {

	private static final String TAG = OiDriverApp.TAG;
	public static final int TIMEOUT_IN_SECONDS = 60;

	Button btnOK;
	Button btnCancel;
	EditText etPIN;
	EditText etJID;
	String pwd;

	LocationClient mLocationClient;
	double mLAT;
	double mLON;

	int mVersionCode = 0;

    private Keyboard mKeyboard;
    private KeyboardView mKeyboardView;

	interface VersionUpdateCallback {
		void onNewUpdateAvailable();
	}

	public void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);		
		setContentView(R.layout.login);

		TextView version = (TextView) findViewById(R.id.textViewVersion);
		PackageInfo info = null;
		try {
			info = getPackageManager().getPackageInfo("com.oidriver", 0);
			version.setText("v" + info.versionName);
			pwd = OiDriverService.getDeviceId(this);
			pwd = pwd.substring(pwd.length() - 4);
		} catch (NameNotFoundException e) {
			
			e.printStackTrace();
		}
		if (info != null)
			mVersionCode = info.versionCode;

        etPIN = (EditText) findViewById(R.id.editTextPin);
        etJID = (EditText) findViewById(R.id.editTextMgr);

		boolean bypass = getIntent().getBooleanExtra("bypass", false);

		// UpdateChecker checker = new UpdateChecker(this, true);
		// checker.checkForUpdateByVersionCode("URL with http:// to the text file with the version code");
		// checker.isUpdateAvailable();
		// checker.downloadAndInstall("URL with http:// to the location of the update apk");
		if (!bypass) {
			VersionUpdater versionUpdater = new VersionUpdater(this, null);
			versionUpdater.execute();
		}
		else {
			// TODO: bypass login
            etPIN.setText(pwd);
            RunOn.mainThreadDelayed(new Runnable() {
                @Override
                public void run() {
                    handleOkClick();
                }
            }, 250);
		}

		final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
		String company = sp.getString("company", OiDriverApp.COMPANY);
		if (!TextUtils.isEmpty(company))
			etJID.setText(company);

        etPIN.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                handleOkClick();
                return true;
            }
        });
        etJID.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                handleOkClick();
                return true;
            }
        });

		btnOK = (Button) findViewById(R.id.buttonOK);
		btnOK.setOnClickListener(this);
		
		btnCancel = (Button) findViewById(R.id.buttonCancel);
		btnCancel.setOnClickListener(this);

		final LocationRequest locationRequest = new LocationRequest();
		locationRequest.setExpirationDuration(60 * 1000); // 1 min
		locationRequest.setSmallestDisplacement(10);

		final LocationListener locationListener = new LocationListener() {
			@Override
			public void onLocationChanged(Location location) {
				try {
					mLAT = location.getLatitude();
					mLON = location.getLongitude();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};

		mLocationClient = new LocationClient(this, new GooglePlayServicesClient.ConnectionCallbacks() {
			@Override
			public void onConnected(Bundle bundle) {
				Location location = mLocationClient.getLastLocation();
				if (location != null) {
					mLAT = location.getLatitude();
					mLON = location.getLongitude();
				}
				mLocationClient.requestLocationUpdates(locationRequest, locationListener);
			}

			@Override
			public void onDisconnected() {

			}
		}, new GooglePlayServicesClient.OnConnectionFailedListener() {
			@Override
			public void onConnectionFailed(ConnectionResult connectionResult) {

			}
		});
		mLocationClient.connect();
		Log.i(TAG, this.getClass().getSimpleName() + ".onCreate()");

        final Configuration systemConfig = getResources().getConfiguration();
        if (systemConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mKeyboard = new Keyboard(this, R.xml.keyboard);
            mKeyboardView = (KeyboardView) findViewById(R.id.keyboard);
            if (mKeyboardView != null) {
                mKeyboardView.setKeyboard(mKeyboard);
                mKeyboardView.setOnKeyboardActionListener(new BasicOnKeyboardActionListener(this));
            }
        }

        etPIN.requestFocus();
	}

	@Override
	protected void onDestroy() {
		mLocationClient.disconnect();
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		if (v == btnOK)
		{
            handleOkClick();
		}
		else if (v == btnCancel)
		{
			setResult(RESULT_CANCELED);
			finish();
		}
	}

    private void handleOkClick() {
        String pin = etPIN.getEditableText().toString();
        if (TextUtils.isEmpty(pin) || !pin.equals(pwd)) {
            Toast.makeText(this, "Incorrect PIN", Toast.LENGTH_LONG).show();
            etPIN.setText("");
            etPIN.requestFocus();
        }
        else
        {
            String company = etJID.getEditableText().toString();
            if (!TextUtils.isEmpty(company))
            {
                final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
                sp.edit().putString("company", company).commit();
                if (company.equalsIgnoreCase("test"))
                {
                    OiDriverApp.MANAGER_JID = Constants.BOOKING_ID_SANDBOX;
                    OiDriverApp.XMPP_SERVICE = Constants.XMPP_SERVICE_SANDBOX;
                    OiDriverApp.COMPANY = Constants.COMPANY_SANDBOX;
                }
                else
                {
                    OiDriverApp.MANAGER_JID = String.format(Locale.UK, Constants.BOOKING_ID_FMT, company);
                    OiDriverApp.XMPP_SERVICE = String.format(Locale.UK, Constants.XMPP_SERVICE_FMT, company);
                    OiDriverApp.COMPANY = company;
                }
            }
            else
            {
                OiDriverApp.MANAGER_JID = Constants.BOOKING_ID_SANDBOX;
                OiDriverApp.XMPP_SERVICE = Constants.XMPP_SERVICE_SANDBOX;
                OiDriverApp.COMPANY = Constants.COMPANY_SANDBOX;
            }

            new ConnectToXmppTask().execute();
        }
    }

    public class VersionUpdater extends AsyncTask<Void, Void, String> {

		Activity mContext;
		HttpClient httpclient;
		HttpGet httpget;
		HttpResponse response;
		HttpEntity httpentity;
		OutputStream outputStream;
		ProgressDialog mProgressDialog;
		String apkName = "oidriver.apk";
		VersionUpdateCallback mCallback;

		public VersionUpdater(Activity act, VersionUpdateCallback callback) {
			mContext = act;
			mCallback = callback;
		}

		protected void onPreExecute () {

			//do not lock screen or drop connection to server on login
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			//initiate progress dialogue to block user input during initial data retrieval
			mProgressDialog = ProgressDialog.show(mContext, null, "Checking for updates", true, false);
		}

		//THDYwEqK44WPdYTOZAZpwEWROS4XsriNB4H9clNz
		@Override
		protected String doInBackground(Void... nothing) {

			if (NetworkUtils.getConnectivityStatus() == NetworkUtils.TYPE_NOT_CONNECTED)
				return "no updates";

			try {
				//set timeouts for httpclient
				HttpParams httpParameters = new BasicHttpParams();
				HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
				HttpConnectionParams.setSoTimeout(httpParameters, 5000);

				//setup http get
				httpclient = new DefaultHttpClient(httpParameters);
//				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, null);
				httpget = new HttpGet("https://s3-eu-west-1.amazonaws.com/oidriver/oidriver.info");

				// Execute HTTP Get Request
				response = httpclient.execute(httpget);
				httpentity = response.getEntity();

				String versionInfo = EntityUtils.toString(httpentity);
				Log.d("VERSION", versionInfo);
				int version = Integer.parseInt(versionInfo);
				if (version <= mVersionCode) {
					Log.i("VERSION", "No updates, exiting.");
					return "no updates";
				}

				httpget = new HttpGet("https://s3-eu-west-1.amazonaws.com/oidriver/oidriver.apk");
				response = httpclient.execute(httpget);
				httpentity = response.getEntity();

				//create location to store apk file
				String path = Environment.getExternalStorageDirectory() + "/download/";
				File file = new File(path);
				file.mkdirs();  //if download folder not already exist
				File outputFile = new File(file, "oidriver.apk");

				//write downloaded file to location
				outputStream = new FileOutputStream(outputFile, false);
				httpentity.writeTo(outputStream);
				outputStream.flush();
				outputStream.close();

				return "success";
			}
			catch (Exception e) {
				return "error: " + e.toString();
			}

		}

		@Override
		protected void onPostExecute(String result) {

			//close update dialog
			try {
				mProgressDialog.dismiss();
			} catch (Exception e) {
				// nothing
			}

			//check if result null or empty
			if (TextUtils.isEmpty(result)) {
				return;
			}

			//update downloaded
			if (result.equals("success"))   {
				//install downloaded .apk file
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/download/" + apkName)), "application/vnd.android.package-archive");
				startActivity(intent);
			}
			//update not downloaded
			else if (result.equals("no updates")) {
				// OK
			}
			else {
				Toast.makeText(mContext, "Could Not Download Updates, Please Try Again Later.", Toast.LENGTH_LONG).show();
			}

			//release screen lock
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}
	}

	class ConnectToXmppTask extends AsyncTask<Void, Void, Boolean> {

		ProgressDialog mProgressDialog;
		boolean mGotLogonMessage = false;
		String mLogonMessage;
		String mErrorMessage;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			mProgressDialog = new ProgressDialog(LoginActivity.this);
			mProgressDialog.setMessage("Please wait. Logging in...");
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialogInterface) {
					cancel(true);
				}
			});
			mProgressDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... voids) {

			XMPPClient client = OiDriverService.getXmppClient();
			boolean connected = client.connectToServer();
			if (!connected)
				return false;

			connected = client.login();
			if (!connected)
				client.disconnect();

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

			OiDriverService.lat = mLAT;
			OiDriverService.lon = mLON;

			OiDriverService.sendIQForGPS(false, Constants.DRIVER_STATUS_LOG);
			return connected;
		}

		@Override
		protected void onPostExecute(Boolean aSuccess) {
			super.onPostExecute(aSuccess);

			if (mProgressDialog != null) {
                try {
                    mProgressDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mProgressDialog = null;
			}

			if (aSuccess && !mGotLogonMessage && !isCancelled()) {
//				OiDriverService.lat = mLAT;
//				OiDriverService.lon = mLON;
				setResult(RESULT_OK);
				finish();
			}
			else if (mGotLogonMessage)
				RunOn.mainThreadDelayed(new Runnable() {
					@Override
					public void run() {
						showLogonMessage();
						;
					}
				}, 1000);
			else {
				if (TextUtils.isEmpty(mErrorMessage))
					Toast.makeText(LoginActivity.this, "NO RESULTS FROM SERVER", Toast.LENGTH_LONG).show();
				else
					Toast.makeText(LoginActivity.this, mErrorMessage, Toast.LENGTH_LONG).show();
			}
		}

		void showLogonMessage() {
			String text = Util.parseLogonMessage(mLogonMessage);
			if (TextUtils.isEmpty(text))
				return;
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View content = inflater.inflate(R.layout.logon_message, null);

			TextView messageView = (TextView)content.findViewById(R.id.textViewMessage);
			messageView.setText(text);

			final Button button1 = (Button)content.findViewById(R.id.button1);
			final AlertDialog bidAlert = new AlertDialog.Builder(LoginActivity.this)
					.setTitle("Message to driver")
					.setIcon(R.drawable.passenger_inverted)
					.setView(content).create();

			button1.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					bidAlert.dismiss();
				}
			});
			bidAlert.show();
		}
	}
}
