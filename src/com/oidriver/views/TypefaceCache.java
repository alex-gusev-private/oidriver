package com.oidriver.views;

import android.content.Context;
import android.graphics.Typeface;

import java.lang.ref.SoftReference;
import java.util.Map;
import java.util.TreeMap;

/**
 * A SoftReference cache to store Typefaces while they're needed.
 *
 */
public class TypefaceCache {
	private static TypefaceCache mInstance;
	public static TypefaceCache getInstance(Context ctx) {
		if(mInstance == null)
			mInstance = new TypefaceCache(ctx.getApplicationContext());
		return mInstance;
	}
	
	private Context mContext;
	private Map<String, SoftReference<Typeface>> mCache = 
			new TreeMap<String, SoftReference<Typeface>>();
	
	private TypefaceCache(Context ctx) {
		if(ctx == null)
			throw new IllegalArgumentException("Need a valid context when creating TypefaceCache");
		
		// Do not potentially hold a reference to a View Context,
		// use the application one instead
		mContext = ctx.getApplicationContext();
	}
	
	private Typeface loadFromFile(String filePath) {
		try {
			return Typeface.createFromFile(filePath);
		} catch(Exception e) {
			return null;
		}
	}
	
	private Typeface loadFromAsset(String assetPath) {
		try {
			return Typeface.createFromAsset(mContext.getAssets(), assetPath);
		}catch(Exception e) {
			return null;
		}
	}
	
	private void put(String path, Typeface typeface) {
		mCache.put(path, new SoftReference<Typeface>(typeface));
	}
	
	private Typeface get(String path) {
		Typeface result = null;
		
		if(mCache.containsKey(path)) {
			SoftReference<Typeface> ref = mCache.get(path);
			result = ref.get();
		}
		
		return result;
	}
	
	public Typeface getAssetFont(String assetPath) {
		Typeface result = get(assetPath);
		
		if(result == null) {
			result = loadFromAsset(assetPath);
			put(assetPath, result);
		}
		
		return result;
	}
	
	public Typeface getFileFont(String filePath) {
		Typeface result = get(filePath);
		
		if(result == null) {
			result = loadFromFile(filePath);
			put(filePath, result);
		}
		
		return result;
	}
}
