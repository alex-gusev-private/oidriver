package com.oidriver.views;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.oidriver.OiDriverActivity;
import com.oidriver.R;
import com.oidriver.runnable.RunOn;
import com.oidriver.xmpp.Util;

public class NewJobView extends RelativeLayout {

    private static final String ARG_TITLE = "title";
    private static final String ARG_JOB_ID = "job_id";
    private static final String ARG_PICKUP = "pickup";
    private static final String ARG_DROPOFF = "dropoff";

    public static final int ACCEPT_JOB_TIMEOUT = 20 * 1000;

    private ProgressBar mJobProgress;

    public NewJobView(Context context) {
        super(context);
        init((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
    }

    public NewJobView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
    }

    public NewJobView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
    }

    public interface NewJobListener {
        void onAccept(String jobId);
        void onReject(String jobId);
        void onTimedOut(String jobId);
    }

    private NewJobListener mListener;

    private String mTitle;
    private String mJobId;
    private String mPickup;
    private String mDropoff;

    CountDownTimer mJobAlertCountdownTimer;

    public void setListener() {
        //setListener(getListener(NewJobListener.class, this));
    }

	public View init(LayoutInflater inflater) {
		final View view = inflater.inflate(R.layout.alert_new_job, null);


        mJobProgress = (ProgressBar) view.findViewById(R.id.progressBarTimeout);

        Button mBtnAccept = (Button) view.findViewById(R.id.button1);
        mBtnAccept.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mListener != null) mListener.onAccept(mJobId);
            }
        });
        Button mBtnReject = (Button) view.findViewById(R.id.button2);
        mBtnReject.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mListener != null) mListener.onReject(mJobId);
            }
        });

        return view;
	}

//    @Override
//    public void onStart() {
//        super.onStart();
//
//        mJobAlertCountdownTimer = createCountDownTimer();
//        RunOn.taskThread(new Runnable() {
//
//            @Override
//            public void run() {
//                ((OiDriverActivity)getActivity()).playSoundSync(R.raw.new_job_offer,
//                        new OiDriverActivity.MediaCallback() {
//
//                            @Override
//                            public void onFinished() {
//                                mJobAlertCountdownTimer.start();
//                            }
//                        });
//            }
//        });
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//
//        if (mJobAlertCountdownTimer != null)
//            mJobAlertCountdownTimer.cancel();
//    }

    private void setListener(NewJobListener listener) {
        mListener = listener;
    }

    /**
     * A helper function that given a type and a fragment, will attempt to extract
     * the fragment's listener.
     *
     * @param cls - the listener's type
     * @param fragment - the fragment whose listener we're trying to find
     * @return - the fragment's listener or null if neither the activity or the parent fragment subclass cls
     */
    @SuppressWarnings("unchecked")
    public static <T> T getListener(Class<T> cls, Fragment fragment) {
        FragmentActivity activity = fragment.getActivity();
        Fragment parent = fragment.getParentFragment();
        Fragment target = fragment.getTargetFragment();

        if(target != null && cls.isAssignableFrom(Util.getClazz(target))) {
            return (T) target;
        } else if(parent != null && cls.isAssignableFrom(Util.getClazz(parent))) {
            return (T) parent;
        } else if (activity != null && cls.isAssignableFrom(activity.getClass())){
            return (T) activity;
        }

        return null;
    }

    private CountDownTimer createCountDownTimer()
    {
        return new CountDownTimer(ACCEPT_JOB_TIMEOUT, 1000) {

            public void onTick(long millisUntilFinished) {
                if (mJobProgress != null)
                    mJobProgress.incrementProgressBy(1);
                OiDriverActivity.playSoundSync(R.raw.job_timer, null);
            }

            public void onFinish() {
                if (mJobProgress != null)
                    mJobProgress.setProgress(mJobProgress.getMax());
                if (mListener != null) mListener.onTimedOut(mJobId);
            }
        };
    }

    public void setData(String title, String pickup, String dropoff) {
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText(title);
        TextView tvPickupAddress = (TextView) findViewById(R.id.tvPickup);
        tvPickupAddress.setText(pickup);
        TextView tvDropoffAddress = (TextView) findViewById(R.id.tvDropoff);
        tvDropoffAddress.setText(dropoff);
    }
}