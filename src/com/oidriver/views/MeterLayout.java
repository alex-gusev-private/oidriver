package com.oidriver.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.oidriver.R;

/**
 * Created by touchnote on 18/12/2013.
 */
public class MeterLayout extends RelativeLayout {
    public MeterLayout(Context context) {
        super(context);
        init(context);
    }

    public MeterLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MeterLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context ctx) {
        LayoutInflater inflater = ((LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
        inflater.inflate(R.layout.meter, this, true);
    }
}
