package com.oidriver.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import com.oidriver.R;

public class ExternalFontTextView extends TextView {

	public ExternalFontTextView(Context context) {
		super(context);
	}
	
	public ExternalFontTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initFromAttrs(attrs);
	}

	public ExternalFontTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initFromAttrs(attrs);
	}
	
	private void initFromAttrs(AttributeSet attrs) {
		TypedArray array = getContext().getTheme().obtainStyledAttributes(
		        attrs,
		        R.styleable.ExternalFontTextView,
		        0, 0);
		
		try {
			String assetPath = array.getString(R.styleable.ExternalFontTextView_fontAsset);
			String filePath = array.getString(R.styleable.ExternalFontTextView_fontFile);
			
			if(!TextUtils.isEmpty(assetPath) && !TextUtils.isEmpty(filePath)) {
				throw new IllegalStateException("View defines both fontAsset and fontFile, choose one!");
			}
			
			if(assetPath != null) {
				Typeface typeface = TypefaceCache.getInstance(getContext()).getAssetFont(assetPath);
				setTypeface(typeface);
			} else if (filePath != null) {
				Typeface typeface = TypefaceCache.getInstance(getContext()).getAssetFont(filePath);
				setTypeface(typeface);
			}
		} finally {
			array.recycle();
		}
	}
}
