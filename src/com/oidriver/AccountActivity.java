package com.oidriver;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;

import com.oidriver.fragments.OiAccountFragment;
import com.oidriver.service.OiDriverService;

import java.util.Calendar;

public class AccountActivity extends FragmentActivity {

	boolean lockedDB = false;
	public static Calendar currentDate = Calendar.getInstance();
//	private SimpleDateFormat sdf = new SimpleDateFormat("MMM DD, yyyy", Locale.UK);
//	private SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
	private OiAccountFragment mAccountFragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);	
		setContentView(R.layout.account);
		
        if (!OiDriverService.dbHelper.tryOpen())
        {
        	Toast.makeText(this, "Database is locked, please try again later!", Toast.LENGTH_LONG).show();
        	finish();
        	return;
        }

        lockedDB = true;
        
        mAccountFragment = (OiAccountFragment) getSupportFragmentManager().findFragmentById(R.id.accountlist_fragment);
//		TextView version = (TextView) findViewById(R.id.textViewVersion);
//		PackageInfo info;
//		try {
//			info = getPackageManager().getPackageInfo("com.oidriver", 0);
//			version.setText("v" + info.versionName);
//		} catch (NameNotFoundException e) {
//			
//			e.printStackTrace();
//		}

        ImageButton btnPrev = (ImageButton) findViewById(R.id.buttonPrev);
        btnPrev.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				currentDate.add(Calendar.DAY_OF_MONTH, -1);
				mAccountFragment.refresh(currentDate.getTime());
			}
		});
        ImageButton btnNext = (ImageButton) findViewById(R.id.buttonNext);
        btnNext.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				currentDate.add(Calendar.DAY_OF_MONTH, 1);
				mAccountFragment.refresh(currentDate.getTime());
			}
		});
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		if (lockedDB)
			OiDriverService.dbHelper.close();
	}
}
