package com.oidriver;

import java.util.Locale;

public class ZoneInfo implements Comparable {

	public String name;
	public String drivers;
	public String passengers;
	
	public ZoneInfo(String name, String drivers, String passengers)
	{
		this.name = name;
		this.drivers = drivers;
		this.passengers = passengers;
	}
	
	public String toString()
	{
		return String.format("%s\t%s\t%s", name, drivers, passengers);
	}

	@Override
	public int compareTo(Object another) {
		
		String anotherName = another.toString().toUpperCase(Locale.UK);
		return name.toUpperCase(Locale.UK).compareToIgnoreCase(anotherName);
	}
}
