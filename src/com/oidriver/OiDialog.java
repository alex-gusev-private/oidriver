package com.oidriver;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class OiDialog extends Dialog {
	protected Context context;
	
	// Constructor.
	public OiDialog(Context context, int contentLayoutId, int titleStringId) {
		this(context, android.R.style.Theme_Dialog, contentLayoutId, titleStringId, 0);
		this.context = context;
		this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
	}

	// Constructor with additional style parameter; use R.style.oidriverDialogBlueBorder for the blue border dialogs
	public OiDialog(Context context, final int style, final int contentLayoutId, final int titleStringId, final int titleIconId){
		this(context, R.layout.dialog_frame, style, contentLayoutId, titleStringId, titleIconId);
	}

	public OiDialog(Context context, final int frameResId, final int style, final int contentLayoutId, final int titleStringId, final int titleIconId) {
		super(context, style);
		setContentView(frameResId);
		this.context = context;
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		
		setTitle(titleStringId);
		setIcon(titleIconId);

		FrameLayout contentView = (FrameLayout) findViewById(R.id.content);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(contentLayoutId, contentView);

		// On Honeycomb the blurring can actually result in the background being masked
		// out, and yes, Google say this is intentional
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			//this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
			this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
		}
		
	}

	public OiDialog(Context context, final int frameResId, final int style, final int contentLayoutId, final String titleString, final int titleIconId) {
		super(context, style);
		setContentView(frameResId);
		this.context = context;
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		
		setTitle(titleString);
		setIcon(titleIconId);

		FrameLayout contentView = (FrameLayout) findViewById(R.id.content);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(contentLayoutId, contentView);

		// On Honeycomb the blurring can actually result in the background being masked
		// out, and yes, Google say this is intentional
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			//this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
			this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
		}
		
	}

	/**
	 * Return the blue border style for tablets and the usual style for other devices
	 * @return
	 */
	public static int getAppropriateStyle(){
		return Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB ? R.style.oidriverDialog : R.style.oidriverDialogBlueBorder;
	}

	@Override
	public void setTitle(CharSequence title) {
		TextView tvTitle = (TextView) findViewById(R.id.title);
		if (tvTitle != null) {
			if (title == null || title.equals("")) {
				tvTitle.setVisibility(View.GONE);
				findViewById(R.id.separator).setVisibility(View.GONE);
			} else {
				tvTitle.setText(title);
				tvTitle.setVisibility(View.VISIBLE);
				findViewById(R.id.separator).setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	public void setTitle(int titleId) {
		if (titleId > 0) {
			this.setTitle(getContext().getText(titleId));
		} else {
			this.setTitle(null);
		}
	}
	
	/**
	 * Set dialog title icon (Like for about dialog.). NB: is only meant to be used when dialog has a title
	 * @param titleIconId
	 */
	public void setIcon(int titleIconId) {
		ImageView imageTitle = (ImageView) findViewById(R.id.title_icon);
		if (imageTitle != null) {
			if (titleIconId > 0) {
				imageTitle.setImageResource(titleIconId);
				imageTitle.setVisibility(View.VISIBLE);
			} else {
				imageTitle.setVisibility(View.GONE);
			}
		}
	}

}
