package com.oidriver;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.oidriver.JobOffersAdapter.OnJobSelectedListener;
import com.oidriver.PurgeBidsTask.PurgeBidsListener;
import com.oidriver.fragments.OiBookingsFragment;
import com.oidriver.fragments.OiJobFragment;
import com.oidriver.fragments.OiMapFragment;
import com.oidriver.fragments.OiMeterFragment;
import com.oidriver.fragments.OiNewJobAlertFragment;
import com.oidriver.fragments.OiZonesFragment;
import com.oidriver.popups.TextPopup;
import com.oidriver.popups.TrianglePopup;
import com.oidriver.receiver.NetworkUtils;
import com.oidriver.receiver.OiAlarmReceiver;
import com.oidriver.runnable.RunOn;
import com.oidriver.service.OiDriverService;
import com.oidriver.service.XmppResultsReceiver;
import com.oidriver.xmpp.BookingIQProvider;
import com.oidriver.xmpp.Util;
import com.oidriver.xmpp.XMPPClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

public class OiDriverActivity extends FragmentActivity
		implements View.OnClickListener, PurgeBidsListener, OnJobSelectedListener,
                   OiNewJobAlertFragment.NewJobListener,
                   OiMapFragment.OiDriverLocationListener {

	private static final String TAG = OiDriverApp.TAG;
	public static final String EMPTY_STRING = "";
	public static final String KEY_RTW = "RTW";
    private static final String FRAGMENT_TAG_ALERT_NEWJOB =
            OiDriverActivity.class.getSimpleName() + ".NEW_JOB";

    private static MediaPlayer mMediaPlayer;

	public Intent service;

	private AlertDialog mBidAlert;
	
	private TextView mCurrentZoneName;
	private ImageView statusConnection;
	private ImageView statusGps;
	private ProgressBar connectionProgress;
	private Timer mConnectinStatusTimer = new Timer();
	
	public OiMapFragment mMapFragment;
	public OiJobFragment mJobFragment;
	public OiBookingsFragment mBookingsFragment;
	private OiZonesFragment mZonesFragment;
    private OiMeterFragment mMeterFragment;

	private ArrayBlockingQueue<AcceptedJob> jobsQueue = new ArrayBlockingQueue<AcceptedJob>(50);

	private Button mInfoJobs;
	private Button mZones;
	private Button mShowJobs;
	private Button mOptions;
	
	boolean zonesRequestOn;
	boolean userLoggedIn = false;

	JobStatusSender mJobStatusSender;

	private ProgressDialog mProgressDialog;

	AlertDialog mDriverStatusAlert;
	public XmppResultsReceiver mXmppResultsReceiver;
	private TextView mReconnectionStatus;

	int[] mStatuses = new int[] {
			R.drawable.disconnected,
			R.drawable.gprs_only,
			R.drawable.connected
	};
	String mVersion;

	//ProgressBar mBatteryBar;

	static int mRunningInstancesCnt = 0;

    /**
     * The Pandora box of flags to control Taximeter behaviour
     */
    public static boolean mIsForeground = false;
    boolean mIsNewIntentRequested = false;
    public static boolean mIsTaximeterVisible = false;

    public interface MediaCallback
	{
		void onFinished();
	}

	public interface OnBookingSearchListener {
		void onSearchCompleted();
	}

	OnBookingSearchListener mOnBookingSearchListener;

    ArrayList<OiMapFragment.OiDriverLocationListener> mLocationListeners = new ArrayList<OiMapFragment.OiDriverLocationListener>();

	@SuppressLint("NewApi")
	public void onCreate(Bundle paramBundle) {
		
		Log.i(TAG, ".onCreate()");
		
		super.onCreate(paramBundle);

		if (!isTaskRoot())
		{
			final Intent intent = getIntent();
			final String intentAction = intent.getAction();
			if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && intentAction != null && intentAction.equals(Intent.ACTION_MAIN))
			{
				Log.w(TAG, "Main Activity is not the root.  Finishing Main Activity instead of launching.");
				finish();
				return;
			}
		}

		PackageInfo info;
		try {
			PackageManager pm = getPackageManager();
			if (pm != null) {
				info = pm.getPackageInfo("com.oidriver", 0);
				if (info != null)
					mVersion = "v" + info.versionName;
				else
					mVersion = "UNKNOWN";
			}
			else
				mVersion = "UNKNOWN";
		} catch (PackageManager.NameNotFoundException e) {

			e.printStackTrace();
		}

		final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
		sp.edit().putInt("RunningCnt", 1).commit();

		mJobStatusSender = new JobStatusSender(this);

		int gpAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (gpAvailable != ConnectionResult.SUCCESS)
		{
			Dialog dlg = GooglePlayServicesUtil.getErrorDialog(gpAvailable, this, 0, new DialogInterface.OnCancelListener() {
				
				@Override
				public void onCancel(DialogInterface dialog) {
					finish();
				}
			});
			dlg.show();
			return;
		}
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);		
		setContentView(R.layout.oidriver_main);	

		Log.i(TAG, ".onCreate()");

		////???? sp.edit().putBoolean("UserLoggedIn", userLoggedIn).commit();
		service = new Intent(this, OiDriverService.class);
		mXmppResultsReceiver = new XmppResultsReceiver(this);
		if (!isMyServiceRunning())
			startService(service);

		Intent loginIntent = new Intent(this, LoginActivity.class);
		userLoggedIn = sp.getBoolean("UserLoggedIn", false);
		long loginTime = sp.getLong("UserLoginTime", 0);
		if (userLoggedIn && System.currentTimeMillis() - loginTime < 12 * 60 * 60 * 1000) {
			loginIntent.putExtra("bypass", true);
		}
		startActivityForResult(loginIntent, Constants.LOGIN_REQUEST_CODE);

		File devVersion = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/com.taxi.dev.txt");
		boolean devMode = false;
		if (devVersion.exists())
			devMode = true;

		// Set the hardware buttons to control the music
	    this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    AudioManager mgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		int volume = devMode ? 0 : mgr.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		mgr.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);

		final Timer tm = new Timer();
		final TextView clock = (TextView) findViewById(R.id.digitalClock1);
		tm.schedule(new TimerTask() {
			
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.UK);
						clock.setText(sdf.format(Calendar.getInstance().getTime()));
					}
				});
			}
		}, 50, 60*1000);

		//mBatteryBar = (ProgressBar) findViewById(R.id.progressBar);

		mCurrentZoneName = (TextView) findViewById(R.id.tvCurrentZone);
		mCurrentZoneName.setSelected(true);
		
		statusConnection = (ImageView) findViewById(R.id.statusImage);
		statusGps = (ImageView) findViewById(R.id.statusGps);
		connectionProgress = (ProgressBar)findViewById(R.id.connectionSpinner);

		mMapFragment = (OiMapFragment) getSupportFragmentManager().findFragmentById(R.id.layoutMap);
		mMapFragment.setMapClickedListener(mMapCLickedListener);
		
		mBookingsFragment = (OiBookingsFragment) getSupportFragmentManager().findFragmentById(R.id.layoutJobPages);
		mJobFragment = (OiJobFragment) getSupportFragmentManager().findFragmentById(R.id.layoutSelectedJob);
		mZonesFragment = (OiZonesFragment) getSupportFragmentManager().findFragmentById(R.id.layoutZones);
        mMeterFragment = (OiMeterFragment) getSupportFragmentManager().findFragmentById(R.id.layoutMeter);
        ////mLocationListeners.add(mMeterFragment);

		hideBookingsFragment(true);
		hideJobFragment(true);
		hideZonesFragment(true);
        hideMeterFragment(true, false);

		final Button infoDrivers = (Button) findViewById(R.id.buttonCars);
		mInfoJobs = (Button) findViewById(R.id.buttonJobs);
		mZones = (Button) findViewById(R.id.button_zones);
		mShowJobs = (Button) findViewById(R.id.button_show_jobs);
		mOptions = (Button) findViewById(R.id.button_options);

		updateDriverRankUI(0);
        Log.wtf("READY", "[0] updateDriverRankUI()");

		//infoDrivers.setEnabled(false);
		infoDrivers.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View content = inflater.inflate(R.layout.custom_driver_status, null);
				if (content == null) {
					Log.e(TAG, "CANNOT CREATE DIALOG");
					showToast("CANNOT CREATE DIALOG", Toast.LENGTH_LONG);
					return;
				}

				final Button button1 = (Button)content.findViewById(R.id.button1);
				final Button button2 = (Button)content.findViewById(R.id.button2);
				final Button button3 = (Button)content.findViewById(R.id.button3);
				final Button button4 = (Button)content.findViewById(R.id.button4);
                final Button button5 = (Button)content.findViewById(R.id.button5);

//                if (!OiDriverService.readyStatus)
//                    button1.setEnabled(false);
//                else
                    button1.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (mDriverStatusAlert != null) {
                                mDriverStatusAlert.dismiss();
                                mDriverStatusAlert = null;
                            }
                            OiDriverService.readyStatusNext = false;
                            sendDriverStatusUpdate(Constants.DRIVER_STATUS_BRK);
                        }
                    });

//                if (OiDriverService.readyStatus)
//                    button2.setEnabled(false);
//                else
                    button2.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (mDriverStatusAlert != null) {
                                mDriverStatusAlert.dismiss();
                                mDriverStatusAlert = null;
                            }
                            OiDriverService.readyStatusNext = true;
                            sendDriverStatusUpdate(Constants.DRIVER_STATUS_RTW);
                        }
                    });

				button3.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View view) {
						if (mDriverStatusAlert != null) {
							mDriverStatusAlert.dismiss();
							mDriverStatusAlert = null;

						}
						playButtonClickSound();
						OiDriverService.sendIQForGPS(false, Constants.DRIVER_STATUS_POS);
					}
				});

				button4.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View view) {
						if (mDriverStatusAlert != null) {
							mDriverStatusAlert.dismiss();
							mDriverStatusAlert = null;
						}
						playButtonClickSound();

						new ConnectToXmppTask().execute();
                    }
				});

                button5.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mDriverStatusAlert != null) {
                            mDriverStatusAlert.dismiss();
                            mDriverStatusAlert = null;
                        }
                        playButtonClickSound();

                        if (mMeterFragment != null) {
                            mIsTaximeterVisible = true;
                            //////mMeterFragment.setData(1, "0.00");
                            hideMeterFragment(false);
                        }
/*
                        // TEST POPUPS OVER METER
                        final Timer tm = new Timer();
                        tm.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                AcceptedJob currentJob = new AcceptedJob();
                                currentJob.setJobId("1");
                                currentJob.setPickup("ALEX");
                                currentJob.setDropoff("MAX");
                                currentJob.setSentBy("alex@touchnote.com");
                                Intent intent = new Intent("com.oidriver.NEW_JOB");
                                intent.putExtra("job", currentJob);
                                intent.putExtra("type", "joboffer");
                                OiDriverApp.getAppContext().sendBroadcast(intent);
                            }
                        }, 3000);
*/
                    }
                });

				final AlertDialog.Builder builder = new AlertDialog.Builder(OiDriverActivity.this)
						.setIcon(R.drawable.passenger_inverted)
						.setView(content);

				TextView message = (TextView) content.findViewById(R.id.textViewPickup);
				if (message != null) {
					CharSequence text = message.getText();
					if (text != null && !TextUtils.isEmpty(text)) {
						String updated = String.format(Locale.UK, text.toString(),
								OiDriverService.readyStatus ?
										Constants.DRIVER_STATUS_RTW : Constants.DRIVER_STATUS_BRK);
						message.setText(updated);
					}
				}
				mDriverStatusAlert = builder.create();
				mDriverStatusAlert.show();
			}
		});
		mZones.setOnClickListener(this);
		mShowJobs.setOnClickListener(this);
		mOptions.setOnClickListener(this);		
		
		mBookingsFragment.populateTotals();
		
		if (sp.contains(KEY_RTW))
		{
			OiDriverService.readyStatus = sp.getBoolean(KEY_RTW, false);
		}
		else
		{
			OiDriverService.readyStatus = false;
			sp.edit().putBoolean(KEY_RTW, OiDriverService.readyStatus).commit();
		}
        Log.wtf("READY", "[0] " + OiDriverService.readyStatus);
		
		registerReceivers();
		
		mConnectinStatusTimer.schedule(new TimerTask() {

			@Override
			public void run() {

				RunOn.mainThread(new Runnable() {

					@Override
					public void run() {

						updateConnectionStatus();
					}
				});
			}
		}, 2000, 30000);

		final TextView tv = (TextView)findViewById(R.id.textViewGPS);
		tv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				OiDriverService.sendIQForGPS(false, "GPS");
			}
		});

		mReconnectionStatus = (TextView) findViewById(R.id.textViewReconnect);
	}

	private void sendDriverStatusUpdate(String status) {
		sendDriverStatusUpdate(status, true);
	}

	private void sendDriverStatusUpdate(String status, boolean click) {

		if (!OiDriverService.isXmppConnected()) {
			OiDriverService srv = OiDriverService.get();
			if (srv != null)
			{
				srv.connectToXmpp();
				Log.e(TAG, "NOT CONNECTED - LOGGING IN...");
				showToast("Reconnecting...", Toast.LENGTH_LONG);
			}
			return;
		}

//		String message = String.format("WK#,0000,1,%s,3,0,%s",
//									  status, OiDriverService.currentZoneName);
//		OiDriverService.sendMessage(message, OiDriverApp.MANAGER_JID);
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Please wait...");
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.show();
		if (click) playButtonClickSound();
		//OiDriverService.sendIQForGPS(false, status);

		// TEST: send it via measage
		String message = String.format("WK#,0000,1,%s,3,0,%s,",
				status, OiDriverService.currentZoneName) +
				XMPPClient.getServiceGpsStringRaw();
		OiDriverService.sendMessage(message, OiDriverApp.MANAGER_JID);

		final CountDownTimer requestTimer = new CountDownTimer(Constants.OIDRIVER_XMPP_PACKET_REPLY_TIMEOUT, 1000) {
			@Override
			public void onTick(long l) {

			}

			@Override
			public void onFinish() {
				if (mProgressDialog != null) {
					if (mProgressDialog.isShowing()) mProgressDialog.dismiss();
					mProgressDialog = null;
				}
			}
		};

		requestTimer.start();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		Log.i(TAG, ".onNewIntent()");
		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		setIntent(intent);

        mIsNewIntentRequested = false;
	}

	@Override
	public void onStart() {
		super.onStart();
		
		Log.i(TAG, ".onStart()");
		try
		{
			resetToMyLocation();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

	protected void onPause() {
//        disconnectFromTaximeterService();

		super.onPause();
		Log.i(TAG, ".onPause()");
		dismissPopups();

        mIsForeground = false;
	}

	protected void onResume() {
		super.onResume();
		Log.i(TAG, ".onResume()");
        mIsForeground = true;
		
		new PurgeBidsTask(this).execute();
		resetToMyLocation();
		//updateDriverStateUI();
		updateDriverRankUI(mCurrentRank);
        Log.wtf("READY", "[1] updateDriverRankUI()");
		updateConnectionStatus();
		
		if (mMapFragment != null)
			statusGps.setImageDrawable(getResources().getDrawable(mStatuses[mMapFragment.getGpsStatusIdx()]));
//			statusGps.setImageDrawable(getResources().getDrawable(
//					mMapFragment.hasGPSFix() ? R.drawable.connected : R.drawable.disconnected));

		if (AcceptedJob.hasActiveJob())
		{
			hideBookingsFragment(true);
			hideJobFragment(false);

//            connectToTaximeterService();
		}

        mIsNewIntentRequested = false;
	}
	
	@Override
	public void onStop() {
		super.onStop();
		Log.i(TAG, ".onStop()");
	}
	
	@Override
	protected void onDestroy() {

		super.onDestroy();
		Log.i(TAG, ".onDestroy()");


		try {
			if (mConnectinStatusTimer != null)
				mConnectinStatusTimer.cancel();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
		sp.edit().putInt("RunningCnt", 0).commit();
		
		try
		{
			if (service != null)
			{
				// Unregister activity's receivers
				unregisterReceivers();
				// Disable further reconnection
//				OiDriverService srv = OiDriverService.get();
				//v145
//				if (srv != null)
//					srv.disableReconnection();
				// Stop the service
				stopService(service);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		mRunningInstancesCnt--;

		// Perform harakiri...
		int pid = android.os.Process.myPid();
		android.os.Process.killProcess(pid);
	}

	public boolean insertJob(AcceptedJob acceptedJob) {

		try {
			Map<String, Object> params = new HashMap<String, Object>();
			
			params.put("booking_id", acceptedJob.getBookingId());
			params.put("name", acceptedJob.getName());
			params.put("pickup", acceptedJob.getPickup());			
			params.put("dropoff", acceptedJob.getDropoff());
			params.put("telephone", acceptedJob.getTelephone());
			params.put("timeofbooking", acceptedJob.getBookingTime());
			params.put("jobid", acceptedJob.getJobId());
			params.put("notes", acceptedJob.getNote());
			params.put("customerpickuplat", acceptedJob.getCustomerPickupLat());
			params.put("customerpickuplon", acceptedJob.getCustomerPickupLon());
			params.put("customerdropofflat", acceptedJob.getCustomerDropoffLat());
			params.put("customerdropofflon", acceptedJob.getCustomerDropoffLon());
			params.put("passengers", acceptedJob.getNop());
			params.put("jobref", acceptedJob.getJobRef());
			params.put("postcode1", acceptedJob.getPostCode1());
			params.put("postcode2", acceptedJob.getPostCode2());
			params.put("housenumber1", acceptedJob.getHouseNumber1());
			params.put("housenumber2", acceptedJob.getHouseNumber2());
			params.put("pickupzoneid", acceptedJob.getPickupZoneId());
			params.put("dropoffzoneid", acceptedJob.getDropoffZoneId());
			params.put("fareprice", acceptedJob.getFarePrice());
			params.put("custtype", acceptedJob.getCustType());
			params.put("status", acceptedJob.getStatus());
			
			boolean success = OiDriverService.dbHelper.insertJob(params);
			if (success)
			{
				try {
					Calendar calNow = Calendar.getInstance();
					String dtStr = acceptedJob.getBookingTime();
					Log.d(TAG, ">>> INSERT JOB: date = " + dtStr);
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
					Date dt = sdf.parse(dtStr);
					Calendar cal = Calendar.getInstance();
					cal.setTime(dt);
					cal.add(Calendar.MINUTE, -30);
					// job is after (now + 30min) - set alarm 
					if (cal.after(calNow))
					{
						OiAlarmReceiver.SetAlarm(this, acceptedJob.getJobId(), cal);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else
			{
				Log.e(TAG, "FAILED TO INSERT JOB TO DB");
			}
			return success;
		} catch (Exception e) {
			Log.e(TAG, "ERROR : INSERTING  AcceptedJob");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean onKeyDown(final int keyCode, final KeyEvent event)  {		
	    if ((keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME) && event.getRepeatCount() == 0) 
	    {
            if (mIsTaximeterVisible) {
                hideMeterFragment(true);
                //////mMeterFragment.onStop();
                return true;
            }

            new AlertDialog.Builder(OiDriverActivity.this)
                .setTitle("OiDriver")
                .setMessage("Are you sure want to leave?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int whichButton)
                    {
                        finish();
                    }
                 })
                .setNegativeButton(android.R.string.no, null)
            .show();
	        return true;
	    }	    
	    else
	    {
	    	return super.onKeyDown(keyCode, event);
	    }
	}
	
	public boolean onCreateOptionsMenu(Menu paramMenu) {
		return false;
	}

	private void doSendJobBid(final Booking booking)
	{
		Location loc = getLastFix();
		String distStr = "";
		try {
			if (loc != null)
				distStr = String.format(Locale.UK, "%.1fmi", 
						Util.getDistance(
								Double.valueOf(booking.getPickupLat()), 
								Double.valueOf(booking.getPickupLon()),
								loc.getLatitude(), 
								loc.getLongitude()));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View content = inflater.inflate(R.layout.custom_new_bid, null);
		if (content == null) {
			Toast.makeText(this, "Cannot inflate bid layout!", Toast.LENGTH_SHORT).show();
			return;
		}
		
		TextView jobAddress1 = (TextView)content.findViewById(R.id.textViewPickup);
		TextView jobAddress2 = (TextView)content.findViewById(R.id.textViewDropoff);
		TextView fare = (TextView)content.findViewById(R.id.textViewFare);
		TextView miles = (TextView)content.findViewById(R.id.textViewMiles);

		jobAddress1.setText(String.format(Locale.UK, "%s",booking.getPickupAddress()));
		jobAddress2.setText(String.format(Locale.UK, "%s",booking.getDropoffAddress()));
		fare.setText(String.format(Locale.UK, "%s","£0.00"));
		miles.setText(String.format(Locale.UK, "%s",distStr));
		
		final Button button1 = (Button)content.findViewById(R.id.button1);
		final Button button2 = (Button)content.findViewById(R.id.button2);
		mBidAlert = new AlertDialog.Builder(OiDriverActivity.this)
					.setTitle(String.format(Locale.UK, "Confirm bid on job %s", booking.getJobId()))
					.setIcon(R.drawable.passenger_inverted)
					.setCancelable(false)
					.setView(content).create();

		button1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				OiDriverService.getXmppClient().sendIQToUnsubscribeFromBooking(booking.getBookingId());
				OiDriverService.sendJobBid(booking);
				booking.setBidSent(true);
				boolean success = OiDriverService.bookings.add(booking);//dbHelper.insertBid(booking);
				if (success)
				{
					OiDriverService.putSentBidsId(booking.getBookingId());
					mBookingsFragment.startLoadingBids(booking.getBookingType());

				}
				////mBidAlert.dismiss();
				mBidAlert.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
				button1.setEnabled(false);
				button2.setEnabled(false);
				mBidRequestCountDown.start();
			}
		});
		button2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mBidAlert.dismiss();
			}
		});
		mBidAlert.show();
	}
	
	public void sendJobBid(final Booking booking)
	{
		Log.i(TAG, "Sending bid for ID = " + booking.getBookingId());
		if (isUIThread("sendJobBid()"))
		{
			doSendJobBid(booking);
		}
		else
		{
			runOnUiThread(new Runnable() {
				public void run() {
					doSendJobBid(booking);
				}
			});
		}
	}
	
	public void showProgressDialog() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				showConnectionSpinner();
			}
		});

	}

	public void hideProgressDialog() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {	
				hideConnectionSpinner();
			}
		});
	}

	public void showToast(final String message, final int duration) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(OiDriverActivity.this, message, duration).show();
			}
		});
	}  
	
	public void showConnectionSpinner()
	{
		runOnUiThread(new Runnable(){
			@Override
			public void run() {
				if (connectionProgress != null) connectionProgress.setVisibility(View.VISIBLE);
			}
		});
		
	}
	public void hideConnectionSpinner()
	{
		runOnUiThread(new Runnable(){
			@Override
			public void run() {
				if (connectionProgress != null) connectionProgress.setVisibility(View.GONE);
			}
		});
	}
	
	/* Defines callbacks for service binding, passed to bindService() 
	private final ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className,	IBinder service) {
			// We've bound to LocalService, cast the IBinder and get LocalService instance
			LocalBinder<OiDriverService> binder = (LocalBinder<OiDriverService>) service;
			mService = binder.getService();
			mBound = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			mBound = false;
		}
	};
	*/

	void registerReceivers()
	{
		registerReceiver(mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

		registerReceiver(newJobReceiver, new IntentFilter("com.oidriver.NEW_JOB"));
		registerReceiver(connectionStatusReceiver, new IntentFilter("com.oidriver.CONN_STATUS"));
		registerReceiver(gpsStatusReceiver, new IntentFilter("com.oidriver.GPS_STATUS"));
		registerReceiver(xmppStatusReceiver, new IntentFilter("com.oidriver.XMPP_STATUS"));
		registerReceiver(searchBookingsReceiver, new IntentFilter("com.oidriver.SEARCH_BOOKINGS"));
		registerReceiver(sendJobBidReceiver, new IntentFilter("com.oidriver.SEND_JOB_BID"));
		registerReceiver(newBookingsAvailableReceiver, new IntentFilter("com.oidriver.NEW_BOOKINGS_AVAILABLE"));
		registerReceiver(jobCancelledReceiver, new IntentFilter("com.oidriver.JOB_CANCELLED"));
		registerReceiver(driverMessageReceiver, new IntentFilter("com.oidriver.DRIVER_MESSAGE"));
		registerReceiver(zonesReceiver, new IntentFilter("com.oidriver.ZONES_SEARCH_FINISHED"));
		registerReceiver(serviceStartedReceiver, new IntentFilter("com.oidriver.SERVICE_STARTED"));
		registerReceiver(bidResponseReceiver, new IntentFilter("com.oidriver.BID_RESPONSE"));
		registerReceiver(zoneChangedReceiver, new IntentFilter("com.oidriver.ZONE_CHANGED"));
		registerReceiver(errorReceiver, new IntentFilter("com.oidriver.SHOW_ERROR"));
		registerReceiver(expiredReceiver, new IntentFilter("com.oidriver.EXPIRED"));
		registerReceiver(driverStatusReceiver,  new IntentFilter("com.oidriver.DRIVER_STATUS"));
		registerReceiver(driverRankReceiver, new IntentFilter("com.oidriver.DRIVER_RANK"));
		registerReceiver(driverLogonMessageReceiver, new IntentFilter("com.oidriver.DRIVER_LOGON_MESSAGE"));
	}

	void unregisterReceivers()
	{
		unregisterReceiver(mBatInfoReceiver);

		unregisterReceiver(newJobReceiver);
		unregisterReceiver(connectionStatusReceiver);
		unregisterReceiver(gpsStatusReceiver);
		unregisterReceiver(xmppStatusReceiver);
		unregisterReceiver(searchBookingsReceiver);
		unregisterReceiver(sendJobBidReceiver);
		unregisterReceiver(newBookingsAvailableReceiver);
		unregisterReceiver(jobCancelledReceiver);
		unregisterReceiver(driverMessageReceiver);
		unregisterReceiver(zonesReceiver);
		unregisterReceiver(serviceStartedReceiver);
		unregisterReceiver(bidResponseReceiver);
		unregisterReceiver(zoneChangedReceiver);
		unregisterReceiver(errorReceiver);
		unregisterReceiver(expiredReceiver);
		unregisterReceiver(driverStatusReceiver);
		unregisterReceiver(driverLogonMessageReceiver);
	}

	// "com.oidriver.XMPP_DISCONNECTED"
/*
	BroadcastReceiver xmppDisconnectedReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			Bundle bundle = intent.getExtras();
			AlertDialog.Builder builder = new AlertDialog.Builder(OiDriverActivity.this);
			builder.setTitle("OiDriver").setMessage("XMPP CONNECTION IS LOST!")
					.setPositiveButton("OK", null)
					.show();
		}
	};
*/

	// "com.oidriver.XMPP_STATUS"
	BroadcastReceiver xmppStatusReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			updateConnectionStatus();
		}
	};

	// "com.oidriver.DRIVER_STATUS"
	BroadcastReceiver driverStatusReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			
			Bundle bundle = intent.getExtras();
            if (bundle == null) return;
            OiDriverService.readyStatus = bundle.getBoolean("status", false);
            Log.wtf("READY", "[2] " + OiDriverService.readyStatus);
			final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
			sp.edit().putBoolean("RTW", OiDriverService.readyStatus).commit();
			//updateDriverStateUI();
			updateDriverRankUI(mCurrentRank);
            Log.wtf("READY", "[3] updateDriverRankUI()");
		}
	};

	// "com.oidriver.SHOW_ERROR"
	BroadcastReceiver errorReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {				
			try
			{
				if (mProgressDialog != null && mProgressDialog.isShowing()) {
					mProgressDialog.dismiss();
					mProgressDialog = null;
				}

				Bundle bundle = intent.getExtras();
				if (bundle == null)
					return;
				ArrayList<String> errors = bundle.getStringArrayList("errors");
                if (errors == null) return;
				for (String error: errors) {
					Log.e(TAG, error);
					Toast.makeText(OiDriverApp.getAppContext(), error, Toast.LENGTH_LONG).show();
				}
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
	};
	
	// "com.oidriver.EXPIRED"
	BroadcastReceiver expiredReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {				
			try
			{
				Bundle bundle = intent.getExtras();
				if (bundle == null)
					return;
				String expiredDate = bundle.getString("expiredDate");
				if (TextUtils.isEmpty(expiredDate))
					return;
				OiDriverService srv = OiDriverService.get();
				if (srv != null)
					srv.doLogout();
				userLoggedIn = false;
				final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
				sp.edit().putBoolean("UserLoggedIn", userLoggedIn).commit();
				startActivityForResult(new Intent(OiDriverActivity.this, LoginActivity.class), Constants.LOGIN_REQUEST_CODE);
				AlertDialog.Builder builder = new AlertDialog.Builder(OiDriverApp.getAppContext());
				builder.setTitle("OiDriver").setMessage("Insurance expired on " + expiredDate + ", cannot work!").setPositiveButton("OK", null).show();
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
	};
	
	// "com.oidriver.ZONE_CHANGED"
	BroadcastReceiver zoneChangedReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {				
			try
			{
				boolean play = intent.getBooleanExtra("playSound", false);
                updateZoneInfo("0", play);
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
	};

	// "com.oidriver.SERVICE_STARTED"
	BroadcastReceiver serviceStartedReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {				
			try
			{
				OiDriverService.get().setResultsReceiver(mXmppResultsReceiver);

		        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
				//OiDriverService.readyStatus = sharedPreferences.getBoolean("RTW", false);
				mMapFragment.centerMap();
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
	};

	// "com.oidriver.NEW_JOB"
	BroadcastReceiver newJobReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {				
			try
			{
                if (!mIsForeground) {
                    resumeActivity(intent);
                    return;
                }

				Bundle bundle = intent.getExtras();
				if (bundle == null) {
					Toast.makeText(OiDriverActivity.this, "NEW JOB: EMPTY DATA", Toast.LENGTH_SHORT).show();
					return;
				}

				AcceptedJob acceptedJob = (AcceptedJob)bundle.getSerializable("job");
				if (acceptedJob == null) {
					Toast.makeText(context, "Cannot handle new job!", Toast.LENGTH_LONG).show();
					return;
				}
                String jobType = bundle.getString("type");
                if (TextUtils.isEmpty(jobType))
                    jobType = "joboffer";

                if (jobType != null && jobType.equalsIgnoreCase("bidoffer")) {
                    playSoundSync(R.raw.bid_jobs, null);
                }

                OiNewJobAlertFragment alertFragment = getNewJobAlertFragment();
                if (alertFragment != null) {
                    String jobId = acceptedJob.getJobId();
                    mJobStatusSender.sendRejectJobToServer(acceptedJob.getSentBy(), jobId, null);
                    return;
                }

				acceptedJob.setStatus(Constants.STATUS_NEW);
				jobsQueue.add(acceptedJob);

                String jobId = acceptedJob.getJobId();
                String title = String.format("%s offer %s",
                        (jobType != null && jobType.equalsIgnoreCase("joboffer") ? "Job" : "Bid"),
                        jobId);
                showNewJobAlert(title, jobId, acceptedJob.getPickupAddress(true), acceptedJob.getDropoffAddress(true));
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	};

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void resumeActivity(Intent intent) {

        ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        if (am == null)
            return;
        int taskId = getTaskId();
        am.moveTaskToFront(taskId, 0);
        mIsTaximeterVisible = false;

        if (intent != null) {
            OiDriverApp.getAppContext().sendBroadcast(intent);
        }
    }

	private static void stopMediaPlayer() {
		if (mMediaPlayer != null) {
			mMediaPlayer.pause();
			mMediaPlayer.stop();
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
	}

	private void handleRejectJob() {
		AcceptedJob job;
		try {
			job = jobsQueue.size() > 0 ?  jobsQueue.take() : null;
		} catch (InterruptedException e) {
			e.printStackTrace();
			Toast.makeText(this, "Cannot reject job!", Toast.LENGTH_LONG).show();
			return;
		}
		if (job != null) {
			// Send REJECTED to the server
			String jobId = job.getJobId();
			mJobStatusSender.sendRejectJobToServer(job.getSentBy(), jobId,
					new JobStatusSender.JobStatusSenderListener() {
						@Override
						public void onStatusSent(String jobId, boolean isSent) {
							hideViewSwitcher();
						}
					});
		}
		else {
			Log.e(TAG, "REJECT JOB QUEUE::TAKE() => EMPTY");
		}
		//playSound(R.raw.job_declined);
		playSoundSync(R.raw.job_declined, null);
	}

	private void handleAcceptJob() {
		AcceptedJob acceptedJob;
		try {
			acceptedJob = jobsQueue.size() > 0 ? jobsQueue.take() : null;
		} catch (InterruptedException e) {
			e.printStackTrace();
			Toast.makeText(this, "Cannot accept job!", Toast.LENGTH_LONG).show();
			return;
		}
		if (acceptedJob != null) {
			// Send ACCEPTED to the server
			acceptedJob.setStatus(Constants.STATUS_ACCEPTED);
			if (!AcceptedJob.hasActiveJob() && !mIsTaximeterVisible)
				acceptedJob.insertOrUpdateJob();
			else {
				final AcceptedJob dbJob = acceptedJob;
				RunOn.taskThread(new Runnable() {
					@Override
					public void run() {
						insertJob(dbJob);
					}
				});
			}
			mJobStatusSender.sendAcceptJobToServer(acceptedJob, new JobStatusSender.JobStatusSenderListener() {
				@Override
				public void onStatusSent(String jobId, boolean isSent) {
					Booking booking = OiDriverService.getBidByJobId(jobId);
					if (booking != null)
					{
						int bookingType = booking.getBookingType();
						boolean success = OiDriverService.bookings.remove(booking);//.dbHelper.deleteBidByJobId(jobId);
						Log.w(TAG, "Deleted bid: " + success);
						if (success)
							mBookingsFragment.startLoadingBids(bookingType == 0 ? 0 : 1);
					}
					boolean res;
					if (AcceptedJob.hasActiveJob())
					{
						if (isJobHidden())
							selectJob(AcceptedJob.getActiveJob());
					}
					else
					{
						res = AcceptedJob.updateJobStatusByJobId(jobId, Constants.STATUS_ACCEPTED);
						AcceptedJob job = AcceptedJob.getActiveJob();
						if (res && job != null)
							selectJob(job);
						else
							mBookingsFragment.startLoadingJobs();
					}
				}
			});

			String notes = acceptedJob.getNote();
			if (!TextUtils.isEmpty(notes)) {
				notes = notes.trim();
				if (!TextUtils.isEmpty(notes) && !notes.equalsIgnoreCase(getString(R.string.no_notes)))
					playSoundSync(R.raw.notes_atached, null);
			}
		}
		else {
			Log.e(TAG, "ACCEPT JOB QUEUE::TAKE() => EMPTY");
		}
	}

	// "com.oidriver.BID_RESPONSE"
	BroadcastReceiver bidResponseReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			try {
				if (mBidRequestCountDown != null)
					mBidRequestCountDown.cancel();
				Bundle bundle = intent.getExtras();
				if (bundle == null) {
					Toast.makeText(OiDriverActivity.this, "BID RESPONSE: EMPTY DATA", Toast.LENGTH_SHORT).show();
					return;
				}

				String jobId = bundle.getString("jobId");
				int act = bundle.getInt("action");
				String mess = bundle.getString("mess");
				Booking booking = OiDriverService.getBidByJobId(jobId);
				boolean success = false;
				if (booking != null)
					success = OiDriverService.bookings.remove(booking);
				if (!success)
				{
					Log.e(TAG, "BidReponseReceiver: Failed to delete bid with jobId = " + jobId);
				}
				////speakOut(mess + ", job from " + booking.getPickupAddress());
				if (mBidAlert != null)
				{
					mBidAlert.setTitle(mess);
					mBidAlert.findViewById(R.id.progressBar).setVisibility(View.GONE);
					//mBidAlert.findViewById(R.id.button1).setEnabled(true);
					mBidAlert.findViewById(R.id.button2).setEnabled(true);
					((Button)mBidAlert.findViewById(R.id.button2)).setText("OK");
				}
				if (booking != null)
				{
					mBookingsFragment.startLoadingBids(booking.getBookingType());

					switch (act)
					{
					case 2:	// advance booking, win bid
						Map<String, Object> params = new HashMap<String, Object>();
						
						params.put("booking_id", booking.getBookingId());
						params.put("name", booking.getPassengerName());
						params.put("pickup", booking.getPickupAddress());
						params.put("dropoff", booking.getDropoffAddress());
						params.put("telephone", EMPTY_STRING);//FIXME
						SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.UK);
						Date dt = booking.getDtBookingTimestamp();
						params.put("timeofbooking", sdf.format(dt));
						params.put("jobid", booking.getJobId());
						params.put("notes", booking.getNotes());
						params.put("customerpickuplat", booking.getPickupLat());
						params.put("customerpickuplon", booking.getPickupLon());
						params.put("customerdropofflat", booking.getDropoffLat());
						params.put("customerdropofflon", booking.getDropoffLon());
						params.put("jobref", EMPTY_STRING);//FIXME
						params.put("postcode1", EMPTY_STRING);//FIXME
						params.put("postcode2", EMPTY_STRING);//FIXME
						params.put("housenumber1", EMPTY_STRING);//FIXME
						params.put("housenumber2", EMPTY_STRING);//FIXME
						params.put("pickupzoneid", booking.getZoneId());
						params.put("dropoffzoneid", EMPTY_STRING);//FIXME
						params.put("fareprice", booking.getFare());
						params.put("custtype", EMPTY_STRING);//FIXME
						params.put("status", Constants.STATUS_ACCEPTED);
						
						OiDriverService.dbHelper.insertJob(params);
						boolean res = mJobStatusSender.sendJobStatusToServer(
										booking.getSentBy(), booking.getJobId(),
										Constants.DISPATCHER_STATUS_ACCEPTED, Constants.TASK_ACCEPTED_JOB,
										new JobStatusSender.JobStatusSenderListener() {
											@Override
											public void onStatusSent(String jobId, boolean isSent) {
												Booking booking = OiDriverService.getBidByJobId(jobId);
												if (booking != null)
												{
													int bookingType = booking.getBookingType();
													boolean success = OiDriverService.bookings.remove(booking);
													Log.w(TAG, "Deleted bid: " + success);
													if (success)
														mBookingsFragment.startLoadingBids(bookingType == 0 ? 0 : 1);
												}
												boolean res;
												if (AcceptedJob.hasActiveJob())
												{
													if (isJobHidden())
														selectJob(AcceptedJob.getActiveJob());
												}
												else
												{
													res = AcceptedJob.updateJobStatusByJobId(jobId, Constants.STATUS_ACCEPTED);
													AcceptedJob job = AcceptedJob.getActiveJob();
													if (res && job != null)
														selectJob(job);
													else
														mBookingsFragment.startLoadingJobs();
												}
											}
										});
						if (!res)
							Log.e(TAG, "[0] Failed to send ACCEPTED status to server for jobid = " + booking.getJobId());
						if (mBookingsFragment != null)
							mBookingsFragment.startLoadingJobs();
						break;
					case 3:	// now booking, win bid
						AcceptedJob.insertJobFromBooking(booking);
						new Handler().postDelayed(new Runnable() {
							
							@Override
							public void run() {
								final AcceptedJob job = AcceptedJob.getActiveJob();
								boolean res = mJobStatusSender.sendJobStatusToServer(
										job.getSentBy(), job.getJobId(),
										Constants.DISPATCHER_STATUS_ACCEPTED, Constants.TASK_ACCEPTED_JOB,
										new JobStatusSender.JobStatusSenderListener() {
											@Override
											public void onStatusSent(String jobId, boolean isSent) {
												Booking booking = OiDriverService.getBidByJobId(jobId);
												if (booking != null)
												{
													int bookingType = booking.getBookingType();
													boolean success =
															OiDriverService.bookings.remove(booking);
													Log.w(TAG, "Deleted bid: " + success);
													if (success)
														mBookingsFragment.startLoadingBids(bookingType == 0 ? 0 : 1);
												}
												boolean res;
												if (AcceptedJob.hasActiveJob())
												{
													if (isJobHidden())
														selectJob(AcceptedJob.getActiveJob());
												}
												else
												{
													res = AcceptedJob.updateJobStatusByJobId(jobId,
															Constants.STATUS_ACCEPTED);
													AcceptedJob job = AcceptedJob.getActiveJob();
													if (res && job != null)
														selectJob(job);
													else
														mBookingsFragment.startLoadingJobs();
												}
											}
										});
								if (res)
									selectJob(job);
								else
									Toast.makeText(OiDriverActivity.this,
											"Failed to send ACCEPTED status to server",
											Toast.LENGTH_SHORT)
										.show();
							}
						}, 3000);
						break;
					}
				}
				else
				{
					mBookingsFragment.startLoadingBids(0);
					mBookingsFragment.startLoadingBids(1);
				}
				int totalBids = OiDriverService.getBidsCount();
				((Button)findViewById(R.id.buttonJobs)).setText("x" + String.valueOf(totalBids));
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	};
	
	//"com.oidriver.JOB_CANCELLED"
	BroadcastReceiver jobCancelledReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			
			try {
				Bundle bundle = intent.getExtras();
				if (bundle == null) {
					Toast.makeText(OiDriverActivity.this, "CANCEL JOB: EMPTY DATA", Toast.LENGTH_SHORT).show();
					return;
				}

				final String jobId = bundle.getString("jobId");
				AcceptedJob job = AcceptedJob.getActiveJob();
				if (job == null)
					job = OiDriverService.dbHelper.getJobByJobId(jobId);
				if (job == null)
				{
					showToast("[0] Cannot find job with ID = " + jobId, Toast.LENGTH_LONG);
					return;
				}
				if (!job.getJobId().equalsIgnoreCase(jobId))
				{
					showToast("[1] Cannot find job with ID = " + jobId, Toast.LENGTH_LONG);
					return;
				}
				final String sendToJid = job.getSentBy();
				
				String pickup = job.getPickupAddress(false);
				String dropoff = job.getDropoffAddress(false);
				
				// Try to delete bid first
				Booking booking = OiDriverService.getBidByJobId(jobId);
				boolean res = booking != null && OiDriverService.bookings.remove(booking);
				if (res)
				{
					Log.d(OiDriverApp.TAG, "Cancelled job " + jobId);
				}
				else
				{
					Log.e(OiDriverApp.TAG, "Failed to cancel job bid: " + jobId);
					res = AcceptedJob.deleteJobByJobId(jobId);
					if (res)
					{
						Log.d(OiDriverApp.TAG, "Cancelled job " + jobId);
					}
					else
					{
						Log.e(OiDriverApp.TAG, "Failed to cancel job: " + jobId);
					}
				}
				
				LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View content = inflater.inflate(R.layout.custom_cancel_job, null);
				if (content == null) {
					Toast.makeText(OiDriverActivity.this, "Cannot inflate cancel job layout!", Toast.LENGTH_SHORT).show();
					return;
				}
				TextView jobAddress1 = (TextView)content.findViewById(R.id.textViewPickup);
				TextView jobAddress2 = (TextView)content.findViewById(R.id.textViewDropoff);

				final Button button1 = (Button)content.findViewById(R.id.button1);
				AlertDialog.Builder builder = new AlertDialog.Builder(OiDriverActivity.this)
					.setTitle(String.format("Job Cancelled: %s", jobId))
					.setIcon(R.drawable.passenger_inverted)
					.setView(content);
				final AlertDialog jobCancelledAlert = builder.create();
				button1.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if (!TextUtils.isEmpty(jobId)) {
							mJobStatusSender.sendEOJ(jobId, sendToJid,
									new JobStatusSender.JobStatusSenderListener() {
										@Override
										public void onStatusSent(String jobId, boolean isSent) {
											resetToMyLocation();

											hideViewSwitcher();
											AcceptedJob job = AcceptedJob.getActiveJob();
											boolean success = AcceptedJob.deleteJobByJobId(jobId);
											if (!success)
												Log.e(TAG, "[2] ERROR: couldn't delete JOB[" + jobId + "]");
											mBookingsFragment.startLoadingJobs();
											resetToMyLocation();
											OiDriverService.dbHelper.insertJobDone(jobId, job.getFarePrice(), "cash");
										}
									});
						}
						jobCancelledAlert.dismiss();
					}
				});
				
				jobAddress1.setText(String.format("Pickup:\t%s",pickup));
				jobAddress2.setText(String.format("Dropoff:\t%s",dropoff));

				jobCancelledAlert.show();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	};

	//"com.oidriver.CONN_STATUS"
	final BroadcastReceiver connectionStatusReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			synchronized (connectionStatusReceiver) {
				try
				{
					updateConnectionStatus();
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	};

	//"com.oidriver.GPS_STATUS"
	BroadcastReceiver gpsStatusReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			try
			{
				Bundle bundle = intent.getExtras();
				if (bundle == null) {
					return;
				}

				int status = bundle.getInt("status", 0);
				Location loc = mMapFragment.getLocation();
				float accuracy = 0f;
				if (loc != null)
				{
					accuracy = loc.getAccuracy();
					//////Log.i(TAG, "GPS STATUS UPDATE: accuracy = " + accuracy);
					if (accuracy > 100 && status >= 1)
						status = 1;
					statusGps.setImageDrawable(getResources().getDrawable(mStatuses[status]));
				}
				else
				{
					statusGps.setImageDrawable(getResources().getDrawable(R.drawable.disconnected));

					if (TextUtils.isEmpty(OiDriverService.currentZoneId) ||
						OiDriverService.currentZoneId.equals("OUT_OF_ZONE")) {
						mCurrentZoneName.setText(getString(R.string.wating_for_gps));
					}
				}
				TextView tv = (TextView)findViewById(R.id.textViewGPS);
				String str = String.format(Locale.UK, "LAT\t%f\nLON\t%f\nACC\t%.2fm",
						OiDriverService.lat, OiDriverService.lon, accuracy);
				tv.setText(str);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	};

	//"com.oidriver.SEARCH_BOOKINGS"
	BroadcastReceiver searchBookingsReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {				
			try
			{
				if (mProgressDialog != null) {
					if (mProgressDialog.isShowing()) mProgressDialog.dismiss();
					mProgressDialog = null;
				}

				int total = intent.getIntExtra("total", 0);
				if (total <= 0)
				{
					((Button)findViewById(R.id.buttonJobs)).setText("x" + String.valueOf(OiDriverService.getBidsCount()/* + OiDriverService.dbHelper.getBidsIDsCount()*/));
					((Button)findViewById(R.id.buttonJobs)).setTextColor(Color.BLACK);
					hideProgressDialog();
					//OiDriverService.sendIQToSubscribeZone(OiDriverService.currentZoneId);
				}
				else {
					int now_bids = intent.getIntExtra("now_bids", 0);
					int adv_bids = intent.getIntExtra("adv_bids", 0);

					int pinkyColour = context.getResources().getColor(R.color.oi_pinkypurple);
					mBookingsFragment.highlightButton(Constants.BIDS_PAGE, now_bids + adv_bids > 0 ? pinkyColour : Color.BLACK);
					hideProgressDialog();

					((Button)findViewById(R.id.buttonJobs)).setText("x" + String.valueOf(OiDriverService.getBidsCount()/* + OiDriverService.dbHelper.getBidsIDsCount()*/));
					((Button)findViewById(R.id.buttonJobs)).setTextColor(Color.BLACK);
				}
				mInfoJobs.setText("x " + OiDriverService.getBidsCount());
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	};

	// "com.oidriver.NEW_BOOKINGS_AVAILABLE"
	BroadcastReceiver newBookingsAvailableReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {				
			try
			{	
				int totalAdded = intent.getIntExtra("is_added", 0);
				int totalDeleted = intent.getIntExtra("is_deleted", 0);
				Log.e(TAG, "ADDED: " + totalAdded + ", DELETED: " + totalDeleted);
				int totalBids = OiDriverService.getBidsCount();
				((Button)findViewById(R.id.buttonJobs)).setText("x" + String.valueOf(totalBids));
				if (totalAdded > 0)
				{
					playSoundSync(R.raw.bid_jobs, null);
					((Button)findViewById(R.id.buttonJobs)).setTextColor(getResources().getColor(R.color.oi_pinkypurple));
				}
				else
				{
					((Button)findViewById(R.id.buttonJobs)).setTextColor(Color.BLACK);
				}
				if (totalDeleted > 0)
				{
					mBookingsFragment.startLoadingBids(0);
					mBookingsFragment.startLoadingBids(1);
				}
				else
				{
					int now_bids = intent.getIntExtra("now_bids", 0);
					int adv_bids = intent.getIntExtra("adv_bids", 0);

					int pinkyColour = context.getResources().getColor(R.color.oi_pinkypurple);
					mBookingsFragment.highlightButton(Constants.BIDS_PAGE, now_bids + adv_bids > 0 ? pinkyColour : Color.BLACK);
					//mBookingsFragment.highlightButton(Constants.BIDS_ADV_PAGE, adv_bids > 0 ? pinkyColour : Color.BLACK);
				}
				if (totalAdded > 0 && mJobFragment.isHidden() && mBookingsFragment.isHidden())
				{
					mBookingsFragment.setRefreshRequiredBids(true);
					mBookingsFragment.setRefreshRequiredJob(true);
					((Button)findViewById(R.id.buttonJobs)).setTextColor(Color.BLACK);

					((TextView)findViewById(R.id.textViewPageTitle)).setText(getResources().getString(R.string.choose_options));
					mBookingsFragment.populateTotals();
					hideBookingsFragment(false);
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	};

	// "com.oidriver.SEND_JOB_BID"
	BroadcastReceiver sendJobBidReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {				
			try
			{	
				Bundle bundle = intent.getExtras();
				String bookingId = bundle != null ? bundle.getString("booking_id") : "";
				if (!TextUtils.isEmpty(bookingId))
				{
					Booking booking = OiDriverService.getBidByBookingId(bookingId);
					if (booking != null && booking.getBookingId().equalsIgnoreCase(bookingId))
						sendJobBid(booking);
					else
					{
						showToast("Failed to find booking with ID: " + bookingId, Toast.LENGTH_LONG);
					}
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	};

	// "com.oidriver.DRIVER_RANK"
	BroadcastReceiver driverRankReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			String queue = intent.getStringExtra("queue");
			String state = intent.getStringExtra("state");
			updateRankInfo(queue, state);
		}
	};

	// "com.oidriver.DRIVER_MESSAGE"
	BroadcastReceiver driverMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {				
			try
			{	
				String message = intent.getStringExtra("message");
				LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View content = inflater.inflate(R.layout.message_to_driver, null);
				if (content == null) {
					Toast.makeText(OiDriverActivity.this, "Cannot inflate driver message layout", Toast.LENGTH_SHORT).show();
					return;
				}
				
				TextView messageView = (TextView)content.findViewById(R.id.textViewMessage);
				messageView.setText(message);
				
				final Button button1 = (Button)content.findViewById(R.id.button1);
				final Button button2 = (Button)content.findViewById(R.id.button2);
				final Button button3 = (Button)content.findViewById(R.id.button3);
				final Button button4 = (Button)content.findViewById(R.id.button4);
				final Button button5 = (Button)content.findViewById(R.id.button5);
				final Button button6 = (Button)content.findViewById(R.id.button6);
				final AlertDialog bidAlert = new AlertDialog.Builder(OiDriverActivity.this)
													.setTitle("Message to driver")
													.setIcon(R.drawable.passenger_inverted)
													.setView(content).create();

				button1.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						sendDriverMessage(button1.getText(), OiDriverApp.MANAGER_JID);
						bidAlert.dismiss();
					}
				});
				button2.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						sendDriverMessage(button2.getText(), OiDriverApp.MANAGER_JID);
						bidAlert.dismiss();
					}
				});
				button3.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						sendDriverMessage(button3.getText(), OiDriverApp.MANAGER_JID);
						bidAlert.dismiss();
					}
				});
				button4.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						sendDriverMessage(button4.getText(), OiDriverApp.MANAGER_JID);
						bidAlert.dismiss();
					}
				});
				button5.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						sendDriverMessage(button5.getText(), OiDriverApp.MANAGER_JID);
						bidAlert.dismiss();
					}
				});
				button6.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						sendDriverMessage(button6.getText(), OiDriverApp.MANAGER_JID);
						bidAlert.dismiss();
					}
				});
				bidAlert.show();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	};

	// "com.oidriver.DRIVER_LOGON_MESSAGE"
	BroadcastReceiver driverLogonMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			try
			{
				if (mProgressDialog != null) {
					mProgressDialog.dismiss();
					mProgressDialog = null;
				}

				String message = intent.getStringExtra("message");
//				Pattern p = Pattern.compile("<body>logon,(.*)</body>");
//				Matcher m = p.matcher(message);
//				String text = null;
//				if (m.find())
//					text = m.group(1);
//				if (TextUtils.isEmpty(text))
//					return;
				if (TextUtils.isEmpty(message))
					return;
				LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View content = inflater.inflate(R.layout.logon_message, null);
                if (content == null) {
                    Toast.makeText(OiDriverApp.getAppContext(), "Cannot create message dialog", Toast.LENGTH_LONG).show();
                    return;
                }

				TextView messageView = (TextView)content.findViewById(R.id.textViewMessage);
				messageView.setText(message);
				updateDriverRankUI(0);
                Log.wtf("READY", "[4] updateDriverRankUI()");

				final Button button1 = (Button)content.findViewById(R.id.button1);
				final AlertDialog bidAlert = new AlertDialog.Builder(OiDriverActivity.this)
						.setTitle("Message to driver")
						.setIcon(R.drawable.passenger_inverted)
						.setView(content).create();

				button1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						bidAlert.dismiss();
					}
				});
				bidAlert.show();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	};

	public void onClick(View v) {
		if (v == mInfoJobs)
		{
//            PackageManager pm = getPackageManager();
//            if (pm != null) {
//                Intent intent = pm.getLaunchIntentForPackage("com.planetcoops.android.taximeter");
//                if (intent != null)
//                    startActivity(intent);
//            }
			playButtonClickSound();
			handlePassengerClick();
		}
		else if (v == mZones)
		{
			playButtonClickSound();
			if (mZonesFragment.isVisible())
			{
				hideZonesFragment(true);
				hideConnectionSpinner();
			}
			else
			{
				if (NetworkUtils.getConnectivityStatus() != NetworkUtils.TYPE_NOT_CONNECTED &&
					OiDriverService.isXmppAuthenticated())
				{
					if (zonesRequestOn)
					{
						showToast("Please wait until current request finishes", Toast.LENGTH_SHORT);
						return;
					}
					showConnectionSpinner();
					OiDriverService.sendIQForZones();
					zonesRequestOn = true;
					
					mZonesCountDown.start();
				}
				else
				{
					hideConnectionSpinner();
					showToast("No connection to the server, please try again later!", Toast.LENGTH_SHORT);
					if (!OiDriverService.isXmppAuthenticated())
					{
						OiDriverService srv = OiDriverService.get();
						if (srv != null)
							srv.connectToXmpp();
					}
				}
			}
		}
		else if (v == mShowJobs)
		{
			playButtonClickSound();
			handlePassengerClick();
		}
		else if (v == mOptions)
		{
			playButtonClickSound();
			startActivityForResult(new Intent(this, OptionsTabsActivity.class), Constants.OPTIONS_REQUEST_CODE);
		}
	}
	
	private boolean isMyServiceRunning() {
	    ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		if (manager == null)
			return false;
		List<RunningServiceInfo> services = manager.getRunningServices(Integer.MAX_VALUE);
		if (services == null)
			return false;
	    for (RunningServiceInfo service : services) {
	        if (OiDriverService.class.getName().equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}
	
	@SuppressWarnings("unused")
	private boolean isAppllicationRunning()
	{
		final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
		if (sp.contains("RunningCnt"))
		{
			int cnt = sp.getInt("RunningCnt", 0);
			if (cnt > 0)
				return true;
		}
		return false;
	}
	
	void selectJob(final AcceptedJob job)
	{
        if (job == null)
            return;
		RunOn.mainThread(new Runnable() {
			@Override
			public void run() {
				AcceptedJob acceptedJob = AcceptedJob.getActiveJob();
				if (acceptedJob == null || !acceptedJob.getJobId().equals(job.getJobId()))
					job.insertOrUpdateJob();
				String nameStr = job.getName();
				String fromStr = job.getPickupAddress(true);
				String idStr = job.getJobId();
				String timeStr = job.getFormattedBookingTime();
				if ((timeStr.endsWith("am") || timeStr.endsWith("pm")) && timeStr.length() > 5)
				{
					SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyy hh:mm:ss a", Locale.UK);
					SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm", Locale.UK);
					try {
						Date d = sdf.parse(timeStr);
						timeStr = sdfTime.format(d);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}

				TextView jobId = (TextView) mJobFragment.getView().findViewById(R.id.textViewJobId);
				TextView name = (TextView) mJobFragment.getView().findViewById(R.id.textViewName);
				TextView from = (TextView) mJobFragment.getView().findViewById(R.id.textViewAddress);
				TextView time = (TextView) mJobFragment.getView().findViewById(R.id.textViewTime);
				jobId.setText(idStr);
				name.setText(nameStr);
				from.setText(fromStr);
				time.setText(timeStr);

				sentSTC = false;

				hideBookingsFragment(true);
				hideJobFragment(false);
			}
		});
	}

	public static boolean isUIThread(String inMethod)
	{
		if (Looper.myLooper() != null && Looper.myLooper() == Looper.getMainLooper())
		{
			Log.i(TAG, "UI Thread: in " + inMethod);
			return true;
		}
		else
		{
			Log.i(TAG, "NON-UI Thread: in " + inMethod);
			return false;
		}
	}
	
	public void resetToMyLocation()
	{
		if (mMapFragment != null)
		{
			RunOn.mainThread(new Runnable() {
				@Override
				public void run() {
					mMapFragment.resetToMyLocation();
				}
			});
		}
	}

	public static void playSoundSync(int soundResId, final MediaCallback callback) {

		Log.i(TAG, "Entered into playSoundSync()");
		if (mMediaPlayer != null)
			stopMediaPlayer();

		mMediaPlayer = MediaPlayer.create(OiDriverApp.getAppContext(), soundResId);
		mMediaPlayer.setLooping(false);
		if (callback != null)
			mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {

					Log.i(TAG, "Released semaphore");
					callback.onFinished();
				}
			});
		mMediaPlayer.start();
	}

	private List<WeakReference<PopupWindow>> mPopups = new ArrayList<WeakReference<PopupWindow>>();
	public void showPopups() {
		Context ctx = this;
		AcceptedJob currentJob = AcceptedJob.getActiveJob();
		if (currentJob != null)
		{
			TextPopup popup;
			if (currentJob.getIntStatus() < 5)
				popup = new TextPopup(ctx, "Pickup address", currentJob.getPickupAddressHousePostcode());
			else
				popup = new TextPopup(ctx, "Dropoff address", currentJob.getDropoffAddressHousePostcode());
			mPopups.add(new WeakReference<PopupWindow>(popup));
			popup.setAutoHide(true);			
			popup.showAbove(mJobFragment.getView().findViewById(R.id.textViewAddress));
		}
	}

	/**
	 * Dismiss popups with animation. Equivalent to {@link #dismissPopups(boolean, boolean)}(false, false)
	 */
	public void dismissPopups() {
		dismissPopups(false, false);
	}
	
	/**
	 * Dismiss popups with or without animation
	 * 
	 * @param immediate - true if we should dismiss the popups without animation
	 */
	public void dismissPopups(boolean immediate, boolean forKeyboard) {
		for (WeakReference<PopupWindow> ref: mPopups) {
			if(ref != null ) {
				PopupWindow popup = ref.get();
				if(popup != null && popup.isShowing()) {
					
					if(popup instanceof TrianglePopup) {
						TrianglePopup trianglePopup = (TrianglePopup) popup;
						
						/* Don't hide popups that can be shown above the keyboard*/
						if(forKeyboard && trianglePopup.getShownAboveKeyboard())
							continue;
						
						trianglePopup.dismiss(immediate);
					}else
						popup.dismiss();
				}
			}
		}
		
		mPopups.clear();
	}
		
	void sendDriverMessage(CharSequence message, String sendToJid)
	{
		String msg = "";
		if (message != null)
			msg = message.toString();
		if (TextUtils.isEmpty(msg))
			return;
		OiDriverService.sendMessage("<driver_reply>" + msg + "</driver_reply>", sendToJid);
	}

	void showZones(ArrayList<ZoneInfo> values)
	{
		hideConnectionSpinner();

		ListView list = (ListView) findViewById(R.id.lvZones);
		list.setEmptyView(findViewById(R.id.textViewNoData));
		ArrayAdapter<ZoneInfo> adapter = 
				new ZonesAdapter(OiDriverApp.getAppContext(), R.id.textViewNoData, values);
		if (values != null && values.size() > 0)
			((ZonesAdapter)adapter).setSelectedZone(0);
		list.setAdapter(adapter);
		hideZonesFragment(false);		
	}

	BroadcastReceiver zonesReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {				
			try
			{
				boolean showZones = true;
				if (!zonesRequestOn)
					showZones = false;
				zonesRequestOn = false;
				if (mZonesCountDown != null)
					mZonesCountDown.cancel();

				// If prev request has expired then we don't need to show zones
				if (!showZones)
					return;
				if (OiDriverService.mZones != null && OiDriverService.mZones.size() > 0)
				{
					Collections.sort(OiDriverService.mZones);
					showZones(OiDriverService.mZones);
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	};

    @SuppressWarnings("unused")
	public ArrayBlockingQueue<AcceptedJob> getJobsQueue()
	{
		return jobsQueue;
	}
	
	public Location getLastFix()
	{
		if (mMapFragment == null)
			return null;//mMyLocationOverlay.getLastFix();
		Location loc = mMapFragment.getMap().getMyLocation();
		if (loc == null) {
			loc = new Location("GPS");
			loc.setLongitude(OiDriverService.lat);
			loc.setLongitude(OiDriverService.lon);
		}

		return loc;
	}

	// From: PurgeBidsListener
	@Override
	public void onCompleted(boolean success) {
		if (success && mBookingsFragment != null)
		{
			int totalBids = OiDriverService.getBidsCount();
			((Button)findViewById(R.id.buttonJobs)).setText("x" + String.valueOf(totalBids));

			mBookingsFragment.startLoadingBids(0);
			mBookingsFragment.startLoadingBids(1);
		}
	}
	//
	
	@Override
	protected Dialog onCreateDialog(int dlgId)
	{
		ProgressDialog dlg = new ProgressDialog(this);
		dlg.setMessage("Loading...");
		dlg.setIndeterminate(true);
		return dlg;
	}
	
	public void dismissAny(int id)
	{
		try {
			dismissDialog(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// From: JobOffersAdapter.OnJobSelectedListener
	@Override
	public void onJobSelected(AcceptedJob job) {
		selectJob(job);
	}

	@Override
	public void onJobSelectionError(String jobId, String error) {
		showToast(error, Toast.LENGTH_LONG);
	}
	//
	
	GoogleMap.OnMapClickListener mMapCLickedListener = new GoogleMap.OnMapClickListener() {
		
		@Override
		public void onMapClick(LatLng arg0) {
			OiDriverService.sendIQForGPS(true, "GPS");
		}
	};
	
	@Override
	protected void  onActivityResult(int requestCode, int resultCode, Intent data) {

		// From Google Update call
		if (requestCode == 0) {
			finish();
		}
		else if (requestCode == Constants.LOGIN_REQUEST_CODE)
		{
			final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
			if (resultCode == RESULT_CANCELED)
			{
				userLoggedIn = false;
				sp.edit().putBoolean("UserLoggedIn", userLoggedIn).commit();
				finish();
			}
			else
			{
				userLoggedIn = true;
				sp.edit()
						.putBoolean("UserLoggedIn", userLoggedIn)
						.putLong("UserLoginTime", System.currentTimeMillis())
					.commit();
				if (OiDriverService.isXmppAuthenticated()) {
					Intent i = new Intent("com.oidriver.XMPP_STATUS");
					i.putExtra("status", 1);
					i.putExtra("spinner", 0);
					OiDriverApp.getAppContext().sendBroadcast(i);

					OiDriverService.get().onLoginSuccessful();
				}
                boolean meterState = sp.getBoolean("isMeterVisible", false);
                if (meterState) {
                    hideMeterFragment(false);
                    mMeterFragment.restoreMeterState();
                }
                else
                    hideMeterFragment(true);

            }
		}
		else if (requestCode == Constants.OPTIONS_REQUEST_CODE)
		{
			if (resultCode == RESULT_FIRST_USER)
			{
				OiDriverService srv = OiDriverService.get();
				if (srv != null)
					srv.doLogout();
				userLoggedIn = false;
				final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
				sp.edit().putBoolean("UserLoggedIn", userLoggedIn).commit();
				startActivityForResult(new Intent(this, LoginActivity.class), Constants.LOGIN_REQUEST_CODE);
			}
		}
        else if (requestCode == Constants.CARD_IO_SCAN_REQUEST_CODE) {
            Util.showNotification("GOT SCAN");
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
                if (scanResult == null) {
                    Util.showNotification("Failed to get card data!");
                    return;
                }

                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                String cardNumber = scanResult.cardNumber;
                int expMonth = scanResult.expiryMonth;
                int expYear = scanResult.expiryYear;
                String cvv = scanResult.cvv;
                String postcode = scanResult.postalCode;

                if (!scanResult.isExpiryValid()) {
                    Util.showNotification("Invalid expiry date!");
                    return;
                }

                if (TextUtils.isEmpty(cardNumber)) {
                    Util.showNotification("Invalid card number!");
                    return;
                }

                if (TextUtils.isEmpty(cvv)) {
                    Util.showNotification("Invalid CVV!");
                    return;
                }

                OiDriverService.sendPaymentInfo(OiMeterFragment.mFareAmount, cardNumber, expMonth, expYear, cvv, postcode);
                OiMeterFragment.mFareAmount = "";
            }
            else {
                Util.showNotification("Scan was canceled.");
            }
        }
	}
	
	public void showMapWithPassenger(AcceptedJob job)
	{
		mMapFragment.showMapWithPassenger(Double.parseDouble(job.getCustomerPickupLat()), Double.parseDouble(job.getCustomerPickupLon()));
	}
	
	public void showMapWithoutPassenger()
	{
		mMapFragment.showMapWithoutPassenger();
	}

	public void hideViewSwitcher()
	{
		hideBookingsFragment(true);
		if (!AcceptedJob.hasActiveJob())
			hideJobFragment(true);
		resetToMyLocation();
	}
	
	public void handlePassengerClick()
	{
		if (!OiDriverService.isXmppAuthenticated()) {
			OiDriverService srv = OiDriverService.get();
			if (srv != null)
			{
				srv.connectToXmpp();
				Log.e(TAG, "NOT CONNECTED - LOGGING IN...");
				showToast("Reconnecting...", Toast.LENGTH_LONG);
			}
			return;
		}
		
		if (OiDriverService.isBlockingOperationProgress())
		{
			showToast("Please wait for background search to finish", Toast.LENGTH_LONG);
			return;
		}

		// Hide layout if it is visible
		if (mBookingsFragment.isVisible())
		{
			hideBookingsFragment(true);
			OiDriverService.sendIQForDrivers();
			((Button)findViewById(R.id.buttonJobs)).setTextColor(Color.BLACK);
			
			// Disable refreshing
			mBookingsFragment.setRefreshRequiredBids(false);
			mBookingsFragment.setRefreshRequiredJob(false);
			
			return;
		}
		
		// For Job do nothing
		if (mJobFragment.isVisible())
			return;

		((Button)findViewById(R.id.buttonJobs)).setTextColor(Color.BLACK);

		mBookingsFragment.setRefreshRequiredBids(true);
		mBookingsFragment.setRefreshRequiredJob(true);

		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Retrieving fresh data.\nPlease wait...");
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.show();

		OiDriverService.sendIQToGetBookings(0, new OnBookingSearchListener() {
			@Override
			public void onSearchCompleted() {
				OiDriverService.getXmppClient().blockingOperationProgress = false;
				RunOn.mainThread(new Runnable() {
					@Override
					public void run() {
						if (mProgressDialog != null) {
							if (mProgressDialog.isShowing()) mProgressDialog.dismiss();
							mProgressDialog = null;
						}

						hideBookingsFragment(false);
					}
				});
			}
		});
	}

	void showBidsTotals() {
		((TextView) findViewById(R.id.textViewPageTitle)).setText(getResources().getString(R.string.choose_options));
		mBookingsFragment.populateTotals();
	}
	
	public void hideZonesFragment(boolean hide)
	{
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		if (hide)
			ft.hide(mZonesFragment);
		else
			ft.show(mZonesFragment);
		ft.commitAllowingStateLoss();
	}

    public void hideMeterFragment(boolean hide) {
        hideMeterFragment(hide, true);
    }

    public void hideMeterFragment(boolean hide, boolean saveState)
    {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (hide) {
            ft.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left,
                    R.anim.in_from_left, R.anim.out_to_right);
            ft.hide(mMeterFragment);
        }
        else {
            mMeterFragment.setData(1, "0.00");
            ft.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left,
                    R.anim.in_from_left, R.anim.out_to_right);
            ft.show(mMeterFragment);
        }
        ft.commitAllowingStateLoss();
        OiDriverActivity.mIsTaximeterVisible = !hide;

        if (saveState) {
            final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
            sp.edit().putBoolean("isMeterVisible", mIsTaximeterVisible).commit();
        }
    }

    public void hideBookingsFragment(boolean hide)
	{
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		if (hide)
			ft.hide(mBookingsFragment);
		else {
			mBookingsFragment.setBidsCount();
			if (BookingIQProvider.mMaxBookingsCount == 0) {
				ft.show(mBookingsFragment);
				showBidsTotals();
			}
		}
		ft.commitAllowingStateLoss();
	}

	public boolean isJobHidden()
	{
		return !(mJobFragment != null && !mJobFragment.isHidden());
	}
	
	public boolean getNavigatedState()
	{
		return mJobFragment != null && mJobFragment.getNavigatedState();
	}
	
	public void hideJobFragment(boolean hide)
	{
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		if (hide) {
			ft.hide(mJobFragment);
			RunOn.mainThread(new Runnable() {
				@Override
				public void run() {
					final Button infoDrivers = (Button) findViewById(R.id.buttonCars);
					infoDrivers.setEnabled(true);
				}
			});
		}
		else
		{
			mJobFragment.resetJobUI();
			ft.show(mJobFragment);
			RunOn.mainThread(new Runnable() {
				@Override
				public void run() {
					final Button infoDrivers = (Button) findViewById(R.id.buttonCars);
					infoDrivers.setEnabled(false);
				}
			});
		}
		ft.commitAllowingStateLoss();
	}

	public interface RequestCountDownListener
	{
		void onTimerFinished();
	}
	
	class RequestCountDownTimer extends CountDownTimer
	{
		RequestCountDownListener mListener;
		
		public RequestCountDownTimer(long millisInFuture, long countDownInterval, RequestCountDownListener listener) {
			super(millisInFuture, countDownInterval);
			mListener = listener;
		}

		@Override
		public void onTick(long millisUntilFinished) {
			
		}

		@Override
		public void onFinish() {
			if (mListener != null)
				mListener.onTimerFinished();
		}		
	}
	
	RequestCountDownTimer mZonesCountDown = new RequestCountDownTimer(30000, 1000, new RequestCountDownListener() {
		
		@Override
		public void onTimerFinished() {
			zonesRequestOn = false;
			hideZonesFragment(true);
			hideConnectionSpinner();
			showToast("Zones request timed out!", Toast.LENGTH_SHORT);
		}
	});

	RequestCountDownTimer mBidRequestCountDown = new RequestCountDownTimer(60000, 1000, new RequestCountDownListener() {
		
		@Override
		public void onTimerFinished() {
			if (mBidAlert != null)
				mBidAlert.dismiss();
			showToast("Bid request timed out!", Toast.LENGTH_SHORT);
		}
	});

	int mCurrentRank = 0;

	private void updateDriverRankUI(int queueRank)
	{
        Log.wtf("READY", "[*] updateDriverRankUI(" + queueRank + ")");
		Button btn = (Button) findViewById(R.id.buttonCars);
		if (btn == null)
			return;

		mCurrentRank = queueRank;
		if (queueRank > 0) {
			btn.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.car_logo), null, null, null);
		}
		else {
			//////////////////////////OiDriverService.readyStatus = false;
			btn.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.car_logo_red), null, null, null);
		}
		btn.setText("#" + queueRank);
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
			mProgressDialog = null;
		}
	}

	public static boolean sentSTC = false;
	
	private void validateSTCByDistance(AcceptedJob job, double srcLat, double srcLon, double dstLat, double dstLon)
	{
		float[] results = new float[1];
		Location.distanceBetween(srcLat, srcLon, dstLat, dstLon, results);
		if (results[0] < 200)
		{
			String _currentTask = mJobStatusSender.getCurrentTask();
			mJobStatusSender.sendSoonToClear(job.getSentBy(), job.getJobId(),
					new JobStatusSender.JobStatusSenderListener() {
						@Override
						public void onStatusSent(String jobId, boolean isSent) {

						}
					});
			mJobStatusSender.setCurrentTask(_currentTask);
			sentSTC = true;
		}
	}
	
	private void validateSTCByZone(AcceptedJob job, String srcZoneId, String dstZoneId)
	{
		if (srcZoneId.equalsIgnoreCase(dstZoneId))
		{
			String _currentTask = mJobStatusSender.getCurrentTask();
			mJobStatusSender.sendSoonToClear(job.getSentBy(), job.getJobId(),
					new JobStatusSender.JobStatusSenderListener() {
						@Override
						public void onStatusSent(String jobId, boolean isSent) {

						}
					});
			mJobStatusSender.setCurrentTask(_currentTask);
			sentSTC = true;
		}
	}
	
	public void validateSTC()
	{
		AcceptedJob job = AcceptedJob.getActiveJob();
		if (job == null || job.getIntStatus() < 2)
			return;

		if (!TextUtils.isEmpty(job.getCustomerDropoffLat()) && !TextUtils.isEmpty(job.getCustomerDropoffLon()))
		{
			Location loc = getLastFix();
			if (loc != null)
				validateSTCByDistance(job, loc.getLatitude(), loc.getLongitude(), Double.parseDouble(job.getCustomerDropoffLat()) , Double.parseDouble(job.getCustomerDropoffLon()));
			return;
		}

		String dropoffZoneId = job.getDropoffZoneId();
		if (!TextUtils.isEmpty(dropoffZoneId))
		{
			validateSTCByZone(job, OiDriverService.currentZoneId, dropoffZoneId);
		}
	}

	public JobStatusSender getJobStatusSender() {
		return mJobStatusSender;
	}

	private void playButtonClickSound() {
		playSoundSync(R.raw.button_click, null);
	}

	private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver(){
		@Override
		public void onReceive(Context arg0, Intent intent) {
			int currentLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
			int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
			int level = 0;
			if (currentLevel >= 0 && scale > 0) {
				level = (currentLevel * 100) / scale;
			}
			//mBatteryBar.setProgress(level);
		}
	};

	class ConnectToXmppTask extends AsyncTask<Void, Void, Boolean> {

		ProgressDialog mProgressDialog;
		boolean mGotLogonMessage = false;
		String mErrorMessage;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			mProgressDialog = new ProgressDialog(OiDriverActivity.this);
			mProgressDialog.setMessage("Please wait.\nResetting connection...");
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialogInterface) {
					cancel(true);
				}
			});
			mProgressDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... voids) {

			XMPPClient client = OiDriverService.getXmppClient();
			client.disconnect();

            XMPPClient.release();

			Intent intent = new Intent("com.oidriver.ZONE_CHANGED");
			intent.putExtra("queueIndex", "0");
			OiDriverApp.getAppContext().sendBroadcast(intent);

            try {
                Thread.sleep(5000);
            } catch (InterruptedException ignored) {

            }

            client = OiDriverService.getXmppClient();
			boolean connected = client.connectToServer();
			if (!connected)
				return false;

			connected = client.login();
			if (!connected)
				client.disconnect();

			//OiDriverService.sendIQForGPS(false, Constants.DRIVER_STATUS_LOG);
			OiDriverService.sendIQForGPS(false, Constants.DRIVER_STATUS_POS);

			return connected;
		}

		@Override
		protected void onPostExecute(Boolean aSuccess) {
			super.onPostExecute(aSuccess);

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();
				mProgressDialog = null;
			}

			if (!aSuccess || mGotLogonMessage || isCancelled()) {
				if (TextUtils.isEmpty(mErrorMessage))
					Toast.makeText(OiDriverActivity.this, "NO RESULTS FROM SERVER", Toast.LENGTH_LONG).show();
				else
					Toast.makeText(OiDriverActivity.this, mErrorMessage, Toast.LENGTH_LONG).show();
			}
		}
	}

	public synchronized void updateConnectionStatus() {

//		final int[] icons = new int[]{R.drawable.disconnected, R.drawable.gprs_only, R.drawable.connected};
		boolean hasNetwork =
				NetworkUtils.getConnectivityStatus() != NetworkUtils.TYPE_NOT_CONNECTED;
		boolean loggedIn = OiDriverService.isXmppAuthenticated();

		int status = 0;
		// Network + not logged in
		if (hasNetwork && !loggedIn)
			status = 1;
		// Both network is OK and logged in
		else if (hasNetwork)
			status = 2;
        if (statusConnection != null) statusConnection.setImageResource(mStatuses[status]);

		TextView tvStatus = (TextView)findViewById(R.id.textViewXMPP);
		if (tvStatus != null) {
			tvStatus.setText((loggedIn ? "LOGGED IN" : "NOT LOGGED IN") + "\n" + mVersion);
		}

		mInfoJobs.setText("x " + OiDriverService.getBidsCount());
	}

	public void showReconnectionStatus(boolean show) {
		mReconnectionStatus.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
	}

	public void updateReconnectionStatus(String text) {
		mReconnectionStatus.setText(text);
	}

	public void updateZoneInfo(@SuppressWarnings("unused")String queueIdx, boolean playSound) {
		final boolean play = playSound;
		RunOn.mainThread(new Runnable() {
			@Override
			public void run() {
				try
				{
					if (!TextUtils.isEmpty(OiDriverService.currentZoneName) &&
							!OiDriverService.currentZoneId.equals("OUT_OF_ZONE")) {
						mCurrentZoneName.setText(OiDriverService.currentZoneName);
						if (play)
							playSoundSync(R.raw.zone_change, null);
					}
					else {
						mCurrentZoneName.setText(R.string.no_zone);
						if (play)
							playSoundSync(R.raw.no_mans_land, null);
					}
					mCurrentZoneName.setSelected(true);
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});
	}

	public void updateRankInfo(final String queue, final String state) {
		RunOn.mainThread(new Runnable() {
			@Override
			public void run() {
				try
				{
					if (mDriverStatusAlert != null) {
						mDriverStatusAlert.dismiss();
						mDriverStatusAlert = null;
					}

					Log.e("RANK", "Q = " + queue + " S = " + state + " L = " +
							(OiDriverService.readyStatus ?
									Constants.DRIVER_STATUS_RTW : Constants.DRIVER_STATUS_BRK));
					String currState = state;
					if (TextUtils.isEmpty(currState))
						currState = Constants.DRIVER_STATUS_BRK;
					if (!currState.equalsIgnoreCase(Constants.DRIVER_STATUS_POS)) {
                        OiDriverService.readyStatus = currState.equals(Constants.DRIVER_STATUS_RTW);
                        Log.wtf("READY", "[1] " + OiDriverService.readyStatus);
					}
					final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
					sp.edit().putBoolean(KEY_RTW, OiDriverService.readyStatus).commit();
					updateDriverRankUI(Integer.parseInt(TextUtils.isEmpty(queue) ? "0" : queue));
                    Log.wtf("READY", "[2] updateDriverRankUI()");
				}
				catch (Exception ignored) {

				}
			}
		});
	}

    public void updateTariff(final String startFare, final String regularFare, final int currencyFactor) {
        RunOn.mainThread(new Runnable() {
            @Override
            public void run() {
                if (TextUtils.isEmpty(startFare) || TextUtils.isEmpty(regularFare))
                    return;
                Double dStartFare = Double.parseDouble(startFare);
                Double dRegularFare = Double.parseDouble(regularFare);
                int iStart = (int) (dStartFare * currencyFactor);
                int iRegular = (int) (dRegularFare * currencyFactor);

                OiMeterFragment.Tariff t0 = new OiMeterFragment.Tariff();
                t0.mID = 0;
                t0.mVehicleType = "car";
                t0.mMinDistance = 1609;
                t0.mMinCharge = iStart;
                t0.mCharge = 10;
                t0.mClickDist = t0.mCharge * 1609 / iRegular - 1; // take 1m off
                t0.mChargeForWating = false;
                t0.mWaitingCost = 10;
                t0.mClickWaiting = 48;
                t0.mWeekDay = false;
                t0.mDescription = "default";
                t0.mCurrencyFactor = currencyFactor;

                String encoded = "";
                JSONObject jObj = new JSONObject();
                try {
                    jObj.put("id", t0.mID);
                    jObj.put("vehicleType", t0.mVehicleType);
                    jObj.put("minDistance", t0.mMinDistance);
                    jObj.put("minCharge", t0.mMinCharge);
                    jObj.put("charge", t0.mCharge);
                    jObj.put("clickDist", t0.mClickDist);
                    jObj.put("chargeForWaiting", t0.mChargeForWating);
                    jObj.put("waitingCost", t0.mWaitingCost);
                    jObj.put("clickWaiting", t0.mClickWaiting);
                    jObj.put("weekDay", t0.mWeekDay);
                    jObj.put("description", t0.mDescription);
                    jObj.put("currencyFactor", t0.mCurrencyFactor);

                    encoded = jObj.toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (!TextUtils.isEmpty(encoded)) {
                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
                    sp.edit().putString("default_tariff", encoded).commit();
                }
            }
        });

    }

    @SuppressWarnings("unused")
	public void setOnBookingSearchListener(OnBookingSearchListener listener) {
		mOnBookingSearchListener = listener;
	}

    public void setHired() {
        mMeterFragment.onHiredClicked();
    }

    /**
     * New Job alert callbacks
     */

    @Override
    public void onAccept(String jobId) {
        stopMediaPlayer();
        removeNewJobAlert();
        handleAcceptJob();
    }

    @Override
    public void onReject(String jobId) {
        stopMediaPlayer();
        removeNewJobAlert();
        handleRejectJob();
    }

    @Override
    public void onTimedOut(String jobId) {
        AcceptedJob job = null;
        try {
            job = jobsQueue.size() > 0 ? jobsQueue.take() : null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (job != null)
        {
            Intent intent = new Intent("com.oidriver.JOB_STATUS");
            intent.putExtra("jobstatus", "WK#,"
                    + job.getJobId()
                    + ","
                    + ""
                    + OiDriverService.getLoggedUserJid()
                    + ",TIMEOUT,0,0");
            intent.putExtra("jobid", job.getJobId());
            sendBroadcast(intent);
        }
        mJobStatusSender.setCurrentTask(Constants.TASK_MISSED_JOB);

        removeNewJobAlert();

        stopMediaPlayer();
    }

    @Override
    public void onHidden() {
        OiNewJobAlertFragment fragment = getNewJobAlertFragment();
        if (fragment != null) {
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
    }

    private void removeNewJobAlert() {
        OiNewJobAlertFragment fragment = getNewJobAlertFragment();
        if (fragment != null)
            fragment.removeFragment();
    }

    private void showNewJobAlert(String title, String jobId, String pickup, String dropoff) {
        FragmentManager fm = getSupportFragmentManager();

        OiNewJobAlertFragment newJobAlertFragment =
                OiNewJobAlertFragment.newInstance(title, jobId, pickup, dropoff);
        fm.beginTransaction()
                .setCustomAnimations(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_top,
                        R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_top)
                .replace(R.id.layoutAlert, newJobAlertFragment, FRAGMENT_TAG_ALERT_NEWJOB)
                .commit();
    }

    public OiNewJobAlertFragment getNewJobAlertFragment() {
        return (OiNewJobAlertFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_ALERT_NEWJOB);
    }

    public Drawable getGpsStatusDrawable() {
        if (mMapFragment != null)
            return getResources().getDrawable(mStatuses[mMapFragment.getGpsStatusIdx()]);
        else
            return getResources().getDrawable(mStatuses[0]);
    }

    public int getGpsStatusIdx() {
        return mMapFragment != null? mMapFragment.getGpsStatusIdx() : 0;
    }

    // From OiMapFragment
    @Override
    public void onLocationChanged(Location location) {
        for (OiMapFragment.OiDriverLocationListener l: mLocationListeners)
            if (l != null) l.onLocationChanged(location);
    }

    public void startScanCardActivity(Intent scanIntent) {
        startActivityForResult(scanIntent, Constants.CARD_IO_SCAN_REQUEST_CODE);
    }
}
