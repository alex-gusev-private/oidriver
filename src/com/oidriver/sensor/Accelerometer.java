package com.oidriver.sensor;

import android.hardware.Sensor;
import android.hardware.SensorEventListener;

/**
 * Created by touchnote on 09/01/2014.
 */

public abstract class Accelerometer implements SensorEventListener {

    protected float lastX;
    protected float lastY;
    protected float lastZ;
    public abstract Point3D getPoint();

    public void onAccuracyChanged(Sensor arg0, int arg1) {
    }
}
