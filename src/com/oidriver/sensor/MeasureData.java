package com.oidriver.sensor;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.LinkedList;

/**
 * Created by Alex Gusev on 09/01/2014.
 * Touchnote Ltd.
 */
public class MeasureData {
    // points from accelerometr
    private LinkedList<Point3D> accData;
    private LinkedList<MeasurePoint> data;
    // timer interval of generating points
    private long interval;

    public MeasureData(long interval) {
        this.interval = interval;
        accData = new LinkedList<Point3D> ();
        data = new LinkedList<MeasurePoint> ();
    }

    public void addPoint(Point3D p){
        accData.add(p);
    }

    public void addPointEx(Point3D p){
        accData.add(p)
        ;
        float speed = 0;
        if(data.size() > 0){
            speed = data.get(data.size()-1).getSpeedAfter();
        }
        data.add(new MeasurePoint(p.getX(), p.getY(), p.getZ(), speed, interval, getAveragePoint()));
    }

    public void process(){

        for(int i = 0; i < accData.size(); ++i){
            Point3D p = accData.get(i);
            float speed = 0;

            if(i > 0){
                speed = data.get(i-1).getSpeedAfter();
            }
            data.add(new MeasurePoint(p.getX(), p.getY(), p.getZ(), speed, interval, getAveragePoint()));
        }
    }

    public boolean saveExt(Context con, String fname) throws Throwable {

        File file = new File(con.getExternalFilesDir(null), fname);
        FileOutputStream os = new FileOutputStream(file);
        OutputStreamWriter out = new OutputStreamWriter(os);
        for (MeasurePoint aData : data) {
            out.write(aData.getStoreString());
        }
        out.close();
        return true;
    }

    private Point3D getAveragePoint() {
        float x = 0;
        float y = 0;
        float z = 0;

        for (Point3D p : accData) {
            x += p.getX();
            y += p.getY();
            z += p.getZ();
        }

        return new Point3D(x, y, z, 1);
    }

    public float getLastSpeed(){
        return data.size() > 0 ? data.getLast().getSpeedAfter() : 0f;
    }

    public float getLastSpeedKm(){
        float ms = getLastSpeed();
        return ms*3.6f;
    }
}
