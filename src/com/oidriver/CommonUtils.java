package com.oidriver;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.util.regex.Pattern;


public class CommonUtils
{
	public static boolean isNetworkAvailable() {
		try
		{
			Context context = OiDriverApp.getAppContext();
			ConnectivityManager connectivity = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity == null) {
				Log.e(OiDriverApp.TAG, "CONNECTIVITY_SERVICE return null");
			} else {
				NetworkInfo[] info = connectivity.getAllNetworkInfo();
				if (info != null) {
					for (int i = 0; i < info.length; i++) {
						if (info[i].getState() == NetworkInfo.State.CONNECTED) {
							return true;
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			if (e != null) e.printStackTrace();
			return false;
		}
		return false;
	}

	public static final String EMAIL_ADDRESS_REGEX = "^([a-zA-Z0-9\\+_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+";

	public static String getDefaultEmail(Context context) {
		final Account[] accounts = AccountManager.get(context).getAccounts();
		for (Account account : accounts) {
			if (Pattern.compile(EMAIL_ADDRESS_REGEX).matcher(account.name)
					.matches()) {
				return account.name;
			}
		}
		return null;
	}

	public static String getDefaultGoogleEmail(Context context) {
		AccountManager accntMgr = AccountManager.get(context);
		Account[] accounts = accntMgr.getAccountsByType("com.google");
		if (accounts != null && accounts.length > 0) {
			return accounts[0].name;
		}

		return getDefaultEmail(context);
	}

    public static void startExternapApp(Activity activity, String packageName) {
        PackageManager pm = activity.getPackageManager();
        if (pm != null) {
            Intent intent = pm.getLaunchIntentForPackage("com.planetcoops.android.taximeter");
            if (intent != null)
                activity.startActivity(intent);
        }
    }

    public static void startOiDriver(Activity activity) {
        PackageManager pm = activity.getPackageManager();
        if (pm != null) {
            Intent intent = pm.getLaunchIntentForPackage("com.oidriver");
            if (intent != null)
                activity.startActivity(intent);
        }
    }
}