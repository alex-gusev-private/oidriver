package com.oidriver.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;

import com.oidriver.AcceptedJob;
import com.oidriver.OiDriverActivity;
import com.oidriver.service.OiDriverService;

import java.util.Calendar;
import java.util.Date;

public class OiAlarmReceiver extends BroadcastReceiver {

	final static String ACTION_JOB_ALARM = "com.oirdiver.JOB_ALARM";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			Bundle bundle = intent.getExtras();
			final String jobId = bundle.getString("job_id");
			AcceptedJob currentJob = OiDriverService.dbHelper.getJobFromDB(jobId);
			if (currentJob == null)
				return;
			if (!AcceptedJob.hasActiveJob())
				currentJob.insertOrUpdateJob();
//			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//			View content = inflater.inflate(R.layout.custom_cancel_job, null);
//			TextView jobAddress1 = (TextView)content.findViewById(R.id.textViewPickup);
//			TextView jobAddress2 = (TextView)content.findViewById(R.id.textViewDropoff);
//
//			final Button button1 = (Button)content.findViewById(R.id.button1);
//			AlertDialog.Builder builder = new AlertDialog.Builder(context)
//				.setTitle(String.format("Job Cancelled: %s", jobId))
//				.setIcon(R.drawable.passenger_inverted)
//				.setView(content);
//			final AlertDialog jobAlert = builder.create();
//			button1.setOnClickListener(new OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					
//					jobAlert.dismiss();
//				}
//			});
//			
//			jobAddress1.setText(String.format("Pickup:\t%s",currentJob.getPickupAddress(false)));
//			jobAddress2.setText(String.format("Dropoff:\t%s",currentJob.getDropoffAddress(false)));
//			
//			jobAlert.show();
			Intent i = new Intent(context, OiDriverActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
			context.startActivity(i);
		} catch (Exception e) {
			Toast.makeText(
					context,
					"There was an error somewhere, but we still received an alarm",
					Toast.LENGTH_SHORT).show();
			e.printStackTrace();

		}
	}
	
	private static Intent getIntent(Context context, String id)
    {
            Intent i = new Intent(context, OiAlarmReceiver.class);
            i.setAction(ACTION_JOB_ALARM);
            i.setData(Uri.parse("timer:" + id));
            return i;
    }
	
	public static void SetAlarm(Context context, String jobId, Calendar cal) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = getIntent(context, jobId);
        intent.putExtra("jobid", jobId);
        intent.putExtra("time", cal.getTimeInMillis());
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
        Date dt = cal.getTime();
        Date dtNow = Calendar.getInstance().getTime();
        long tm = cal.getTimeInMillis();
        Log.i("ALARM", String.format("SET: cal = " + (String) DateFormat.format("yyyy-MM-dd HH:mm:ss", dt) + 
        							 ", now = " + (String) DateFormat.format("yyyy-MM-dd HH:mm:ss", dtNow)));
        am.set(AlarmManager.RTC_WAKEUP, tm, sender);
	}

	public static void SetRepeatingAlarm(Context context) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = getIntent(context, "0");
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        long firstTime = SystemClock.elapsedRealtime();
        am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, firstTime, (long)(60*1000L), pi);
	}
	
	public static void CancelAlarm(Context context, String jobId) {
	        Intent intent = getIntent(context, jobId);
	        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
	        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
	        alarmManager.cancel(sender);
	}
}