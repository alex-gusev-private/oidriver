package com.oidriver.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.oidriver.OiDriverApp;
import com.oidriver.xmpp.XMPPClient;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

public class SmsReceiver extends BroadcastReceiver 
{
	// All available column names in SMS table
    // [_id, thread_id, address, 
	// person, date, protocol, read, 
	// status, type, reply_path_present, 
	// subject, body, service_center, 
	// locked, error_code, seen]
	
	public static final String SMS_EXTRA_NAME = "pdus";
	public static final String SMS_URI = "content://sms";
	
	public static final String ADDRESS = "address";
    public static final String PERSON = "person";
    public static final String DATE = "date";
    public static final String READ = "read";
    public static final String STATUS = "status";
    public static final String TYPE = "type";
    public static final String BODY = "body";
    public static final String SEEN = "seen";
    
    public static final int MESSAGE_TYPE_INBOX = 1;
    public static final int MESSAGE_TYPE_SENT = 2;
    
    public static final int MESSAGE_IS_NOT_READ = 0;
    public static final int MESSAGE_IS_READ = 1;
    
    public static final int MESSAGE_IS_NOT_SEEN = 0;
    public static final int MESSAGE_IS_SEEN = 1;
	
    // Change the password here or give a user possibility to change it
    public static final byte[] PASSWORD = new byte[]{ 0x20, 0x32, 0x34, 0x47, (byte) 0x84, 0x33, 0x58 };
    
    public String currentBuffer = "";
    private Hashtable<String, String> hashMessages = new Hashtable<String, String>();
    public static String JOB_CENTER_NUMBER = "";
    
	public void onReceive( Context context, Intent intent ) 
	{
		// Get SMS map from Intent
        Bundle extras = intent.getExtras();
        if ( extras != null )
        {
            // Get received SMS array
            Object[] smsExtra = (Object[]) extras.get( SMS_EXTRA_NAME );
            for ( int i = 0; i < smsExtra.length; ++i )
            {
            	SmsMessage sms = SmsMessage.createFromPdu((byte[])smsExtra[i]);
            	
            	String body = sms.getMessageBody().toString();
            	String address = sms.getOriginatingAddress();
            	Log.i(OiDriverApp.TAG,body);
            	String text = hashMessages.get(address);
            	if (text == null)
            		text = "";
            	text += body;
            	hashMessages.put(address, text);
            }
            
            Set<String> keys = hashMessages.keySet();
            for (String key: keys)
            {
            	String text = hashMessages.get(key);
            	String[] messages = text.split("<DocumentElement");
            	if (messages == null || messages.length == 0)
            		continue;
            	SmsReceiver.JOB_CENTER_NUMBER = key;
            	hashMessages.remove(key);
            	for (String message: messages) {
            		int idx = message.indexOf("</DocumentElement>");
	    			if (idx > -1)
	    			{
	    				message = message.substring(0, idx + "</DocumentElement>".length());
	    				Log.i(OiDriverApp.TAG,"EXTRACTED:\n" + message);
	    				try {
							XMPPClient.parseJob("<DocumentElement " + message, OiDriverApp.MANAGER_JID);
						} catch (ParserConfigurationException e) {
							e.printStackTrace();
						} catch (SAXException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
	    			}
            	}
            }
        }
        
        // WARNING!!! 
        // If you uncomment next line then received SMS will not be put to incoming.
        // Be careful!
        // this.abortBroadcast(); 
	}
	
	
}
