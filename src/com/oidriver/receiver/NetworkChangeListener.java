package com.oidriver.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.oidriver.OiDriverApp;

public class NetworkChangeListener extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {        
        try
        {
	        ConnectivityManager cm =
			        (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
	        NetworkInfo anet = cm.getActiveNetworkInfo();
	        boolean isConnected = anet != null && anet.isConnectedOrConnecting();
	        Intent myIntent = new Intent("com.oidriver.CONNECTION_CHANGE");
	        myIntent.putExtra("isConnected", isConnected);
        	OiDriverApp.getAppContext().sendBroadcast(myIntent);
        }
        catch(Exception e)
        {
        	Log.e(OiDriverApp.TAG, "ERROR : Network Check");
        	e.printStackTrace();
        }
    }
}
