package com.oidriver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.widget.Toast;

import com.bugsnag.android.Bugsnag;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

//OiDriver-CrashReports : formkey=dE83TGZmWkY2WWlVdlI5dFM5QS1zdFE6MQ
//@ReportsCrashes(formKey = "dE83TGZmWkY2WWlVdlI5dFM5QS1zdFE6MQ",
//                              mode = ReportingInteractionMode.TOAST,
//                              forceCloseDialogAfterToast = false, // optional, default false
//                              resToastText = R.string.crash_toast_text)

public class OiDriverApp extends Application {

	public static final int ICS_VERSION_VALUE = 14;
	public static final String BUGSNAG_APP_ID = "374d3da8317f392aa6debbf41472e309";
	private static Context mContext = null;
	public static final String TAG = "OIDRIVER";
	public static double PAGER_FINGER_FACTOR = 5;
	public static String MANAGER_JID = Constants.BOOKING_ID;
	public static String XMPP_SERVICE = Constants.XMPP_SERVICE;
	public static String COMPANY = Constants.COMPANY;

	@SuppressLint("NewApi")
	@Override
	public void onCreate() {

		OiDriverApp.mContext = getApplicationContext() != null ? getApplicationContext() : this;
    	
		/*if (Build.VERSION.SDK_INT >= ICS_VERSION_VALUE) {
			StrictMode.ThreadPolicy.Builder builderThread = new StrictMode.ThreadPolicy.Builder();
			StrictMode.VmPolicy.Builder builderVM = new StrictMode.VmPolicy.Builder();

			builderThread.permitAll();

			StrictMode.setThreadPolicy(builderThread.build());
			StrictMode.setVmPolicy(builderVM.detectAll().penaltyLog().build());
		}*/
		
		Bugsnag.register(this, BUGSNAG_APP_ID);
		String userId = CommonUtils.getDefaultGoogleEmail(getAppContext());
		if (!TextUtils.isEmpty(userId))
			Bugsnag.setUserId(userId);
		
		if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1)
			PAGER_FINGER_FACTOR = Constants.PAGER_BUTTON_SCROLL_FACTOR_GB;
		else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
			PAGER_FINGER_FACTOR = Constants.PAGER_BUTTON_SCROLL_FACTOR_ICS;
		else
			PAGER_FINGER_FACTOR = Constants.PAGER_BUTTON_SCROLL_FACTOR_JB;
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
	}

	public static Context getAppContext() {
		return OiDriverApp.mContext;
	}

	// ---sends an SMS message to another device---
	public void sendSMS(String phoneNumber, String message) {
		String SENT = "SMS_SENT";
		String DELIVERED = "SMS_DELIVERED";

		PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
				SENT), 0);

		PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
				new Intent(DELIVERED), 0);

		// ---when the SMS has been sent---
		registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					Toast.makeText(getBaseContext(), "SMS sent",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					Toast.makeText(getBaseContext(), "Generic failure",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
					Toast.makeText(getBaseContext(), "No service",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
					Toast.makeText(getBaseContext(), "Null PDU",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
					Toast.makeText(getBaseContext(), "Radio off",
							Toast.LENGTH_SHORT).show();
					break;
				}
			}
		}, new IntentFilter(SENT));

		// ---when the SMS has been delivered---
		registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					Toast.makeText(getBaseContext(), "SMS delivered",
							Toast.LENGTH_SHORT).show();
					break;
				case Activity.RESULT_CANCELED:
					Toast.makeText(getBaseContext(), "SMS not delivered",
							Toast.LENGTH_SHORT).show();
					break;
				}
			}
		}, new IntentFilter(DELIVERED));

		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
	}
	
	public static double getPagerScrollFactorCtrl()
	{
		return PAGER_FINGER_FACTOR;
	}

	public static double getPagerScrollFactorTouch()
	{
		return Constants.PAGER_FINGER_SCROLL_FACTOR;
	}
	
	private static OiDriverApp instance;

	public static OiDriverApp getInstance() {
		if (instance == null)
			throw new IllegalStateException();
		return instance;
	}
//
//	private final ArrayList<Object> registeredManagers;
//	/**
//	 * Unmodifiable collections of managers that implement some common
//	 * interface.
//	 */
//	private Map<Class<? extends BaseManagerInterface>, Collection<? extends BaseManagerInterface>> managerInterfaces;
//	
//	private Map<Class<? extends BaseUIListener>, Collection<? extends BaseUIListener>> uiListeners;
	
	/**
	 * Thread to execute tasks in background..
	 */
	private final ExecutorService backgroundExecutor;

	/**
	 * Handler to execute runnable in UI thread.
	 */
	private final Handler handler;

	/**
	 * Where data load was requested.
	 */
	private boolean serviceStarted;

	/**
	 * Whether application was initialized.
	 */
	private boolean initialized;

	/**
	 * Whether user was notified about some action in contact list activity
	 * after application initialization.
	 */
	private boolean notified;

	/**
	 * Whether application is to be closed.
	 */
	private boolean closing;

	/**
	 * Whether onServiceDestroy() has been called.
	 */
	private boolean closed;

	/**
	 * Future for loading process.
	 */
	private Future<Void> loadFuture;

//	private final Runnable timerRunnable = new Runnable() {
//
//		@Override
//		public void run() {
//			for (OnTimerListener listener : getManagers(OnTimerListener.class))
//				listener.onTimer();
//			if (!closing)
//				startTimer();
//		}
//
//	};
	
	public OiDriverApp() {
		instance = this;
		serviceStarted = false;
		initialized = false;
		notified = false;
		closing = false;
		closed = false;
//		managerInterfaces = new HashMap<Class<? extends BaseManagerInterface>, Collection<? extends BaseManagerInterface>>();
//		registeredManagers = new ArrayList<Object>();

		handler = new Handler();
		backgroundExecutor = Executors
				.newSingleThreadExecutor(new ThreadFactory() {
					@Override
					public Thread newThread(Runnable runnable) {
						Thread thread = new Thread(runnable,
								"Background executor service");
						thread.setPriority(Thread.MIN_PRIORITY);
						thread.setDaemon(true);
						return thread;
					}
				});
	}

	/**
	 * Start periodically callbacks.
	 */
//	private void startTimer() {
//		runOnUiThreadDelay(timerRunnable, OnTimerListener.DELAY);
//	}

	/**
	 * Register new manager.
	 * 
	 * @param manager
	 */
//	public void addManager(Object manager) {
//		registeredManagers.add(manager);
//	}

	/**
	 * @param cls
	 *            Requested class of managers.
	 * @return List of registered manager.
	 */
//	@SuppressWarnings("unchecked")
//	public <T extends BaseManagerInterface> Collection<T> getManagers(
//			Class<T> cls) {
//		if (closed)
//			return Collections.emptyList();
//		Collection<T> collection = (Collection<T>) managerInterfaces.get(cls);
//		if (collection == null) {
//			collection = new ArrayList<T>();
//			for (Object manager : registeredManagers)
//				if (cls.isInstance(manager))
//					collection.add((T) manager);
//			collection = Collections.unmodifiableCollection(collection);
//			managerInterfaces.put(cls, collection);
//		}
//		return collection;
//	}

	/**
	 * Request to clear application data.
	 */
//	public void requestToClear() {
//		runInBackground(new Runnable() {
//			@Override
//			public void run() {
//				clear();
//			}
//		});
//	}
//
//	private void clear() {
//		for (Object manager : registeredManagers)
//			if (manager instanceof OnClearListener)
//				((OnClearListener) manager).onClear();
//	}

	/**
	 * Request to wipe all sensitive application data.
	 */
//	public void requestToWipe() {
//		runInBackground(new Runnable() {
//			@Override
//			public void run() {
//				clear();
//				for (Object manager : registeredManagers)
//					if (manager instanceof OnWipeListener)
//						((OnWipeListener) manager).onWipe();
//			}
//		});
//	}
//
//	@SuppressWarnings("unchecked")
//	private <T extends BaseUIListener> Collection<T> getOrCreateUIListeners(
//			Class<T> cls) {
//		Collection<T> collection = (Collection<T>) uiListeners.get(cls);
//		if (collection == null) {
//			collection = new ArrayList<T>();
//			uiListeners.put(cls, collection);
//		}
//		return collection;
//	}

	/**
	 * @param cls
	 *            Requested class of listeners.
	 * @return List of registered UI listeners.
	 */
//	public <T extends BaseUIListener> Collection<T> getUIListeners(Class<T> cls) {
//		if (closed)
//			return Collections.emptyList();
//		return Collections.unmodifiableCollection(getOrCreateUIListeners(cls));
//	}

	/**
	 * Register new listener.
	 * 
	 * Should be called from {@link Activity#onResume()}.
	 * 
	 * @param cls
	 * @param listener
	 */
//	public <T extends BaseUIListener> void addUIListener(Class<T> cls,
//			T listener) {
//		getOrCreateUIListeners(cls).add(listener);
//	}

	/**
	 * Unregister listener.
	 * 
	 * Should be called from {@link Activity#onPause()}.
	 * 
	 * @param cls
	 * @param listener
	 */
//	public <T extends BaseUIListener> void removeUIListener(Class<T> cls,
//			T listener) {
//		getOrCreateUIListeners(cls).remove(listener);
//	}
	
	/**
	 * Notify about error.
	 * 
	 * @param resourceId
	 */
//	public void onError(final int resourceId) {
//		runOnUiThread(new Runnable() {
//			@Override
//			public void run() {
//				for (OnErrorListener onErrorListener : getUIListeners(OnErrorListener.class))
//					onErrorListener.onError(resourceId);
//			}
//		});
//	}

	/**
	 * Notify about error.
	 * 
	 * @param networkException
	 */
//	public void onError(NetworkException networkException) {
//		//LogManager.exception(this, networkException);
//		onError(networkException.getResourceId());
//	}

	/**
	 * Submits request to be executed in background.
	 * 
	 * @param runnable
	 */
	public void runInBackground(final Runnable runnable) {
		backgroundExecutor.submit(new Runnable() {
			@Override
			public void run() {
				try {
					runnable.run();
				} catch (Exception e) {
					//LogManager.exception(runnable, e);
				}
			}
		});
	}

	/**
	 * Submits request to be executed in UI thread.
	 * 
	 * @param runnable
	 */
	public void runOnUiThread(final Runnable runnable) {
		handler.post(runnable);
	}

	/**
	 * Submits request to be executed in UI thread.
	 * 
	 * @param runnable
	 * @param delayMillis
	 */
	public void runOnUiThreadDelay(final Runnable runnable, long delayMillis) {
		handler.postDelayed(runnable, delayMillis);
	}

}