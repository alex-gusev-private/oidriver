package com.oidriver;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.oidriver.service.OiDriverService;

import java.util.ArrayList;

public class JobOffersAdapter extends ArrayAdapter<AcceptedJob> {
	
	Context mContext;
	
	interface OnJobSelectedListener
	{
		void onJobSelected(AcceptedJob job);
		void onJobSelectionError(String jobId, String error);
	}
	
	OnJobSelectedListener mListener;

	public JobOffersAdapter(Context context, int textViewResourceId, ArrayList<AcceptedJob> items, OnJobSelectedListener listener) {
		super(context, textViewResourceId, items);
		mContext = context;
		mListener = listener;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = null;
		
		if (convertView == null)
		{
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.joboffers_row, null);
		}
		else
		{
			view = convertView;
		}
		
		AcceptedJob item = getItem(position);
		if (item != null && view != null)
		{
			String nameStr = item.getName();
			String fromStr = item.getPickupAddress(false);
			String toStr = item.getDropoffAddress(false);
			String timeStr = item.getFormattedBookingTime();
			
			TextView name = (TextView) view.findViewById(R.id.textViewName);
			TextView from = (TextView) view.findViewById(R.id.textViewFrom);
			TextView to = (TextView) view.findViewById(R.id.textViewTo);
			TextView time = (TextView) view.findViewById(R.id.textViewTime);
			name.setText(nameStr);
			from.setText(fromStr);
			to.setText(toStr);
			time.setText(timeStr);
			final Button btn = (Button)view.findViewById(R.id.buttonOps);
			btn.setText(item.getShortStatus());
			btn.setOnClickListener(new OpsButtonListener(item.getJobId()));
		}
		
		return view;
	}
	
	class OpsButtonListener implements OnClickListener {

		String mJobId;
		
		public OpsButtonListener(String jobId)
		{
			mJobId = jobId;
		}
		
		@Override
		public void onClick(View v) {
			
			// Get job from DB here as we are in the list
			AcceptedJob job = OiDriverService.dbHelper.getJobByJobId(mJobId);
			if (job == null)
				job = AcceptedJob.getActiveJob();
			if (job == null)
				return;
			
			if (mListener != null)
				mListener.onJobSelected(job);
		}
	}
}