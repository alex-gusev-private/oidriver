package com.oidriver;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AcceptedJob implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6075875949765409218L;
	
	private Integer id = 0;
	private String bookingId;
	private String name;
	private String pickup;
	private String dropoff;
	private String telephone;
	private String bookingTime;
	private String jobId;
	private String note;
	private String customerPickupLat;
	private String customerPickupLon;
	private String customerDropoffLat;
	private String customerDropoffLon;
	private String nop;
	private String jobRef;
	private String postCode1;
	private String postCode2;
	private String houseNumber1;
	private String houseNumber2;
	private String pickupZoneId;
	private String dropoffZoneId;
	private String farePrice;
	private String custType;
	private String status = "1";
	private String sentby = OiDriverApp.MANAGER_JID;
	
	public String getBookingId() {
		return bookingId;
	}
	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus() {
		return status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPickup() {
		return pickup;
	}
	public void setPickup(String pickup) {
		this.pickup = pickup;
	}
	public String getDropoff() {
		return dropoff;
	}
	public void setDropoff(String dropoff) {
		this.dropoff = dropoff;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getBookingTime() {
		return bookingTime;
	}
	public String getFormattedBookingTime() {
		String time = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.UK);
		SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm", Locale.UK);
		try {
			Date d = sdf.parse(bookingTime);
			time = sdfTime.format(d);
		} catch (ParseException e) {
			// Ignore the exception as it is most probably due to the fact 
			// that string is in 'time' form already.
			time = bookingTime;
		}
		return time;
	}
	public void setBookingTime(String bookingTime) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
		//SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm", Locale.UK);
		try {
			Date d = sdf.parse(bookingTime);
			//this.bookingTime = sdfTime.format(d);
			this.bookingTime = sdf.format(d);
		} catch (ParseException e) {
			// Ignore the exception as it is most probably due to the fact 
			// that string is in 'time' form already.
			this.bookingTime = bookingTime;
		}
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getCustomerPickupLat() {
		return customerPickupLat;
	}
	public void setCustomerPickupLat(String customerPickupLat) {
		this.customerPickupLat = customerPickupLat;
	}
	public String getCustomerPickupLon() {
		return customerPickupLon;
	}
	public void setCustomerPickupLon(String customerPickupLon) {
		this.customerPickupLon = customerPickupLon;
	}
	public String getCustomerDropoffLat() {
		return customerDropoffLat;
	}
	public void setCustomerDropoffLat(String customerDropoffLat) {
		this.customerDropoffLat = customerDropoffLat;
	}
	public String getCustomerDropoffLon() {
		return customerDropoffLon;
	}
	public void setCustomerDropoffLon(String customerDropoffLon) {
		this.customerDropoffLon = customerDropoffLon;
	}
	public String getNop() {
		return nop;
	}
	public void setNop(String nop) {
		this.nop = nop;
	}
	public String getJobRef() {
		return jobRef;
	}
	public void setJobRef(String jobRef) {
		this.jobRef = jobRef;
	}
	public String getPostCode1() {
		return postCode1;
	}
	public void setPostCode1(String postCode1) {
		this.postCode1 = postCode1;
	}
	public String getPostCode2() {
		return postCode2;
	}
	public void setPostCode2(String postCode2) {
		this.postCode2 = postCode2;
	}
	public String getHouseNumber1() {
		return houseNumber1;
	}
	public void setHouseNumber1(String houseNumber1) {
		this.houseNumber1 = houseNumber1;
	}
	public String getHouseNumber2() {
		return houseNumber2;
	}
	public void setHouseNumber2(String houseNumber2) {
		this.houseNumber2 = houseNumber2;
	}
	public String getPickupZoneId() {
		return pickupZoneId;
	}
	public void setPickupZoneId(String pickupZoneId) {
		this.pickupZoneId = pickupZoneId;
	}
	public String getDropoffZoneId() {
		return dropoffZoneId;
	}
	public void setDropoffZoneId(String dropoffZoneId) {
		this.dropoffZoneId = dropoffZoneId;
	}
	public String getFarePrice() {
		return farePrice;
	}
	public void setFarePrice(String farePrice) {
		this.farePrice = farePrice;
	}
	public String getCustType() {
		return custType;
	}
	public void setCustType(String custType) {
		this.custType = custType;
	}
	public String getSentBy() {
		return sentby;
	}
	public void setSentBy(String sentBy) {
		this.sentby = sentBy;
	}
	public int getIntStatus() {
		
		if (status.equalsIgnoreCase(Constants.STATUS_ACCEPTED))
			return 1;
		else if (status.equalsIgnoreCase(Constants.STATUS_DOW))
			return 2;
		else if (status.equalsIgnoreCase(Constants.STATUS_REJECTED))
			return 3;				
		else if (status.equalsIgnoreCase(Constants.STATUS_NO_JOB))
			return 4;
		else if (status.equalsIgnoreCase(Constants.STATUS_POB))
			return 5;
		else if (status.equalsIgnoreCase(Constants.STATUS_STC))
			return 6;
		else if (status.equalsIgnoreCase(Constants.STATUS_EOJ)) 
			return 7;
		else if (status.equalsIgnoreCase(Constants.STATUS_MISSED))
			return 8;

		return 0;
	}
	public String getShortStatus() {
		if (status.equalsIgnoreCase(Constants.STATUS_ACCEPTED))
			return "START";
		else if (status.equalsIgnoreCase(Constants.STATUS_DOW))
			return "DOW";
		else if (status.equalsIgnoreCase(Constants.STATUS_REJECTED))
			return "REJ";				
		else if (status.equalsIgnoreCase(Constants.STATUS_NO_JOB))
			return "NO JOB";
		else if (status.equalsIgnoreCase(Constants.STATUS_POB))
			return "POB";
		else if (status.equalsIgnoreCase(Constants.STATUS_STC))
			return "STC";
		else if (status.equalsIgnoreCase(Constants.STATUS_EOJ)) 
			return "EOJ";
		else if (status.equalsIgnoreCase(Constants.STATUS_MISSED))
			return "MISSED";
		return "NEW";
	}
	
	public String getPickupAddress(boolean addHomeNumber)
	{
		String addr = "";
		if (!TextUtils.isEmpty(houseNumber1) && addHomeNumber)
		{
			if (!TextUtils.isDigitsOnly(houseNumber1))
				addr += houseNumber1;
			else
			{
				int num = Integer.parseInt(houseNumber1);
				if (num > 0)
					addr += houseNumber1;
			}
		}
		if (!TextUtils.isEmpty(pickup))
			addr += (addr.length() > 0 ? ", " : "") + pickup;
		if (!TextUtils.isEmpty(postCode1))
			addr += (addr.length() > 0 ? ", " : "") + postCode1;
		
		return addr;
	}

	public String getDropoffAddress(boolean addHomeNumber)
	{
		String addr = "";
		if (!TextUtils.isEmpty(houseNumber2) && addHomeNumber)
		{
			if (!TextUtils.isDigitsOnly(houseNumber2))
				addr += houseNumber2;
			else
			{
				int num = Integer.parseInt(houseNumber2);
				if (num > 0)
					addr += houseNumber2;
			}
		}
		if (!TextUtils.isEmpty(dropoff))
			addr += (addr.length() > 0 ? ", " : "") + dropoff;
		if (!TextUtils.isEmpty(postCode2))
			addr += (addr.length() > 0 ? ", " : "") + postCode2;
		
		return addr;
	}
	
	public String getPickupAddressPostcode()
	{
		String addr = "";
		if (!TextUtils.isEmpty(pickup))
			addr +=  pickup;
		if (!TextUtils.isEmpty(postCode1))
			addr += (addr.length() > 0 ? "\n" : "") + postCode1;
		
		return addr;
	}

	public String getDropoffAddressPostcode()
	{
		String addr = "";
		if (!TextUtils.isEmpty(dropoff))
			addr += dropoff;
		if (!TextUtils.isEmpty(postCode2))
			addr += (addr.length() > 0 ? "\n" : "") + postCode2;
		
		return addr;
	}
	
	public String getPickupAddressHousePostcode()
	{
		String addr = "";
		if (!TextUtils.isEmpty(houseNumber1))
		{
			if (!TextUtils.isDigitsOnly(houseNumber1))
				addr += houseNumber1;
			else
			{
				int num = Integer.parseInt(houseNumber1);
				if (num > 0)
					addr += houseNumber1;
			}
		}
		if (!TextUtils.isEmpty(pickup))
			addr +=  (addr.length() > 0 ? " " : "") + pickup;
		if (!TextUtils.isEmpty(postCode1))
			addr += (addr.length() > 0 ? "\n" : "") + postCode1;
		
		return addr;
	}

	public String getDropoffAddressHousePostcode()
	{
		String addr = "";
		if (!TextUtils.isEmpty(houseNumber2))
		{
			if (!TextUtils.isDigitsOnly(houseNumber2))
				addr += houseNumber2;
			else
			{
				int num = Integer.parseInt(houseNumber2);
				if (num > 0)
					addr += houseNumber2;
			}
		}
		if (!TextUtils.isEmpty(dropoff))
			addr += (addr.length() > 0 ? " " : "") + dropoff;
		if (!TextUtils.isEmpty(postCode2))
			addr += (addr.length() > 0 ? "\n" : "") + postCode2;
		
		return addr;
	}
	
	// SharedPreferences serialisation methods
	public boolean insertOrUpdateJob()
	{
		JSONObject jObj = new JSONObject();
		try {
			jObj.put("id", id);
			jObj.put("bookingId", bookingId);
			jObj.put("name", name);
			jObj.put("pickup", pickup);
			jObj.put("dropoff", dropoff);
			jObj.put("telephone", telephone);
			jObj.put("bookingTime", bookingTime);
			jObj.put("jobId", jobId);
			jObj.put("note", note);
			jObj.put("customerPickupLat", customerPickupLat);
			jObj.put("customerPickupLon", customerPickupLon);
			jObj.put("customerDropoffLat", customerDropoffLat);
			jObj.put("customerDropoffLon", customerDropoffLon);
			jObj.put("nop", nop);
			jObj.put("jobRef", jobRef);
			jObj.put("postCode1", postCode1);
			jObj.put("postCode2", postCode2);
			jObj.put("houseNumber1", houseNumber1);
			jObj.put("houseNumber2", houseNumber2);
			jObj.put("pickupZoneId", pickupZoneId);
			jObj.put("dropoffZoneId", dropoffZoneId);
			jObj.put("farePrice", farePrice);
			jObj.put("custType", custType);
			jObj.put("status", status);
			jObj.put("sentby", sentby);
			
			String str = jObj.toString();
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
			return sp.edit().putString("CurrentJob", str).commit();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean insertJobFromBooking(Booking booking)
	{
		JSONObject jObj = new JSONObject();
		try {
			jObj.put("id", "0");
			jObj.put("bookingId", booking.getBookingId());
			jObj.put("name", booking.getPassengerName());
			jObj.put("pickup", booking.getPickupAddress());
			jObj.put("dropoff", booking.getDropoffAddress());
			jObj.put("telephone", "");//FIXME
			//SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.UK);
			//Date dt = booking.getDtBookingTimestamp();
			//jObj.put("bookingTime", sdf.format(dt));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.UK);
			Date dt = booking.getDtBookingTimestamp();
			jObj.put("bookingTime", sdf.format(dt));
			jObj.put("jobId", booking.getJobId());
			jObj.put("note", booking.getNotes());
			jObj.put("customerPickupLat", booking.getPickupLat());
			jObj.put("customerPickupLon", booking.getPickupLon());
			jObj.put("customerDropoffLat", booking.getDropoffLat());
			jObj.put("customerDropoffLon", booking.getDropoffLon());
			jObj.put("nop", "");//FIXME
			jObj.put("jobRef", "");//FIXME
			jObj.put("postCode1", "");//FIXME
			jObj.put("postCode2", "");//FIXME
			jObj.put("houseNumber1", "");//FIXME
			jObj.put("houseNumber2", "");//FIXME
			jObj.put("pickupZoneId", booking.getZoneId());
			jObj.put("dropoffZoneId", "");//FIXME
			jObj.put("farePrice", booking.getFare());
			jObj.put("custType", "");//FIXME
			jObj.put("status", Constants.STATUS_ACCEPTED);
			jObj.put("sentby", booking.getSentBy());
			
			String str = jObj.toString();
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
			return sp.edit().putString("CurrentJob", str).commit();
		} catch (JSONException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static AcceptedJob getActiveJob()
	{
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
		if (!sp.contains("CurrentJob"))
			return null;
		String str = sp.getString("CurrentJob", "");
		if (TextUtils.isEmpty(str))
			return null;
		
		try {
			AcceptedJob job = new AcceptedJob();
			JSONObject jObj = new JSONObject(str);
			job.id = jObj.getInt("id");
			job.bookingId = jObj.getString("bookingId");
			job.name = jObj.getString("name");
			job.pickup = jObj.getString("pickup");
			job.dropoff = jObj.getString("dropoff");
			job.telephone = jObj.getString("telephone");
			job.bookingTime = jObj.getString("bookingTime");
			job.jobId = jObj.getString("jobId");
			job.note = jObj.getString("note");
			job.customerPickupLat = jObj.getString("customerPickupLat");
			job.customerPickupLon = jObj.getString("customerPickupLon");
			job.customerDropoffLat = jObj.getString("customerDropoffLat");
			job.customerDropoffLon = jObj.getString("customerDropoffLon");
			job.nop = jObj.getString("nop");
			job.jobRef = jObj.getString("jobRef");
			job.postCode1 = jObj.getString("postCode1");
			job.postCode2 = jObj.getString("postCode2");
			job.houseNumber1 = jObj.getString("houseNumber1");
			job.houseNumber2 = jObj.getString("houseNumber2");
			job.pickupZoneId = jObj.getString("pickupZoneId");
			job.dropoffZoneId = jObj.getString("dropoffZoneId");
			job.farePrice = jObj.getString("farePrice");
			job.custType = jObj.getString("custType");
			job.status = jObj.getString("status");
			try {
				job.sentby = jObj.getString("sentby");
			} catch (Exception e) {
				job.sentby = OiDriverApp.MANAGER_JID;
			}
			
			return job;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean updateJobStatusByJobId(String jobId, String status)
	{
		AcceptedJob job = getActiveJob();
		if (job == null)
		{
			Log.e(OiDriverApp.TAG, "NO ACTIVE JOB FOUND FOR ID = " + jobId);
			return false;
		}
		if (!job.getJobId().equalsIgnoreCase(jobId))
		{
			Log.e(OiDriverApp.TAG, String.format("ACTIVE JOB(%s) IS NOT THE ONE THAT REQUESTED UPDATE(%s)", job.getJobId(), jobId));
			return false;
		}
		job.status = status;
		return job.insertOrUpdateJob();
	}
	
	public static boolean deleteJobByJobId(String jobId)
	{
		AcceptedJob job = getActiveJob();
		if (job == null)
		{
			Log.e(OiDriverApp.TAG, "NO ACTIVE JOB FOUND FOR JOB ID = " + jobId);
			return false;
		}
		if (!job.getJobId().equalsIgnoreCase(jobId))
		{
			Log.e(OiDriverApp.TAG, String.format("ACTIVE JOB(%s) IS NOT THE ONE THAT REQUESTED UPDATE(%s)", job.getJobId(), jobId));
			return false;
		}
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
		return sp.edit().remove("CurrentJob").commit();
	}	
	
	public static boolean hasActiveJob()
	{
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
		String str = sp.getString("CurrentJob", "");
		return !TextUtils.isEmpty(str);
	}

    public static boolean resetActiveJob()
    {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(OiDriverApp.getAppContext());
        return sp.edit().remove("CurrentJob").commit();
    }
}
