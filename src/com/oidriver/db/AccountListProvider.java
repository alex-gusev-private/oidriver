/*
 * Copyright (c) 2011, Lauren Darcey and Shane Conder
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are 
 * permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of 
 *   conditions and the following disclaimer.
 *   
 * * Redistributions in binary form must reproduce the above copyright notice, this list 
 *   of conditions and the following disclaimer in the documentation and/or other 
 *   materials provided with the distribution.
 *   
 * * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used
 *   to endorse or promote products derived from this software without specific prior 
 *   written permission.
 *   
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF 
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * <ORGANIZATION> = Mamlambo
 */
package com.oidriver.db;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.oidriver.OiDriverApp;
import com.oidriver.service.OiDriverService;

public class AccountListProvider extends ContentProvider {

    //private TutListDatabase mDB;

    private static final String AUTHORITY = "com.oidriver.db.AccountListProvider";
    public static final int JOBS_DONE = 100;
    public static final int JOB_DONE_ID = 110;

    private static final String JOBS_BASE_PATH = "jobs";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + JOBS_BASE_PATH);

    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/jobs_done";
    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/jobs_done";

    private static final UriMatcher sURIMatcher = new UriMatcher(
            UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI(AUTHORITY, JOBS_BASE_PATH, JOBS_DONE);
        sURIMatcher.addURI(AUTHORITY, JOBS_BASE_PATH + "/#", JOB_DONE_ID);
    }

    @Override
    public boolean onCreate() {
        ///mDB = new TutListDatabase(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables("jobs_done");
        String params = uri.getLastPathSegment();

        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
        case JOB_DONE_ID:
            queryBuilder.appendWhere("jobid = " + params);
            break;
        case JOBS_DONE:
            break;
        default:
            throw new IllegalArgumentException("Unknown URI");
        }

        Cursor cursor = queryBuilder.query(OiDriverService.dbHelper.getDB(),
                projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        
        return cursor;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        OiDriverService.dbHelper.open();
        int rowsAffected = 0;
		try {
	        SQLiteDatabase sqlDB = OiDriverService.dbHelper.getDB();
			rowsAffected = 0;
			switch (uriType) {
			case JOBS_DONE:
			    rowsAffected = sqlDB.delete("jobs_done",
			            selection, selectionArgs);
			    break;
			case JOB_DONE_ID:
			    String id = uri.getLastPathSegment();
			    if (TextUtils.isEmpty(selection)) {
			        rowsAffected = sqlDB.delete("jobs_done",
			                "jobid ="  + id, null);
			    } else {
			        rowsAffected = sqlDB.delete("jobs_done",
			                selection + " and jobid = " + id,
			                selectionArgs);
			    }
			    break;
			default:
			    throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
			}
			getContext().getContentResolver().notifyChange(uri, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			OiDriverService.dbHelper.close();
		}
        return rowsAffected;
    }

    @Override
    public String getType(Uri uri) {
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
        case JOBS_DONE:
            return CONTENT_TYPE;
        case JOB_DONE_ID:
            return CONTENT_ITEM_TYPE;
        default:
            return null;
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        if (uriType != JOBS_DONE) {
            throw new IllegalArgumentException("Invalid URI for insert");
        }
        OiDriverService.dbHelper.open();
        try {
            SQLiteDatabase sqlDB = OiDriverService.dbHelper.getDB();
            long newID = sqlDB.insertOrThrow("jobs_done",
                    null, values);
            if (newID > 0) {
                Uri newUri = ContentUris.withAppendedId(uri, newID);
                getContext().getContentResolver().notifyChange(uri, null);
                return newUri;
            } else {
                throw new SQLException("Failed to insert row into " + uri);
            }
        } catch (SQLiteConstraintException e) {
            Log.i(OiDriverApp.TAG, "Ignoring constraint failure.");
        }
        finally {
        	OiDriverService.dbHelper.close();
        }
        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        int rowsAffected = 0;
        OiDriverService.dbHelper.open();
		try {
			SQLiteDatabase sqlDB = OiDriverService.dbHelper.getDB();

			switch (uriType) {
			case JOB_DONE_ID:
			    String id = uri.getLastPathSegment();
			    StringBuilder modSelection = new StringBuilder("jobid = " + id);

			    if (!TextUtils.isEmpty(selection)) {
			        modSelection.append(" AND " + selection);
			    }

			    rowsAffected = sqlDB.update("jobs_done",
			            values, modSelection.toString(), null);
			    break;
			case JOBS_DONE:
			    rowsAffected = sqlDB.update("jobs_done",
			            values, selection, selectionArgs);
			    break;
			default:
			    throw new IllegalArgumentException("Unknown or Invalid URI");
			}
			getContext().getContentResolver().notifyChange(uri, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
	        OiDriverService.dbHelper.close();
		}
        return rowsAffected;
    }
}
