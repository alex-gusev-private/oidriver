package com.oidriver.db;

public class JobDoneInfo {

	String jobId;
	String farePrice;
	String paymentMethod;
	
	public JobDoneInfo() {
		jobId = "";
		farePrice = "";
		paymentMethod = "cash";
	}

	public JobDoneInfo(String id, String fare) {
		jobId = id;
		farePrice = fare;
		paymentMethod = "cash";
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getFarePrice() {
		return farePrice;
	}

	public void setFarePrice(String farePrice) {
		this.farePrice = farePrice;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
}
