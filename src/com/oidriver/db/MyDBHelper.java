package com.oidriver.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDBHelper  extends SQLiteOpenHelper{

	
	
	private static final String DATABASE_NAME = "oidriver.db";

	private static final int DATABASE_VERSION = 13;

	private static final String JOBS = "create table if not exists jobs (" + 
			"_id integer primary key autoincrement, " +
			"booking_id text not null, " +
			"name text not null default '', " +
			"pickup text not null default '', " + 
			"dropoff text not null default '', " + 
			"telephone text not null default '', " + 
			"timeofbooking text not null default '', " + 
			"jobid text UNIQUE, " + 
			"notes TEXT not null default '', " +
			"customerpickuplat text not null default '0.0', " + 
			"customerpickuplon text not null default '0.0', " + 
			"customerdropofflat text not null default '0.0', " + 
			"customerdropofflon text not null default '0.0', " +
			"passengers text not null default '0', " + 
			"jobref text not null default '', " + 
			"postcode1 text not null default '', " +
			"postcode2 text not null default '', " + 
			"housenumber1 text not null default '0', " + 
			"housenumber2 text not null default '0', " + 
			"pickupzoneid text not null default '0', " + 
			"dropoffzoneid text not null default '0', " +
			"fareprice text not null default '0.0', " + 
			"custtype text not null default '', " + 
			"status text not null default 'ACCEPTED'," +
			"sentby text);";
	private static final String JOBS_INDEX = "CREATE UNIQUE INDEX IF NOT EXISTS jobs_idx ON jobs(booking_id)";

	private static final String BIDS =  "create table if not exists bids (_id integer primary key autoincrement, " + 
										    "booking_id text not null, " +
											"passenger_name text not null default '', " +
											"zone_id text not null default '0', " +
											"job_id text not null default '0', " +
											"booking_time text not null default '', " +
											"status text not null default 'NEW', " +
											"pickup text not null default '', " +
											"dropoff text not null default '', " +
											"pickup_lat text not null default '0.0', " +
											"pickup_lon text not null default '0,0', " +
											"dropoff_lat text not null default '0.0', " +
											"dropoff_lon text not null default '0.0', " +
											"notes text not null default '', " +
											"fare text not null default '', " +
											"ratings text not null default '', " +
											"vehicle_type text not null default '', " +
											"booking_type int not null default 0," +
											"sent_bid int not null default 0," +
											"sentby text)";
	private static final String BIDS_INDEX = "CREATE UNIQUE INDEX IF NOT EXISTS bids_idx ON bids(booking_id)";
	
//	private static final String BIDS_ID =  "create table if not exists bids_id (_id integer primary key autoincrement, booking_id text not null)";
//	private static final String BIDS_ID_INDEX = "CREATE UNIQUE INDEX IF NOT EXISTS bids_id_idx ON bids_id(booking_id)";

	private static final String JOBS_DONE = "create table if not exists jobs_done (" + 
			"_id integer primary key autoincrement, " +
			"jobid text UNIQUE, " + 
			"fareprice text not null default '0.0'," +
			"payment text not null default 'cash'," +
			"jobdate text not null default CURRENT_TIMESTAMP)";
	private static final String JOBS_DONE_INDEX = "CREATE UNIQUE INDEX IF NOT EXISTS jobs_done_idx ON jobs_done(jobid)";
	
	public MyDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(JOBS);
		db.execSQL(JOBS_INDEX);
		db.execSQL(BIDS);
		db.execSQL(BIDS_INDEX);
		db.execSQL(JOBS_DONE);
		db.execSQL(JOBS_DONE_INDEX);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS current_job");
		db.execSQL("DROP TABLE IF EXISTS jobs");
		db.execSQL("DROP INDEX IF EXISTS jobs_idx");
		db.execSQL("DROP TABLE IF EXISTS bids");
		db.execSQL("DROP INDEX IF EXISTS bids_idx");
		db.execSQL("DROP TABLE IF EXISTS jobs_done");
		db.execSQL("DROP INDEX IF EXISTS jobs_done_idx");
		onCreate(db);
	}
}