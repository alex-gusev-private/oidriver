package com.oidriver.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.oidriver.AcceptedJob;
import com.oidriver.OiDriverApp;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Semaphore;

public class MyDBAdapter {
	
	public static final String JOBS_TABLE = "jobs";
	public static final String BIDS_TABLE = "bids";
	public static final String BIDS_ID_TABLE = "bids_id";
	public static final String JOBS_DONE_TABLE = "jobs_done";
	
	private SQLiteDatabase database;
	private MyDBHelper dbHelper;

	public MyDBAdapter() {
		
	}
	public static final Semaphore databaselLockSemaphore = new Semaphore(1);
	public MyDBAdapter open() throws SQLException {
		
		try {
			databaselLockSemaphore.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
			return null;
		}
		Log.i(OiDriverApp.TAG, "Acquired DB Semaphore");

		if(database == null || !database.isOpen())
		{
			dbHelper = new MyDBHelper(OiDriverApp.getAppContext());
			database = dbHelper.getWritableDatabase();
			
		}
		return this;
	}
	
	public boolean tryOpen() throws SQLException {
		
		try {
			if (!databaselLockSemaphore.tryAcquire())
				return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		Log.i(OiDriverApp.TAG, "Acquired DB Semaphore");

		if(database == null || !database.isOpen())
		{
			dbHelper = new MyDBHelper(OiDriverApp.getAppContext());
			database = dbHelper.getWritableDatabase();
			
		}
		return true;
	}

	public MyDBHelper getHelper()
	{
		return dbHelper;
	}

	public SQLiteDatabase getDB()
	{
		return database;
	}

	public Boolean isDBOpen()
	{
		try
		{
			return database.isOpen();
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Checking DB state");
			return false;
		}
	}
	
	public void close() {
		try
		{
			dbHelper.close();
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Closing DB");
		}
		databaselLockSemaphore.release();
		Log.i(OiDriverApp.TAG, "Released DB Semaphore");
	}
	
	private ContentValues createContentValues(Map<String, Object> params) {
		ContentValues values = new ContentValues();
		for(Entry<String, Object> entry : params.entrySet())
		{
			values.put(entry.getKey(), entry.getValue()+"");
		}
		return values;
	}

	////////////////////////////////// JOB ////////////////////////////////// 
	public boolean insertJob(Map<String, Object> params)
	{
		try
		{
			open();
			ContentValues initialValues = createContentValues(params);
			long idForNewRow = database.replace(JOBS_TABLE, null, initialValues);
			if (idForNewRow < 0)
			{
				Log.e(OiDriverApp.TAG, "Failed to insert job");
				return false;
			}
			
			return true;
			
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Inserting CurrentJob");
			e.printStackTrace();
			return false;
		}
		finally
		{
			close();
		}
	}

	public AcceptedJob getJobFromDB(String bookingId)
	{
		Cursor currentJobCursor = null;
		try
		{
			open();
			currentJobCursor =  database.query(JOBS_TABLE, null, 
					"booking_id = '" + bookingId + "'", null, null,	null, null);

			AcceptedJob currentJob = null;
			if (currentJobCursor.moveToFirst())
				currentJob = getAcceptedJobFromCursor(currentJobCursor);
			return currentJob;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting Job by ID");
			e.printStackTrace();
			return null;
		}		
		finally
		{
			if(currentJobCursor != null && !currentJobCursor.isClosed())
			{
				currentJobCursor.close();
			}
			close();
		}
	}
	
	public AcceptedJob getJobByJobId(String jobId)
	{
		Cursor currentJobCursor = null;
		try
		{
			open();
			currentJobCursor =  database.query(JOBS_TABLE, null, 
					"jobid = '" + jobId + "'", null, null,	null, null);

			AcceptedJob currentJob = null;
			if (currentJobCursor.moveToFirst())
				currentJob = getAcceptedJobFromCursor(currentJobCursor);
			return currentJob;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting Job by ID");
			e.printStackTrace();
			return null;
		}		
		finally
		{
			if(currentJobCursor != null && !currentJobCursor.isClosed())
			{
				currentJobCursor.close();
			}
			close();
		}
	}
	
	public ArrayList<AcceptedJob> getAllActiveJobs()
	{
		Cursor currentJobCursor = null;
		try
		{
			open();
			currentJobCursor =  database.query(JOBS_TABLE, null, 
					"status NOT IN ('EOJ', 'NO_JOB', 'REJECT_JOB')", 
					null, null,	null, null);
			
			ArrayList<AcceptedJob> list = new ArrayList<AcceptedJob>();
						
			while (currentJobCursor.moveToNext())
			{
				AcceptedJob currentJob = getAcceptedJobFromCursor(currentJobCursor);
				list.add(currentJob);
			}
			
			return list;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting All Actve Jobs");
			e.printStackTrace();
			return null;
		}		
		finally
		{
			if(currentJobCursor != null && !currentJobCursor.isClosed())
			{
				currentJobCursor.close();
			}
			close();
		}
	}
	
	public int getActiveJobsCount()
	{
		Cursor currentJobCursor = null;
		try
		{
			open();
			currentJobCursor =  database.rawQuery("select count(_id) from jobs WHERE status NOT IN ('EOJ', 'NO_JOB', 'REJECT_JOB')", null);
			if (currentJobCursor.moveToNext())
			{
				Integer count = currentJobCursor.getInt(0);			
				return count;
			}
			else
				return 0;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting CurrentJob count");
			e.printStackTrace();
			return 0;
		}		
		finally
		{
			if(currentJobCursor != null && !currentJobCursor.isClosed())
			{
				currentJobCursor.close();
			}
			close();
		}
	}
	
//	public AcceptedJob getCurrentJob()
//	{
//		Cursor currentJobCursor = null;
//		try
//		{
//			open();
//			currentJobCursor =  database.query(JOBS_TABLE, null, "status NOT IN ('ACCEPT_JOB')", null, null,	null, null);
//			
//			AcceptedJob currentJob = null;
//			if (currentJobCursor.moveToFirst())
//				currentJob = getAcceptedJobFromCursor(currentJobCursor);
//			return currentJob;
//		}
//		catch(Exception e)
//		{
//			Log.e(OiDriverApp.TAG, "ERROR : Getting Currently Active Job");
//			e.printStackTrace();
//			return null;
//		}		
//		finally
//		{
//			if(currentJobCursor != null && !currentJobCursor.isClosed())
//			{
//				currentJobCursor.close();
//			}
//			close();
//		}
//	}

//	public Boolean updateJobStatus(Map<String, Object> params)
//	{
//		try
//		{
//			open();
//			ContentValues updateValues = createContentValues(params);
//			
//			return database.update(JOBS_TABLE, updateValues, null, null) > 0;
//		}
//		catch(Exception e)
//		{
//			Log.e(OiDriverApp.TAG, "ERROR : Updating Job Status");
//			e.printStackTrace();
//			return false;
//		}	
//		finally
//		{
//			close();
//		}
//	}
//
//	public Boolean updateJobStatus(Map<String, Object> params, String jobId)
//	{
//		try
//		{
//			open();
//			ContentValues updateValues = createContentValues(params);
//			
//			return database.update(JOBS_TABLE, updateValues, "jobid = '" + jobId + "'", null) > 0;
//		}
//		catch(Exception e)
//		{
//			Log.e(OiDriverApp.TAG, "ERROR : Updating Job Status");
//			e.printStackTrace();
//			return false;
//		}	
//		finally
//		{
//			close();
//		}
//	}

	public boolean deleteJobs() 
	{
		try
		{
			open();
			return database.delete(JOBS_TABLE, null, null) > 0;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Deleting ALL Jobs");
			e.printStackTrace();
			return false;
		}
		finally
		{
			close();
		}
	}
	
	public boolean deleteJob(String bookingId) 
	{
		try
		{
			open();
			return database.delete(JOBS_TABLE, "booking_id = '" + bookingId + "'", null) > 0;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Deleting Job");
			e.printStackTrace();
			return false;
		}
		finally
		{
			close();
		}
	}

	public boolean deleteJobByJobId(String jobId) 
	{
		try
		{
			open();
			return database.delete(JOBS_TABLE, "jobId = '" + jobId + "'", null) > 0;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Deleting Job");
			e.printStackTrace();
			return false;
		}
		finally
		{
			close();
		}
	}

	public AcceptedJob popJob(String bookingId) 
	{
		Cursor currentJobCursor = null;
		try
		{
			open();
			currentJobCursor =  database.query(JOBS_TABLE, null, 
					"booking_id = '" + bookingId + "'", null, null,	null, null);

			AcceptedJob currentJob = null;
			if (currentJobCursor.moveToFirst())
				currentJob = getAcceptedJobFromCursor(currentJobCursor);
			if (currentJob != null)
			{
				int recs = database.delete(JOBS_TABLE, "booking_id = '" + bookingId + "'", null);
				if (recs <= 0)
				{
					Log.e(OiDriverApp.TAG, "Cannot delete active job record with ID = " + bookingId);
				}
			}
			return currentJob;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting Job by ID");
			e.printStackTrace();
			return null;
		}		
		finally
		{
			if(currentJobCursor != null && !currentJobCursor.isClosed())
			{
				currentJobCursor.close();
			}
			close();
		}
	}
/*
	////////////////////////////////// BIDS //////////////////////////////////
	public boolean insertBid(Booking booking)
	{
		Cursor cursor = null;
		try
		{
			open();
			ContentValues cv = new ContentValues();
			cv.put("booking_id", booking.getBookingId());
			cv.put("passenger_name", booking.getPassengerName());
			cv.put("zone_id", booking.getZoneId());
			cv.put("job_id", booking.getJobId());
			cv.put("booking_time", booking.getBookingTime());
			cv.put("status", booking.getStatus());
			cv.put("pickup", booking.getPickupAddress());
			cv.put("dropoff", booking.getDropoffAddress());
			cv.put("pickup_lat", booking.getPickupLat());
			cv.put("pickup_lon", booking.getPickupLon());
			cv.put("dropoff_lat", booking.getDropoffLat());
			cv.put("dropoff_lon", booking.getDropoffLon());
			cv.put("notes", booking.getNotes());
			cv.put("ratings", booking.getRatings());
			cv.put("fare", booking.getFare());
			cv.put("vehicle_type", booking.getVehicleType());
			cv.put("booking_type", booking.getBookingType());
			cv.put("sent_bid", booking.isBidSent() ? 1 : 0);
			cv.put("sentby", booking.getSentBy());
			
			long idForNewRow = database.replace(BIDS_TABLE, null, cv);
			return idForNewRow > -1;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Inserting Booking ID = " + booking.getBookingId());
			e.printStackTrace();
			return false;
		}		
		finally
		{
			if(cursor != null && !cursor.isClosed())
			{
				cursor.close();
			}
			close();
		}
	}

	public boolean bidExists(String bookingId)
	{
		Cursor cursor = null;
		try
		{
			open();
			cursor =  database.query(BIDS_TABLE, new String[] {"booking_id"}, "booking_id = '" + bookingId + "'", null,	null, null, null);
			
			Integer count = cursor.getCount();			
			if(count == 0)
			{
				return false;
			}			
			if (!cursor.moveToFirst())
			{
				return false;
			}
			return true;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting Booking ID = " + bookingId);
			e.printStackTrace();
			return false;
		}		
		finally
		{
			if(cursor != null && !cursor.isClosed())
			{
				cursor.close();
			}
			close();
		}
	}
	
	public Booking getBidByJobId(String bookingId)
	{
		Cursor cursor = null;
		try
		{
			open();
			cursor =  database.query(BIDS_TABLE, null, "booking_id = '" + bookingId + "'", null,	null, null, null);
			
			Integer count = cursor.getCount();			
			if(count == 0)
			{
				return null;
			}			
			if (!cursor.moveToFirst())
			{
				return null;
			}
			Booking booking = getBookingFromCursor(cursor);
			return booking;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting Booking ID = " + bookingId);
			e.printStackTrace();
			return null;
		}		
		finally
		{
			if(cursor != null && !cursor.isClosed())
			{
				cursor.close();
			}
			close();
		}
	}

	public Booking getBidByJobId(String jobId) 
	{
		Cursor cursor = null;
		try
		{
			open();
			cursor =  database.query(BIDS_TABLE, null, "job_id = '" + jobId + "'", null, null, null, null);
			
			Integer count = cursor.getCount();			
			if(count == 0)
			{
				return null;
			}			
			if (!cursor.moveToFirst())
			{
				return null;
			}
			Booking booking = getBookingFromCursor(cursor);
			return booking;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting Booking for jobID = " + jobId);
			e.printStackTrace();
			return null;
		}		
		finally
		{
			if(cursor != null && !cursor.isClosed())
			{
				cursor.close();
			}
			close();
		}
	}
	
	public ArrayList<Booking> getBidsNow()
	{
		Cursor cursor = null;
		try
		{
			open();
			cursor =  database.query(BIDS_TABLE, null, "booking_type = 0", null,	null, null, null);
			
			Integer count = cursor.getCount();			
			if(count == 0)
			{
				return null;
			}
			ArrayList<Booking> arr = new ArrayList<Booking>();
			while (cursor.moveToNext())
			{
				Booking booking = getBookingFromCursor(cursor);
				arr.add(booking);
			}
			return arr;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting Bookings NOW");
			e.printStackTrace();
			return null;
		}		
		finally
		{
			if(cursor != null && !cursor.isClosed())
			{
				cursor.close();
			}
			close();
		}
	}
	
	public ArrayList<Booking> getBidsAdv()
	{
		Cursor cursor = null;
		try
		{
			open();
			cursor =  database.query(BIDS_TABLE, null, "booking_type = 1", null,	null, null, null);
			
			Integer count = cursor.getCount();			
			if(count == 0)
			{
				return null;
			}
			ArrayList<Booking> arr = new ArrayList<Booking>();
			while (cursor.moveToNext())
			{
				Booking booking = getBookingFromCursor(cursor);
				arr.add(booking);
			}
			return arr;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting Bookings ADV");
			e.printStackTrace();
			return null;
		}		
		finally
		{
			if(cursor != null && !cursor.isClosed())
			{
				cursor.close();
			}
			close();
		}
	}

	public Boolean deleteBidByBookingId(String bookingId)
	{
		try
		{
			open();
			return database.delete(BIDS_TABLE, "booking_id = '" + bookingId + "'", null) > 0;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Deleting Booking ID = " + bookingId);
			e.printStackTrace();
			return false;
		}
		finally
		{
			close();
		}
	}
	
	public Boolean deleteBidByJobId(String jobId) 
	{
		try
		{
			open();
			return database.delete(BIDS_TABLE, "job_id = '" + jobId + "'", null) > 0;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Deleting Booking for JOB ID = " + jobId);
			e.printStackTrace();
			return false;
		}
		finally
		{
			close();
		}
	}
	
	public boolean deleteAllBids()
	{
		try
		{
			open();
			return database.delete(BIDS_TABLE, null, null) > 0;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Deleting All bids");
			e.printStackTrace();
			return false;
		}
		finally
		{
			close();
		}
	}

	public int deleteAllBids(String where)
	{
		try
		{
			open();
			return database.delete(BIDS_TABLE, where, null);
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Deleting All bids");
			e.printStackTrace();
			return 0;
		}
		finally
		{
			close();
		}
	}

	public int getBidsCount()
	{
		Cursor cursor = null;
		try
		{
			open();
			cursor =  database.rawQuery("select _id from bids", null);
			Integer count = cursor.getCount();
			Log.w("BIDS COUNT", "" + count);
			return count;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting Bids count");
			e.printStackTrace();
			return 0;
		}		
		finally
		{
			if(cursor != null && !cursor.isClosed())
			{
				cursor.close();
			}
			close();
		}
	}
	
	public int getBidsNOWCount()
	{
		Cursor cursor = null;
		try
		{
			open();
			cursor =  database.rawQuery("select _id from bids where booking_type = 0", null);
			Integer count = cursor.getCount();			
			return count;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting Bids NOW count");
			e.printStackTrace();
			return 0;
		}		
		finally
		{
			if(cursor != null && !cursor.isClosed())
			{
				cursor.close();
			}
			close();
		}
	}
	
	public int getBidsADVCount()
	{
		Cursor cursor = null;
		try
		{
			open();
			cursor =  database.rawQuery("select _id from bids where booking_type = 1", null);
			Integer count = cursor.getCount();			
			return count;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting Bids ADV count");
			e.printStackTrace();
			return 0;
		}		
		finally
		{
			if(cursor != null && !cursor.isClosed())
			{
				cursor.close();
			}
			close();
		}
	}
	
	public Cursor getBidsNowCursor()
	{
//		Cursor cursor = null;
		try
		{
			//open();
			return database.query(BIDS_TABLE, null, "booking_type = 0", null,	null, null, null);
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting Bookings NOW");
			e.printStackTrace();
			return null;
		}		
		finally
		{
//			if(cursor != null && !cursor.isClosed())
//			{
//				cursor.close();
//			}
			//close();
		}
	}
	
	public Cursor getBidsAdvCursor()
	{
		//Cursor cursor = null;
		try
		{
			//open();
			return database.query(BIDS_TABLE, null, "booking_type = 1", null,	null, null, null);
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting Bookings NOW");
			e.printStackTrace();
			return null;
		}		
		finally
		{
//			if(cursor != null && !cursor.isClosed())
//			{
//				cursor.close();
//			}
			//close();
		}
	}
	
	public Cursor getJobsCursor()
	{
		Cursor currentJobCursor = null;
		try
		{
			//open();
			currentJobCursor =  database.query(JOBS_TABLE, new String[] { "_id", "name, pickup," +
					"dropoff, telephone, timeofbooking, jobid, notes" +
					",customerpickuplat, customerpickuplon, customerdropofflat, customerdropofflon,"+
					"passengers, jobref, postcode1,"+
					"postcode2, housenumber1, housenumber2, pickupzoneid, dropoffzoneid,"+
					"fareprice, custtype, status", "managerjid"}, "status NOT IN ('EOJ', 'NO_JOB', 'REJECT_JOB')", null, null,	null, null);
			
			return currentJobCursor;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting Jobs Cursor");
			e.printStackTrace();
			return null;
		}		
		finally
		{
//			if(currentJobCursor != null && !currentJobCursor.isClosed())
//			{
//				currentJobCursor.close();
//			}
			//close();
		}
	}
	
	public ArrayList<String> getSentBidsIds()
	{
		Cursor cursor = null;
		try
		{
			open();
			cursor =  database.query(BIDS_TABLE, null, "sent_bid = 1", null,	null, null, null);
			
			Integer count = cursor.getCount();			
			if(count == 0)
			{
				return null;
			}
			ArrayList<String> arr = new ArrayList<String>();
			while (cursor.moveToNext())
			{
				arr.add(cursor.getString(cursor.getColumnIndex("booking_id")));
			}
			return arr;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting Bookings NOW");
			e.printStackTrace();
			return null;
		}		
		finally
		{
			if(cursor != null && !cursor.isClosed())
			{
				cursor.close();
			}
			close();
		}
	}
	
	////////////////////////////////// HELPER FUNCTIONS //////////////////////////////////
	private Booking getBookingFromCursor(Cursor cursor)
	{
		Booking booking = new Booking();
		booking.setBookingId(cursor.getString(cursor.getColumnIndex("booking_id")));
		booking.setPassengerName(cursor.getString(cursor.getColumnIndex("passenger_name")));
		booking.setZoneId(cursor.getString(cursor.getColumnIndex("zone_id")));
		booking.setJobId(cursor.getString(cursor.getColumnIndex("job_id")));
		booking.setBookingTime(cursor.getString(cursor.getColumnIndex("booking_time")));
		booking.setStatus(cursor.getString(cursor.getColumnIndex("status")));
		booking.setPickupAddress(cursor.getString(cursor.getColumnIndex("pickup")));
		booking.setDropoffAddress(cursor.getString(cursor.getColumnIndex("dropoff")));
		booking.setPickupLat(cursor.getString(cursor.getColumnIndex("pickup_lat")));
		booking.setPickupLon(cursor.getString(cursor.getColumnIndex("pickup_lon")));
		booking.setDropoffLat(cursor.getString(cursor.getColumnIndex("dropoff_lat")));
		booking.setDropoffLon(cursor.getString(cursor.getColumnIndex("dropoff_lon")));
		booking.setNotes(cursor.getString(cursor.getColumnIndex("notes")));
		booking.setFare(cursor.getString(cursor.getColumnIndex("fare")));
		booking.setRatings(cursor.getString(cursor.getColumnIndex("ratings")));
		booking.setVehicleType(cursor.getString(cursor.getColumnIndex("vehicle_type")));
		booking.setBookingType(cursor.getInt(cursor.getColumnIndex("booking_type")));
		booking.setBidSent(cursor.getInt(cursor.getColumnIndex("sent_bid")) > 0);
		booking.setSentBy(cursor.getString(cursor.getColumnIndex("sentby")));
		
		return booking;
	}
*/
	private AcceptedJob getAcceptedJobFromCursor(Cursor currentJobCursor)
	{
		AcceptedJob currentJob = new AcceptedJob();
		
		currentJob.setBookingId(currentJobCursor.getString(currentJobCursor.getColumnIndex("booking_id")));
		currentJob.setName(currentJobCursor.getString(currentJobCursor.getColumnIndex("name")));
		currentJob.setPickup(currentJobCursor.getString(currentJobCursor.getColumnIndex("pickup")));
		currentJob.setDropoff(currentJobCursor.getString(currentJobCursor.getColumnIndex("dropoff")));
		currentJob.setTelephone(currentJobCursor.getString(currentJobCursor.getColumnIndex("telephone")));
		currentJob.setBookingTime(currentJobCursor.getString(currentJobCursor.getColumnIndex("timeofbooking")));
		currentJob.setJobId(currentJobCursor.getString(currentJobCursor.getColumnIndex("jobid")));
		currentJob.setNote(currentJobCursor.getString(currentJobCursor.getColumnIndex("notes")));
		currentJob.setCustomerPickupLat(currentJobCursor.getString(currentJobCursor.getColumnIndex("customerpickuplat")));
		currentJob.setCustomerPickupLon(currentJobCursor.getString(currentJobCursor.getColumnIndex("customerpickuplon")));
		currentJob.setCustomerDropoffLat(currentJobCursor.getString(currentJobCursor.getColumnIndex("customerdropofflat")));
		currentJob.setCustomerDropoffLon(currentJobCursor.getString(currentJobCursor.getColumnIndex("customerdropofflon")));
		currentJob.setNop(currentJobCursor.getString(currentJobCursor.getColumnIndex("passengers")));
		currentJob.setJobRef(currentJobCursor.getString(currentJobCursor.getColumnIndex("jobref")));
		currentJob.setPostCode1(currentJobCursor.getString(currentJobCursor.getColumnIndex("postcode1")));
		currentJob.setPostCode2(currentJobCursor.getString(currentJobCursor.getColumnIndex("postcode2")));
		currentJob.setHouseNumber1(currentJobCursor.getString(currentJobCursor.getColumnIndex("housenumber1")));
		currentJob.setHouseNumber2(currentJobCursor.getString(currentJobCursor.getColumnIndex("housenumber2")));
		currentJob.setPickupZoneId(currentJobCursor.getString(currentJobCursor.getColumnIndex("pickupzoneid")));
		currentJob.setDropoffZoneId(currentJobCursor.getString(currentJobCursor.getColumnIndex("dropoffzoneid")));
		currentJob.setFarePrice(currentJobCursor.getString(currentJobCursor.getColumnIndex("fareprice")));
		currentJob.setCustType(currentJobCursor.getString(currentJobCursor.getColumnIndex("custtype")));
		currentJob.setStatus(currentJobCursor.getString(currentJobCursor.getColumnIndex("status")));
		currentJob.setSentBy(currentJobCursor.getString(currentJobCursor.getColumnIndex("sentby")));
		
		return currentJob;
	}
	
	public ArrayList<JobDoneInfo> getDoneJobs()
	{
		Cursor cursor = null;
		try
		{
			open();
			cursor =  database.query(JOBS_DONE_TABLE, null, 
					null, null, null, null, null);
			
			ArrayList<JobDoneInfo> list = new ArrayList<JobDoneInfo>();
						
			while (cursor.moveToNext())
			{
				JobDoneInfo job = new JobDoneInfo(cursor.getString(0), cursor.getString(1));
				list.add(job);
			}
			
			return list;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting All Actve Jobs");
			e.printStackTrace();
			return null;
		}		
		finally
		{
			if(cursor != null && !cursor.isClosed())
			{
				cursor.close();
			}
			close();
		}
	}

	public Cursor getDoneJobsCursor()
	{
		Cursor cursor = null;
		try
		{
			open();
			cursor =  database.query(JOBS_DONE_TABLE, null, 
					null, null, null, null, null);			
			return cursor;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Getting All Actve Jobs");
			e.printStackTrace();
			return null;
		}		
		finally
		{
			close();
		}
	}
	
	public boolean insertJobDone(String jobId, String fare, String paymentMethod)
	{
		try
		{
			open();
			ContentValues cv = new ContentValues();
			cv.put("jobid", jobId);
			cv.put("fareprice", fare);
			cv.put("payment", paymentMethod);
			
			long idForNewRow = database.replace(JOBS_DONE_TABLE, null, cv);
			return idForNewRow > -1;
		}
		catch(Exception e)
		{
			Log.e(OiDriverApp.TAG, "ERROR : Inserting Finished Job ID = " + jobId);
			e.printStackTrace();
			return false;
		}		
		finally
		{
			close();
		}
	}
}