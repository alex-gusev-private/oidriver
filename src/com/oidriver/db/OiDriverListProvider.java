package com.oidriver.db;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.oidriver.service.OiDriverService;

public class OiDriverListProvider extends ContentProvider {

    ////////private TutListDatabase OiDriverService.dbHelper.getHeper();

    private static final String AUTHORITY = "com.oidriver.db.OiDriverListProvider";
    public static final int BIDS = 100;
    public static final int BIDS_ID = 101;
    public static final int JOBS = 110;
    public static final int JOBS_ID = 111;

    private static final String OIDRIVER_BASE_PATH = "oidriver";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
            + "/" + OIDRIVER_BASE_PATH);

    public static final String CONTENT_ITEM_TYPE_BID = ContentResolver.CURSOR_ITEM_BASE_TYPE
            + "/oidriver-bid";
    public static final String CONTENT_TYPE_BID = ContentResolver.CURSOR_DIR_BASE_TYPE
            + "/oidriver-bids";
    public static final String CONTENT_ITEM_TYPE_JOB = ContentResolver.CURSOR_ITEM_BASE_TYPE
            + "/oidriver-job";
    public static final String CONTENT_TYPE_JOB = ContentResolver.CURSOR_DIR_BASE_TYPE
            + "/oidriver-jobs";

    private static final UriMatcher sURIMatcher = new UriMatcher(
            UriMatcher.NO_MATCH);
    static {
        sURIMatcher.addURI(AUTHORITY, OIDRIVER_BASE_PATH + "/bids", BIDS);
        sURIMatcher.addURI(AUTHORITY, OIDRIVER_BASE_PATH + "/bids_now/#", BIDS_ID);
        sURIMatcher.addURI(AUTHORITY, OIDRIVER_BASE_PATH + "/jobs", JOBS);
        sURIMatcher.addURI(AUTHORITY, OIDRIVER_BASE_PATH + "/jobs/#", JOBS_ID);
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

    	int uriType = sURIMatcher.match(uri);
        
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(uriType != JOBS ? "bids" : "jobs");

        switch (uriType) {
        case BIDS:
            //queryBuilder.appendWhere("booking_type=0");
            break;
        case BIDS_ID:
        	String id0 = uri.getLastPathSegment();
            //queryBuilder.appendWhere("booking_type=0");
            queryBuilder.appendWhere("bookingId='" + id0 + "'");
            break;
        case JOBS:
            queryBuilder.appendWhere("status NOT IN ('EOJ', 'NO_JOB', 'REJECT_JOB')");
            break;
        case JOBS_ID:
        	String id3 = uri.getLastPathSegment();
            queryBuilder.appendWhere("status NOT IN ('EOJ', 'NO_JOB', 'REJECT_JOB')");
            queryBuilder.appendWhere("jobid='" + id3 + "'");
            break;
        default:
            throw new IllegalArgumentException("Unknown URI");
        }

        Cursor cursor = queryBuilder.query(OiDriverService.dbHelper.getHelper().getReadableDatabase(),
                projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        String id = uri.getLastPathSegment();
        SQLiteDatabase sqlDB = OiDriverService.dbHelper.getHelper().getWritableDatabase();
        int rowsAffected = 0;
        switch (uriType) {
        case BIDS:
        case BIDS_ID:
            rowsAffected = sqlDB.delete(MyDBAdapter.BIDS_TABLE, selection, selectionArgs);
            break;
        case JOBS:
        case JOBS_ID:
        	rowsAffected = sqlDB.delete(MyDBAdapter.JOBS_TABLE, selection, selectionArgs);
            break;
        default:
            throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsAffected;
    }

    @Override
    public String getType(Uri uri) {
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
        case BIDS:
            return CONTENT_TYPE_BID;
        case BIDS_ID:
            return CONTENT_ITEM_TYPE_BID;
        case JOBS:
            return CONTENT_TYPE_JOB;
        case JOBS_ID:
            return CONTENT_ITEM_TYPE_JOB;
        default:
            return null;
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        if (uriType != BIDS && uriType != JOBS) {
            throw new IllegalArgumentException("Invalid URI for insert");
        }
        SQLiteDatabase sqlDB = OiDriverService.dbHelper.getHelper().getWritableDatabase();
        long newID = -1;
        if (uriType == BIDS)
        	newID = sqlDB.replace(MyDBAdapter.BIDS_TABLE, null, values);
        else
        	newID = sqlDB.replace(MyDBAdapter.JOBS_TABLE, null, values);
        if (newID > 0) {
            Uri newUri = ContentUris.withAppendedId(uri, newID);
            getContext().getContentResolver().notifyChange(uri, null);
            return newUri;
        } else {
            throw new SQLException("Failed to insert row into " + uri);
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = OiDriverService.dbHelper.getHelper().getWritableDatabase();

        int rowsAffected;

        switch (uriType) {
        case BIDS_ID:
            String id = uri.getLastPathSegment();
            StringBuilder modSelection = new StringBuilder("bookingId=" + id);

            if (!TextUtils.isEmpty(selection)) {
                modSelection.append(" AND " + selection);
            }

            rowsAffected = sqlDB.update(MyDBAdapter.BIDS_TABLE,
                    values, modSelection.toString(), null);
            break;
        case BIDS:
            rowsAffected = sqlDB.update(MyDBAdapter.BIDS_TABLE,
                    values, selection, selectionArgs);
            break;
        case JOBS_ID:
            String id2 = uri.getLastPathSegment();
            StringBuilder modSelection2 = new StringBuilder("jobid=" + id2);

            if (!TextUtils.isEmpty(selection)) {
                modSelection2.append(" AND " + selection);
            }

            rowsAffected = sqlDB.update(MyDBAdapter.JOBS_TABLE,
                    values, modSelection2.toString(), null);
            break;
        case JOBS:
            rowsAffected = sqlDB.update(MyDBAdapter.JOBS_TABLE,
                    values, selection, selectionArgs);
            break;
        default:
            throw new IllegalArgumentException("Unknown URI");
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsAffected;
    }
}
