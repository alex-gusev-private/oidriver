package com.oidriver;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TabHost;
import android.widget.TabHost.TabContentFactory;

import com.oidriver.fragments.OptionsPage1Fragment;
import com.oidriver.fragments.OptionsPage2Fragment;
import com.oidriver.fragments.OptionsPage3Fragment;

import java.util.HashMap;
import java.util.List;
import java.util.Vector;

/**
 * The <code>TabsViewPagerFragmentActivity</code> class implements the Fragment activity that maintains a TabHost using a ViewPager.
 * @author mwho
 */
public class OptionsTabsActivity extends FragmentActivity implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {

	private TabHost mTabHost;
	private ViewPager mViewPager;
	private HashMap<String, TabInfo> mapTabInfo = new HashMap<String, OptionsTabsActivity.TabInfo>();
	private OptionsPagerAdapter mPagerAdapter;
	/**
	 *
	 * @author mwho
	 * Maintains extrinsic info of a tab's construct
	 */
	private class TabInfo {
		 private String tag;
         private Class<?> clss;
         private Bundle args;
         private Fragment fragment;
         TabInfo(String tag, Class<?> clazz, Bundle args) {
        	 this.tag = tag;
        	 this.clss = clazz;
        	 this.args = args;
         }

	}
	/**
	 * A simple factory that returns dummy views to the Tabhost
	 * @author mwho
	 */
	class TabFactory implements TabContentFactory {

		private final Context mContext;

	    /**
	     * @param context
	     */
	    public TabFactory(Context context) {
	        mContext = context;
	    }

	    /** (non-Javadoc)
	     * @see android.widget.TabHost.TabContentFactory#createTabContent(java.lang.String)
	     */
	    public View createTabContent(String tag) {
	        View v = new View(mContext);
	        v.setMinimumWidth(0);
	        v.setMinimumHeight(0);
	        return v;
	    }

	}
	/** (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);	
		
		// Inflate the layout
		setContentView(R.layout.options);
		// Initialise the TabHost
		this.initialiseTabHost(savedInstanceState);
		if (savedInstanceState != null) {
            mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab")); //set the tab as per the saved state
        }
		// Intialise ViewPager
		this.intialiseViewPager();
	}

	/** (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onSaveInstanceState(android.os.Bundle)
     */
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("tab", mTabHost.getCurrentTabTag()); //save the tab selected
        super.onSaveInstanceState(outState);
    }

    /**
     * Initialise ViewPager
     */
    private void intialiseViewPager() {

        final Configuration systemConfig = getResources().getConfiguration();

		List<Fragment> fragments = new Vector<Fragment>();
		fragments.add(OptionsPage1Fragment.newInstance());
//        if (systemConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
//            fragments.add(OptionsPage3Fragment.newInstance());
        fragments.add(OptionsPage2Fragment.newInstance());

		this.mPagerAdapter  = new OptionsPagerAdapter(super.getSupportFragmentManager(), fragments);
		//
		this.mViewPager = (ViewPager)super.findViewById(R.id.viewpager);
		this.mViewPager.setAdapter(this.mPagerAdapter);
		this.mViewPager.setOnPageChangeListener(this);
    }

	/**
	 * Initialise the Tab Host
	 */
	private void initialiseTabHost(Bundle args) {
        final Configuration systemConfig = getResources().getConfiguration();

		mTabHost = (TabHost)findViewById(android.R.id.tabhost);
        mTabHost.setup();
        TabInfo tabInfo;
//        if (systemConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            OptionsTabsActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Driver Info").setIndicator("Driver Info"), ( tabInfo = new TabInfo("DriverInfo", OptionsPage1Fragment.class, args)));
            this.mapTabInfo.put(tabInfo.tag, tabInfo);
//        }
//        else {
//            OptionsTabsActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Driver Info").setIndicator("Driver Info"), ( tabInfo = new TabInfo("DriverInfo", OptionsPage1Fragment.class, args)));
//            this.mapTabInfo.put(tabInfo.tag, tabInfo);
//            OptionsTabsActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Ranges").setIndicator("Ranges"), ( tabInfo = new TabInfo("Ranges", OptionsPage3Fragment.class, args)));
//            this.mapTabInfo.put(tabInfo.tag, tabInfo);
//        }
        OptionsTabsActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Status").setIndicator("Alarm"), ( tabInfo = new TabInfo("Alarm", OptionsPage2Fragment.class, args)));
        this.mapTabInfo.put(tabInfo.tag, tabInfo);

        mTabHost.setOnTabChangedListener(this);
	}

	/**
	 * Add Tab content to the Tabhost
	 * @param activity
	 * @param tabHost
	 * @param tabSpec
	 * @param tabInfo
	 */
	private static void AddTab(OptionsTabsActivity activity, TabHost tabHost, TabHost.TabSpec tabSpec, TabInfo tabInfo) {
		// Attach a Tab view factory to the spec
		tabSpec.setContent(activity.new TabFactory(activity));
        tabHost.addTab(tabSpec);
	}

	/** (non-Javadoc)
	 * @see android.widget.TabHost.OnTabChangeListener#onTabChanged(java.lang.String)
	 */
	public void onTabChanged(String tag) {
		//TabInfo newTab = this.mapTabInfo.get(tag);
		int pos = this.mTabHost.getCurrentTab();
		this.mViewPager.setCurrentItem(pos);
    }

	/* (non-Javadoc)
	 * @see android.support.v4.view.ViewPager.OnPageChangeListener#onPageScrolled(int, float, int)
	 */
	@Override
	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see android.support.v4.view.ViewPager.OnPageChangeListener#onPageSelected(int)
	 */
	@Override
	public void onPageSelected(int position) {
		// TODO Auto-generated method stub
		this.mTabHost.setCurrentTab(position);
	}

	/* (non-Javadoc)
	 * @see android.support.v4.view.ViewPager.OnPageChangeListener#onPageScrollStateChanged(int)
	 */
	@Override
	public void onPageScrollStateChanged(int state) {
		// TODO Auto-generated method stub

	}
}

