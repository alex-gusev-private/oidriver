package com.oidriver;

public class Constants {
	public static final String XMPP_SERVICE_SANDBOX = "test.prosody.im";	
	public static final String BOOKING_ID_SANDBOX = "manager@test.prosody.im";
	public static final String COMPANY_SANDBOX = "lee";
	
	public static final String XMPP_SERVICE = "goldnblack.xmpp.abmarbarros.com";	
	public static final String XMPP_SERVICE_FMT = "%s.xmpp.abmarbarros.com";	
	public static final String BOOKING_ID = "manager@goldnblack.xmpp.abmarbarros.com";
	public static final String BOOKING_ID_FMT = "manager@%s.xmpp.abmarbarros.com";
	public static final String COMPANY = "goldnblack";
	public static final int MAX_BOOKINGS_COUNT = 20;
	public static final String NAMESPACE_BOOKING_SEARCH = "http://rappidcars.co.uk/protocol/booking#search";

	public static int ALARM_INTERVAL = 15; // in mins
	public static int BOOKING_ASAP_THRESHOLD = 15;
	public static int ZOOM_LEVEL = 15;
	public static double PAGER_BUTTON_SCROLL_FACTOR_JB = 9;
	public static double PAGER_BUTTON_SCROLL_FACTOR_ICS = 6;
	public static double PAGER_BUTTON_SCROLL_FACTOR_GB = 5;
	public static double PAGER_FINGER_SCROLL_FACTOR = 2.5;
	
	public static String SMS_TARGET = "+447765295140";//"+447588366148";
	public static String SMS_TARGET_LOCAL = "07588366148";
	
	public static final int MESSAGE_ERROR = 0x999;
	public static final int MESSAGE_NEW_JOB = 0x1000;
	public static final int MESSAGE_GPS_OFF = 0x1001;
	public static final int MESSAGE_GPS_ON = 0x1002;
	public static final int MESSAGE_CONNECTING = 0x1003;
	public static final int MESSAGE_CONNECTED = 0x1004;
	public static final int MESSAGE_CENTER_MYLOCATION = 0x1005;
	public static final int MESSAGE_SHOW_PROGRESS_SPINNER = 0x1006;
	public static final int MESSAGE_HIDE_PROGRESS_SPINNER = 0x1007;
	
	public static final int BIDS_NOW_LIST_LOADER = 1;
	public static final int BIDS_ADV_LIST_LOADER = 2;
	public static final int JOBS_LIST_LOADER = 3;
	
	//public static final int BIDS_NOW_PAGE = 1;
	//public static final int BIDS_ADV_PAGE = 2;
	public static final int BIDS_PAGE = 1;
	public static final int JOBS_PAGE = 2;

	public static final String FRAGMENT_TAG_BOOKINGS = "com.touchnote.android.FRAGMENT_TAG_BOOKINGS";
	
	public static final int LOGIN_REQUEST_CODE = 0x1000;
	public static final int OPTIONS_REQUEST_CODE = 0x1001;
	
	public static final int OIDRIVER_XMPP_PACKET_REPLY_TIMEOUT = 60000;

	public static final String DISPATCHER_STATUS_ACCEPTED 	= "ACCEPTED";
	public static final String DISPATCHER_STATUS_DOW 		= "DRIVER ON WAY";
	public static final String DISPATCHER_STATUS_POB 		= "PASSENGER ON BOARD";
	public static final String DISPATCHER_STATUS_EOJ 		= "END OF JOB";
	public static final String DISPATCHER_STATUS_NO_JOB 	= "NO JOB";
	public static final String DISPATCHER_STATUS_REJECTED	= "REJECTED";
	public static final String DISPATCHER_STATUS_RETURN_JOB	= "RETURN JOB"; 
	public static final String DISPATCHER_STATUS_STC		= "SOON TO CLEAR"; 

	public static final String STATUS_NEW 		= "NEW";
	public static final String STATUS_ACCEPTED 	= "ACCEPTED";
	public static final String STATUS_DOW 		= "DOW";
	public static final String STATUS_POB 		= "POB";
	public static final String STATUS_EOJ 		= "EOJ";
	public static final String STATUS_STC 		= "STC";
	public static final String STATUS_REJECTED 	= "REJECTED";
	public static final String STATUS_MISSED 	= "MISSED_JOB";
	public static final String STATUS_NO_JOB 	= "NO_JOB";
	public static final String STATUS_RETURNED	= "RETURNED_JOB";
	
	public static final String TASK_ACCEPTED_JOB 	= "TASK_ACCEPTED_JOB";
	public static final String TASK_REJECTED_JOB 	= "TASK_REJECTED_JOB";
	public static final String TASK_MISSED_JOB 		= "TASK_MISSED_JOB";
	public static final String TASK_EOJ				= "TASK_EOJ";
	public static final String TASK_DOW				= "TASK_DOW";
	public static final String TASK_POB				= "TASK_POB";
	public static final String TASK_NO_JOB			= "TASK_NO_JOB";
	public static final String TASK_RETURNED		= "TASK_RETURNED_JOB"; 
	public static final String TASK_STC				= "TASK_STC";

	public static final String DRIVER_STATUS_RTW    = "RTW";
	public static final String DRIVER_STATUS_BRK    = "BRK";
	public static final String DRIVER_STATUS_LOG    = "LOG";
	public static final String DRIVER_STATUS_POS    = "POS";

    public static final String TAXIMETER_PACKAGE = "com.planetcoops.android.taximeter";
    public static int CARD_IO_SCAN_REQUEST_CODE = 100;
}
