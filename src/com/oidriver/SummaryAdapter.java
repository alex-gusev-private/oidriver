package com.oidriver;

import android.content.ContentValues;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SummaryAdapter extends BaseAdapter  {
	
	Context mContext;
	ContentValues mValues;
	
	public SummaryAdapter(Context context, ContentValues values)
	{
		super();
		mContext = context;
		mValues = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = null;
		
		if (convertView == null)
		{
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.totals_table, null);
		}
		else
		{
			view = convertView;
		}

		if (view != null)
		{
			TextView now = (TextView) view.findViewById(R.id.textViewNow);
			TextView jobs = (TextView) view.findViewById(R.id.textViewBookings);
			
			String str = mValues.getAsString("BIDS");
			now.setText(str);
			str = mValues.getAsString("BOOKINGS");
			jobs.setText(str);
		}
		
		return view;
	}

	@Override
	public int getCount() {
		return 1;
	}

	@Override
	public Object getItem(int position) {
		return mValues;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
}