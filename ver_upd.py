#!/bin/env python
import os
from xml.dom.minidom import parse

dom1 = parse("AndroidManifest.xml")
oldVersion = dom1.documentElement.getAttribute("android:versionName")
oldVersionCode = dom1.documentElement.getAttribute("android:versionCode")
versionNumbers = oldVersion.split('.')

versionNumbers[-1] = unicode(int(versionNumbers[-1]) + 1).zfill(2)
newVersionCode = int(oldVersionCode) + 1
dom1.documentElement.setAttribute("android:versionName", u'.'.join(versionNumbers))
dom1.documentElement.setAttribute("android:versionCode", str(newVersionCode))

with open("AndroidManifest.xml", 'wb') as f:
    for line in dom1.toxml("utf-8"):
        f.write(line)
c = open("client_baseline.properties", 'wb')
c.write("release_version=0\n")
c.write("baseline_version_minor=09\n")
c.write("baseline_version_build=" + str(newVersionCode) + "\n")
